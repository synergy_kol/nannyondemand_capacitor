"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providercongratulation_providercongratulation_module_ts"],{

/***/ 62910:
/*!******************************************************************************************!*\
  !*** ./src/app/Provider/providercongratulation/providercongratulation-routing.module.ts ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProvidercongratulationPageRoutingModule": () => (/* binding */ ProvidercongratulationPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providercongratulation_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providercongratulation.page */ 87541);




const routes = [
    {
        path: '',
        component: _providercongratulation_page__WEBPACK_IMPORTED_MODULE_0__.ProvidercongratulationPage
    }
];
let ProvidercongratulationPageRoutingModule = class ProvidercongratulationPageRoutingModule {
};
ProvidercongratulationPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProvidercongratulationPageRoutingModule);



/***/ }),

/***/ 6186:
/*!**********************************************************************************!*\
  !*** ./src/app/Provider/providercongratulation/providercongratulation.module.ts ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProvidercongratulationPageModule": () => (/* binding */ ProvidercongratulationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providercongratulation_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providercongratulation-routing.module */ 62910);
/* harmony import */ var _providercongratulation_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providercongratulation.page */ 87541);







let ProvidercongratulationPageModule = class ProvidercongratulationPageModule {
};
ProvidercongratulationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providercongratulation_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProvidercongratulationPageRoutingModule
        ],
        declarations: [_providercongratulation_page__WEBPACK_IMPORTED_MODULE_1__.ProvidercongratulationPage]
    })
], ProvidercongratulationPageModule);



/***/ }),

/***/ 87541:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providercongratulation/providercongratulation.page.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProvidercongratulationPage": () => (/* binding */ ProvidercongratulationPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providercongratulation_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providercongratulation.page.html */ 79236);
/* harmony import */ var _providercongratulation_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providercongratulation.page.scss */ 20979);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 78099);





let ProvidercongratulationPage = class ProvidercongratulationPage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ngOnInit() {
    }
    onContinue() {
        this.navCtrl.navigateForward('/login');
    }
};
ProvidercongratulationPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.NavController }
];
ProvidercongratulationPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-providercongratulation',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providercongratulation_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providercongratulation_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProvidercongratulationPage);



/***/ }),

/***/ 79236:
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providercongratulation/providercongratulation.page.html ***!
  \*************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"congratulation_screen\">\n    <div class=\"congratulation_panel\">\n      <img src=\"assets/images/congratulation_img.png\" alt=\"congratulation_img\" />\n      <h3>Congratulations!</h3>\n      <p>You have successfully registered yourself as a nanny.</p>\n      <ion-button color=\"success\" expand=\"full\" shape=\"round\" (click)=\"onContinue()\" class=\"cus_btn\">Continue</ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 20979:
/*!**********************************************************************************!*\
  !*** ./src/app/Provider/providercongratulation/providercongratulation.page.scss ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = ".congratulation_screen {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  display: table;\n  padding: 40px;\n}\n.congratulation_screen .congratulation_panel {\n  display: table-cell;\n  text-align: center;\n  vertical-align: middle;\n}\n.congratulation_screen .congratulation_panel img {\n  width: 70%;\n}\n.congratulation_screen .congratulation_panel h3 {\n  padding: 40px 0 10px 0;\n  margin: 0;\n  color: #333333;\n  font-size: 26px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.congratulation_screen .congratulation_panel p {\n  margin: 0;\n  margin-bottom: 40px;\n  color: #666666;\n  font-size: 16px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  line-height: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVyY29uZ3JhdHVsYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtFQUFvQixjQUFBO0VBQWdCLGFBQUE7QUFLbkU7QUFKSTtFQUF1QixtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixzQkFBQTtBQVNwRTtBQVJRO0VBQUssVUFBQTtBQVdiO0FBVlE7RUFBSSxzQkFBQTtFQUF3QixTQUFBO0VBQVcsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBa0JsRztBQWpCUTtFQUFHLFNBQUE7RUFBVyxtQkFBQTtFQUFzQixjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0MsaUJBQUE7QUEwQnJJIiwiZmlsZSI6InByb3ZpZGVyY29uZ3JhdHVsYXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbmdyYXR1bGF0aW9uX3NjcmVlbntcclxuICAgIHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDEwMCU7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgZGlzcGxheTogdGFibGU7IHBhZGRpbmc6IDQwcHg7XHJcbiAgICAuY29uZ3JhdHVsYXRpb25fcGFuZWx7IGRpc3BsYXk6IHRhYmxlLWNlbGw7IHRleHQtYWxpZ246IGNlbnRlcjsgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICBpbWd7IHdpZHRoOiA3MCU7IH1cclxuICAgICAgICBoM3sgcGFkZGluZzogNDBweCAwIDEwcHggMDsgbWFyZ2luOiAwOyBjb2xvcjogIzMzMzMzMzsgZm9udC1zaXplOiAyNnB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG4gICAgICAgIHB7IG1hcmdpbjogMDsgbWFyZ2luLWJvdHRvbTogNDBweDsgIGNvbG9yOiAjNjY2NjY2OyBmb250LXNpemU6IDE2cHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyBsaW5lLWhlaWdodDogMjRweDsgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providercongratulation_providercongratulation_module_ts.js.map