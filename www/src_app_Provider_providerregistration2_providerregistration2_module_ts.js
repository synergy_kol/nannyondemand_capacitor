"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerregistration2_providerregistration2_module_ts"],{

/***/ 30485:
/*!****************************************************************************************!*\
  !*** ./src/app/Provider/providerregistration2/providerregistration2-routing.module.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Providerregistration2PageRoutingModule": () => (/* binding */ Providerregistration2PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerregistration2_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerregistration2.page */ 94063);




const routes = [
    {
        path: '',
        component: _providerregistration2_page__WEBPACK_IMPORTED_MODULE_0__.Providerregistration2Page
    }
];
let Providerregistration2PageRoutingModule = class Providerregistration2PageRoutingModule {
};
Providerregistration2PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], Providerregistration2PageRoutingModule);



/***/ }),

/***/ 20264:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providerregistration2/providerregistration2.module.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Providerregistration2PageModule": () => (/* binding */ Providerregistration2PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerregistration2_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerregistration2-routing.module */ 30485);
/* harmony import */ var _providerregistration2_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerregistration2.page */ 94063);







let Providerregistration2PageModule = class Providerregistration2PageModule {
};
Providerregistration2PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerregistration2_routing_module__WEBPACK_IMPORTED_MODULE_0__.Providerregistration2PageRoutingModule
        ],
        declarations: [_providerregistration2_page__WEBPACK_IMPORTED_MODULE_1__.Providerregistration2Page]
    })
], Providerregistration2PageModule);



/***/ }),

/***/ 94063:
/*!******************************************************************************!*\
  !*** ./src/app/Provider/providerregistration2/providerregistration2.page.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Providerregistration2Page": () => (/* binding */ Providerregistration2Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerregistration2_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerregistration2.page.html */ 11177);
/* harmony import */ var _providerregistration2_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerregistration2.page.scss */ 74223);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 30692);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 57154);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







//import { FilePath } from '@ionic-native/file-path/ngx';


let Providerregistration2Page = class Providerregistration2Page {
    constructor(navCtrl, loadingController, toastController, storage, camera, actionSheet, platform, 
    //private filePath: FilePath,
    file, authService, alertController) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.storage = storage;
        this.camera = camera;
        this.actionSheet = actionSheet;
        this.platform = platform;
        this.file = file;
        this.authService = authService;
        this.alertController = alertController;
        this.about = '';
        this.identitytype = '';
        this.checkdrive = false;
        this.ischeckdrive = 'No';
        this.checksmoke = false;
        this.ischecksmoke = 'No';
        this.checkpets = false;
        this.ischeckpets = 'No';
        this.checkpolice = false;
        this.preData = [];
        this.isDisabled = false;
        this.isLoading = false;
        this.serveridentityImages = [];
        this.identityImages = [];
        this.servercertificateImages = [];
        this.certificateImages = [];
        this.serverinterventionImages = [];
        this.interventionImages = [];
        this.serverhomeImages = [];
        this.homeImages = [];
        this.serverpoliceImages = [];
        this.policeImages = [];
        this.website = [];
        this.userdata = [];
        this.data = [];
        this.profileData = [];
        this.hideextra = true;
        this.step2Data = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    getDetails() {
        this.storage.get('providerRegsData').then((result) => {
            this.hideLoader();
            this.preData = result;
            console.log('PreStep Data:', this.preData);
        });
    }
    ionViewWillEnter() {
        this.showLoader('Loading...');
        this.storage.get('userDetails').then((val) => {
            if (val != null) {
                this.hideextra = false;
                this.userdata = val;
                this.getProfileDetails();
            }
            else {
                this.storage.get('setp2').then((val) => {
                    this.step2Data = val;
                    this.about = this.step2Data.aboutnote;
                    this.identitytype = this.step2Data.identytype;
                    if (this.step2Data.willdriver == "Yes") {
                        this.checkdrive = true;
                    }
                    else {
                        this.checkdrive = false;
                    }
                    if (this.step2Data.issmoke == "Yes") {
                        this.checksmoke = true;
                    }
                    else {
                        this.checksmoke = false;
                    }
                    if (this.step2Data.comfortablepet == "Yes") {
                        this.checkpets = true;
                    }
                    else {
                        this.checkpets = false;
                    }
                });
                this.website = this.step2Data.website;
            }
        });
        this.getDetails();
        this.loginToken();
    }
    getProfileDetails() {
        var body = {
            user_id: this.userdata.user_id,
        };
        this.authService.postData("get-profile", body).then(result => {
            this.data = result;
            console.log("profile: ", this.data);
            this.profileData = this.data.result.data;
            this.about = this.profileData.about_note;
            this.identitytype = this.profileData.identy_type;
            if (this.profileData.will_driver == "Yes") {
                this.checkdrive = true;
            }
            else {
                this.checkdrive = false;
            }
            if (this.profileData.is_smoke == "Yes") {
                this.checksmoke = true;
            }
            else {
                this.checksmoke = false;
            }
            if (this.profileData.comfortable_pet == "Yes") {
                this.checkpets = true;
            }
            else {
                this.checkpets = false;
            }
            if (this.profileData.police_doc != "") {
                this.checkpolice = true;
            }
            else {
                this.checkpolice = false;
            }
            this.serveridentityImages = this.profileData.documents;
            this.servercertificateImages = this.profileData.certificated;
            this.serverinterventionImages = this.profileData.intervention;
            this.serverhomeImages = this.profileData.home;
            this.serverpoliceImages = this.profileData.police_doc;
            this.website = this.profileData.website;
            this.hideLoader();
        }, error => {
            this.hideLoader();
        });
    }
    rmserIdentityImage(id, type) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Remove? It will be removed permanently.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Wait...');
                        setTimeout(() => {
                            var body = {
                                image_id: id,
                                image_type: type
                            };
                            this.authService.postData("remove-image", body).then(result => {
                                this.data = result;
                                console.log("Removed: ", this.data);
                                this.hideLoader();
                                var body = {
                                    user_id: this.userdata.user_id,
                                };
                                this.authService.postData("get-profile", body).then(result => {
                                    this.data = result;
                                    this.profileData = this.data.result.data;
                                    this.serveridentityImages = this.profileData.documents;
                                }, error => {
                                    this.hideLoader();
                                });
                            });
                        }, 3000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    rmserCertificateImage(id, type) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Remove? It will be removed permanently.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Wait...');
                        setTimeout(() => {
                            var body = {
                                image_id: id,
                                image_type: type
                            };
                            this.authService.postData("remove-image", body).then(result => {
                                this.data = result;
                                console.log("Removed: ", this.data);
                                this.hideLoader();
                                var body = {
                                    user_id: this.userdata.user_id,
                                };
                                this.authService.postData("get-profile", body).then(result => {
                                    this.data = result;
                                    this.profileData = this.data.result.data;
                                    this.servercertificateImages = this.profileData.certificated;
                                }, error => {
                                    this.hideLoader();
                                });
                            });
                        }, 3000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    rmserInterventionImage(id, type) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Remove? It will be removed permanently.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Wait...');
                        setTimeout(() => {
                            var body = {
                                image_id: id,
                                image_type: type
                            };
                            this.authService.postData("remove-image", body).then(result => {
                                this.data = result;
                                console.log("Removed: ", this.data);
                                this.hideLoader();
                                var body = {
                                    user_id: this.userdata.user_id,
                                };
                                this.authService.postData("get-profile", body).then(result => {
                                    this.data = result;
                                    this.profileData = this.data.result.data;
                                    this.serverinterventionImages = this.profileData.intervention;
                                }, error => {
                                    this.hideLoader();
                                });
                            });
                        }, 3000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    rmserHomeImage(id, type) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Remove? It will be removed permanently.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Wait...');
                        setTimeout(() => {
                            var body = {
                                image_id: id,
                                image_type: type
                            };
                            this.authService.postData("remove-image", body).then(result => {
                                this.data = result;
                                console.log("Removed: ", this.data);
                                this.hideLoader();
                                var body = {
                                    user_id: this.userdata.user_id,
                                };
                                this.authService.postData("get-profile", body).then(result => {
                                    this.data = result;
                                    this.profileData = this.data.result.data;
                                    this.serverhomeImages = this.profileData.home;
                                }, error => {
                                    this.hideLoader();
                                });
                            });
                        }, 3000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    rmserPoliceImage(id, type) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Remove? It will be removed permanently.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Wait...');
                        setTimeout(() => {
                            var body = {
                                image_id: id,
                                image_type: type
                            };
                            this.authService.postData("remove-image", body).then(result => {
                                this.data = result;
                                console.log("Removed: ", this.data);
                                this.hideLoader();
                                var body = {
                                    user_id: this.userdata.user_id,
                                };
                                this.authService.postData("get-profile", body).then(result => {
                                    this.data = result;
                                    this.profileData = this.data.result.data;
                                    this.serverpoliceImages = this.profileData.police_doc;
                                }, error => {
                                    this.hideLoader();
                                });
                            });
                        }, 3000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    uploadIdentity() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takeIdentity(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takeIdentity(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takeIdentity(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            /* let data = {
              image: 'data:image/jpeg;base64,' + imagePath
            } */
            this.identityImages.push('data:image/jpeg;base64,' + imagePath);
        }, (err) => {
            console.log(err);
        });
    }
    rmimage(ind) {
        this.identityImages.splice(ind, 1);
    }
    uploadCertificate() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takeCertificate(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takeCertificate(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takeCertificate(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            /* let data = {
              image: 'data:image/jpeg;base64,' + imagePath
            } */
            this.certificateImages.push('data:image/jpeg;base64,' + imagePath);
        }, (err) => {
            console.log(err);
        });
    }
    rmcerimage(ind) {
        this.certificateImages.splice(ind, 1);
    }
    uploadIntervention() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takeIntervention(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takeIntervention(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takeIntervention(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            /* let data = {
              image: 'data:image/jpeg;base64,' + imagePath
            } */
            this.interventionImages.push('data:image/jpeg;base64,' + imagePath);
        }, (err) => {
            console.log(err);
        });
    }
    rmintrimage(ind) {
        this.interventionImages.splice(ind, 1);
    }
    uploadHome() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takeHome(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takeHome(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takeHome(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            /* let data = {
              image: 'data:image/jpeg;base64,' + imagePath
            } */
            this.homeImages.push('data:image/jpeg;base64,' + imagePath);
        }, (err) => {
            console.log(err);
        });
    }
    rmhomeimage(ind) {
        this.homeImages.splice(ind, 1);
    }
    uploadPolice() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takePolice(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePolice(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takePolice(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            /* let data = {
              image: 'data:image/jpeg;base64,' + imagePath
            } */
            this.policeImages.push('data:image/jpeg;base64,' + imagePath);
        }, (err) => {
            console.log(err);
        });
    }
    rmpoliceimage(ind) {
        this.policeImages.splice(ind, 1);
    }
    driveChange(evt) {
        this.checkdrive == !this.checkdrive;
        if (evt.detail.checked == true) {
            this.ischeckdrive = "Yes";
        }
        else {
            this.ischeckdrive = "No";
        }
    }
    smokeChange(evt) {
        this.checksmoke == !this.checksmoke;
        if (evt.detail.checked == true) {
            this.ischecksmoke = "Yes";
        }
        else {
            this.ischecksmoke = "No";
        }
    }
    petChange(evt) {
        this.checkpets == !this.checkpets;
        if (evt.detail.checked == true) {
            this.ischeckpets = "Yes";
        }
        else {
            this.ischeckpets = "No";
        }
    }
    onBack() {
        this.navCtrl.pop();
    }
    onNext() {
        if (this.about.trim() == '') {
            this.presentToast('Please enter somthing about you');
        }
        else if (this.identitytype == '') {
            this.presentToast('Please select Indentity Type');
        }
        else if (this.serveridentityImages.length == 0 && this.identityImages.length == 0) {
            this.presentToast('Please upload Identity Images');
        }
        else {
            if (this.checkpolice == true) {
                if (this.policeImages.length == 0 && this.serverpoliceImages.length == 0) {
                    this.presentToast('Please upload Images of Police Records.');
                }
                else {
                    this.onSteptwo();
                }
            }
            else {
                this.onSteptwo();
            }
        }
    }
    onSteptwo() {
        this.showLoader('Please wait...');
        var object = {
            servicetype: this.preData.servicetype,
            fname: this.preData.fname,
            lname: this.preData.lname,
            dob: this.preData.dob,
            phone: this.preData.phone,
            emgnphone: this.preData.emgnphone,
            email: this.preData.email,
            password: this.preData.password,
            address: this.preData.address,
            state: this.preData.state,
            city: this.preData.city,
            zip: this.preData.zip,
            aboutnote: this.about,
            identytype: this.identitytype,
            willdriver: this.ischeckdrive,
            issmoke: this.ischecksmoke,
            comfortablepet: this.ischeckpets,
            identitydocuments: this.identityImages,
            certificates: this.certificateImages,
            interventionreports: this.interventionImages,
            homeimages: this.homeImages,
            policeimages: this.policeImages,
            website: this.website
        };
        this.presentToast('Successfully Proceed..');
        this.storage.set("providerRegsData", object);
        this.storage.set("setp2", object);
        setTimeout(() => {
            this.hideLoader();
            this.navCtrl.navigateForward("/providerregistration3");
        }, 100);
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
Providerregistration2Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ToastController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_3__.Camera },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ActionSheetController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.Platform },
    { type: _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_4__.File },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_5__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.AlertController }
];
Providerregistration2Page = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-providerregistration2',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerregistration2_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerregistration2_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], Providerregistration2Page);



/***/ }),

/***/ 11177:
/*!***********************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerregistration2/providerregistration2.page.html ***!
  \***********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title *ngIf=\"hideextra\">Become A Nanny</ion-title>\n    <ion-title *ngIf=\"!hideextra\">Edit Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"step\">\n    <ul>\n      <li class=\"activestep\"><a></a><span>Step 1</span></li>\n      <li class=\"activestep\"><a></a><span>Step 2</span></li>\n      <li><a></a><span>Step 3</span></li>\n    </ul>\n  </div>\n  <div class=\"register_panel\">\n    <p style=\"color: #f44336; font-size: 12px; margin: 0 0 15px 0; text-align: right;\">* About Note & Identity Proof\n      field mandatory *\n    </p>\n    <ion-textarea maxlength=\"200\" class=\"cust_input\" [(ngModel)]=\"about\" style=\"height: auto; border-radius: 25px;\"\n      placeholder=\"Say something about you\">\n    </ion-textarea>\n    <p style=\"color: #999999; font-size: 12px; margin: 0 0 15px 0; text-align: right;\">{{about.length}}/200\n    </p>\n    <ion-select class=\"cust_input\" [(ngModel)]=\"identitytype\" placeholder=\"Select a identity proof\"\n      [interfaceOptions]=\"customActionSheetOptions\" interface=\"action-sheet\">\n      <ion-select-option value=\"Driving Licence\" *ngIf=\"preData.servicetype != 3\">Driving Licence</ion-select-option>\n      <ion-select-option value=\"Passport\" *ngIf=\"preData.servicetype != 3\">Passport</ion-select-option>\n      <ion-select-option value=\"Childcare License Number\" *ngIf=\"preData.servicetype == 3\">Childcare License Number</ion-select-option>\n    </ion-select>\n    <ion-item lines=\"none\" class=\"cust_input\" style=\"padding: 0 5px !important;\" (click)=\"uploadIdentity()\">\n      <ion-label class=\"uplaodtext\">Upload Identity Documents</ion-label>\n      <ion-badge class=\"uplaodbtn\" slot=\"end\">Upload</ion-badge>\n    </ion-item>\n    <ion-input *ngIf=\"preData.servicetype == 3\" class=\"cust_input\" placeholder=\"Website Address\" type=\"text\" [(ngModel)]=\"website\"></ion-input>\n    <div style=\"padding: 0 12px;\">\n      <div class=\"sml_img\" *ngFor=\"let item of serveridentityImages\">\n        <ion-img [src]=\"item.image\"></ion-img>\n        <span (click)=\"rmserIdentityImage(item.provider_identity_document_id, 'identity_documents')\">x</span>\n      </div>\n      <div class=\"sml_img\" *ngFor=\"let item of identityImages; let i = index\">\n        <ion-img [src]=\"item\"></ion-img>\n        <span (click)=\"rmimage(i)\">x</span>\n      </div>\n    </div>\n    <ion-item lines=\"none\" class=\"cust_input\" style=\"padding: 0 5px !important;\" (click)=\"uploadCertificate()\">\n      <ion-label class=\"uplaodtext\">Any Certificates</ion-label>\n      <ion-badge class=\"uplaodbtn\" slot=\"end\">Upload</ion-badge>\n    </ion-item>\n    <div style=\"padding: 0 12px;\">\n      <div class=\"sml_img\" *ngFor=\"let item of servercertificateImages\">\n        <ion-img [src]=\"item.image\"></ion-img>\n        <span (click)=\"rmserCertificateImage(item.provider_certificate_id, 'certificates')\">x</span>\n      </div>\n      <div class=\"sml_img\" *ngFor=\"let item of certificateImages; let i = index\">\n        <ion-img [src]=\"item\"></ion-img>\n        <span (click)=\"rmcerimage(i)\">x</span>\n      </div>\n    </div>\n    <ion-item lines=\"none\" class=\"cust_input\" style=\"padding: 0 5px !important;\" (click)=\"uploadIntervention()\">\n      <ion-label class=\"uplaodtext\">Child Intervention Report</ion-label>\n      <ion-badge class=\"uplaodbtn\" slot=\"end\">Upload</ion-badge>\n    </ion-item>\n    <div style=\"padding: 0 12px;\">\n      <div class=\"sml_img\" *ngFor=\"let item of serverinterventionImages\">\n        <ion-img [src]=\"item.image\"></ion-img>\n        <span (click)=\"rmserInterventionImage(item.provider_intervention_report_id, 'intervention_reports')\">x</span>\n      </div>\n      <div class=\"sml_img\" *ngFor=\"let item of interventionImages; let i = index\">\n        <ion-img [src]=\"item\"></ion-img>\n        <span (click)=\"rmintrimage(i)\">x</span>\n      </div>\n    </div>\n    <ion-item lines=\"none\" class=\"cust_input\" style=\"padding: 0 5px !important;\" (click)=\"uploadHome()\">\n      <ion-label class=\"uplaodtext\">Upload Home Image</ion-label>\n      <ion-badge class=\"uplaodbtn\" slot=\"end\">Upload</ion-badge>\n    </ion-item>\n    <div style=\"padding: 0 12px;\">\n      <div class=\"sml_img\" *ngFor=\"let item of serverhomeImages\">\n        <ion-img [src]=\"item.image\"></ion-img>\n        <span (click)=\"rmserHomeImage(item.provider_home_image_id, 'home_images')\">x</span>\n      </div>\n      <div class=\"sml_img\" *ngFor=\"let item of homeImages; let i = index\">\n        <ion-img [src]=\"item\"></ion-img>\n        <span (click)=\"rmhomeimage(i)\">x</span>\n      </div>\n    </div>\n    <ion-item lines=\"none\" style=\"margin-top: 20px;\">\n      <ion-label style=\"font-size: 15px;\">Have you any Police Record?</ion-label>\n      <ion-toggle color=\"success\" [(ngModel)]=\"checkpolice\"></ion-toggle>\n    </ion-item>\n    <div *ngIf=\"checkpolice\">\n      <ion-item lines=\"none\" class=\"cust_input\" style=\"padding: 0 5px !important;\" (click)=\"uploadPolice()\">\n        <ion-label class=\"uplaodtext\">Upload Police File</ion-label>\n        <ion-badge class=\"uplaodbtn\" slot=\"end\">Upload</ion-badge>\n      </ion-item>\n      <div style=\"padding: 0 12px;\">\n        <div class=\"sml_img\" *ngFor=\"let item of serverpoliceImages\">\n          <ion-img [src]=\"item.image\"></ion-img>\n          <span (click)=\"rmserPoliceImage(item.provider_certificate_id, 'police_doc')\">x</span>\n        </div>\n        <div class=\"sml_img\" *ngFor=\"let item of policeImages; let i = index\">\n          <ion-img [src]=\"item\"></ion-img>\n          <span (click)=\"rmpoliceimage(i)\">x</span>\n        </div>\n      </div>\n    </div>\n    <ion-item lines=\"none\" style=\"margin-top: 20px;\">\n      <ion-label style=\"font-size: 15px;\">Willing to drive ?</ion-label>\n      <ion-toggle color=\"success\" [(ngModel)]=\"checkdrive\" [checked]=\"checkpet\" (ionChange)=\"driveChange($event)\">\n      </ion-toggle>\n    </ion-item>\n    <ion-item lines=\"none\">\n      <ion-label style=\"font-size: 15px;\">Do you smoke?</ion-label>\n      <ion-toggle color=\"success\" [(ngModel)]=\"checksmoke\" [checked]=\"checksmoke\" (ionChange)=\"smokeChange($event)\">\n      </ion-toggle>\n    </ion-item>\n    <ion-item lines=\"none\">\n      <ion-label style=\"font-size: 15px;\">Comfortable with pets?</ion-label>\n      <ion-toggle color=\"success\" [(ngModel)]=\"checkpets\" [checked]=\"checkpets\" (ionChange)=\"petChange($event)\">\n      </ion-toggle>\n    </ion-item>\n  </div>\n  <ion-toolbar class=\"ftr\">\n    <ion-button color=\"dark\" slot=\"start\" fill=\"clear\" shape=\"round\" (click)=\"onBack()\" class=\"sml_cus_btn\"\n      style=\"box-shadow: none;\">\n      <img src=\"assets/images/black_l_arrow.png\" />&nbsp; Back</ion-button>\n    <ion-button color=\"success\" slot=\"end\" shape=\"round\" (click)=\"onNext()\" class=\"sml_cus_btn\">Next &nbsp;<img\n        src=\"assets/images/white_r_arrow.png\" /></ion-button>\n  </ion-toolbar>\n</ion-content>");

/***/ }),

/***/ 74223:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providerregistration2/providerregistration2.page.scss ***!
  \********************************************************************************/
/***/ ((module) => {

module.exports = ".step {\n  background: #e7fef2;\n  padding: 8px 0;\n}\n.step ul {\n  padding: 0;\n  width: 300px;\n  margin: 0 auto;\n  margin-bottom: 5px;\n  list-style: none;\n  text-align: center;\n  position: relative;\n}\n.step ul li {\n  display: inline-block;\n  position: relative;\n  padding: 0 15%;\n  z-index: 999;\n}\n.step ul li a {\n  display: block;\n  width: 22px;\n  height: 22px;\n  background: #fff;\n  border: #41b578 solid 1px;\n  border-radius: 50%;\n  position: relative;\n}\n.step ul li span {\n  display: block;\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 7px 0 0 -5px;\n}\n.step ul li:first-child {\n  padding-left: 0;\n}\n.step ul li:last-child {\n  padding-right: 0;\n  text-align: right;\n}\n.step ul li.activestep a {\n  width: 30px;\n  height: 30px;\n  background: #41b578;\n  position: relative;\n  z-index: 999;\n  margin-top: 0;\n  margin-left: -3px;\n  top: 5px;\n}\n.step ul li.activestep a:after {\n  width: 13px;\n  height: 5px;\n  content: \"\";\n  position: absolute;\n  border-left: 3px #ffffff solid;\n  border-bottom: 3px #ffffff solid;\n  transform: rotate(-45deg);\n  left: 6px;\n  top: 8px;\n}\n.step ul:after {\n  background: #cccccc;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 18px;\n  width: 80%;\n  height: 2px;\n  content: \"\";\n  margin: 0 auto;\n  z-index: 0;\n}\n.register_panel {\n  padding: 20px;\n}\n.sml_cus_btn {\n  box-shadow: #9be2bd 0 0 15px 5px;\n  margin: 20px;\n  border-radius: 30px;\n}\n.uplaodtext {\n  color: #666666;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n.uplaodbtn {\n  color: #41b578;\n  font-size: 11px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  background-color: #ffffff;\n  padding: 6px 12px;\n  border-radius: 30px;\n  text-transform: uppercase;\n  margin: 0;\n}\n.sml_img {\n  position: relative;\n  width: 68px;\n  height: 60px;\n  border-radius: 6px;\n  margin: 1px 3px;\n  overflow: hidden;\n  display: inline-block;\n}\n.sml_img ion-img {\n  object-fit: cover;\n  width: 100%;\n}\n.sml_img span {\n  position: absolute;\n  color: red;\n  top: 3px;\n  right: 3px;\n  background: #ffffff;\n  padding: 3px 6px 4px 6px;\n  border-radius: 15px;\n  z-index: 99;\n  font-size: 13px;\n  font-weight: bold;\n  line-height: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVycmVnaXN0cmF0aW9uMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUFxQixjQUFBO0FBRXpCO0FBREk7RUFDSSxVQUFBO0VBQVksWUFBQTtFQUFjLGNBQUE7RUFBZ0Isa0JBQUE7RUFBb0IsZ0JBQUE7RUFBa0Isa0JBQUE7RUFBb0Isa0JBQUE7QUFTNUc7QUFSUTtFQUNJLHFCQUFBO0VBQXVCLGtCQUFBO0VBQW9CLGNBQUE7RUFBZ0IsWUFBQTtBQWF2RTtBQVpZO0VBQUcsY0FBQTtFQUFnQixXQUFBO0VBQWEsWUFBQTtFQUFjLGdCQUFBO0VBQWtCLHlCQUFBO0VBQTBCLGtCQUFBO0VBQW1CLGtCQUFBO0FBcUJ6SDtBQXBCWTtFQUFNLGNBQUE7RUFBZ0IsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLG9CQUFBO0FBNEIzSDtBQTFCUTtFQUFnQixlQUFBO0FBNkJ4QjtBQTVCUTtFQUFlLGdCQUFBO0VBQWtCLGlCQUFBO0FBZ0N6QztBQS9CUTtFQUFrQixXQUFBO0VBQWEsWUFBQTtFQUFjLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW9CLFlBQUE7RUFBYyxhQUFBO0VBQWUsaUJBQUE7RUFBbUIsUUFBQTtBQXlDOUk7QUF4Q1E7RUFBdUIsV0FBQTtFQUFhLFdBQUE7RUFBYSxXQUFBO0VBQWEsa0JBQUE7RUFBb0IsOEJBQUE7RUFBZ0MsZ0NBQUE7RUFBa0MseUJBQUE7RUFBMkIsU0FBQTtFQUFXLFFBQUE7QUFtRGxNO0FBaERJO0VBQ0ksbUJBQUE7RUFBcUIsa0JBQUE7RUFBb0IsT0FBQTtFQUFTLFFBQUE7RUFBVSxTQUFBO0VBQVcsVUFBQTtFQUFZLFdBQUE7RUFBYSxXQUFBO0VBQWEsY0FBQTtFQUFnQixVQUFBO0FBMkRySTtBQXhEQTtFQUFpQixhQUFBO0FBNERqQjtBQTNEQTtFQUFjLGdDQUFBO0VBQWtDLFlBQUE7RUFBYyxtQkFBQTtBQWlFOUQ7QUFoRUE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0NBQUE7QUFtRUo7QUFqRUE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esb0NBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLFNBQUE7QUFvRUo7QUFsRUE7RUFDSSxrQkFBQTtFQUFvQixXQUFBO0VBQWEsWUFBQTtFQUFjLGtCQUFBO0VBQW9CLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0IscUJBQUE7QUEyRTFHO0FBMUVJO0VBQVEsaUJBQUE7RUFBbUIsV0FBQTtBQThFL0I7QUE3RUk7RUFBSyxrQkFBQTtFQUFvQixVQUFBO0VBQVksUUFBQTtFQUFVLFVBQUE7RUFBWSxtQkFBQTtFQUFxQix3QkFBQTtFQUEwQixtQkFBQTtFQUFxQixXQUFBO0VBQWEsZUFBQTtFQUFpQixpQkFBQTtFQUFtQixpQkFBQTtBQTBGcEwiLCJmaWxlIjoicHJvdmlkZXJyZWdpc3RyYXRpb24yLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdGVweyBcclxuICAgIGJhY2tncm91bmQ6ICNlN2ZlZjI7IHBhZGRpbmc6IDhweCAwO1xyXG4gICAgdWx7XHJcbiAgICAgICAgcGFkZGluZzogMDsgd2lkdGg6IDMwMHB4OyBtYXJnaW46IDAgYXV0bzsgbWFyZ2luLWJvdHRvbTogNXB4OyBsaXN0LXN0eWxlOiBub25lOyB0ZXh0LWFsaWduOiBjZW50ZXI7IHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBsaXsgXHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgcG9zaXRpb246IHJlbGF0aXZlOyBwYWRkaW5nOiAwIDE1JTsgei1pbmRleDogOTk5O1xyXG4gICAgICAgICAgICBheyBkaXNwbGF5OiBibG9jazsgd2lkdGg6IDIycHg7IGhlaWdodDogMjJweDsgYmFja2dyb3VuZDogI2ZmZjsgYm9yZGVyOiM0MWI1Nzggc29saWQgMXB4OyBib3JkZXItcmFkaXVzOjUwJTsgcG9zaXRpb246IHJlbGF0aXZlOyB9ICBcclxuICAgICAgICAgICAgc3BhbnsgZGlzcGxheTogYmxvY2s7IGNvbG9yOiAjNjY2NjY2OyBmb250LXNpemU6IDEzcHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjsgbWFyZ2luOiA3cHggMCAwIC01cHg7IH1cclxuICAgICAgICB9XHJcbiAgICAgICAgbGk6Zmlyc3QtY2hpbGR7IHBhZGRpbmctbGVmdDogMDsgfVxyXG4gICAgICAgIGxpOmxhc3QtY2hpbGR7IHBhZGRpbmctcmlnaHQ6IDA7IHRleHQtYWxpZ246IHJpZ2h0OyB9XHJcbiAgICAgICAgbGkuYWN0aXZlc3RlcCBhIHsgd2lkdGg6IDMwcHg7IGhlaWdodDogMzBweDsgYmFja2dyb3VuZDogIzQxYjU3ODsgcG9zaXRpb246IHJlbGF0aXZlOyB6LWluZGV4OiA5OTk7IG1hcmdpbi10b3A6IDA7IG1hcmdpbi1sZWZ0OiAtM3B4OyB0b3A6IDVweDsgfVxyXG4gICAgICAgIGxpLmFjdGl2ZXN0ZXAgYTphZnRlcnsgd2lkdGg6IDEzcHg7IGhlaWdodDogNXB4OyBjb250ZW50OiBcIlwiOyBwb3NpdGlvbjogYWJzb2x1dGU7IGJvcmRlci1sZWZ0OiAzcHggI2ZmZmZmZiBzb2xpZDsgYm9yZGVyLWJvdHRvbTogM3B4ICNmZmZmZmYgc29saWQ7IHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7IGxlZnQ6IDZweDsgdG9wOiA4cHg7IH1cclxuICAgIFxyXG4gICAgfVxyXG4gICAgdWw6YWZ0ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNjY2NjY2M7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgbGVmdDogMDsgcmlnaHQ6IDA7IHRvcDogMThweDsgd2lkdGg6IDgwJTsgaGVpZ2h0OiAycHg7IGNvbnRlbnQ6IFwiXCI7IG1hcmdpbjogMCBhdXRvOyB6LWluZGV4OiAwO1xyXG4gICAgfVxyXG59XHJcbi5yZWdpc3Rlcl9wYW5lbHsgcGFkZGluZzogMjBweDt9XHJcbi5zbWxfY3VzX2J0bnsgYm94LXNoYWRvdzogIzliZTJiZCAwIDAgMTVweCA1cHg7IG1hcmdpbjogMjBweDsgYm9yZGVyLXJhZGl1czogMzBweDsgfVxyXG4udXBsYW9kdGV4dHtcclxuICAgIGNvbG9yOiAjNjY2NjY2O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG4udXBsYW9kYnRue1xyXG4gICAgY29sb3I6ICM0MWI1Nzg7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNnB4IDEycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG4uc21sX2ltZ3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTsgd2lkdGg6IDY4cHg7IGhlaWdodDogNjBweDsgYm9yZGVyLXJhZGl1czogNnB4OyBtYXJnaW46IDFweCAzcHg7IG92ZXJmbG93OiBoaWRkZW47IGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGlvbi1pbWd7b2JqZWN0LWZpdDogY292ZXI7IHdpZHRoOiAxMDAlO31cclxuICAgIHNwYW57cG9zaXRpb246IGFic29sdXRlOyBjb2xvcjogcmVkOyB0b3A6IDNweDsgcmlnaHQ6IDNweDsgYmFja2dyb3VuZDogI2ZmZmZmZjsgcGFkZGluZzogM3B4IDZweCA0cHggNnB4OyBib3JkZXItcmFkaXVzOiAxNXB4OyB6LWluZGV4OiA5OTsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogYm9sZDsgbGluZS1oZWlnaHQ6IDEycHg7fVxyXG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerregistration2_providerregistration2_module_ts.js.map