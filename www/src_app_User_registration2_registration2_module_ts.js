"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_registration2_registration2_module_ts"],{

/***/ 15758:
/*!********************************************************************!*\
  !*** ./src/app/User/registration2/registration2-routing.module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Registration2PageRoutingModule": () => (/* binding */ Registration2PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _registration2_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registration2.page */ 72411);




const routes = [
    {
        path: '',
        component: _registration2_page__WEBPACK_IMPORTED_MODULE_0__.Registration2Page
    }
];
let Registration2PageRoutingModule = class Registration2PageRoutingModule {
};
Registration2PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], Registration2PageRoutingModule);



/***/ }),

/***/ 89991:
/*!************************************************************!*\
  !*** ./src/app/User/registration2/registration2.module.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Registration2PageModule": () => (/* binding */ Registration2PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _registration2_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registration2-routing.module */ 15758);
/* harmony import */ var _registration2_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration2.page */ 72411);







let Registration2PageModule = class Registration2PageModule {
};
Registration2PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _registration2_routing_module__WEBPACK_IMPORTED_MODULE_0__.Registration2PageRoutingModule
        ],
        declarations: [_registration2_page__WEBPACK_IMPORTED_MODULE_1__.Registration2Page]
    })
], Registration2PageModule);



/***/ }),

/***/ 72411:
/*!**********************************************************!*\
  !*** ./src/app/User/registration2/registration2.page.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Registration2Page": () => (/* binding */ Registration2Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_registration2_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./registration2.page.html */ 67219);
/* harmony import */ var _registration2_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration2.page.scss */ 88090);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let Registration2Page = class Registration2Page {
    constructor(navCtrl, loadingController, toastController, storage, authService) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.storage = storage;
        this.authService = authService;
        this.customAlertOptions = {
            header: 'Choose Cares'
        };
        this.pop = false;
        this.childageyear = 0;
        this.childagemonth = 0;
        this.childData = [];
        this.about = '';
        this.ispetinhouse = "No";
        this.checkpet = false;
        this.checkgovsub = false;
        this.isgovsubsidy = "No";
        this.medical = '';
        this.careData = [];
        this.careList = [
            { title: "Cooking" },
            { title: "Pickup and Drop off from school" },
            { title: "Housekeeping" },
            { title: "Home Work Help" },
            { title: "Dog Walk" }
        ];
        this.otherbox = false;
        this.othercare = '';
        this.data = [];
        this.userData = [];
        this.preData = '';
        this.isDisabled = false;
        this.isLoading = false;
        this.profileData = [];
        this.hideextra = true;
        this.getcareData = [];
        this.userId = '';
        this.userdata = [];
        this.step2Data = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    getDetails() {
        this.storage.get('userRegsData').then((result) => {
            this.hideLoader();
            this.preData = result;
            console.log('PreStep Data:', this.preData);
        });
    }
    ionViewWillEnter() {
        this.showLoader('Loading...');
        this.storage.get('userDetails').then((val) => {
            if (val != null) {
                this.userId = val.user_id;
                this.hideextra = false;
                this.userData = val;
                this.getProfileDetails();
            }
            else {
                this.storage.get('setp2').then((val) => {
                    this.step2Data = val;
                    this.isgovsubsidy = this.step2Data.govt_subsidy;
                    this.about = this.step2Data.about_note;
                    this.medical = this.step2Data.medical_condition;
                    this.ispetinhouse = this.step2Data.pets_in_house;
                    this.childData = this.step2Data.children_info;
                    this.getcareData = this.step2Data.additional_care;
                    this.othercare = this.step2Data.other_care;
                });
            }
        });
        this.getDetails();
        this.loginToken();
    }
    getProfileDetails() {
        var body = {
            user_id: this.userData.user_id,
        };
        this.authService.postData("get-profile", body).then(result => {
            this.data = result;
            console.log("profile: ", this.data);
            this.profileData = this.data.result.data;
            this.isgovsubsidy = this.profileData.govt_subsidy;
            this.about = this.profileData.about_note;
            this.medical = this.profileData.medical_condition;
            this.ispetinhouse = this.profileData.pets_in_house;
            this.childData = this.profileData.children_info;
            this.getcareData = this.profileData.additional_care;
            this.othercare = this.profileData.other_care;
            console.log("test data:", this.getcareData);
            if (this.profileData.govt_subsidy == "Yes") {
                this.checkgovsub = true;
            }
            else {
                this.checkgovsub = false;
            }
            if (this.profileData.pets_in_house == "Yes") {
                this.checkpet = true;
            }
            else {
                this.checkpet = false;
            }
            this.hideLoader();
        }, error => {
            this.hideLoader();
        });
    }
    addMore() {
        this.pop = true;
    }
    closepop() {
        this.pop = false;
    }
    onSave() {
        this.showLoader('Please wait...');
        let data = {
            age: this.childageyear,
            month: this.childagemonth
        };
        this.childData.push(data);
        this.presentToast('Successfully Child Saved');
        setTimeout(() => {
            this.pop = false;
            this.hideLoader();
            this.childageyear = 0;
            this.childagemonth = 0;
        }, 3000);
    }
    qntyearDown() {
        if (this.childageyear <= 0) {
            this.presentToast("Sorry! You can't.");
        }
        else {
            this.childageyear--;
        }
    }
    qntyearUp() {
        this.childageyear++;
    }
    qntmonthDown() {
        if (this.childagemonth <= 0) {
            this.presentToast("Sorry! You can't.");
        }
        else {
            this.childagemonth--;
        }
    }
    qntmonthUp() {
        this.childagemonth++;
    }
    petChange(evt) {
        this.checkpet == !this.checkpet;
        if (evt.detail.checked == true) {
            this.ispetinhouse = "Yes";
        }
        else {
            this.ispetinhouse = "No";
        }
    }
    subsidyChange(evtgov) {
        this.checkgovsub == !this.checkgovsub;
        if (evtgov.detail.checked == true) {
            this.isgovsubsidy = "Yes";
        }
        else {
            this.isgovsubsidy = "No";
        }
    }
    selectCare() {
        /* if (event.target.checked) {
          this.careData.push(checkbox);
        } else {
          let index = this.removeCheckedFromArray(checkbox);
          this.careData.splice(index, 1);
        }
        if (checkbox == "Others") {
          this.otherbox = !this.otherbox;
        } */
        console.log(this.getcareData);
    }
    removeCheckedFromArray(checkbox) {
        return this.careData.findIndex((category) => {
            return category === checkbox;
        });
    }
    onBack() {
        this.navCtrl.pop();
    }
    onSubmit() {
        if (this.about.trim() == '') {
            this.presentToast('Please enter somthing about you');
        }
        else if (this.childData.length == 0) {
            this.presentToast('Please Need to add at least One Child Minimum.');
        }
        else if (this.medical.trim() == '') {
            this.presentToast('Please enter Medical Condition');
        }
        else {
            this.showLoader('Please wait...');
            var body = {
                f_name: this.preData.fname,
                l_name: this.preData.lname,
                dob: this.preData.dob,
                phone: this.preData.phone,
                emg_phone: this.preData.emgnphone,
                email: this.preData.email,
                password: this.preData.password,
                address: this.preData.address,
                state: this.preData.state,
                city: this.preData.city,
                zip: this.preData.zip,
                govt_subsidy: this.isgovsubsidy,
                about_note: this.about,
                children_no: this.childData.length,
                medical_condition: this.medical,
                pets_in_house: this.ispetinhouse,
                children_info: this.childData,
                additional_care: this.getcareData,
                other_care: this.othercare
            };
            this.authService.postData("parent-registration", body).then(result => {
                this.hideLoader();
                this.data = result;
                this.userData = this.data.result.data;
                console.log("Registration: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.storage.remove('userRegsData');
                    this.storage.remove('setp1');
                    this.storage.remove('setp2');
                    this.presentToast("Thanks! You has been succesfully Registered.");
                    this.storage.set("userDetails", this.userData);
                    this.navCtrl.navigateRoot('/congratulation');
                }
                else {
                    this.presentToast(this.data.status.message);
                    var object = {
                        govt_subsidy: this.isgovsubsidy,
                        about_note: this.about,
                        children_no: this.childData.length,
                        medical_condition: this.medical,
                        pets_in_house: this.ispetinhouse,
                        children_info: this.childData,
                        additional_care: this.getcareData,
                        other_care: this.othercare
                    };
                    this.storage.set("setp2", object);
                }
            }, error => {
                this.hideLoader();
            });
        }
    }
    oneditSave() {
        if (this.about.trim() == '') {
            this.presentToast('Please enter somthing about you');
        }
        else if (this.childData.length == 0) {
            this.presentToast('Please Need to add at least One Child Minimum.');
        }
        else if (this.medical.trim() == '') {
            this.presentToast('Please enter Medical Condition');
        }
        else {
            this.showLoader('Please wait...');
            var body = {
                user_id: this.userId,
                f_name: this.preData.fname,
                l_name: this.preData.lname,
                dob: this.preData.dob,
                phone: this.preData.phone,
                emg_phone: this.preData.emgnphone,
                email: this.preData.email,
                address: this.preData.address,
                state: this.preData.state,
                city: this.preData.city,
                zip: this.preData.zip,
                govt_subsidy: this.isgovsubsidy,
                about_note: this.about,
                children_no: this.childData.length,
                medical_condition: this.medical,
                pets_in_house: this.ispetinhouse,
                children_info: this.childData,
                additional_care: this.getcareData,
                other_care: this.othercare
            };
            this.authService.postData("update-parent", body).then(result => {
                this.hideLoader();
                this.data = result;
                this.userData = this.data.result.data;
                console.log("Edit Registration: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.storage.remove('userRegsData');
                    this.presentToast("Thanks! You has been succesfully Updated.");
                    this.navCtrl.navigateRoot('/myprofile');
                }
                else {
                    this.presentToast(this.data.status.message);
                }
            }, error => {
                this.hideLoader();
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
Registration2Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider }
];
Registration2Page = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-registration2',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_registration2_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_registration2_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], Registration2Page);



/***/ }),

/***/ 67219:
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/registration2/registration2.page.html ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Parent Registration</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"step\">\n    <ul>\n      <li class=\"activestep\"><a></a><span>Step 1</span></li>\n      <li style=\"opacity: 0;\"><a></a></li>\n      <li class=\"activestep\"><a></a><span>Step 2</span></li>\n    </ul>\n  </div>\n  <div class=\"register_panel\">\n    <p style=\"color: #f44336; font-size: 12px; margin: 0 0 3px 0; text-align: right;\">* About, Add Child & Medical fields are mandatory *\n    </p>\n    <ion-textarea maxlength=\"200\" class=\"cust_input\" style=\"height: auto; border-radius: 25px;\" placeholder=\"Say something about you\"\n      [(ngModel)]=\"about\" (keyup.enter)=\"onKeyboard(Field1)\">\n    </ion-textarea>\n    <p style=\"color: #999999; font-size: 12px; margin: 0 0 15px 0; text-align: right;\">{{about.length}}/200</p>\n    <ion-item lines=\"none\" class=\"pl_input\" style=\"padding: 0 !important;\" *ngFor=\"let data of childData;  let i = index\">\n      <ion-label style=\"font-size: 15px;\">Child {{i+1}} Age</ion-label>\n      <ion-button slot=\"end\" color=\"warning\" size=\"small\" shape=\"round\" style=\"box-shadow: none; text-transform: lowercase;\">{{data.age}} yr {{data.month}} mth</ion-button>\n    </ion-item>\n    <div style=\"text-align: center;\">\n      <ion-button color=\"warning\" size=\"small\" shape=\"round\" (click)=\"addMore()\" style=\"text-transform: capitalize; height: 33px; width: 120px;\">Add Child</ion-button>\n    </div>\n    <ion-item lines=\"none\">\n      <ion-label style=\"font-size: 15px;\">Pets in House?</ion-label>\n      <ion-toggle color=\"warning\" [(ngModel)]=\"checkpet\" [checked]=\"checkpet\" (ionChange)=\"petChange($event)\"></ion-toggle>\n    </ion-item>\n\n    <h3 style=\"padding: 0 10px; font-size: 16px;\">Any Medical Condition?</h3>\n    <ion-input class=\"pl_input\" placeholder=\"Like: Medical, Allergy, etc..\" type=\"text\" [(ngModel)]=\"medical\"\n    (keyup.enter)=\"onKeyboard()\" #Field1></ion-input>\n\n    <ion-item lines=\"none\">\n      <p style=\"font-size: 15px;\">Will you apply for a goverment subsidy?</p>\n      <ion-toggle color=\"warning\" style=\"width: 60px;\" [(ngModel)]=\"checkgovsub\" [checked]=\"checkgovsub\" (ionChange)=\"subsidyChange($event)\"></ion-toggle>\n    </ion-item>\n\n    <h3 style=\"padding: 0 10px; font-size: 16px;\">What additional care needed?</h3>\n    <ion-select multiple=\"true\" class=\"pl_input\" [(ngModel)]=\"getcareData\" (ionChange)=\"selectCare()\" placeholder=\"Select Care\" [interfaceOptions]=\"customAlertOptions\">\n      <ion-select-option [value]=\"data.title\" *ngFor=\"let data of careList\">{{data.title}}</ion-select-option>\n   </ion-select>\n    <!-- <div *ngIf=\"getcareData != ''\" style=\"background: #f2f2f2; padding: 10px; border-radius: 8px; margin: 15px 0; overflow: hidden;\">\n      <span *ngFor=\"let item of getcareData\" class=\"tion\">{{item.care}}</span>\n    </div>\n    <ion-item lines=\"none\" *ngFor=\"let item of careList\">\n      <ion-checkbox slot=\"start\" style=\"margin: 0; margin-right: 10px;\" color=\"warning\" (ionChange)=\"selectCare($event,item.title)\"></ion-checkbox>\n      <ion-label class=\"uplaodtext\">{{item.title}}</ion-label>\n    </ion-item> -->\n    <ion-input *ngIf=\"otherbox\" class=\"pl_input\" placeholder=\"Like: Gardening, Beauty Care, etc..\" type=\"text\" [(ngModel)]=\"othercare\"></ion-input>\n  </div>\n  <ion-toolbar class=\"ftr\">\n    <ion-button color=\"dark\" slot=\"start\" fill=\"clear\" shape=\"round\" (click)=\"onBack()\" class=\"sml_cus_btn\"\n      style=\"box-shadow: none;\">\n      <img src=\"assets/images/black_l_arrow.png\" />&nbsp; Back</ion-button>\n    <ion-button *ngIf=\"hideextra\" color=\"warning\" slot=\"end\" shape=\"round\" (click)=\"onSubmit()\" class=\"sml_cus_btn\">Submit</ion-button>\n    <ion-button *ngIf=\"!hideextra\" color=\"warning\" slot=\"end\" shape=\"round\" (click)=\"oneditSave()\" class=\"sml_cus_btn\">Save</ion-button>\n  </ion-toolbar>\n</ion-content>\n\n<div class=\"custompop\" *ngIf=\"pop\">\n  <div class=\"custompop_in\">\n    <div class=\"popin\">\n      <h3>Add Child</h3>\n      <ion-item lines=\"none\" class=\"pl_input\" style=\"padding: 0 !important; margin-bottom: 30px; height: 60px;\">\n        <ion-label style=\"font-size: 14px; margin-top: 22px;\">Child Age</ion-label>\n        <div style=\"margin-right: 5px;\">\n          <label style=\"font-size: 12px;\">Year</label>\n          <div class=\"plusminus\">\n            <span class=\"btnleft\" (click)=\"qntyearDown()\">-</span>\n            <span class=\"number\">{{childageyear}}</span>\n            <span class=\"btnright\" (click)=\"qntyearUp()\">+</span>\n          </div>\n        </div>\n        <div style=\"margin-left: 5px;\">\n          <label style=\"font-size: 12px;\">Month</label>\n          <div class=\"plusminus\">\n            <span class=\"btnleft\" (click)=\"qntmonthDown()\">-</span>\n            <span class=\"number\">{{childagemonth}}</span>\n            <span class=\"btnright\" (click)=\"qntmonthUp()\">+</span>\n          </div>\n        </div>\n      </ion-item>\n      <div style=\"display: flex;\">\n        <ion-button color=\"dark\" expand=\"full\" shape=\"round\" (click)=\"closepop()\" class=\"sml_cus_btn\" style=\"width: 100%; box-shadow: none; margin: 0 3px;\">Cancel</ion-button>\n        <ion-button color=\"warning\" expand=\"full\" shape=\"round\" (click)=\"onSave()\" class=\"sml_cus_btn\" style=\"width: 100%; box-shadow: none; margin: 0 3px;\">Save</ion-button>\n      </div>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ 88090:
/*!************************************************************!*\
  !*** ./src/app/User/registration2/registration2.page.scss ***!
  \************************************************************/
/***/ ((module) => {

module.exports = ".step {\n  background: #fff0e3;\n  padding: 8px 0;\n}\n.step ul {\n  padding: 0;\n  width: 300px;\n  margin: 0 auto;\n  margin-bottom: 5px;\n  list-style: none;\n  text-align: center;\n  position: relative;\n}\n.step ul li {\n  display: inline-block;\n  position: relative;\n  padding: 0 15%;\n  z-index: 999;\n}\n.step ul li a {\n  display: block;\n  width: 22px;\n  height: 22px;\n  background: #fff;\n  border: #f38320 solid 1px;\n  border-radius: 50%;\n  position: relative;\n}\n.step ul li span {\n  display: block;\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 7px 0 0 -5px;\n}\n.step ul li:first-child {\n  padding-left: 0;\n}\n.step ul li:last-child {\n  padding-right: 0;\n  text-align: right;\n}\n.step ul li.activestep a {\n  width: 30px;\n  height: 30px;\n  background: #f38320;\n  position: relative;\n  z-index: 999;\n  margin-top: 0;\n  margin-left: -3px;\n  top: 5px;\n}\n.step ul li.activestep a:after {\n  width: 13px;\n  height: 5px;\n  content: \"\";\n  position: absolute;\n  border-left: 3px #ffffff solid;\n  border-bottom: 3px #ffffff solid;\n  transform: rotate(-45deg);\n  left: 6px;\n  top: 8px;\n}\n.step ul:after {\n  background: #cccccc;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 18px;\n  width: 80%;\n  height: 2px;\n  content: \"\";\n  margin: 0 auto;\n  z-index: 0;\n}\n.register_panel {\n  padding: 20px;\n}\n.sml_cus_btn {\n  box-shadow: #fdb77a 0 0 15px 5px;\n  margin: 20px;\n  border-radius: 30px;\n}\n.cust_input:hover {\n  border-color: #f38320;\n  box-shadow: #fff0e3 0 0 6px 4px;\n}\n.pl_input {\n  width: 100%;\n  height: 50px;\n  background: #f2f2f2;\n  --background: #f2f2f2;\n  border-radius: 50px;\n  color: #555555;\n  font-size: 15px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 10px 0;\n  padding: 0 20px !important;\n  border: 1px #f2f2f2 solid;\n}\n.pl_input input {\n  padding: 0 !important;\n  --placeholder-opacity: 0.2;\n}\n.phl:hover {\n  border-color: #41b578;\n  box-shadow: #e7fef2 0 0 6px 4px;\n  background: #ffffff;\n}\n.plusminus {\n  background: #f2f2f2;\n  width: 75px;\n  border-radius: 30px;\n  border: #f38320 1px solid;\n  color: #f38320;\n  text-align: center;\n  display: flex;\n  height: 25px;\n  line-height: 18px;\n}\n.plusminus .btnleft {\n  padding: 1px;\n  width: 25px;\n}\n.plusminus .btnright {\n  padding: 1px;\n  width: 25px;\n}\n.plusminus .number {\n  padding: 2px;\n  width: 30px;\n  background: #f38320;\n  color: #ffffff;\n  font-size: 11px;\n}\n.custompop {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.7);\n  top: 0;\n  left: 0;\n  z-index: 99;\n  width: 100%;\n  height: 100%;\n  display: table;\n}\n.custompop .custompop_in {\n  display: table-cell;\n  vertical-align: middle;\n  padding: 0 20px;\n}\n.custompop .popin {\n  background: #ffffff;\n  padding: 20px;\n  width: 100%;\n  border-radius: 20px;\n}\n.custompop .popin h3 {\n  font-size: 20px;\n  font-weight: bold;\n  text-align: center;\n  margin: 0 0 20px 0;\n}\n.tion {\n  background: #f38320;\n  padding: 8px 12px;\n  border-radius: 30px;\n  display: inline-block;\n  margin: 2px;\n  color: #ffffff;\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdHJhdGlvbjIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFBcUIsY0FBQTtBQUV6QjtBQURJO0VBQ0ksVUFBQTtFQUFZLFlBQUE7RUFBYyxjQUFBO0VBQWdCLGtCQUFBO0VBQW9CLGdCQUFBO0VBQWtCLGtCQUFBO0VBQW9CLGtCQUFBO0FBUzVHO0FBUlE7RUFDSSxxQkFBQTtFQUF1QixrQkFBQTtFQUFvQixjQUFBO0VBQWdCLFlBQUE7QUFhdkU7QUFaWTtFQUFHLGNBQUE7RUFBZ0IsV0FBQTtFQUFhLFlBQUE7RUFBYyxnQkFBQTtFQUFrQix5QkFBQTtFQUEwQixrQkFBQTtFQUFtQixrQkFBQTtBQXFCekg7QUFwQlk7RUFBTSxjQUFBO0VBQWdCLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxvQkFBQTtBQTRCM0g7QUExQlE7RUFBZ0IsZUFBQTtBQTZCeEI7QUE1QlE7RUFBZSxnQkFBQTtFQUFrQixpQkFBQTtBQWdDekM7QUEvQlE7RUFBa0IsV0FBQTtFQUFhLFlBQUE7RUFBYyxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixZQUFBO0VBQWMsYUFBQTtFQUFlLGlCQUFBO0VBQW1CLFFBQUE7QUF5QzlJO0FBeENRO0VBQXVCLFdBQUE7RUFBYSxXQUFBO0VBQWEsV0FBQTtFQUFhLGtCQUFBO0VBQW9CLDhCQUFBO0VBQWdDLGdDQUFBO0VBQWtDLHlCQUFBO0VBQTJCLFNBQUE7RUFBVyxRQUFBO0FBbURsTTtBQWhESTtFQUNJLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW9CLE9BQUE7RUFBUyxRQUFBO0VBQVUsU0FBQTtFQUFXLFVBQUE7RUFBWSxXQUFBO0VBQWEsV0FBQTtFQUFhLGNBQUE7RUFBZ0IsVUFBQTtBQTJEckk7QUF4REE7RUFBaUIsYUFBQTtBQTREakI7QUEzREE7RUFBYyxnQ0FBQTtFQUFrQyxZQUFBO0VBQWMsbUJBQUE7QUFpRTlEO0FBaEVBO0VBQW1CLHFCQUFBO0VBQXVCLCtCQUFBO0FBcUUxQztBQXBFQTtFQUNJLFdBQUE7RUFBYSxZQUFBO0VBQWMsbUJBQUE7RUFBcUIscUJBQUE7RUFBdUIsbUJBQUE7RUFBb0IsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGNBQUE7RUFBZ0IsMEJBQUE7RUFBNEIseUJBQUE7QUFrRnBPO0FBakZJO0VBQU0scUJBQUE7RUFBdUIsMEJBQUE7QUFxRmpDO0FBbkZBO0VBQVkscUJBQUE7RUFBdUIsK0JBQUE7RUFBaUMsbUJBQUE7QUF5RnBFO0FBeEZBO0VBQVcsbUJBQUE7RUFBcUIsV0FBQTtFQUFhLG1CQUFBO0VBQXFCLHlCQUFBO0VBQTJCLGNBQUE7RUFBZ0Isa0JBQUE7RUFBb0IsYUFBQTtFQUFlLFlBQUE7RUFBYyxpQkFBQTtBQW9HOUo7QUFuR0k7RUFBUyxZQUFBO0VBQWMsV0FBQTtBQXVHM0I7QUF0R0k7RUFBVSxZQUFBO0VBQWMsV0FBQTtBQTBHNUI7QUF6R0k7RUFBUyxZQUFBO0VBQWMsV0FBQTtFQUFhLG1CQUFBO0VBQXFCLGNBQUE7RUFBZ0IsZUFBQTtBQWdIN0U7QUE5R0E7RUFDSSxrQkFBQTtFQUFvQiw4QkFBQTtFQUFnRCxNQUFBO0VBQVEsT0FBQTtFQUFTLFdBQUE7RUFBYSxXQUFBO0VBQWEsWUFBQTtFQUFjLGNBQUE7QUF3SGpJO0FBdkhJO0VBQWMsbUJBQUE7RUFBcUIsc0JBQUE7RUFBd0IsZUFBQTtBQTRIL0Q7QUEzSEk7RUFBTyxtQkFBQTtFQUFxQixhQUFBO0VBQWUsV0FBQTtFQUFhLG1CQUFBO0FBaUk1RDtBQWhJUTtFQUFHLGVBQUE7RUFBaUIsaUJBQUE7RUFBbUIsa0JBQUE7RUFBb0Isa0JBQUE7QUFzSW5FO0FBbklBO0VBQ0ksbUJBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUFzSUoiLCJmaWxlIjoicmVnaXN0cmF0aW9uMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RlcHsgXHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmMGUzOyBwYWRkaW5nOiA4cHggMDtcclxuICAgIHVse1xyXG4gICAgICAgIHBhZGRpbmc6IDA7IHdpZHRoOiAzMDBweDsgbWFyZ2luOiAwIGF1dG87IG1hcmdpbi1ib3R0b206IDVweDsgbGlzdC1zdHlsZTogbm9uZTsgdGV4dC1hbGlnbjogY2VudGVyOyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbGl7IFxyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IHBvc2l0aW9uOiByZWxhdGl2ZTsgcGFkZGluZzogMCAxNSU7IHotaW5kZXg6IDk5OTtcclxuICAgICAgICAgICAgYXsgZGlzcGxheTogYmxvY2s7IHdpZHRoOiAyMnB4OyBoZWlnaHQ6IDIycHg7IGJhY2tncm91bmQ6ICNmZmY7IGJvcmRlcjojZjM4MzIwIHNvbGlkIDFweDsgYm9yZGVyLXJhZGl1czo1MCU7IHBvc2l0aW9uOiByZWxhdGl2ZTsgfSAgXHJcbiAgICAgICAgICAgIHNwYW57IGRpc3BsYXk6IGJsb2NrOyBjb2xvcjogIzY2NjY2NjsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7IG1hcmdpbjogN3B4IDAgMCAtNXB4OyB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxpOmZpcnN0LWNoaWxkeyBwYWRkaW5nLWxlZnQ6IDA7IH1cclxuICAgICAgICBsaTpsYXN0LWNoaWxkeyBwYWRkaW5nLXJpZ2h0OiAwOyB0ZXh0LWFsaWduOiByaWdodDsgfVxyXG4gICAgICAgIGxpLmFjdGl2ZXN0ZXAgYSB7IHdpZHRoOiAzMHB4OyBoZWlnaHQ6IDMwcHg7IGJhY2tncm91bmQ6ICNmMzgzMjA7IHBvc2l0aW9uOiByZWxhdGl2ZTsgei1pbmRleDogOTk5OyBtYXJnaW4tdG9wOiAwOyBtYXJnaW4tbGVmdDogLTNweDsgdG9wOiA1cHg7IH1cclxuICAgICAgICBsaS5hY3RpdmVzdGVwIGE6YWZ0ZXJ7IHdpZHRoOiAxM3B4OyBoZWlnaHQ6IDVweDsgY29udGVudDogXCJcIjsgcG9zaXRpb246IGFic29sdXRlOyBib3JkZXItbGVmdDogM3B4ICNmZmZmZmYgc29saWQ7IGJvcmRlci1ib3R0b206IDNweCAjZmZmZmZmIHNvbGlkOyB0cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpOyBsZWZ0OiA2cHg7IHRvcDogOHB4OyB9XHJcbiAgICBcclxuICAgIH1cclxuICAgIHVsOmFmdGVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjY2NjY2NjOyBwb3NpdGlvbjogYWJzb2x1dGU7IGxlZnQ6IDA7IHJpZ2h0OiAwOyB0b3A6IDE4cHg7IHdpZHRoOiA4MCU7IGhlaWdodDogMnB4OyBjb250ZW50OiBcIlwiOyBtYXJnaW46IDAgYXV0bzsgei1pbmRleDogMDtcclxuICAgIH1cclxufVxyXG4ucmVnaXN0ZXJfcGFuZWx7IHBhZGRpbmc6IDIwcHg7IH1cclxuLnNtbF9jdXNfYnRueyBib3gtc2hhZG93OiAjZmRiNzdhIDAgMCAxNXB4IDVweDsgbWFyZ2luOiAyMHB4OyBib3JkZXItcmFkaXVzOiAzMHB4OyB9XHJcbi5jdXN0X2lucHV0OmhvdmVyeyBib3JkZXItY29sb3I6ICNmMzgzMjA7IGJveC1zaGFkb3c6ICNmZmYwZTMgMCAwIDZweCA0cHg7fVxyXG4ucGxfaW5wdXR7XHJcbiAgICB3aWR0aDogMTAwJTsgaGVpZ2h0OiA1MHB4OyBiYWNrZ3JvdW5kOiAjZjJmMmYyOyAtLWJhY2tncm91bmQ6ICNmMmYyZjI7IGJvcmRlci1yYWRpdXM6NTBweDsgY29sb3I6ICM1NTU1NTU7IGZvbnQtc2l6ZTogMTVweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmOyBtYXJnaW46IDEwcHggMDsgcGFkZGluZzogMCAyMHB4ICFpbXBvcnRhbnQ7IGJvcmRlcjogMXB4ICNmMmYyZjIgc29saWQ7XHJcbiAgICBpbnB1dHtwYWRkaW5nOiAwICFpbXBvcnRhbnQ7IC0tcGxhY2Vob2xkZXItb3BhY2l0eTogMC4yO30gICAgXHJcbn1cclxuLnBobDpob3ZlcnsgYm9yZGVyLWNvbG9yOiAjNDFiNTc4OyBib3gtc2hhZG93OiAjZTdmZWYyIDAgMCA2cHggNHB4OyBiYWNrZ3JvdW5kOiAjZmZmZmZmOyB9XHJcbi5wbHVzbWludXN7YmFja2dyb3VuZDogI2YyZjJmMjsgd2lkdGg6IDc1cHg7IGJvcmRlci1yYWRpdXM6IDMwcHg7IGJvcmRlcjogI2YzODMyMCAxcHggc29saWQ7IGNvbG9yOiAjZjM4MzIwOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGRpc3BsYXk6IGZsZXg7IGhlaWdodDogMjVweDsgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAuYnRubGVmdHtwYWRkaW5nOiAxcHg7IHdpZHRoOiAyNXB4O31cclxuICAgIC5idG5yaWdodHtwYWRkaW5nOiAxcHg7IHdpZHRoOiAyNXB4O31cclxuICAgIC5udW1iZXIge3BhZGRpbmc6IDJweDsgd2lkdGg6IDMwcHg7IGJhY2tncm91bmQ6ICNmMzgzMjA7IGNvbG9yOiAjZmZmZmZmOyBmb250LXNpemU6IDExcHg7fVxyXG59XHJcbi5jdXN0b21wb3B7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7IGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IDAuNyk7IHRvcDogMDsgbGVmdDogMDsgei1pbmRleDogOTk7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDEwMCU7IGRpc3BsYXk6IHRhYmxlO1xyXG4gICAgLmN1c3RvbXBvcF9pbntkaXNwbGF5OiB0YWJsZS1jZWxsOyB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyBwYWRkaW5nOiAwIDIwcHg7fVxyXG4gICAgLnBvcGlue2JhY2tncm91bmQ6ICNmZmZmZmY7IHBhZGRpbmc6IDIwcHg7IHdpZHRoOiAxMDAlOyBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgIGgze2ZvbnQtc2l6ZTogMjBweDsgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtYWxpZ246IGNlbnRlcjsgbWFyZ2luOiAwIDAgMjBweCAwO31cclxuICAgIH1cclxufVxyXG4udGlvbntcclxuICAgIGJhY2tncm91bmQ6ICNmMzgzMjA7XHJcbiAgICBwYWRkaW5nOiA4cHggMTJweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBtYXJnaW46IDJweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_registration2_registration2_module_ts.js.map