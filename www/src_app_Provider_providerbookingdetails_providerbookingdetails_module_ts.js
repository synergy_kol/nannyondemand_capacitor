"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerbookingdetails_providerbookingdetails_module_ts"],{

/***/ 92795:
/*!******************************************************************************************!*\
  !*** ./src/app/Provider/providerbookingdetails/providerbookingdetails-routing.module.ts ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderbookingdetailsPageRoutingModule": () => (/* binding */ ProviderbookingdetailsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerbookingdetails_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerbookingdetails.page */ 66275);




const routes = [
    {
        path: '',
        component: _providerbookingdetails_page__WEBPACK_IMPORTED_MODULE_0__.ProviderbookingdetailsPage
    }
];
let ProviderbookingdetailsPageRoutingModule = class ProviderbookingdetailsPageRoutingModule {
};
ProviderbookingdetailsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProviderbookingdetailsPageRoutingModule);



/***/ }),

/***/ 43845:
/*!**********************************************************************************!*\
  !*** ./src/app/Provider/providerbookingdetails/providerbookingdetails.module.ts ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderbookingdetailsPageModule": () => (/* binding */ ProviderbookingdetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerbookingdetails_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerbookingdetails-routing.module */ 92795);
/* harmony import */ var _providerbookingdetails_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerbookingdetails.page */ 66275);







let ProviderbookingdetailsPageModule = class ProviderbookingdetailsPageModule {
};
ProviderbookingdetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerbookingdetails_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProviderbookingdetailsPageRoutingModule
        ],
        declarations: [_providerbookingdetails_page__WEBPACK_IMPORTED_MODULE_1__.ProviderbookingdetailsPage]
    })
], ProviderbookingdetailsPageModule);



/***/ }),

/***/ 66275:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providerbookingdetails/providerbookingdetails.page.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderbookingdetailsPage": () => (/* binding */ ProviderbookingdetailsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerbookingdetails_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerbookingdetails.page.html */ 27142);
/* harmony import */ var _providerbookingdetails_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerbookingdetails.page.scss */ 95064);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);








let ProviderbookingdetailsPage = class ProviderbookingdetailsPage {
    constructor(navCtrl, authService, loadingController, toastController, storage, activatedRoute) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.storage = storage;
        this.activatedRoute = activatedRoute;
        this.isLoading = false;
        this.isDisabled = false;
        this.userdata = [];
        this.data = [];
        this.bookingDetails = '';
        this.bookingID = '';
        this.activatedRoute.queryParams.subscribe((res) => {
            this.bookingID = res.bookID;
        });
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
                booking_detail_id: this.bookingID
            };
            this.authService.postData("get-booking-details", body).then(result => {
                this.data = result;
                console.log("Booking Details: ", this.data);
                this.bookingDetails = this.data.result.data[0];
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    onPayOut() {
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
                booking_detail_id: this.bookingID
            };
            this.authService.postData("post-payment-request", body).then(result => {
                this.hideLoader();
                this.data = result;
                console.log("Payout: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.presentToast(this.data.status.message);
                    this.getDetails();
                }
                else {
                    this.presentToast(this.data.status.message);
                }
            }, error => {
                this.hideLoader();
            });
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
ProviderbookingdetailsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute }
];
ProviderbookingdetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-providerbookingdetails',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerbookingdetails_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerbookingdetails_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProviderbookingdetailsPage);



/***/ }),

/***/ 27142:
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerbookingdetails/providerbookingdetails.page.html ***!
  \*************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Booking Details</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button style=\"color: #ffffff;\">\n        <a href=\"tel:{{bookingDetails.parent_phone}}\" style=\"text-decoration: none !important;\"><img style=\"height: 28px; margin-right: 5px;\" src=\"assets/images/call.png\" /></a>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div style=\"padding: 0 20px;\">\n    <div class=\"blokingbox\">\n      <ion-grid class=\"booking_bottom\">\n        <ion-row>\n          <ion-col>\n            <h5 style=\"font-size: 18px; padding: 5px 0;\">#{{bookingDetails.booking_id}}</h5>\n          </ion-col>\n          <ion-col>\n            <h5 style=\"font-size: 18px; padding: 5px 0; text-align: right;\">${{bookingDetails.amount}}</h5>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n    <div class=\"blokingbox\">\n      <div class=\"blokingbox_top\">\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <div class=\"booking_box_img\">\n                <div class=\"booking_box_img_l\"><img [src]=\"bookingDetails.parent_profile_image != '' ? bookingDetails.parent_profile_image : 'assets/images/avatar.png'\" /></div>\n                <div class=\"booking_box_img_r\">\n                  <h3>{{bookingDetails.parent_fname}} {{bookingDetails.parent_lname}}</h3>\n                  <span>{{bookingDetails.parent_phone}}</span>\n                </div>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n      <ion-grid class=\"booking_bottom\">\n        <ion-row>\n          <ion-col>\n            <h5>Booking Date</h5>\n            <h3>{{bookingDetails.date}}</h3>\n          </ion-col>\n          <ion-col>\n            <h5>Time Slot</h5>\n            <h3 style=\"text-transform: capitalize;\">{{bookingDetails.time_slot}}</h3>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <ion-grid class=\"person_info\">\n        <ion-row>\n          <ion-col>\n            <h5>No of children:<span> {{bookingDetails.parent_children_no}}</span></h5>\n            <h5>Booking Status: <b>Confirmed</b></h5>\n            <h5 *ngIf=\"bookingDetails.service_is_completed == 1\">Payment Status:\n              <b *ngIf=\"bookingDetails.payment_request == 1\" style=\"color: #ff5722;\">Pending</b> \n              <b *ngIf=\"bookingDetails.payment_request == 2\" style=\"color: #ff5722;\">Paid</b>\n            </h5>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <div class=\"locationbooking\">\n        <img class=\"locationbooking_l\" src=\"assets/images/green_location.png\" alt=\"green_location\" />\n        <h4>{{bookingDetails.parent_address}}</h4>\n        <!-- <img class=\"locationbooking_r\" src=\"assets/images/green_direction.png\" alt=\"green_direction\" /> -->\n      </div>\n    </div>\n    <div style=\"padding: 10px 30px;\" *ngIf=\"bookingDetails.service_is_completed == 1\">\n      <ion-button *ngIf=\"bookingDetails.payment_request == 0\" color=\"success\" expand=\"full\" shape=\"round\" (click)=\"onPayOut()\" class=\"cus_btn\">Payout Request</ion-button>\n      <ion-button *ngIf=\"bookingDetails.payment_request == 1\" style=\"opacity: 0.4;\" color=\"success\" expand=\"full\" shape=\"round\" class=\"cus_btn\">Payout Request Sent</ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 95064:
/*!**********************************************************************************!*\
  !*** ./src/app/Provider/providerbookingdetails/providerbookingdetails.page.scss ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = ".blokingbox {\n  box-shadow: #dddddd 0 0 10px;\n  border-radius: 8px;\n  margin: 20px 0;\n}\n\n.blokingbox_top {\n  border-bottom: #dddddd solid 1px;\n  padding: 5px 0;\n}\n\n.booking_box_img {\n  display: flex;\n}\n\n.booking_box_img .booking_box_img_l {\n  width: 40px;\n  height: 40px;\n  border-radius: 50%;\n  margin-right: 10px;\n}\n\n.booking_box_img .booking_box_img_l img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n\n.booking_box_img .booking_box_img_r {\n  margin: 4px 0;\n}\n\n.booking_box_img .booking_box_img_r h3 {\n  margin: 0;\n  font-size: 15px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  color: #222222;\n}\n\n.booking_box_img .booking_box_img_r span {\n  color: #444444;\n  font-weight: 400;\n  font-size: 13px;\n  font-style: italic;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.booking_bottom {\n  padding: 5px;\n}\n\n.booking_bottom h5 {\n  padding: 0 0 5px 0px;\n  margin: 0;\n  color: #41b578;\n  font-size: 11px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.booking_bottom h3 {\n  padding: 0;\n  margin: 0;\n  color: #444444;\n  font-size: 13px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.locationbooking {\n  position: relative;\n  background: #f6f6f6;\n  padding: 15px 55px;\n  border-top: #dddddd solid 1px;\n}\n\n.locationbooking h4 {\n  padding: 0;\n  margin: 7px 0;\n  color: #888888;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.locationbooking_l {\n  position: absolute;\n  top: 15px;\n  left: 15px;\n  height: 30px;\n}\n\n.locationbooking_r {\n  position: absolute;\n  top: 19px;\n  right: 15px;\n  height: 25px;\n}\n\n.person_info {\n  padding: 5px;\n  border-top: #dddddd solid 1px;\n}\n\n.person_info h5 {\n  margin: 5px 0;\n  color: #888888;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.person_info span, .person_info b {\n  color: #41b578;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVyYm9va2luZ2RldGFpbHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQWEsNEJBQUE7RUFBOEIsa0JBQUE7RUFBb0IsY0FBQTtBQUkvRDs7QUFIQTtFQUFpQixnQ0FBQTtFQUFpQyxjQUFBO0FBUWxEOztBQU5BO0VBQ0ksYUFBQTtBQVNKOztBQVJJO0VBQ0ksV0FBQTtFQUFZLFlBQUE7RUFBYSxrQkFBQTtFQUFtQixrQkFBQTtBQWFwRDs7QUFaUTtFQUFLLFdBQUE7RUFBWSxZQUFBO0VBQWEsa0JBQUE7QUFpQnRDOztBQWZJO0VBQW1CLGFBQUE7QUFrQnZCOztBQWpCUTtFQUFHLFNBQUE7RUFBVyxlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGNBQUE7QUF3Qi9GOztBQXZCUTtFQUFNLGNBQUE7RUFBZ0IsZ0JBQUE7RUFBaUIsZUFBQTtFQUFpQixrQkFBQTtFQUFvQixvQ0FBQTtBQThCcEY7O0FBM0JBO0VBQWlCLFlBQUE7QUErQmpCOztBQTlCSTtFQUFJLG9CQUFBO0VBQXNCLFNBQUE7RUFBVyxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7QUFzQzVGOztBQXJDSTtFQUFJLFVBQUE7RUFBWSxTQUFBO0VBQVcsY0FBQTtFQUFnQixlQUFBO0VBQWdCLGdCQUFBO0VBQWtCLG9DQUFBO0FBNkNqRjs7QUEzQ0E7RUFDSSxrQkFBQTtFQUFvQixtQkFBQTtFQUFxQixrQkFBQTtFQUFvQiw2QkFBQTtBQWlEakU7O0FBaERJO0VBQUksVUFBQTtFQUFZLGFBQUE7RUFBYyxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7QUF3RHJGOztBQXREQTtFQUFvQixrQkFBQTtFQUFvQixTQUFBO0VBQVUsVUFBQTtFQUFZLFlBQUE7QUE2RDlEOztBQTVEQTtFQUFvQixrQkFBQTtFQUFvQixTQUFBO0VBQVUsV0FBQTtFQUFhLFlBQUE7QUFtRS9EOztBQWxFQTtFQUFjLFlBQUE7RUFBYyw2QkFBQTtBQXVFNUI7O0FBdEVJO0VBQ0ksYUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtBQXdFUjs7QUFyRUE7RUFDSSxjQUFBO0FBd0VKIiwiZmlsZSI6InByb3ZpZGVyYm9va2luZ2RldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsb2tpbmdib3h7IGJveC1zaGFkb3c6ICNkZGRkZGQgMCAwIDEwcHg7IGJvcmRlci1yYWRpdXM6IDhweDsgbWFyZ2luOiAyMHB4IDA7IH1cclxuLmJsb2tpbmdib3hfdG9weyBib3JkZXItYm90dG9tOiNkZGRkZGQgc29saWQgMXB4OyBwYWRkaW5nOiA1cHggMDsgfVxyXG5cclxuLmJvb2tpbmdfYm94X2ltZ3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuYm9va2luZ19ib3hfaW1nX2x7IFxyXG4gICAgICAgIHdpZHRoOjQwcHg7IGhlaWdodDo0MHB4OyBib3JkZXItcmFkaXVzOjUwJTsgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIGltZ3sgd2lkdGg6MTAwJTsgaGVpZ2h0OjEwMCU7IGJvcmRlci1yYWRpdXM6NTAlOyB9XHJcbiAgICB9XHJcbiAgICAuYm9va2luZ19ib3hfaW1nX3J7bWFyZ2luOiA0cHggMDtcclxuICAgICAgICBoM3ttYXJnaW46IDA7IGZvbnQtc2l6ZTogMTVweDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGNvbG9yOiAjMjIyMjIyOyB9XHJcbiAgICAgICAgc3BhbnsgY29sb3I6ICM0NDQ0NDQ7IGZvbnQtd2VpZ2h0OjQwMDsgZm9udC1zaXplOiAxM3B4OyBmb250LXN0eWxlOiBpdGFsaWM7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICB9XHJcbn1cclxuLmJvb2tpbmdfYm90dG9teyBwYWRkaW5nOiA1cHg7XHJcbiAgICBoNXsgcGFkZGluZzogMCAwIDVweCAwcHg7IG1hcmdpbjogMDsgY29sb3I6ICM0MWI1Nzg7IGZvbnQtc2l6ZTogMTFweDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IH1cclxuICAgIGgzeyBwYWRkaW5nOiAwOyBtYXJnaW46IDA7IGNvbG9yOiAjNDQ0NDQ0OyBmb250LXNpemU6MTNweDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IH1cclxufVxyXG4ubG9jYXRpb25ib29raW5ne1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlOyBiYWNrZ3JvdW5kOiAjZjZmNmY2OyBwYWRkaW5nOiAxNXB4IDU1cHg7IGJvcmRlci10b3A6I2RkZGRkZCBzb2xpZCAxcHg7XHJcbiAgICBoNHsgcGFkZGluZzogMDsgbWFyZ2luOjdweCAwOyBjb2xvcjogIzg4ODg4ODsgZm9udC1zaXplOiAxNHB4OyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG59XHJcbi5sb2NhdGlvbmJvb2tpbmdfbHsgcG9zaXRpb246IGFic29sdXRlOyB0b3A6MTVweDsgbGVmdDogMTVweDsgaGVpZ2h0OiAzMHB4OyB9XHJcbi5sb2NhdGlvbmJvb2tpbmdfcnsgcG9zaXRpb246IGFic29sdXRlOyB0b3A6MTlweDsgcmlnaHQ6IDE1cHg7IGhlaWdodDogMjVweDsgfVxyXG4ucGVyc29uX2luZm97IHBhZGRpbmc6IDVweDsgYm9yZGVyLXRvcDojZGRkZGRkIHNvbGlkIDFweDtcclxuICAgIGg1e1xyXG4gICAgICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgICAgICAgY29sb3I6ICM4ODg4ODg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XHJcbiAgICB9XHJcbn1cclxuLnBlcnNvbl9pbmZvIHNwYW4sIC5wZXJzb25faW5mbyBie1xyXG4gICAgY29sb3I6IzQxYjU3ODtcclxufSJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerbookingdetails_providerbookingdetails_module_ts.js.map