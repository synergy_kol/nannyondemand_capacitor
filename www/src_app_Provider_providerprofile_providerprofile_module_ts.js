"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerprofile_providerprofile_module_ts"],{

/***/ 34966:
/*!****************************************************************************!*\
  !*** ./src/app/Provider/providerprofile/providerprofile-routing.module.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderprofilePageRoutingModule": () => (/* binding */ ProviderprofilePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerprofile_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerprofile.page */ 62311);




const routes = [
    {
        path: '',
        component: _providerprofile_page__WEBPACK_IMPORTED_MODULE_0__.ProviderprofilePage
    }
];
let ProviderprofilePageRoutingModule = class ProviderprofilePageRoutingModule {
};
ProviderprofilePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProviderprofilePageRoutingModule);



/***/ }),

/***/ 29343:
/*!********************************************************************!*\
  !*** ./src/app/Provider/providerprofile/providerprofile.module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderprofilePageModule": () => (/* binding */ ProviderprofilePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerprofile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerprofile-routing.module */ 34966);
/* harmony import */ var _providerprofile_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerprofile.page */ 62311);







//import { StarRatingModule } from 'ionic5-star-rating';
//import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
let ProviderprofilePageModule = class ProviderprofilePageModule {
};
ProviderprofilePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerprofile_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProviderprofilePageRoutingModule,
            //StarRatingModule,
            //NgxIonicImageViewerModule
        ],
        declarations: [_providerprofile_page__WEBPACK_IMPORTED_MODULE_1__.ProviderprofilePage]
    })
], ProviderprofilePageModule);



/***/ }),

/***/ 62311:
/*!******************************************************************!*\
  !*** ./src/app/Provider/providerprofile/providerprofile.page.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderprofilePage": () => (/* binding */ ProviderprofilePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerprofile_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerprofile.page.html */ 80010);
/* harmony import */ var _providerprofile_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerprofile.page.scss */ 3562);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 30692);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);





//import { FilePath } from '@ionic-native/file-path/ngx';
//import { File } from '@ionic-native/file/ngx';



let ProviderprofilePage = class ProviderprofilePage {
    constructor(navCtrl, storage, loadingController, authService, actionSheet, camera, platform, 
    //private filePath: FilePath,
    //private file: File,
    toastController, alertController) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.loadingController = loadingController;
        this.authService = authService;
        this.actionSheet = actionSheet;
        this.camera = camera;
        this.platform = platform;
        this.toastController = toastController;
        this.alertController = alertController;
        this.isLoading = false;
        this.isDisabled = false;
        this.userdata = [];
        this.data = [];
        this.profileData = [];
        this.profilePic = [];
        this.galleryData = [];
        this.allGPimages = [];
        this.GPimage = '';
        this.reviewData = [];
        this.showabout = false;
        this.abactive = "";
        this.showgallery = true;
        this.glactive = "activetab";
        this.showreview = false;
        this.rvactive = "";
    }
    ngOnInit() {
    }
    about() {
        this.showabout = true;
        this.showgallery = false;
        this.showreview = false;
        this.abactive = "activetab";
        this.glactive = "";
        this.rvactive = "";
    }
    gallery() {
        this.showabout = false;
        this.showgallery = true;
        this.showreview = false;
        this.abactive = "";
        this.glactive = "activetab";
        this.rvactive = "";
    }
    review() {
        this.showabout = false;
        this.showgallery = false;
        this.showreview = true;
        this.abactive = "";
        this.glactive = "";
        this.rvactive = "activetab";
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.getGallery();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-profile", body).then(result => {
                this.data = result;
                console.log("profile: ", this.data);
                this.profileData = this.data.result.data;
                this.profilePic = this.profileData.profile_image;
                this.reviewData = this.profileData.reviews;
                this.hideLoader();
                this.age = this.calculateAge(this.age);
            }, error => {
                this.hideLoader();
            });
        });
    }
    calculateAge(dob) {
        this.age = dob;
        var dob_entry = this.profileData.dob;
        var today = new Date();
        var birthDate = new Date(dob_entry);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    getGallery() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-photo-gallery-images", body).then(result => {
                this.data = result;
                console.log("Gallery: ", this.data);
                this.galleryData = this.data.result.data;
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    uploadPP() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takePP(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePP(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takePP(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            this.profilePic = 'data:image/jpeg;base64,' + imagePath;
            this.saveProfilePic(this.profilePic);
        }, (err) => {
            console.log(err);
        });
    }
    saveProfilePic(pic) {
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
                image: pic
            };
            this.authService.postData("ProfileImageUpload", body).then(result => {
                this.data = result;
                console.log("Profile Image: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.presentToast("Profile Picture Succesfully Uploaded.");
                    this.getDetails();
                }
                else {
                    this.presentToast(this.data.status.message);
                    this.hideLoader();
                }
            }, error => {
                this.hideLoader();
            });
        });
    }
    uploadGP() {
        let actionSheet = this.actionSheet.create({
            header: 'Select Image Source',
            buttons: [{
                    text: 'Load from Library',
                    handler: () => {
                        this.takeGP(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                }, {
                    text: 'Use Camera',
                    handler: () => {
                        this.takeGP(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }]
        }).then(a => {
            a.present();
        });
    }
    takeGP(sourceType) {
        // Create options for the Camera Dialog
        var options = {
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            let data = {
                image: 'data:image/jpeg;base64,' + imagePath
            };
            this.allGPimages.push(data);
            this.GPimage = this.allGPimages;
            this.saveGalleryPic(this.GPimage);
        }, (err) => {
            console.log(err);
        });
    }
    saveGalleryPic(GPpic) {
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
                images: GPpic
            };
            this.authService.postData("save-photo-gallery-images", body).then(result => {
                this.data = result;
                console.log("Saved Gallery Image: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.presentToast("Image Succesfully Uploaded.");
                    this.allGPimages = [];
                    this.getGallery();
                }
                else {
                    this.presentToast(this.data.status.message);
                    this.hideLoader();
                }
            }, error => {
                this.hideLoader();
            });
        });
    }
    rmimage(uID, imgID) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Remove?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Delete',
                    handler: () => {
                        this.showLoader('Removing...');
                        setTimeout(() => {
                            var body = {
                                user_id: uID,
                                image_id: imgID
                            };
                            this.authService.postData("remove-gallery-image", body).then(result => {
                                this.data = result;
                                console.log("Remove Image: ", this.data);
                                if (this.data.status.error_code == 0) {
                                    this.presentToast("Image Succesfully Removed.");
                                    this.getGallery();
                                }
                                else {
                                    this.presentToast(this.data.status.message);
                                    this.hideLoader();
                                }
                            }, error => {
                                this.hideLoader();
                            });
                        }, 3000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    goEdit() {
        this.navCtrl.navigateForward('/providerregistration1');
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
ProviderprofilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_4__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ActionSheetController },
    { type: _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_2__.Camera },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.AlertController }
];
ProviderprofilePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-providerprofile',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerprofile_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerprofile_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProviderprofilePage);



/***/ }),

/***/ 80010:
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerprofile/providerprofile.page.html ***!
  \***********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>My Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"profile_img_section\">\n    <ion-img [src]=\"profilePic != '' ? profilePic : 'assets/images/avatar.png'\"></ion-img>\n    <div class=\"profile_upload_btn\" (click)=\"uploadPP()\"><img src=\"assets/images/white_camera.png\" alt=\"white_camera\" /></div>\n  </div>\n  <div class=\"profile_info\">\n    <h4>{{profileData.fname}} {{profileData.lname}}, {{age}}</h4>\n    <p><img src=\"assets/images/location_icon.png\" alt=\"location_icon\" />&nbsp;{{profileData.address}},\n      {{profileData.city}}, {{profileData.zip}}\n    </p>\n\n    <div class=\"review_panel\">\n      <ion-grid class=\"reviewgrid\">\n        <ion-row>\n          <ion-col>\n            <h5>Review</h5>\n            <div style=\"display: flex; align-items: baseline;\">\n              <ionic5-star-rating #rating activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\"\n                defaultColor=\"#c4c4c4\" readonly=\"true\" rating=\"{{profileData.avg_rate}}\" fontSize=\"15px\" style=\"margin-top: 5px;\">\n              </ionic5-star-rating>\n              <span  *ngIf=\"profileData.avg_rate > 0\" style=\"margin-left: 6px; position: relative; top: -2px; font-size: 13px;\">({{profileData.reviews.length}})</span>\n           </div>\n          </ion-col>\n          <ion-col>\n            <h5>Experience</h5>\n            <h4>{{profileData.experience}} years</h4>\n          </ion-col>\n          <ion-col>\n            <h5>Rate</h5>\n            <h4>$ {{profileData.rate}}/hr</h4>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n\n    <div class=\"tabpanel\">\n      <ul>\n        <li (click)=\"about()\" [ngClass]=\"abactive\"><a>About</a></li>\n        <li (click)=\"gallery()\" [ngClass]=\"glactive\"><a>Gallery</a></li>\n        <li (click)=\"review()\" [ngClass]=\"rvactive\"><a>Review</a></li>\n      </ul>\n\n      <div style=\"padding: 0 30px; margin-bottom: 10px;\" *ngIf=\"showabout\">\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Full Name:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.fname}} {{profileData.lname}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Email:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.email}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Phone:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.phone}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Address:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.address}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">State:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.state}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">City:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.city}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Zip:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.zip}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">About Note:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{profileData.about_note}}</p>\n      </div>\n\n      <div style=\"padding: 0 10px; margin-bottom: 10px;\" *ngIf=\"showgallery\">\n        <div class=\"gallery_box\" (click)=\"uploadGP()\">\n          <div class=\"addgallery\">\n            <img src=\"assets/images/white_camera2.png\" alt=\"white_camera2\" />\n            <span>Add Image</span>\n          </div>\n        </div>\n        <div class=\"gallery_box\" *ngFor=\"let item of galleryData\">\n          <ion-img ionImgViewer scheme=\"dark\" [src]=\"item.image\"></ion-img>\n          <strong (click)=\"rmimage(item.user_id, item.gallery_image_id)\">x</strong>\n        </div>\n      </div>\n\n      <div style=\"padding: 0 15px; margin-bottom: 10px;\" *ngIf=\"showreview\">\n        <p *ngIf=\"reviewData == ''\" style=\"font-weight: bold; font-size: 18px; color: #999999;\">No Review Found!</p>\n        <div *ngIf=\"reviewData != ''\">\n          <ion-item *ngFor=\"let data of reviewData\">\n            <ion-avatar slot=\"start\">\n              <img [src]=\"data.parent_profile_image != '' ? data.parent_profile_image : 'assets/images/avatar.png'\" />\n            </ion-avatar>\n            <ion-label>\n              <h3><strong>{{data.parent_first_name}} {{data.parent_last_name}}</strong></h3>\n              <ionic5-star-rating #rating activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\"\n                  defaultColor=\"#c4c4c4\" readonly=\"true\" rating=\"{{data.rate}}\" fontSize=\"13px\">\n              </ionic5-star-rating>\n              <p style=\"text-align: left;\">{{data.message}}</p>\n            </ion-label>\n          </ion-item>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer (click)=\"goEdit()\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-button color=\"light\" fill=\"clear\" expand=\"full\"\n      style=\"font-weight: bold; font-family: 'Open Sans', sans-serif; font-weight: 600; text-transform: uppercase; font-size: 14px;\">\n      Edit Profile</ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 3562:
/*!********************************************************************!*\
  !*** ./src/app/Provider/providerprofile/providerprofile.page.scss ***!
  \********************************************************************/
/***/ ((module) => {

module.exports = ".profile_img_section {\n  width: 140px;\n  height: 140px;\n  border-radius: 50%;\n  margin: 23px auto;\n  position: relative;\n}\n.profile_img_section ion-img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  overflow: hidden;\n  border: 6px #eeeeee solid;\n  object-fit: cover;\n}\n.profile_upload_btn {\n  width: 42px;\n  height: 42px;\n  border-radius: 50%;\n  background: #41b578;\n  text-align: center;\n  border: 5px solid #fff;\n  position: absolute;\n  bottom: 6px;\n  right: 0;\n  padding: 8px 0 0 0;\n}\n.profile_upload_btn img {\n  width: auto;\n  height: auto;\n  border-radius: 0;\n}\n.profile_info h4 {\n  padding: 0;\n  margin: 0;\n  color: #000;\n  font-size: 20px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.profile_info p {\n  padding: 5px 0 0 0;\n  margin: 0;\n  color: #000;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.reviewgrid ion-row ion-col {\n  text-align: center;\n  border-right: #41b578 solid 1px;\n}\n.reviewgrid ion-row ion-col img {\n  padding-top: 5px;\n}\n.reviewgrid ion-row ion-col h5 {\n  padding: 0;\n  margin: 0;\n  color: #000000;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n.reviewgrid ion-row ion-col h4 {\n  padding: 5px 0 0 0;\n  margin: 0;\n  color: #41b578;\n  font-size: 15px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.reviewgrid ion-row ion-col:last-child {\n  border-right: 0;\n}\n.reviewgrid {\n  border-top: #41b578 solid 1px;\n  border-bottom: #41b578 solid 1px;\n  margin: 19px 0 27px 0;\n}\n.tabpanel ul {\n  padding: 0 0 35px 0;\n  margin: 0;\n  list-style: none;\n  text-align: center;\n}\n.tabpanel ul li {\n  display: inline-block;\n  margin: 0 15px;\n}\n.tabpanel ul li a {\n  display: block;\n  padding: 0;\n  color: #666666;\n  font-size: 18px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n  text-decoration: none;\n  padding: 0 10px;\n  position: relative;\n}\n.tabpanel ul li.activetab a:after {\n  position: absolute;\n  bottom: -10px;\n  width: 100%;\n  background: #41b578;\n  height: 2px;\n  content: \"\";\n  left: 0;\n}\n.tabpanel ul li.activetab a {\n  color: #41b578;\n}\n.gallery_box {\n  display: inline-block;\n  width: 31.33%;\n  margin: 1%;\n  border-radius: 10px;\n  vertical-align: top;\n  height: 100px;\n  overflow: hidden;\n  position: relative;\n}\n.gallery_box ion-img {\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n}\n.gallery_box strong {\n  position: absolute;\n  font-weight: normal;\n  color: #ffffff;\n  top: 5px;\n  right: 5px;\n  background: red;\n  padding: 3px 6px 4px 6px;\n  border-radius: 15px;\n  z-index: 99;\n  font-size: 13px;\n  font-weight: bold;\n  line-height: 12px;\n}\n.addgallery {\n  background: #41b578;\n  text-align: center;\n  padding: 19px 0;\n}\n.addgallery img {\n  width: auto !important;\n}\n.addgallery span {\n  color: #fff;\n  font-weight: 400;\n  font-size: 14px;\n  font-family: \"Open Sans\", sans-serif;\n  display: block;\n  text-transform: uppercase;\n  padding: 10px 0 0 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVycHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBc0IsWUFBQTtFQUFjLGFBQUE7RUFBZSxrQkFBQTtFQUFtQixpQkFBQTtFQUFtQixrQkFBQTtBQU16RjtBQUxJO0VBQVMsV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtFQUFtQixnQkFBQTtFQUFrQix5QkFBQTtFQUEyQixpQkFBQTtBQWF4RztBQVhBO0VBQ0ksV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtFQUFtQixtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixzQkFBQTtFQUF1QixrQkFBQTtFQUFvQixXQUFBO0VBQWEsUUFBQTtFQUFVLGtCQUFBO0FBdUI3SjtBQXRCSTtFQUFLLFdBQUE7RUFBYSxZQUFBO0VBQWMsZ0JBQUE7QUEyQnBDO0FBekJBO0VBQWtCLFVBQUE7RUFBWSxTQUFBO0VBQVcsV0FBQTtFQUFhLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0Msa0JBQUE7QUFtQy9IO0FBbENBO0VBQWlCLGtCQUFBO0VBQW9CLFNBQUE7RUFBVyxXQUFBO0VBQWEsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxrQkFBQTtBQTRDdEk7QUExQ0E7RUFDSSxrQkFBQTtFQUFvQiwrQkFBQTtBQThDeEI7QUE3Q0k7RUFBSyxnQkFBQTtBQWdEVDtBQS9DSTtFQUFJLFVBQUE7RUFBWSxTQUFBO0VBQVcsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBdURsRjtBQXRESTtFQUFJLGtCQUFBO0VBQXFCLFNBQUE7RUFBVyxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7QUE4RDNGO0FBNURBO0VBQXdDLGVBQUE7QUFnRXhDO0FBL0RBO0VBQWEsNkJBQUE7RUFBOEIsZ0NBQUE7RUFBaUMscUJBQUE7QUFxRTVFO0FBcEVBO0VBQWMsbUJBQUE7RUFBcUIsU0FBQTtFQUFXLGdCQUFBO0VBQWtCLGtCQUFBO0FBMkVoRTtBQTFFQTtFQUFpQixxQkFBQTtFQUF1QixjQUFBO0FBK0V4QztBQTlFQTtFQUFtQixjQUFBO0VBQWdCLFVBQUE7RUFBWSxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0MscUJBQUE7RUFBdUIsZUFBQTtFQUFnQixrQkFBQTtBQTBGL0s7QUF6RkE7RUFBbUMsa0JBQUE7RUFBb0IsYUFBQTtFQUFjLFdBQUE7RUFBYSxtQkFBQTtFQUFxQixXQUFBO0VBQWEsV0FBQTtFQUFhLE9BQUE7QUFtR2pJO0FBbEdBO0VBQTZCLGNBQUE7QUFzRzdCO0FBckdBO0VBQ0kscUJBQUE7RUFBdUIsYUFBQTtFQUFlLFVBQUE7RUFBWSxtQkFBQTtFQUFxQixtQkFBQTtFQUFxQixhQUFBO0VBQWUsZ0JBQUE7RUFBa0Isa0JBQUE7QUErR2pJO0FBOUdJO0VBQVEsV0FBQTtFQUFhLFlBQUE7RUFBYyxpQkFBQTtBQW1IdkM7QUFsSEk7RUFBTyxrQkFBQTtFQUFvQixtQkFBQTtFQUFxQixjQUFBO0VBQWdCLFFBQUE7RUFBVSxVQUFBO0VBQVksZUFBQTtFQUFpQix3QkFBQTtFQUEwQixtQkFBQTtFQUFxQixXQUFBO0VBQWEsZUFBQTtFQUFpQixpQkFBQTtFQUFtQixpQkFBQTtBQWdJM007QUE5SEE7RUFDSSxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixlQUFBO0FBbUk3QztBQWxJSTtFQUFLLHNCQUFBO0FBcUlUO0FBcElJO0VBQU0sV0FBQTtFQUFhLGdCQUFBO0VBQWtCLGVBQUE7RUFBaUIsb0NBQUE7RUFBc0MsY0FBQTtFQUFnQix5QkFBQTtFQUEyQixtQkFBQTtBQTZJM0kiLCJmaWxlIjoicHJvdmlkZXJwcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9maWxlX2ltZ19zZWN0aW9ueyB3aWR0aDogMTQwcHg7IGhlaWdodDogMTQwcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBtYXJnaW46IDIzcHggYXV0bzsgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaW9uLWltZ3sgd2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgYm9yZGVyLXJhZGl1czo1MCU7IG92ZXJmbG93OiBoaWRkZW47IGJvcmRlcjogNnB4ICNlZWVlZWUgc29saWQ7IG9iamVjdC1maXQ6IGNvdmVyO31cclxufVxyXG4ucHJvZmlsZV91cGxvYWRfYnRue1xyXG4gICAgd2lkdGg6IDQycHg7IGhlaWdodDogNDJweDsgYm9yZGVyLXJhZGl1czo1MCU7IGJhY2tncm91bmQ6ICM0MWI1Nzg7IHRleHQtYWxpZ246IGNlbnRlcjsgYm9yZGVyOjVweCBzb2xpZCAjZmZmOyBwb3NpdGlvbjogYWJzb2x1dGU7IGJvdHRvbTogNnB4OyByaWdodDogMDsgcGFkZGluZzogOHB4IDAgMCAwO1xyXG4gICAgaW1neyB3aWR0aDogYXV0bzsgaGVpZ2h0OiBhdXRvOyBib3JkZXItcmFkaXVzOjA7IH1cclxufVxyXG4ucHJvZmlsZV9pbmZvIGg0eyBwYWRkaW5nOiAwOyBtYXJnaW46IDA7IGNvbG9yOiAjMDAwOyBmb250LXNpemU6IDIwcHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cclxuLnByb2ZpbGVfaW5mbyBweyBwYWRkaW5nOiA1cHggMCAwIDA7IG1hcmdpbjogMDsgY29sb3I6ICMwMDA7IGZvbnQtc2l6ZTogMTRweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IHRleHQtYWxpZ246IGNlbnRlcjsgfVxyXG5cclxuLnJldmlld2dyaWQgaW9uLXJvdyBpb24tY29seyBcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgYm9yZGVyLXJpZ2h0OiM0MWI1Nzggc29saWQgMXB4OyBcclxuICAgIGltZ3sgcGFkZGluZy10b3A6IDVweDsgfVxyXG4gICAgaDV7IHBhZGRpbmc6IDA7IG1hcmdpbjogMDsgY29sb3I6ICMwMDAwMDA7IGZvbnQtc2l6ZTogMTRweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IH1cclxuICAgIGg0eyBwYWRkaW5nOiA1cHggMCAwIDA7ICBtYXJnaW46IDA7IGNvbG9yOiAjNDFiNTc4OyBmb250LXNpemU6IDE1cHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbn1cclxuLnJldmlld2dyaWQgaW9uLXJvdyBpb24tY29sOmxhc3QtY2hpbGR7IGJvcmRlci1yaWdodDowOyB9XHJcbi5yZXZpZXdncmlkeyBib3JkZXItdG9wOiM0MWI1Nzggc29saWQgMXB4OyBib3JkZXItYm90dG9tOiM0MWI1Nzggc29saWQgMXB4OyBtYXJnaW46IDE5cHggMCAyN3B4IDA7IH1cclxuLnRhYnBhbmVsIHVseyBwYWRkaW5nOiAwIDAgMzVweCAwOyBtYXJnaW46IDA7IGxpc3Qtc3R5bGU6IG5vbmU7IHRleHQtYWxpZ246IGNlbnRlcjsgfVxyXG4udGFicGFuZWwgdWwgbGl7IGRpc3BsYXk6IGlubGluZS1ibG9jazsgbWFyZ2luOiAwIDE1cHg7IH1cclxuLnRhYnBhbmVsIHVsIGxpIGF7IGRpc3BsYXk6IGJsb2NrOyBwYWRkaW5nOiAwOyBjb2xvcjogIzY2NjY2NjsgZm9udC1zaXplOiAxOHB4OyBmb250LXdlaWdodDogNjAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgdGV4dC1kZWNvcmF0aW9uOiBub25lOyBwYWRkaW5nOjAgMTBweDsgcG9zaXRpb246IHJlbGF0aXZlOyB9XHJcbi50YWJwYW5lbCB1bCBsaS5hY3RpdmV0YWIgYTphZnRlcnsgcG9zaXRpb246IGFic29sdXRlOyBib3R0b206LTEwcHg7IHdpZHRoOiAxMDAlOyBiYWNrZ3JvdW5kOiAjNDFiNTc4OyBoZWlnaHQ6IDJweDsgY29udGVudDogXCJcIjsgbGVmdDogMDsgfVxyXG4udGFicGFuZWwgdWwgbGkuYWN0aXZldGFiIGF7IGNvbG9yOiM0MWI1Nzg7IH1cclxuLmdhbGxlcnlfYm94e1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB3aWR0aDogMzEuMzMlOyBtYXJnaW46IDElOyBib3JkZXItcmFkaXVzOiAxMHB4OyB2ZXJ0aWNhbC1hbGlnbjogdG9wOyBoZWlnaHQ6IDEwMHB4OyBvdmVyZmxvdzogaGlkZGVuOyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBpb24taW1ne3dpZHRoOiAxMDAlOyBoZWlnaHQ6IDEwMCU7IG9iamVjdC1maXQ6IGNvdmVyO31cclxuICAgIHN0cm9uZ3twb3NpdGlvbjogYWJzb2x1dGU7IGZvbnQtd2VpZ2h0OiBub3JtYWw7IGNvbG9yOiAjZmZmZmZmOyB0b3A6IDVweDsgcmlnaHQ6IDVweDsgYmFja2dyb3VuZDogcmVkOyBwYWRkaW5nOiAzcHggNnB4IDRweCA2cHg7IGJvcmRlci1yYWRpdXM6IDE1cHg7IHotaW5kZXg6IDk5OyBmb250LXNpemU6IDEzcHg7IGZvbnQtd2VpZ2h0OiBib2xkOyBsaW5lLWhlaWdodDogMTJweDt9XHJcbn1cclxuLmFkZGdhbGxlcnl7IFxyXG4gICAgYmFja2dyb3VuZDogIzQxYjU3ODsgdGV4dC1hbGlnbjogY2VudGVyOyBwYWRkaW5nOiAxOXB4IDA7XHJcbiAgICBpbWd7IHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7IH1cclxuICAgIHNwYW57IGNvbG9yOiAjZmZmOyBmb250LXdlaWdodDogNDAwOyBmb250LXNpemU6IDE0cHg7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyBkaXNwbGF5OiBibG9jazsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTsgcGFkZGluZzogMTBweCAwIDAgMDsgfVxyXG59XHJcbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerprofile_providerprofile_module_ts.js.map