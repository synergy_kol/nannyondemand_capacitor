"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_wrte-review_wrte-review_module_ts"],{

/***/ 57226:
/*!****************************************************************!*\
  !*** ./src/app/User/wrte-review/wrte-review-routing.module.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WrteReviewPageRoutingModule": () => (/* binding */ WrteReviewPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _wrte_review_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./wrte-review.page */ 15324);




const routes = [
    {
        path: '',
        component: _wrte_review_page__WEBPACK_IMPORTED_MODULE_0__.WrteReviewPage
    }
];
let WrteReviewPageRoutingModule = class WrteReviewPageRoutingModule {
};
WrteReviewPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], WrteReviewPageRoutingModule);



/***/ }),

/***/ 63024:
/*!********************************************************!*\
  !*** ./src/app/User/wrte-review/wrte-review.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WrteReviewPageModule": () => (/* binding */ WrteReviewPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _wrte_review_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./wrte-review-routing.module */ 57226);
/* harmony import */ var _wrte_review_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./wrte-review.page */ 15324);







//import { StarRatingModule } from 'ionic5-star-rating';
let WrteReviewPageModule = class WrteReviewPageModule {
};
WrteReviewPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _wrte_review_routing_module__WEBPACK_IMPORTED_MODULE_0__.WrteReviewPageRoutingModule,
            //StarRatingModule
        ],
        declarations: [_wrte_review_page__WEBPACK_IMPORTED_MODULE_1__.WrteReviewPage]
    })
], WrteReviewPageModule);



/***/ }),

/***/ 15324:
/*!******************************************************!*\
  !*** ./src/app/User/wrte-review/wrte-review.page.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WrteReviewPage": () => (/* binding */ WrteReviewPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_wrte_review_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./wrte-review.page.html */ 80317);
/* harmony import */ var _wrte_review_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./wrte-review.page.scss */ 86599);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);








let WrteReviewPage = class WrteReviewPage {
    constructor(navCtrl, loadingController, toastController, authService, activatedRoute, storage) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.authService = authService;
        this.activatedRoute = activatedRoute;
        this.storage = storage;
        this.isDisabled = false;
        this.isLoading = false;
        this.rating = 3;
        this.message = '';
        this.nanny_id = '';
        this.userdata = [];
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.loginToken();
    }
    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((res) => {
            this.nanny_id = res.nanny_id;
            console.log(this.nanny_id);
        });
    }
    logRatingChange(rating) {
        console.log("changed rating: ", rating);
        // do your stuff
    }
    onSubmit() {
        if (this.message.trim() == '') {
            this.presentToast('Please enter Message');
        }
        else {
            this.showLoader('Please wait...');
            this.storage.get('userDetails').then((result) => {
                this.data = result;
                var body = {
                    user_id: this.data.user_id,
                    nany_id: this.nanny_id,
                    rate: this.rating,
                    message: this.message
                };
                this.authService.postData("post-review", body).then(result => {
                    this.data = result;
                    console.log("post-review: ", this.data);
                    this.hideLoader();
                    if (this.data.status.error_code == 0) {
                        var object = {
                            nanny_id: this.nanny_id
                        };
                        this.navCtrl.navigateBack(["/nannydetails"], { queryParams: object });
                        this.presentToast(this.data.status.message);
                    }
                    else {
                        this.presentToast(this.data.status.message);
                    }
                }, error => {
                    this.hideLoader();
                });
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
WrteReviewPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthServiceProvider },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage }
];
WrteReviewPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-wrte-review',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_wrte_review_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_wrte_review_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], WrteReviewPage);



/***/ }),

/***/ 80317:
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/wrte-review/wrte-review.page.html ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"nannydetails\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Write Review</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"register_panel\">\n    <div style=\"padding: 0 0px 10px 10px\">\n      <h4 style=\"font-size: 14px;\">Rating for Nanny</h4>\n      <ionic5-star-rating [(ngModel)]=\"rating\" activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\" defaultColor=\"#c4c4c4\"\n        readonly=\"false\" fontSize=\"25px\" (ratingChanged)=\"logRatingChange($event)\">\n      </ionic5-star-rating>\n    </div>\n    <ion-textarea rows=\"4\" cols=\"20\" class=\"cust_input\" placeholder=\"Your Comment\" type=\"text\" [(ngModel)]=\"message\"\n      (keyup.enter)=\"onKeyboard()\" #Field1 style=\"height: 120px; padding-top: 10px !important;border-radius: 25px;\"></ion-textarea>\n      <ion-button color=\"warning\" expand=\"full\" shape=\"round\" (click)=\"onSubmit()\" class=\"cus_btn\">Submit</ion-button>\n    </div>\n</ion-content>");

/***/ }),

/***/ 86599:
/*!********************************************************!*\
  !*** ./src/app/User/wrte-review/wrte-review.page.scss ***!
  \********************************************************/
/***/ ((module) => {

module.exports = ".step {\n  background: #fff0e3;\n  padding: 8px 0;\n}\n.step ul {\n  padding: 0;\n  width: 300px;\n  margin: 0 auto;\n  margin-bottom: 5px;\n  list-style: none;\n  text-align: center;\n  position: relative;\n}\n.step ul li {\n  display: inline-block;\n  position: relative;\n  padding: 0 15%;\n  z-index: 999;\n}\n.step ul li a {\n  display: block;\n  width: 22px;\n  height: 22px;\n  background: #fff;\n  border: #f38320 solid 1px;\n  border-radius: 50%;\n  position: relative;\n}\n.step ul li span {\n  display: block;\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 7px 0 0 -5px;\n}\n.step ul li:first-child {\n  padding-left: 0;\n}\n.step ul li:last-child {\n  padding-right: 0;\n  text-align: right;\n}\n.step ul li.activestep a {\n  width: 30px;\n  height: 30px;\n  background: #f38320;\n  position: relative;\n  z-index: 999;\n  margin-top: 0;\n  margin-left: -3px;\n  top: 5px;\n}\n.step ul li.activestep a:after {\n  width: 13px;\n  height: 5px;\n  content: \"\";\n  position: absolute;\n  border-left: 3px #ffffff solid;\n  border-bottom: 3px #ffffff solid;\n  transform: rotate(-45deg);\n  left: 6px;\n  top: 8px;\n}\n.step ul:after {\n  background: #cccccc;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 18px;\n  width: 80%;\n  height: 2px;\n  content: \"\";\n  margin: 0 auto;\n  z-index: 0;\n}\n.register_panel {\n  padding: 20px;\n  padding-bottom: 0;\n}\n.sml_cus_btn {\n  box-shadow: #fdb77a 0 0 15px 5px;\n  margin: 20px;\n  border-radius: 30px;\n}\n.cust_input {\n  border-color: #f38320;\n  box-shadow: #fff0e3 0 0 6px 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndydGUtcmV2aWV3LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0VBQXFCLGNBQUE7QUFFekI7QUFESTtFQUNJLFVBQUE7RUFBWSxZQUFBO0VBQWMsY0FBQTtFQUFnQixrQkFBQTtFQUFvQixnQkFBQTtFQUFrQixrQkFBQTtFQUFvQixrQkFBQTtBQVM1RztBQVJRO0VBQ0kscUJBQUE7RUFBdUIsa0JBQUE7RUFBb0IsY0FBQTtFQUFnQixZQUFBO0FBYXZFO0FBWlk7RUFBRyxjQUFBO0VBQWdCLFdBQUE7RUFBYSxZQUFBO0VBQWMsZ0JBQUE7RUFBa0IseUJBQUE7RUFBMEIsa0JBQUE7RUFBbUIsa0JBQUE7QUFxQnpIO0FBcEJZO0VBQU0sY0FBQTtFQUFnQixjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0Msb0JBQUE7QUE0QjNIO0FBMUJRO0VBQWdCLGVBQUE7QUE2QnhCO0FBNUJRO0VBQWUsZ0JBQUE7RUFBa0IsaUJBQUE7QUFnQ3pDO0FBL0JRO0VBQWtCLFdBQUE7RUFBYSxZQUFBO0VBQWMsbUJBQUE7RUFBcUIsa0JBQUE7RUFBb0IsWUFBQTtFQUFjLGFBQUE7RUFBZSxpQkFBQTtFQUFtQixRQUFBO0FBeUM5STtBQXhDUTtFQUF1QixXQUFBO0VBQWEsV0FBQTtFQUFhLFdBQUE7RUFBYSxrQkFBQTtFQUFvQiw4QkFBQTtFQUFnQyxnQ0FBQTtFQUFrQyx5QkFBQTtFQUEyQixTQUFBO0VBQVcsUUFBQTtBQW1EbE07QUFoREk7RUFDSSxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixPQUFBO0VBQVMsUUFBQTtFQUFVLFNBQUE7RUFBVyxVQUFBO0VBQVksV0FBQTtFQUFhLFdBQUE7RUFBYSxjQUFBO0VBQWdCLFVBQUE7QUEyRHJJO0FBeERBO0VBQWlCLGFBQUE7RUFBZSxpQkFBQTtBQTZEaEM7QUE1REE7RUFBYyxnQ0FBQTtFQUFrQyxZQUFBO0VBQWMsbUJBQUE7QUFrRTlEO0FBakVBO0VBQWEscUJBQUE7RUFBdUIsK0JBQUE7QUFzRXBDIiwiZmlsZSI6IndydGUtcmV2aWV3LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zdGVweyBcclxuICAgIGJhY2tncm91bmQ6ICNmZmYwZTM7IHBhZGRpbmc6IDhweCAwO1xyXG4gICAgdWx7XHJcbiAgICAgICAgcGFkZGluZzogMDsgd2lkdGg6IDMwMHB4OyBtYXJnaW46IDAgYXV0bzsgbWFyZ2luLWJvdHRvbTogNXB4OyBsaXN0LXN0eWxlOiBub25lOyB0ZXh0LWFsaWduOiBjZW50ZXI7IHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBsaXsgXHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgcG9zaXRpb246IHJlbGF0aXZlOyBwYWRkaW5nOiAwIDE1JTsgei1pbmRleDogOTk5O1xyXG4gICAgICAgICAgICBheyBkaXNwbGF5OiBibG9jazsgd2lkdGg6IDIycHg7IGhlaWdodDogMjJweDsgYmFja2dyb3VuZDogI2ZmZjsgYm9yZGVyOiNmMzgzMjAgc29saWQgMXB4OyBib3JkZXItcmFkaXVzOjUwJTsgcG9zaXRpb246IHJlbGF0aXZlOyB9ICBcclxuICAgICAgICAgICAgc3BhbnsgZGlzcGxheTogYmxvY2s7IGNvbG9yOiAjNjY2NjY2OyBmb250LXNpemU6IDEzcHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjsgbWFyZ2luOiA3cHggMCAwIC01cHg7IH1cclxuICAgICAgICB9XHJcbiAgICAgICAgbGk6Zmlyc3QtY2hpbGR7IHBhZGRpbmctbGVmdDogMDsgfVxyXG4gICAgICAgIGxpOmxhc3QtY2hpbGR7IHBhZGRpbmctcmlnaHQ6IDA7IHRleHQtYWxpZ246IHJpZ2h0OyB9XHJcbiAgICAgICAgbGkuYWN0aXZlc3RlcCBhIHsgd2lkdGg6IDMwcHg7IGhlaWdodDogMzBweDsgYmFja2dyb3VuZDogI2YzODMyMDsgcG9zaXRpb246IHJlbGF0aXZlOyB6LWluZGV4OiA5OTk7IG1hcmdpbi10b3A6IDA7IG1hcmdpbi1sZWZ0OiAtM3B4OyB0b3A6IDVweDsgfVxyXG4gICAgICAgIGxpLmFjdGl2ZXN0ZXAgYTphZnRlcnsgd2lkdGg6IDEzcHg7IGhlaWdodDogNXB4OyBjb250ZW50OiBcIlwiOyBwb3NpdGlvbjogYWJzb2x1dGU7IGJvcmRlci1sZWZ0OiAzcHggI2ZmZmZmZiBzb2xpZDsgYm9yZGVyLWJvdHRvbTogM3B4ICNmZmZmZmYgc29saWQ7IHRyYW5zZm9ybTogcm90YXRlKC00NWRlZyk7IGxlZnQ6IDZweDsgdG9wOiA4cHg7IH1cclxuICAgIFxyXG4gICAgfVxyXG4gICAgdWw6YWZ0ZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNjY2NjY2M7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgbGVmdDogMDsgcmlnaHQ6IDA7IHRvcDogMThweDsgd2lkdGg6IDgwJTsgaGVpZ2h0OiAycHg7IGNvbnRlbnQ6IFwiXCI7IG1hcmdpbjogMCBhdXRvOyB6LWluZGV4OiAwO1xyXG4gICAgfVxyXG59XHJcbi5yZWdpc3Rlcl9wYW5lbHsgcGFkZGluZzogMjBweDsgcGFkZGluZy1ib3R0b206IDA7IH1cclxuLnNtbF9jdXNfYnRueyBib3gtc2hhZG93OiAjZmRiNzdhIDAgMCAxNXB4IDVweDsgbWFyZ2luOiAyMHB4OyBib3JkZXItcmFkaXVzOiAzMHB4OyB9XHJcbi5jdXN0X2lucHV0eyBib3JkZXItY29sb3I6ICNmMzgzMjA7IGJveC1zaGFkb3c6ICNmZmYwZTMgMCAwIDZweCA0cHg7fVxyXG4iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_wrte-review_wrte-review_module_ts.js.map