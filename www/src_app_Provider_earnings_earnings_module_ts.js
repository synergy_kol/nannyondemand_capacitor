"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_earnings_earnings_module_ts"],{

/***/ 62943:
/*!**************************************************************!*\
  !*** ./src/app/Provider/earnings/earnings-routing.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EarningsPageRoutingModule": () => (/* binding */ EarningsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _earnings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./earnings.page */ 38243);




const routes = [
    {
        path: '',
        component: _earnings_page__WEBPACK_IMPORTED_MODULE_0__.EarningsPage
    }
];
let EarningsPageRoutingModule = class EarningsPageRoutingModule {
};
EarningsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], EarningsPageRoutingModule);



/***/ }),

/***/ 4395:
/*!******************************************************!*\
  !*** ./src/app/Provider/earnings/earnings.module.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EarningsPageModule": () => (/* binding */ EarningsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _earnings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./earnings-routing.module */ 62943);
/* harmony import */ var _earnings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./earnings.page */ 38243);







let EarningsPageModule = class EarningsPageModule {
};
EarningsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _earnings_routing_module__WEBPACK_IMPORTED_MODULE_0__.EarningsPageRoutingModule
        ],
        declarations: [_earnings_page__WEBPACK_IMPORTED_MODULE_1__.EarningsPage]
    })
], EarningsPageModule);



/***/ }),

/***/ 38243:
/*!****************************************************!*\
  !*** ./src/app/Provider/earnings/earnings.page.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EarningsPage": () => (/* binding */ EarningsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_earnings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./earnings.page.html */ 36944);
/* harmony import */ var _earnings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./earnings.page.scss */ 20674);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let EarningsPage = class EarningsPage {
    constructor(authService, loadingController, storage, navCtrl) {
        this.authService = authService;
        this.loadingController = loadingController;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.isLoading = false;
        this.userdata = [];
        this.data = [];
        this.earningData = [];
        this.nofound = false;
        this.totalearning = '';
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-my-earning", body).then(result => {
                this.data = result;
                console.log("Earnings: ", this.data);
                this.totalearning = this.data.result.data.total_earning;
                this.earningData = this.data.result.data.list;
                if (this.earningData == null) {
                    this.nofound = true;
                }
                else {
                    this.nofound = false;
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
EarningsPage.ctorParameters = () => [
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController }
];
EarningsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-earnings',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_earnings_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_earnings_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], EarningsPage);



/***/ }),

/***/ 36944:
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/earnings/earnings.page.html ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>My Earnings</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"nofound\" style=\"padding:50px 20px; color: #bfbfbf; font-weight: bold; font-size: 22px; text-align: center;\">[ Sorry! Nothing Found. ]</div>\n  <div *ngIf=\"!nofound\" class=\"total_top\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>Total Earning</ion-col>\n        <ion-col class=\"total_top_right\">${{totalearning}}</ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <div style=\"padding: 0 20px;\">\n    <div class=\"earning_box\" *ngFor=\"let item of earningData\">\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <span>#{{item.booking_id}}</span>\n            <h3>{{item.parent_parent}} {{item.parent_lname}}</h3>\n            <i>{{item.date}}</i>\n          </ion-col>\n          <ion-col>\n            <div class=\"eraning_r\">\n              <h4>${{item.amount}}</h4>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>    \n  </div>\n</ion-content>");

/***/ }),

/***/ 20674:
/*!******************************************************!*\
  !*** ./src/app/Provider/earnings/earnings.page.scss ***!
  \******************************************************/
/***/ ((module) => {

module.exports = ".total_top {\n  background: #e7fef2;\n  padding: 8px 5px;\n  color: #222222;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 18px;\n}\n\n.total_top_right {\n  text-align: right;\n}\n\n.earning_box {\n  background: #ffffff;\n  box-shadow: #dddddd 0 0 10px;\n  border-radius: 8px;\n  margin: 20px 0;\n  padding: 5px;\n}\n\n.earning_box span {\n  padding: 0;\n  margin: 0;\n  color: #999999;\n  font-size: 14px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.earning_box i {\n  padding: 0;\n  margin: 0;\n  color: #999999;\n  font-size: 14px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.earning_box h3 {\n  padding: 3px 0;\n  margin: 0;\n  font-size: 18px;\n  color: #000;\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: 700;\n}\n\n.eraning_r {\n  text-align: right;\n}\n\n.eraning_r h4 {\n  margin: 18px 0;\n  color: #41b578;\n  font-size: 22px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVhcm5pbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFZLG1CQUFBO0VBQW9CLGdCQUFBO0VBQWtCLGNBQUE7RUFBZ0IsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0MsZUFBQTtBQU8xSDs7QUFOQTtFQUFrQixpQkFBQTtBQVVsQjs7QUFUQTtFQUNJLG1CQUFBO0VBQXFCLDRCQUFBO0VBQThCLGtCQUFBO0VBQW9CLGNBQUE7RUFBZ0IsWUFBQTtBQWdCM0Y7O0FBZkk7RUFBTSxVQUFBO0VBQVksU0FBQTtFQUFXLGNBQUE7RUFBZSxlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBdUJuRjs7QUF0Qkk7RUFBRyxVQUFBO0VBQVksU0FBQTtFQUFXLGNBQUE7RUFBZSxlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBOEJoRjs7QUE3Qkk7RUFBSSxjQUFBO0VBQWUsU0FBQTtFQUFXLGVBQUE7RUFBZ0IsV0FBQTtFQUFZLG9DQUFBO0VBQXNDLGdCQUFBO0FBcUNwRzs7QUFuQ0E7RUFBWSxpQkFBQTtBQXVDWjs7QUF0Q0k7RUFBSSxjQUFBO0VBQWdCLGNBQUE7RUFBZ0IsZUFBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtBQTZDMUUiLCJmaWxlIjoiZWFybmluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvdGFsX3RvcHsgYmFja2dyb3VuZDojZTdmZWYyOyBwYWRkaW5nOiA4cHggNXB4OyBjb2xvcjogIzIyMjIyMjsgZm9udC13ZWlnaHQ6IDgwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMThweDsgfVxyXG4udG90YWxfdG9wX3JpZ2h0eyB0ZXh0LWFsaWduOiByaWdodDsgfVxyXG4uZWFybmluZ19ib3h7IFxyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjsgYm94LXNoYWRvdzogI2RkZGRkZCAwIDAgMTBweDsgYm9yZGVyLXJhZGl1czogOHB4OyBtYXJnaW46IDIwcHggMDsgcGFkZGluZzogNXB4O1xyXG4gICAgc3BhbnsgcGFkZGluZzogMDsgbWFyZ2luOiAwOyBjb2xvcjojOTk5OTk5OyBmb250LXNpemU6IDE0cHg7IGZvbnQtd2VpZ2h0OiA2MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICBpeyBwYWRkaW5nOiAwOyBtYXJnaW46IDA7IGNvbG9yOiM5OTk5OTk7IGZvbnQtc2l6ZTogMTRweDsgZm9udC13ZWlnaHQ6IDYwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IH1cclxuICAgIGgzeyBwYWRkaW5nOjNweCAwOyBtYXJnaW46IDA7IGZvbnQtc2l6ZToxOHB4OyBjb2xvcjojMDAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgZm9udC13ZWlnaHQ6IDcwMDsgfVxyXG59XHJcbi5lcmFuaW5nX3J7IHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgaDR7IG1hcmdpbjogMThweCAwOyBjb2xvcjogIzQxYjU3ODsgZm9udC1zaXplOjIycHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbn1cclxuIl19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_earnings_earnings_module_ts.js.map