"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_nannydetails_nannydetails_module_ts"],{

/***/ 50824:
/*!******************************************************************!*\
  !*** ./src/app/User/nannydetails/nannydetails-routing.module.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NannydetailsPageRoutingModule": () => (/* binding */ NannydetailsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _nannydetails_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./nannydetails.page */ 70911);




const routes = [
    {
        path: '',
        component: _nannydetails_page__WEBPACK_IMPORTED_MODULE_0__.NannydetailsPage
    }
];
let NannydetailsPageRoutingModule = class NannydetailsPageRoutingModule {
};
NannydetailsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], NannydetailsPageRoutingModule);



/***/ }),

/***/ 43909:
/*!**********************************************************!*\
  !*** ./src/app/User/nannydetails/nannydetails.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NannydetailsPageModule": () => (/* binding */ NannydetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _nannydetails_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./nannydetails-routing.module */ 50824);
/* harmony import */ var _nannydetails_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./nannydetails.page */ 70911);







//import { StarRatingModule } from 'ionic5-star-rating';
//import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
let NannydetailsPageModule = class NannydetailsPageModule {
};
NannydetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _nannydetails_routing_module__WEBPACK_IMPORTED_MODULE_0__.NannydetailsPageRoutingModule,
            // StarRatingModule,
            // NgxIonicImageViewerModule
        ],
        declarations: [_nannydetails_page__WEBPACK_IMPORTED_MODULE_1__.NannydetailsPage]
    })
], NannydetailsPageModule);



/***/ }),

/***/ 70911:
/*!********************************************************!*\
  !*** ./src/app/User/nannydetails/nannydetails.page.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NannydetailsPage": () => (/* binding */ NannydetailsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_nannydetails_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./nannydetails.page.html */ 47263);
/* harmony import */ var _nannydetails_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./nannydetails.page.scss */ 18778);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 17897);








let NannydetailsPage = class NannydetailsPage {
    constructor(storage, navCtrl, activatedRoute, authService, toastController, loadingController) {
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.activatedRoute = activatedRoute;
        this.authService = authService;
        this.toastController = toastController;
        this.loadingController = loadingController;
        this.hidepopup = false;
        this.nanny_id = '';
        this.isLoading = false;
        this.isDisabled = false;
        this.nannyData = [];
        this.nannyGalleryData = [];
        this.nannyReviewData = [];
        this.profile_image = '';
        this.username = '';
        this.email = '';
        this.phone = '';
        this.userAge = '';
        this.address = '';
        this.state = '';
        this.experience = '';
        this.rate = '';
        this.rate_type = '';
        this.city = '';
        this.zip = '';
        this.avg_rate = '';
        this.showabout = false;
        this.abactive = "";
        this.showgallery = true;
        this.glactive = "activetab";
        this.showreview = false;
        this.rvactive = "";
        this.availabilities = [];
        this.userdata = [];
        this.activatedRoute.queryParams.subscribe((res) => {
            this.nanny_id = res.nanny_id;
            console.log("nanny id: ", this.nanny_id);
        });
    }
    ngOnInit() { }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.nannyDetails();
        this.loginToken();
    }
    nannyDetails() {
        this.storage.get('userDetails').then((val) => {
            var body1 = {
                user_id: val.user_id,
                nany_id: this.nanny_id
            };
            this.authService.postData("get-nany-list", body1).then(result => {
                this.data = result;
                this.nannyData = this.data.result.data;
                this.profile_image = this.nannyData[0].profile_image;
                this.username = this.nannyData[0].fname + ' ' + this.nannyData[0].lname;
                this.email = this.nannyData[0].email;
                this.phone = this.nannyData[0].phone;
                this.userAge = this.splitAge(this.nannyData[0].age);
                this.address = this.nannyData[0].address;
                this.experience = this.nannyData[0].experience;
                this.avg_rate = this.nannyData[0].avg_rate;
                this.rate = this.nannyData[0].rate;
                this.rate_type = this.nannyData[0].rate_type;
                this.state = this.nannyData[0].state;
                this.city = this.nannyData[0].city;
                this.zip = this.nannyData[0].zip;
                this.availabilities = this.nannyData[0].availabilities;
                console.log("Nanny Details: ", this.data);
                var body2 = {
                    user_id: this.nanny_id
                };
                this.authService.postData("get-photo-gallery-images", body2).then(result => {
                    this.data = result;
                    this.nannyGalleryData = this.data.result.data;
                    console.log("nannyGalleryData: ", this.nannyGalleryData);
                    var body3 = {
                        user_id: val.user_id,
                        nany_id: this.nanny_id
                    };
                    this.authService.postData("get-review", body3).then(result => {
                        this.data = result;
                        this.nannyReviewData = this.data.result.data;
                        console.log("nannyReviewData: ", this.nannyReviewData);
                        this.hideLoader();
                    }, error => {
                        this.hideLoader();
                    });
                }, error => {
                    this.hideLoader();
                });
            }, error => {
                this.hideLoader();
            });
        });
    }
    about() {
        this.showabout = true;
        this.showgallery = false;
        this.showreview = false;
        this.abactive = "activetab";
        this.glactive = "";
        this.rvactive = "";
    }
    gallery() {
        this.showabout = false;
        this.showgallery = true;
        this.showreview = false;
        this.abactive = "";
        this.glactive = "activetab";
        this.rvactive = "";
    }
    review() {
        this.showabout = false;
        this.showgallery = false;
        this.showreview = true;
        this.abactive = "";
        this.glactive = "";
        this.rvactive = "activetab";
    }
    splitAge(age) {
        var ageArr = age.split(' ');
        return ageArr[0];
    }
    onCheck() {
        var object = {
            nanny_id: this.nanny_id,
            rate_type: this.rate_type,
            rate: this.rate,
            availabilities: JSON.stringify(this.availabilities)
        };
        this.navCtrl.navigateForward(["/calender-view"], { queryParams: object });
    }
    writeReview() {
        var object = {
            nanny_id: this.nanny_id
        };
        this.navCtrl.navigateForward(["/wrte-review"], { queryParams: object });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
NannydetailsPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
NannydetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-nannydetails',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_nannydetails_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_nannydetails_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], NannydetailsPage);



/***/ }),

/***/ 47263:
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/nannydetails/nannydetails.page.html ***!
  \*************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Nanny Details</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"profile_img_section\">\n    <img [src]=\"profile_image != '' ? profile_image : 'assets/images/avatar.png'\" alt=\"profile_img\" />\n\n    <!-- <div class=\"profile_upload_btn\"><img src=\"assets/images/white_camera.png\" alt=\"white_camera\" /></div> -->\n  </div>\n  <div class=\"profile_info\">\n    <h4>{{username}}, {{userAge}}</h4>\n    <p><img src=\"assets/images/location_icon.png\" alt=\"location_icon\" />&nbsp;{{address}}, {{city}}, {{state}}, {{zip}}\n    </p>\n\n    <div class=\"review_panel\">\n      <ion-grid class=\"reviewgrid\">\n        <ion-row>\n          <ion-col>\n            <h5>Review</h5>\n            <div style=\"display: flex; align-items: baseline;\">\n              <ionic5-star-rating #rating activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\"\n                defaultColor=\"#c4c4c4\" readonly=\"true\" rating=\"{{avg_rate}}\" fontSize=\"15px\">\n              </ionic5-star-rating>\n              <p style=\"margin: 0; margin-left: 10px;\">({{nannyReviewData.length}})</p>\n            </div>\n          </ion-col>\n          <ion-col>\n            <h5>Experience</h5>\n            <h4>{{experience}} years</h4>\n          </ion-col>\n          <ion-col>\n            <h5>Rate</h5>\n            <h4>${{rate}}/<span *ngIf=\"rate_type == 'Daily'\">dly</span><span *ngIf=\"rate_type == 'Hourly'\">hr</span></h4>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n\n    </div>\n\n    <div class=\"tabpanel\">\n      <ul>\n        <li (click)=\"about()\" [ngClass]=\"abactive\"><a>About</a></li>\n        <li (click)=\"gallery()\" [ngClass]=\"glactive\"><a>Gallery</a></li>\n        <li (click)=\"review()\" [ngClass]=\"rvactive\"><a>Review</a></li>\n      </ul>\n      <div style=\"padding: 0 30px; margin-bottom: 10px;\" *ngIf=\"showabout\">\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Full Name:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{username}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Email:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{email}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Phone:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{phone}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Address:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{address}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">State:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{state}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">City:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{city}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">Zip:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{zip}}</p>\n        <p style=\"text-align: left; font-size: 14px; color: #999999;\">About Note:</p>\n        <p style=\"text-align: left; font-weight: bold; font-size: 16px; color: #666666;\">{{nannyData[0].about_note}}</p>\n      </div>\n      <div style=\"padding: 0 10px; margin-bottom: 10px;\" *ngIf=\"showgallery\">\n        <p *ngIf=\"nannyGalleryData.length == 0\" style=\"font-weight: bold; font-size: 18px; color: #999999;\">No Image\n          Found!</p>\n        <div class=\"gallery_box\" *ngFor=\"let data of nannyGalleryData\">\n          <ion-img ionImgViewer scheme=\"dark\" [src]=\"data.image\"></ion-img>\n        </div>\n      </div>\n      <div style=\"padding: 0 30px; margin-bottom: 10px;\" *ngIf=\"showreview\">\n        <p *ngIf=\"nannyReviewData.length == 0\" style=\"font-weight: bold; font-size: 18px; color: #999999;\">No Review Found!</p>\n        <ion-button *ngIf=\"nannyData[0].is_booked == 1\" (click)=\"writeReview()\" style=\"margin-top: 10px;\" class=\"more_btn\">Write a Review</ion-button>\n        <ion-item button style=\"--padding-start: 0; --padding-end: 0;\" *ngFor=\"let data of nannyReviewData\" (click)=\"testClick()\">\n          <ion-avatar slot=\"start\">\n            <img [src]=\"data.parent_profile_image != '' ? data.parent_profile_image : 'assets/images/avatar.png'\">\n          </ion-avatar>\n          <ion-label>\n            <h2><strong>{{data.parent_fname}} {{data.parent_lname}}</strong></h2>\n            <ionic5-star-rating activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\"\n                defaultColor=\"#c4c4c4\" readonly=\"true\" rating=\"{{data.rate}}\" fontSize=\"15px\">\n              </ionic5-star-rating>\n            <p style=\"text-align: left;\">{{data.message}}</p>\n          </ion-label>\n        </ion-item>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer (click)=\"onCheck()\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-button color=\"light\" fill=\"clear\" expand=\"full\"\n      style=\"font-weight: bold; font-family: 'Open Sans', sans-serif; font-weight: 600; text-transform: uppercase; font-size: 14px;\">\n      Check Availability</ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 18778:
/*!**********************************************************!*\
  !*** ./src/app/User/nannydetails/nannydetails.page.scss ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = ".profile_img_section {\n  width: 140px;\n  height: 140px;\n  border-radius: 50%;\n  margin: 23px auto;\n  position: relative;\n}\n.profile_img_section img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n.profile_upload_btn {\n  width: 42px;\n  height: 42px;\n  border-radius: 50%;\n  background: #f38320;\n  text-align: center;\n  border: 5px solid #fff;\n  position: absolute;\n  bottom: 0;\n  right: 0;\n  padding: 8px 0 0 0;\n}\n.profile_upload_btn img {\n  width: auto;\n  height: auto;\n  border-radius: 0;\n}\n.profile_info h4 {\n  padding: 0;\n  margin: 0;\n  color: #000;\n  font-size: 20px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.profile_info p {\n  padding: 5px 0 0 0;\n  margin: 0;\n  color: #000;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.reviewgrid ion-row ion-col {\n  text-align: center;\n  border-right: #f38320 solid 1px;\n}\n.reviewgrid ion-row ion-col img {\n  padding-top: 5px;\n}\n.reviewgrid ion-row ion-col h5 {\n  padding: 0;\n  margin: 0;\n  color: #000000;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n.reviewgrid ion-row ion-col h4 {\n  padding: 5px 0 0 0;\n  margin: 0;\n  color: #f38320;\n  font-size: 15px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.reviewgrid ion-row ion-col:last-child {\n  border-right: 0;\n}\n.reviewgrid {\n  border-top: #f38320 solid 1px;\n  border-bottom: #f38320 solid 1px;\n  margin: 19px 0 27px 0;\n}\n.tabpanel ul {\n  padding: 0 0 35px 0;\n  margin: 0;\n  list-style: none;\n  text-align: center;\n}\n.tabpanel ul li {\n  display: inline-block;\n  margin: 0 15px;\n}\n.tabpanel ul li a {\n  display: block;\n  padding: 0;\n  color: #666666;\n  font-size: 18px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n  text-decoration: none;\n  padding: 0 10px;\n  position: relative;\n}\n.tabpanel ul li.activetab a:after {\n  position: absolute;\n  bottom: -10px;\n  width: 100%;\n  background: #f38320;\n  height: 2px;\n  content: \"\";\n  left: 0;\n}\n.tabpanel ul li.activetab a {\n  color: #f38320;\n}\n.gallery_box {\n  display: inline-block;\n  width: 31.33%;\n  margin: 1%;\n  border-radius: 10px;\n  vertical-align: top;\n  height: 100px;\n  overflow: hidden;\n}\n.gallery_box ion-img {\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n}\n.addgallery {\n  background: #f38320;\n  text-align: center;\n  padding: 19px 0;\n}\n.addgallery img {\n  width: auto !important;\n}\n.addgallery span {\n  color: #fff;\n  font-weight: 400;\n  font-size: 14px;\n  font-family: \"Open Sans\", sans-serif;\n  display: block;\n  text-transform: uppercase;\n  padding: 10px 0 0 0;\n}\n.cus_btn {\n  margin: 0;\n}\n.cust_input {\n  padding-left: 15px !important;\n  padding-right: 15px !important;\n}\n.more_btn {\n  letter-spacing: 0;\n  padding: 0 5px;\n  border-radius: 8px;\n  color: #f38320 !important;\n  font-size: 12px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n  --background: #fff;\n  text-transform: capitalize;\n  border: #f38320 solid 2px;\n  width: 100%;\n  height: 30px;\n  --box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hbm55ZGV0YWlscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBc0IsWUFBQTtFQUFjLGFBQUE7RUFBZSxrQkFBQTtFQUFtQixpQkFBQTtFQUFtQixrQkFBQTtBQU16RjtBQUxJO0VBQUssV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtBQVVwQztBQVJBO0VBQ0ksV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtFQUFtQixtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixzQkFBQTtFQUF1QixrQkFBQTtFQUFvQixTQUFBO0VBQVcsUUFBQTtFQUFVLGtCQUFBO0FBb0IzSjtBQW5CSTtFQUFLLFdBQUE7RUFBYSxZQUFBO0VBQWMsZ0JBQUE7QUF3QnBDO0FBdEJBO0VBQWtCLFVBQUE7RUFBWSxTQUFBO0VBQVcsV0FBQTtFQUFhLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0Msa0JBQUE7QUFnQy9IO0FBL0JBO0VBQWlCLGtCQUFBO0VBQW9CLFNBQUE7RUFBVyxXQUFBO0VBQWEsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxrQkFBQTtBQXlDdEk7QUF2Q0E7RUFDSSxrQkFBQTtFQUFvQiwrQkFBQTtBQTJDeEI7QUExQ0k7RUFBSyxnQkFBQTtBQTZDVDtBQTVDSTtFQUFJLFVBQUE7RUFBWSxTQUFBO0VBQVcsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBb0RsRjtBQW5ESTtFQUFJLGtCQUFBO0VBQXFCLFNBQUE7RUFBVyxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7QUEyRDNGO0FBekRBO0VBQXdDLGVBQUE7QUE2RHhDO0FBNURBO0VBQWEsNkJBQUE7RUFBOEIsZ0NBQUE7RUFBaUMscUJBQUE7QUFrRTVFO0FBakVBO0VBQWMsbUJBQUE7RUFBcUIsU0FBQTtFQUFXLGdCQUFBO0VBQWtCLGtCQUFBO0FBd0VoRTtBQXZFQTtFQUFpQixxQkFBQTtFQUF1QixjQUFBO0FBNEV4QztBQTNFQTtFQUFtQixjQUFBO0VBQWdCLFVBQUE7RUFBWSxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0MscUJBQUE7RUFBdUIsZUFBQTtFQUFnQixrQkFBQTtBQXVGL0s7QUF0RkE7RUFBbUMsa0JBQUE7RUFBb0IsYUFBQTtFQUFjLFdBQUE7RUFBYSxtQkFBQTtFQUFxQixXQUFBO0VBQWEsV0FBQTtFQUFhLE9BQUE7QUFnR2pJO0FBL0ZBO0VBQTZCLGNBQUE7QUFtRzdCO0FBbEdBO0VBQ0kscUJBQUE7RUFBdUIsYUFBQTtFQUFlLFVBQUE7RUFBWSxtQkFBQTtFQUFxQixtQkFBQTtFQUFxQixhQUFBO0VBQWUsZ0JBQUE7QUEyRy9HO0FBMUdJO0VBQVEsV0FBQTtFQUFhLFlBQUE7RUFBYyxpQkFBQTtBQStHdkM7QUE3R0E7RUFDSSxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixlQUFBO0FBa0g3QztBQWpISTtFQUFLLHNCQUFBO0FBb0hUO0FBbkhJO0VBQU0sV0FBQTtFQUFhLGdCQUFBO0VBQWtCLGVBQUE7RUFBaUIsb0NBQUE7RUFBc0MsY0FBQTtFQUFnQix5QkFBQTtFQUEyQixtQkFBQTtBQTRIM0k7QUExSEE7RUFBUyxTQUFBO0FBOEhUO0FBN0hBO0VBQ0ksNkJBQUE7RUFBK0IsOEJBQUE7QUFpSW5DO0FBL0hBO0VBQVcsaUJBQUE7RUFBbUIsY0FBQTtFQUFnQixrQkFBQTtFQUFvQix5QkFBQTtFQUEyQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGtCQUFBO0VBQW9CLDBCQUFBO0VBQTRCLHlCQUFBO0VBQTJCLFdBQUE7RUFBYSxZQUFBO0VBQWMsa0JBQUE7QUErSTVRIiwiZmlsZSI6Im5hbm55ZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJvZmlsZV9pbWdfc2VjdGlvbnsgd2lkdGg6IDE0MHB4OyBoZWlnaHQ6IDE0MHB4OyBib3JkZXItcmFkaXVzOjUwJTsgbWFyZ2luOiAyM3B4IGF1dG87IHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGltZ3sgd2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgYm9yZGVyLXJhZGl1czo1MCU7IH1cclxufVxyXG4ucHJvZmlsZV91cGxvYWRfYnRue1xyXG4gICAgd2lkdGg6IDQycHg7IGhlaWdodDogNDJweDsgYm9yZGVyLXJhZGl1czo1MCU7IGJhY2tncm91bmQ6ICNmMzgzMjA7IHRleHQtYWxpZ246IGNlbnRlcjsgYm9yZGVyOjVweCBzb2xpZCAjZmZmOyBwb3NpdGlvbjogYWJzb2x1dGU7IGJvdHRvbTogMDsgcmlnaHQ6IDA7IHBhZGRpbmc6IDhweCAwIDAgMDtcclxuICAgIGltZ3sgd2lkdGg6IGF1dG87IGhlaWdodDogYXV0bzsgYm9yZGVyLXJhZGl1czowOyB9XHJcbn1cclxuLnByb2ZpbGVfaW5mbyBoNHsgcGFkZGluZzogMDsgbWFyZ2luOiAwOyBjb2xvcjogIzAwMDsgZm9udC1zaXplOiAyMHB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgdGV4dC1hbGlnbjogY2VudGVyOyB9XHJcbi5wcm9maWxlX2luZm8gcHsgcGFkZGluZzogNXB4IDAgMCAwOyBtYXJnaW46IDA7IGNvbG9yOiAjMDAwOyBmb250LXNpemU6IDE0cHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cclxuXHJcbi5yZXZpZXdncmlkIGlvbi1yb3cgaW9uLWNvbHsgXHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IGJvcmRlci1yaWdodDojZjM4MzIwIHNvbGlkIDFweDsgXHJcbiAgICBpbWd7IHBhZGRpbmctdG9wOiA1cHg7IH1cclxuICAgIGg1eyBwYWRkaW5nOiAwOyBtYXJnaW46IDA7IGNvbG9yOiAjMDAwMDAwOyBmb250LXNpemU6IDE0cHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICBoNHsgcGFkZGluZzogNXB4IDAgMCAwOyAgbWFyZ2luOiAwOyBjb2xvcjogI2YzODMyMDsgZm9udC1zaXplOiAxNXB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG59XHJcbi5yZXZpZXdncmlkIGlvbi1yb3cgaW9uLWNvbDpsYXN0LWNoaWxkeyBib3JkZXItcmlnaHQ6MDsgfVxyXG4ucmV2aWV3Z3JpZHsgYm9yZGVyLXRvcDojZjM4MzIwIHNvbGlkIDFweDsgYm9yZGVyLWJvdHRvbTojZjM4MzIwIHNvbGlkIDFweDsgbWFyZ2luOiAxOXB4IDAgMjdweCAwOyB9XHJcbi50YWJwYW5lbCB1bHsgcGFkZGluZzogMCAwIDM1cHggMDsgbWFyZ2luOiAwOyBsaXN0LXN0eWxlOiBub25lOyB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cclxuLnRhYnBhbmVsIHVsIGxpeyBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IG1hcmdpbjogMCAxNXB4OyB9XHJcbi50YWJwYW5lbCB1bCBsaSBheyBkaXNwbGF5OiBibG9jazsgcGFkZGluZzogMDsgY29sb3I6ICM2NjY2NjY7IGZvbnQtc2l6ZTogMThweDsgZm9udC13ZWlnaHQ6IDYwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IHRleHQtZGVjb3JhdGlvbjogbm9uZTsgcGFkZGluZzowIDEwcHg7IHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxyXG4udGFicGFuZWwgdWwgbGkuYWN0aXZldGFiIGE6YWZ0ZXJ7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgYm90dG9tOi0xMHB4OyB3aWR0aDogMTAwJTsgYmFja2dyb3VuZDogI2YzODMyMDsgaGVpZ2h0OiAycHg7IGNvbnRlbnQ6IFwiXCI7IGxlZnQ6IDA7IH1cclxuLnRhYnBhbmVsIHVsIGxpLmFjdGl2ZXRhYiBheyBjb2xvcjojZjM4MzIwOyB9XHJcbi5nYWxsZXJ5X2JveHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jazsgd2lkdGg6IDMxLjMzJTsgbWFyZ2luOiAxJTsgYm9yZGVyLXJhZGl1czogMTBweDsgdmVydGljYWwtYWxpZ246IHRvcDsgaGVpZ2h0OiAxMDBweDsgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGlvbi1pbWd7d2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgb2JqZWN0LWZpdDogY292ZXI7fVxyXG59XHJcbi5hZGRnYWxsZXJ5eyBcclxuICAgIGJhY2tncm91bmQ6ICNmMzgzMjA7IHRleHQtYWxpZ246IGNlbnRlcjsgcGFkZGluZzogMTlweCAwO1xyXG4gICAgaW1neyB3aWR0aDogYXV0byAhaW1wb3J0YW50OyB9XHJcbiAgICBzcGFueyBjb2xvcjogI2ZmZjsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1zaXplOiAxNHB4OyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgZGlzcGxheTogYmxvY2s7IHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7IHBhZGRpbmc6IDEwcHggMCAwIDA7IH1cclxufVxyXG4uY3VzX2J0bnttYXJnaW46IDA7fVxyXG4uY3VzdF9pbnB1dCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHggIWltcG9ydGFudDsgcGFkZGluZy1yaWdodDogMTVweCAhaW1wb3J0YW50O1xyXG59XHJcbi5tb3JlX2J0bnsgbGV0dGVyLXNwYWNpbmc6IDA7IHBhZGRpbmc6IDAgNXB4OyBib3JkZXItcmFkaXVzOiA4cHg7IGNvbG9yOiAjZjM4MzIwICFpbXBvcnRhbnQ7IGZvbnQtc2l6ZTogMTJweDsgZm9udC13ZWlnaHQ6IDYwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IC0tYmFja2dyb3VuZDogI2ZmZjsgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7IGJvcmRlcjogI2YzODMyMCBzb2xpZCAycHg7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDMwcHg7IC0tYm94LXNoYWRvdzogbm9uZTsgfSJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_nannydetails_nannydetails_module_ts.js.map