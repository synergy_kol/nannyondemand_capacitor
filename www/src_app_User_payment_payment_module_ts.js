"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_payment_payment_module_ts"],{

/***/ 17582:
/*!********************************************************!*\
  !*** ./src/app/User/payment/payment-routing.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaymentPageRoutingModule": () => (/* binding */ PaymentPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _payment_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./payment.page */ 51439);




const routes = [
    {
        path: '',
        component: _payment_page__WEBPACK_IMPORTED_MODULE_0__.PaymentPage
    }
];
let PaymentPageRoutingModule = class PaymentPageRoutingModule {
};
PaymentPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PaymentPageRoutingModule);



/***/ }),

/***/ 99711:
/*!************************************************!*\
  !*** ./src/app/User/payment/payment.module.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaymentPageModule": () => (/* binding */ PaymentPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _payment_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./payment-routing.module */ 17582);
/* harmony import */ var _payment_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payment.page */ 51439);







let PaymentPageModule = class PaymentPageModule {
};
PaymentPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _payment_routing_module__WEBPACK_IMPORTED_MODULE_0__.PaymentPageRoutingModule
        ],
        declarations: [_payment_page__WEBPACK_IMPORTED_MODULE_1__.PaymentPage]
    })
], PaymentPageModule);



/***/ }),

/***/ 51439:
/*!**********************************************!*\
  !*** ./src/app/User/payment/payment.page.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaymentPage": () => (/* binding */ PaymentPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_payment_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./payment.page.html */ 34515);
/* harmony import */ var _payment_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payment.page.scss */ 47217);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _awesome_cordova_plugins_stripe_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/stripe/ngx */ 117);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ 17897);







//import { PayPal, PayPalConfiguration, PayPalPayment } from '@ionic-native/paypal/ngx';

let PaymentPage = class PaymentPage {
    constructor(stripe, 
    //private payPal: PayPal,
    navCtrl, loadingController, toastController, authService, storage) {
        this.stripe = stripe;
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.authService = authService;
        this.storage = storage;
        this.paymentData = [{ name: 'Saved Cards' }, { name: 'New Card' }, { name: 'Paypal' }];
        this.selectedChip = 1;
        this.cardholdername = '';
        this.cardnumber = '';
        this.expirydate = '';
        this.cvv = '';
        this.remember = false;
        this.isDisabled = false;
        this.isLoading = false;
        this.month = '';
        this.year = '';
        this.cardData = [];
        this.paydata = [];
        this.currentDate = (new Date()).getFullYear();
        this.maxDate = (new Date()).getFullYear() + 100;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.showLoader('Loading...');
        this.storage.get('payDetails').then((val) => {
            this.paydata = val;
            console.log('Pay:', this.paydata);
        });
        this.storage.get('userDetails').then((result) => {
            this.data = result;
            this.userID = this.data.user_id;
            var body = {
                user_id: this.data.user_id,
                scheduled_list: this.paydata
            };
            this.authService.postData("get-nanny-charge", body).then(result => {
                this.data = result;
                console.log("GetCharge:", this.data);
                this.order_final_total = this.data.result.data.charge;
                this.hideLoader();
            }, error => {
                this.hideLoader();
                console.log(error);
            });
        });
    }
    getMonth(mydate) {
        var dateTimeArr = mydate.split('T');
        var dateArr = dateTimeArr[0];
        var newDateArr = String(dateArr).split('-');
        return newDateArr[1].toString();
    }
    getYear(mydate) {
        var dateTimeArr = mydate.split('T');
        var dateArr = dateTimeArr[0];
        var newDateArr = String(dateArr).split('-');
        return newDateArr[0].toString;
    }
    cardSelected(data, ind) {
        this.mycardData = data;
        this.cardSelectedChip = ind;
        console.log(this.mycardData);
    }
    onPay() {
        //this.navCtrl.navigateRoot('/successpayment');
        if (this.selectedChip == 0) {
            if (this.cardSelectedChip == null) {
                this.presentToast('Please select Card');
            }
            else {
                this.showLoader('Please wait...');
                this.storage.get('userDetails').then((result) => {
                    this.data = result;
                    this.userID = this.data.user_id;
                    this.userName = this.data.fname + " " + this.data.lname;
                    console.log(this.mycardData.account_name);
                    this.stripe.setPublishableKey('pk_live_51Hqj8AJBHcxndDjZdKEFWyi1OBXjVg85DHnud7KQuuwJfBbehO3ZLcUlM9zISiDHt60spgXxa3ffny1IaMhKXirJ00wNYdDrFE');
                    let card = {
                        number: this.mycardData.card_no,
                        expMonth: this.mycardData.exp_month,
                        expYear: this.mycardData.exp_year,
                        cvc: this.mycardData.card_cvc
                    };
                    this.stripe.createCardToken(card)
                        .then(token => {
                        console.log("token: ", token.id);
                        var body = {
                            user_id: this.userID,
                            //nany_id: this.nanny_id,
                            booking_amount: this.order_final_total,
                            transaction_type: 'stripe',
                            token_id: token.id
                        };
                        this.authService.postData("store-booking", body).then(result => {
                            this.data = result;
                            if (this.data.status.error_code == 0) {
                                this.hideLoader();
                                this.presentToast(this.data.status.message);
                                this.navCtrl.navigateForward('successpayment');
                                console.log(this.data);
                            }
                            else {
                                console.log(this.data.status.message);
                            }
                        }, error => {
                            this.hideLoader();
                            console.log(error);
                        });
                    })
                        .catch(error => {
                        console.error("error: ", error);
                        this.hideLoader();
                    });
                });
            }
        }
        else if (this.selectedChip == 1) {
            if (this.remember == true) {
                var dateTimeArr = this.expirydate.split('T');
                var dateArr = dateTimeArr[0];
                var newDateArr = String(dateArr).split('-');
                this.month = newDateArr[1];
                this.year = newDateArr[0];
                if (this.cardholdername.trim() == '') {
                    this.presentToast('Please enter your Full Name');
                }
                else if (this.cardnumber.trim() == '') {
                    this.presentToast('Please enter Card Number');
                }
                else if (this.expirydate.trim() == '') {
                    this.presentToast('Please enter Expiry Date');
                }
                else if (this.cvv.trim() == '') {
                    this.presentToast('Please enter CVC number');
                }
                else {
                    this.showLoader('Please wait...');
                    this.storage.get('userDetails').then((result) => {
                        this.data = result;
                        this.userID = this.data.user_id;
                        this.userName = this.data.fname + " " + this.data.lname;
                        var body = {
                            user_id: this.userID,
                            card_no: this.cardnumber,
                            exp_month: this.month,
                            exp_year: this.year,
                            cvc: this.cvv,
                            account_name: this.cardholdername
                        };
                        this.authService.postData("save-card", body).then(result => {
                            this.data = result;
                            console.log("save-card: ", this.data);
                            if (this.data.status.error_code == 0) {
                                this.stripe.setPublishableKey('pk_live_51Hqj8AJBHcxndDjZdKEFWyi1OBXjVg85DHnud7KQuuwJfBbehO3ZLcUlM9zISiDHt60spgXxa3ffny1IaMhKXirJ00wNYdDrFE');
                                let card = {
                                    number: this.cardnumber,
                                    expMonth: this.month,
                                    expYear: this.year,
                                    cvc: this.cvv
                                };
                                this.stripe.createCardToken(card)
                                    .then(token => {
                                    console.log("token: ", token.id);
                                    var body = {
                                        user_id: this.userID,
                                        //nany_id: this.nanny_id,
                                        booking_amount: this.order_final_total,
                                        transaction_type: 'stripe',
                                        token_id: token.id
                                    };
                                    this.authService.postData("store-booking", body).then(result => {
                                        this.data = result;
                                        if (this.data.status.error_code == 0) {
                                            this.hideLoader();
                                            this.presentToast(this.data.status.message);
                                            this.navCtrl.navigateForward('successpayment');
                                            console.log(this.data);
                                        }
                                        else {
                                            console.log(this.data.status.message);
                                        }
                                    }, error => {
                                        this.hideLoader();
                                        console.log(error);
                                    });
                                })
                                    .catch(error => {
                                    console.error("error: ", error);
                                    this.hideLoader();
                                });
                            }
                            else {
                                console.log(this.data.status.message);
                                this.hideLoader();
                            }
                        }, error => {
                            this.hideLoader();
                            console.log(error);
                        });
                    });
                }
            }
            else {
                var dateTimeArr = this.expirydate.split('T');
                var dateArr = dateTimeArr[0];
                var newDateArr = String(dateArr).split('-');
                this.month = newDateArr[1];
                this.year = newDateArr[0];
                if (this.cardholdername.trim() == '') {
                    this.presentToast('Please enter your Full Name');
                }
                else if (this.cardnumber.trim() == '') {
                    this.presentToast('Please enter Card Number');
                }
                else if (this.expirydate.trim() == '') {
                    this.presentToast('Please enter Expiry Date');
                }
                else if (this.cvv.trim() == '') {
                    this.presentToast('Please enter CVC number');
                }
                else {
                    console.log(this.expirydate);
                    this.showLoader('Please wait...');
                    this.storage.get('userDetails').then((result) => {
                        this.data = result;
                        this.userID = this.data.user_id;
                        this.userName = this.data.fname + " " + this.data.lname;
                        this.stripe.setPublishableKey('pk_live_51Hqj8AJBHcxndDjZdKEFWyi1OBXjVg85DHnud7KQuuwJfBbehO3ZLcUlM9zISiDHt60spgXxa3ffny1IaMhKXirJ00wNYdDrFE');
                        let card = {
                            number: this.cardnumber,
                            expMonth: this.month,
                            expYear: this.year,
                            cvc: this.cvv
                        };
                        this.stripe.createCardToken(card)
                            .then(token => {
                            console.log("token: ", token.id);
                            var body = {
                                user_id: this.userID,
                                //nany_id: this.nanny_id,
                                booking_amount: this.order_final_total,
                                transaction_type: 'stripe',
                                token_id: token.id
                            };
                            this.authService.postData("store-booking", body).then(result => {
                                this.data = result;
                                if (this.data.status.error_code == 0) {
                                    this.hideLoader();
                                    this.presentToast(this.data.status.message);
                                    this.navCtrl.navigateForward('successpayment');
                                    console.log(this.data);
                                }
                                else {
                                    console.log(this.data.status.message);
                                }
                            }, error => {
                                this.hideLoader();
                                console.log(error);
                            });
                        })
                            .catch(error => {
                            console.error("error: ", error);
                            this.hideLoader();
                        });
                    });
                }
            }
        }
        // else {
        //   this.storage.get('userDetails').then((result) => {
        //     this.data = result;
        //     this.userID = this.data.user_id;
        //     this.userName = this.data.fname + " " + this.data.lname;
        //     this.payPal.init({
        //       PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
        //       PayPalEnvironmentSandbox: 'AbzaJjjdSZ7ub9OZwF_8Ns40vJBy0WCzHeiYzTxA6BmmugGLzgNL_NbBvmaixcXQGc7Bc_O-7ONLMHoJ'
        //     }).then(() => {
        //       this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        //       })).then(() => {
        //         let payment = new PayPalPayment((this.order_final_total).toString(), 'USD', 'Description', 'sale');
        //         this.payPal.renderSinglePaymentUI(payment).then((response) => {
        //           console.log(response);
        //           this.paypalData = response;
        //           var body = {
        //             user_id: this.userID,
        //             //nany_id: this.nanny_id,
        //             booking_amount: this.order_final_total,
        //             transaction_type: 'paypal',
        //             transaction_id: this.paypalData.response.id
        //           };
        //           this.authService.postData("store-booking", body).then(result => {
        //             this.data = result;
        //             if (this.data.status.error_code == 0) {
        //               this.hideLoader();
        //               this.presentToast(this.data.status.message);
        //               this.navCtrl.navigateForward('successpayment');
        //               console.log(this.data);
        //             } else {
        //               console.log(this.data.status.message);
        //             }
        //           },
        //             error => {
        //               this.hideLoader();
        //               console.log(error);
        //             });
        //         }, () => {
        //           this.hideLoader();
        //         });
        //       }, () => {
        //         this.hideLoader();
        //       });
        //     }, () => {
        //       this.hideLoader();
        //     });
        //   });
        // }
    }
    onFilterPayment(id) {
        this.selectedChip = id;
        if (id == 0) {
            this.showLoader('Please wait...');
            this.storage.get('userDetails').then((result) => {
                this.data = result;
                this.userID = this.data.user_id;
                var body = {
                    user_id: this.userID
                };
                this.authService.postData("get-cards", body).then(result => {
                    this.data = result;
                    this.cardData = this.data.result.data;
                    console.log("get-cards: ", this.data);
                    this.hideLoader();
                }, error => {
                    this.hideLoader();
                    console.log(error);
                });
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
PaymentPage.ctorParameters = () => [
    { type: _awesome_cordova_plugins_stripe_ngx__WEBPACK_IMPORTED_MODULE_2__.Stripe },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__.Storage }
];
PaymentPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-payment',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_payment_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_payment_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], PaymentPage);



/***/ }),

/***/ 34515:
/*!***************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/payment/payment.page.html ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Make Payment</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"total_top\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"9\">Total Payable Amount\n          <!-- <img src=\"assets/images/info_icon_orng.png\"\n            style=\"height: 18px; top: 3px; left: 3px; position: relative;\" /> -->\n        </ion-col>\n        <ion-col size=\"3\" class=\"total_top_right\">${{order_final_total}}</ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <div style=\"padding: 10px 20px;\">\n    <h3>Selected Booking Date</h3>\n    <div class=\"select_method\">\n      <ion-item lines=\"none\" class=\"pl_input\"\n        style=\"padding: 0 !important; position: relative; --background: #eeeeee; margin: 1px 0;\"\n        *ngFor=\"let data of paydata.reverse();\">\n        <ion-label style=\"font-size: 14px;\">{{data.date}}</ion-label>\n        <ion-badge slot=\"end\" color=\"warning\" style=\"border-radius: 20px; padding: 3px 10px; margin-right: 5px;\">\n          {{data.time_slot}} / $ {{data.amount}}\n        </ion-badge>\n      </ion-item>\n    </div>\n    <h3>Select your method</h3>\n    <div class=\"select_method\">\n      <ul>\n        <li [class.pay_method_active]=\"selectedChip == 0\" (click)=\"onFilterPayment(0)\">\n          <a>\n            <div style=\"text-align: center; display: table-cell; vertical-align: middle;\">\n              <span>Saved Cards</span>\n              <img class=\"cardmethod_normal\" src=\"assets/images/card_icon.png\" />\n              <img class=\"cardmethod_normal_active\" src=\"assets/images/card_icon_white.png\" />\n            </div>\n          </a>\n        </li>\n        <li [class.pay_method_active]=\"selectedChip == 1\" (click)=\"onFilterPayment(1)\">\n          <a>\n            <div style=\"text-align: center; display: table-cell; vertical-align: middle;\">\n              <span>New Cards</span>\n              <img class=\"cardmethod_normal\" src=\"assets/images/card_icon.png\" />\n              <img class=\"cardmethod_normal_active\" src=\"assets/images/card_icon_white.png\" />\n            </div>\n          </a>\n        </li>\n        <li [class.pay_method_active]=\"selectedChip == 2\" (click)=\"onFilterPayment(2)\">\n          <a>\n            <div style=\"text-align: center; display: table-cell; vertical-align: middle;\">\n              <img class=\"cardmethod_normal\" src=\"assets/images/paypal_icon_normal.png\" />\n              <img class=\"cardmethod_normal_active\" src=\"assets/images/paypal_icon_active.png\" />\n            </div>\n          </a>\n        </li>\n      </ul>\n    </div>\n    <div *ngIf=\"selectedChip == 0\">\n      <ion-card class=\"driver-card-box\" *ngFor=\"let data of cardData; let i = index\" (click)=\"cardSelected(data, i)\">\n        <div style=\"display: flex;align-items: center;justify-content: space-between;padding-right: 10px;\">\n          <ion-card-header>\n            <ion-card-subtitle class=\"card-subtitle\">Cardholder's Name</ion-card-subtitle>\n            <ion-card-title class=\"card-title\">{{data.account_name}}</ion-card-title>\n          </ion-card-header>\n          <ion-icon [name]=\"cardSelectedChip == i ? 'checkmark-circle-outline' : 'ellipse-outline'\"\n            style=\"font-size: 28px;\"></ion-icon>\n          <!-- <ion-icon [name]=\"ellipse-outline\" style=\"font-size: 28px;\"></ion-icon> -->\n          <!--  <ion-radio-group value=\"biff\">\n            <ion-radio mode=\"android\" ></ion-radio>\n          </ion-radio-group> -->\n\n          <!-- <ion-checkbox mode=\"ios\" color=\"success\"></ion-checkbox> -->\n        </div>\n        <ion-card-header>\n          <ion-card-subtitle class=\"card-subtitle\">Card Number</ion-card-subtitle>\n          <ion-card-title class=\"card-title\">{{data.card_no}}</ion-card-title>\n        </ion-card-header>\n        <ion-card-content>\n          <ul>\n            <li>\n              <h4>Expiry Date</h4>\n              <h3>{{data.exp_month}}/{{data.exp_year}}</h3>\n            </li>\n            <li>\n              <h4>CVC</h4>\n              <h3>{{data.card_cvc}}</h3>\n            </li>\n          </ul>\n        </ion-card-content>\n      </ion-card>\n    </div>\n    <div *ngIf=\"selectedChip == 1\">\n      <h3>Enter Your Payment Details</h3>\n      <div class=\"cust_input payment_textbox\">\n        <ion-label>CARDHOLDER NAME</ion-label>\n        <ion-input placeholder=\"Please Enter Your Name\" [(ngModel)]=\"cardholdername\"></ion-input>\n      </div>\n      <div class=\"cust_input payment_textbox\">\n        <ion-label>CARD Number</ion-label>\n        <ion-input placeholder=\"**** **** **** 1234\" [(ngModel)]=\"cardnumber\"></ion-input>\n      </div>\n      <div style=\"display: flex;\">\n        <div class=\"cust_input payment_textbox\" style=\"margin-right: 1%;\">\n          <ion-label>Expiry Date</ion-label>\n          <ion-datetime placeholder=\"\" [min]=\"currentDate\" [max]=\"maxDate\" displayFormat=\"MM / YY\"\n            [(ngModel)]=\"expirydate\"></ion-datetime>\n        </div>\n        <div class=\"cust_input payment_textbox\" style=\"margin-left: 1%;\">\n          <ion-label>CVC\n            <!--  <span>?</span> -->\n          </ion-label>\n          <ion-input placeholder=\"***\" [(ngModel)]=\"cvv\"></ion-input>\n        </div>\n      </div>\n      <ion-item lines=\"none\">\n        <ion-label style=\"font-size: 14px;\">Remember my card details</ion-label>\n        <ion-toggle color=\"warning\" [(ngModel)]=\"remember\"></ion-toggle>\n      </ion-item>\n    </div>\n\n    <div style=\"padding: 30px 30px 0 30px;\">\n      <ion-button color=\"warning\" slot=\"end\" shape=\"round\" expand=\"full\" (click)=\"onPay()\" class=\"cus_btn\">Pay Now\n      </ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 47217:
/*!************************************************!*\
  !*** ./src/app/User/payment/payment.page.scss ***!
  \************************************************/
/***/ ((module) => {

module.exports = ".total_top {\n  background: #fff0e3;\n  padding: 6px 5px;\n  color: #f38320;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 17px;\n}\n\n.total_top_right {\n  text-align: right;\n}\n\nh3 {\n  color: #222222;\n  font-size: 18px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.select_method ul {\n  padding: 0 0 15px 0;\n  margin: 0;\n  list-style: none;\n  text-align: center;\n}\n\n.select_method ul li {\n  display: inline-block;\n  vertical-align: middle;\n  margin: 0 1%;\n  width: 31.33%;\n  position: relative;\n}\n\n.select_method ul li a {\n  padding: 5px;\n  text-align: center;\n  background: #f6f6f6;\n  border-radius: 5px;\n  border: #ccc solid 1px;\n  text-decoration: none;\n  height: 95px;\n  display: table;\n  width: 100%;\n}\n\n.select_method ul li a span {\n  color: #999999;\n  font-size: 12px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  display: block;\n  padding: 0 0 5px 0;\n}\n\n.select_method ul li a img.cardmethod_normal {\n  display: inline-block;\n}\n\n.select_method ul li a img.cardmethod_normal_active {\n  display: none;\n}\n\n.select_method ul li.pay_method_active a {\n  background: #f38320;\n  color: #fff;\n}\n\n.select_method ul li.pay_method_active a img.cardmethod_normal {\n  display: none;\n}\n\n.select_method ul li.pay_method_active a img.cardmethod_normal_active {\n  display: inline-block;\n}\n\n.select_method ul li.pay_method_active a span {\n  color: #fff;\n}\n\n.payment_textbox {\n  height: auto;\n  border-radius: 20px;\n  padding: 10px 15px !important;\n}\n\n.payment_textbox ion-input, .payment_textbox ion-datetime {\n  height: 40px;\n  background: #f9f9f9;\n  padding: 5px 10px !important;\n  border-radius: 10px;\n  margin: 8px 0 5px 0;\n  color: #000000;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.payment_textbox ion-label {\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.payment_textbox ion-label span {\n  background: #cccccc;\n  padding: 0px 7px;\n  color: #000000;\n  font-weight: bold;\n  border-radius: 30px;\n  margin-left: 5px;\n  font-size: 15px;\n}\n\n.payment_textbox ion-datetime {\n  padding-top: 10px !important;\n}\n\n.cust_input:hover {\n  border-color: #f38320;\n  box-shadow: #fff0e3 0 0 6px 4px;\n}\n\nion-card.selected {\n  background: red;\n}\n\n.driver-card-box {\n  background: #f38320;\n  color: #fff;\n  margin: 10px 0;\n  font-family: \"Poppins\", sans-serif;\n  position: relative;\n  border-radius: 12px;\n}\n\n.driver-card-box span {\n  position: absolute;\n  right: 0;\n  top: 5px;\n  padding: 15px;\n  color: #ffffff;\n  z-index: 99;\n  font-size: 22px;\n  line-height: 15px;\n}\n\n.card-title {\n  color: #fff;\n  text-transform: uppercase;\n  font-size: 16px;\n  margin: 5px 0 0 0;\n  font-weight: bold;\n  letter-spacing: 1px;\n}\n\n.card-subtitle {\n  color: #fff;\n  margin: 0;\n  text-transform: capitalize;\n}\n\n.driver-card-box ul {\n  padding: 0;\n  margin: 0;\n  list-style: none;\n  display: flex;\n  justify-content: space-between;\n}\n\n.driver-card-box ul h4 {\n  font-size: 14px;\n}\n\n.driver-card-box ul h3 {\n  font-size: 16px;\n  color: #fff;\n  font-weight: bold;\n  letter-spacing: 1px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBheW1lbnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQVksbUJBQUE7RUFBb0IsZ0JBQUE7RUFBa0IsY0FBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxlQUFBO0FBTzFIOztBQU5BO0VBQWtCLGlCQUFBO0FBVWxCOztBQVRBO0VBQUcsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBZ0J0RDs7QUFkQTtFQUNJLG1CQUFBO0VBQXFCLFNBQUE7RUFBVyxnQkFBQTtFQUFpQixrQkFBQTtBQW9CckQ7O0FBbkJJO0VBQ0kscUJBQUE7RUFBdUIsc0JBQUE7RUFBd0IsWUFBQTtFQUFjLGFBQUE7RUFBZSxrQkFBQTtBQXlCcEY7O0FBeEJRO0VBQUcsWUFBQTtFQUFjLGtCQUFBO0VBQW9CLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW1CLHNCQUFBO0VBQXVCLHFCQUFBO0VBQXVCLFlBQUE7RUFBYyxjQUFBO0VBQWdCLFdBQUE7QUFtQ2pLOztBQWxDWTtFQUFNLGNBQUE7RUFBZ0IsZUFBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxjQUFBO0VBQWdCLGtCQUFBO0FBMEMxSDs7QUFwQ0E7RUFBOEMscUJBQUE7QUF3QzlDOztBQXZDQTtFQUFxRCxhQUFBO0FBMkNyRDs7QUExQ0E7RUFBMEMsbUJBQUE7RUFBcUIsV0FBQTtBQStDL0Q7O0FBOUNBO0VBQWdFLGFBQUE7QUFrRGhFOztBQWpEQTtFQUF1RSxxQkFBQTtBQXFEdkU7O0FBcERBO0VBQStDLFdBQUE7QUF3RC9DOztBQXREQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0FBeURKOztBQXhESTtFQUF5QixZQUFBO0VBQWMsbUJBQUE7RUFBcUIsNEJBQUE7RUFBOEIsbUJBQUE7RUFBcUIsbUJBQUE7RUFBcUIsY0FBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtBQWtFMUs7O0FBakVJO0VBQVcsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0FBdUVsRTs7QUF0RVE7RUFDSSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUF3RVo7O0FBckVJO0VBQWEsNEJBQUE7QUF3RWpCOztBQXRFQTtFQUFtQixxQkFBQTtFQUF1QiwrQkFBQTtBQTJFMUM7O0FBMUVBO0VBQ0ksZUFBQTtBQTZFSjs7QUEzRUE7RUFDQyxtQkFBQTtFQUNBLFdBQUE7RUFDRyxjQUFBO0VBQ0Esa0NBQUE7RUFDQSxrQkFBQTtFQUVBLG1CQUFBO0FBNkVKOztBQTVFSTtFQUFLLGtCQUFBO0VBQW9CLFFBQUE7RUFBVSxRQUFBO0VBQVUsYUFBQTtFQUFlLGNBQUE7RUFBZ0IsV0FBQTtFQUFhLGVBQUE7RUFBaUIsaUJBQUE7QUFzRjlHOztBQXBGQTtFQUNDLFdBQUE7RUFDQSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0csbUJBQUE7QUF1Rko7O0FBckZBO0VBQ0MsV0FBQTtFQUNBLFNBQUE7RUFDQSwwQkFBQTtBQXdGRDs7QUF0RkE7RUFDQyxVQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBeUZEOztBQXZGQTtFQUNDLGVBQUE7QUEwRkQ7O0FBeEZBO0VBQ0MsZUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNHLG1CQUFBO0FBMkZKIiwiZmlsZSI6InBheW1lbnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRvdGFsX3RvcHsgYmFja2dyb3VuZDojZmZmMGUzOyBwYWRkaW5nOiA2cHggNXB4OyBjb2xvcjogI2YzODMyMDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMTdweDsgfVxyXG4udG90YWxfdG9wX3JpZ2h0eyB0ZXh0LWFsaWduOiByaWdodDsgfVxyXG5oM3tjb2xvcjogIzIyMjIyMjsgZm9udC1zaXplOiAxOHB4OyBmb250LXdlaWdodDogNjAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjt9XHJcblxyXG4uc2VsZWN0X21ldGhvZCB1bHtcclxuICAgIHBhZGRpbmc6IDAgMCAxNXB4IDA7IG1hcmdpbjogMDsgbGlzdC1zdHlsZTpub25lOyB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBsaSB7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrOyB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyBtYXJnaW46IDAgMSU7IHdpZHRoOiAzMS4zMyU7IHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBheyBwYWRkaW5nOiA1cHg7IHRleHQtYWxpZ246IGNlbnRlcjsgYmFja2dyb3VuZDogI2Y2ZjZmNjsgYm9yZGVyLXJhZGl1czo1cHg7IGJvcmRlcjojY2NjIHNvbGlkIDFweDsgdGV4dC1kZWNvcmF0aW9uOiBub25lOyBoZWlnaHQ6IDk1cHg7IGRpc3BsYXk6IHRhYmxlOyB3aWR0aDogMTAwJTsgXHJcbiAgICAgICAgICAgIHNwYW57IGNvbG9yOiAjOTk5OTk5OyBmb250LXNpemU6MTJweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGRpc3BsYXk6IGJsb2NrOyBwYWRkaW5nOiAwIDAgNXB4IDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcbi5zZWxlY3RfbWV0aG9kIHVsIGxpIGEgaW1nLmNhcmRtZXRob2Rfbm9ybWFseyBkaXNwbGF5OmlubGluZS1ibG9jazsgfVxyXG4uc2VsZWN0X21ldGhvZCB1bCBsaSBhIGltZy5jYXJkbWV0aG9kX25vcm1hbF9hY3RpdmV7IGRpc3BsYXk6bm9uZTsgfVxyXG4uc2VsZWN0X21ldGhvZCB1bCBsaS5wYXlfbWV0aG9kX2FjdGl2ZSBheyBiYWNrZ3JvdW5kOiAjZjM4MzIwOyBjb2xvcjogI2ZmZjsgfVxyXG4uc2VsZWN0X21ldGhvZCB1bCBsaS5wYXlfbWV0aG9kX2FjdGl2ZSBhIGltZy5jYXJkbWV0aG9kX25vcm1hbHsgZGlzcGxheTpub25lOyB9XHJcbi5zZWxlY3RfbWV0aG9kIHVsIGxpLnBheV9tZXRob2RfYWN0aXZlIGEgaW1nLmNhcmRtZXRob2Rfbm9ybWFsX2FjdGl2ZXsgZGlzcGxheTppbmxpbmUtYmxvY2s7IH1cclxuLnNlbGVjdF9tZXRob2QgdWwgbGkucGF5X21ldGhvZF9hY3RpdmUgYSBzcGFueyBjb2xvcjogI2ZmZjsgfVxyXG5cclxuLnBheW1lbnRfdGV4dGJveHtcclxuICAgIGhlaWdodDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDE1cHggIWltcG9ydGFudDtcclxuICAgIGlvbi1pbnB1dCwgaW9uLWRhdGV0aW1leyBoZWlnaHQ6IDQwcHg7IGJhY2tncm91bmQ6ICNmOWY5Zjk7IHBhZGRpbmc6IDVweCAxMHB4ICFpbXBvcnRhbnQ7IGJvcmRlci1yYWRpdXM6IDEwcHg7IG1hcmdpbjogOHB4IDAgNXB4IDA7IGNvbG9yOiAjMDAwMDAwOyBmb250LXdlaWdodDogNjAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7fVxyXG4gICAgaW9uLWxhYmVseyBjb2xvcjogIzY2NjY2NjsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgXHJcbiAgICAgICAgc3BhbntcclxuICAgICAgICAgICAgYmFja2dyb3VuZDogI2NjY2NjYztcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4IDdweDtcclxuICAgICAgICAgICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgaW9uLWRhdGV0aW1le3BhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7fVxyXG59XHJcbi5jdXN0X2lucHV0OmhvdmVyeyBib3JkZXItY29sb3I6ICNmMzgzMjA7IGJveC1zaGFkb3c6ICNmZmYwZTMgMCAwIDZweCA0cHg7fVxyXG5pb24tY2FyZC5zZWxlY3RlZCB7XHJcbiAgICBiYWNrZ3JvdW5kOiByZWQ7XHJcbiAgfVxyXG4uZHJpdmVyLWNhcmQtYm94IHtcclxuXHRiYWNrZ3JvdW5kOiAjZjM4MzIwO1xyXG5cdGNvbG9yOiAjZmZmO1xyXG4gICAgbWFyZ2luOiAxMHB4IDA7XHJcbiAgICBmb250LWZhbWlseTogJ1BvcHBpbnMnLCBzYW5zLXNlcmlmO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgLy9wYWRkaW5nOiA4cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgc3Bhbntwb3NpdGlvbjogYWJzb2x1dGU7IHJpZ2h0OiAwOyB0b3A6IDVweDsgcGFkZGluZzogMTVweDsgY29sb3I6ICNmZmZmZmY7IHotaW5kZXg6IDk5OyBmb250LXNpemU6IDIycHg7IGxpbmUtaGVpZ2h0OiAxNXB4O31cclxufVxyXG4uY2FyZC10aXRsZSB7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0dGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHRmb250LXNpemU6IDE2cHg7XHJcblx0bWFyZ2luOiA1cHggMCAwIDA7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG59XHJcbi5jYXJkLXN1YnRpdGxlIHtcclxuXHRjb2xvcjogI2ZmZjtcclxuXHRtYXJnaW46IDA7XHJcblx0dGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuLmRyaXZlci1jYXJkLWJveCB1bCB7XHJcblx0cGFkZGluZzogMDtcclxuXHRtYXJnaW46IDA7XHJcblx0bGlzdC1zdHlsZTogbm9uZTtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4uZHJpdmVyLWNhcmQtYm94IHVsIGg0IHtcclxuXHRmb250LXNpemU6IDE0cHg7XHJcbn1cclxuLmRyaXZlci1jYXJkLWJveCB1bCBoMyB7XHJcblx0Zm9udC1zaXplOjE2cHg7XHJcblx0Y29sb3I6ICNmZmY7XHJcblx0Zm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_payment_payment_module_ts.js.map