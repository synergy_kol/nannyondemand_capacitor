"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_welcome_welcome_module_ts"],{

/***/ 71640:
/*!***************************************************!*\
  !*** ./src/app/welcome/welcome-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WelcomePageRoutingModule": () => (/* binding */ WelcomePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _welcome_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./welcome.page */ 78822);




const routes = [
    {
        path: '',
        component: _welcome_page__WEBPACK_IMPORTED_MODULE_0__.WelcomePage
    }
];
let WelcomePageRoutingModule = class WelcomePageRoutingModule {
};
WelcomePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], WelcomePageRoutingModule);



/***/ }),

/***/ 35783:
/*!*******************************************!*\
  !*** ./src/app/welcome/welcome.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WelcomePageModule": () => (/* binding */ WelcomePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _welcome_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./welcome-routing.module */ 71640);
/* harmony import */ var _welcome_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./welcome.page */ 78822);







let WelcomePageModule = class WelcomePageModule {
};
WelcomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _welcome_routing_module__WEBPACK_IMPORTED_MODULE_0__.WelcomePageRoutingModule
        ],
        declarations: [_welcome_page__WEBPACK_IMPORTED_MODULE_1__.WelcomePage]
    })
], WelcomePageModule);



/***/ }),

/***/ 78822:
/*!*****************************************!*\
  !*** ./src/app/welcome/welcome.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WelcomePage": () => (/* binding */ WelcomePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_welcome_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./welcome.page.html */ 4656);
/* harmony import */ var _welcome_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./welcome.page.scss */ 5604);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 78099);





let WelcomePage = class WelcomePage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ngOnInit() {
    }
    goUser() {
        this.navCtrl.navigateForward('/registration1');
    }
    goProvider() {
        this.navCtrl.navigateForward('/providerregistration1');
    }
    goLogin() {
        this.navCtrl.navigateForward('/login');
    }
};
WelcomePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.NavController }
];
WelcomePage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-welcome',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_welcome_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_welcome_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], WelcomePage);



/***/ }),

/***/ 4656:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/welcome/welcome.page.html ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"welcome_top\">\n    <img src=\"assets/images/welcome_bg.jpg\" alt=\"welcome_bg\" />\n  </div>\n  <div class=\"welcomelogo\">\n    <img src=\"assets/images/welcome_logo.jpg\" alt=\"welcome_logo\" />\n  </div>\n  <div class=\"welcome_text\">\n    <h4>Welcome to</h4>\n    <h2>Nanny on Demand</h2>\n    <ion-button color=\"warning\" expand=\"full\" shape=\"round\" (click)=\"goUser()\" class=\"cus_btn\">Looking for a provider\n    </ion-button>\n    <ion-button color=\"success\" expand=\"full\" shape=\"round\" (click)=\"goProvider()\" class=\"cus_btn\">Become a provider\n    </ion-button>\n    <p (click)=\"goLogin()\"> Already have an account? <a>Login</a></p>\n  </div>\n</ion-content>");

/***/ }),

/***/ 5604:
/*!*******************************************!*\
  !*** ./src/app/welcome/welcome.page.scss ***!
  \*******************************************/
/***/ ((module) => {

module.exports = ".welcomelogo {\n  text-align: center;\n}\n.welcomelogo img {\n  width: 50%;\n}\n.welcome_text {\n  padding: 0 30px;\n}\n.welcome_text h4 {\n  padding: 40px 0 0 0;\n  margin: 0;\n  font-size: 20px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.welcome_text h2 {\n  padding: 0 0 20px 0;\n  margin: 0;\n  font-size: 24px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.welcome_text p {\n  text-align: center;\n  color: #000000;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 20px 0;\n}\n.welcome_text p a {\n  color: #000000;\n  font-size: 14px;\n  text-decoration: underline;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlbGNvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQWMsa0JBQUE7QUFFZDtBQURLO0VBQUssVUFBQTtBQUlWO0FBRkE7RUFBZSxlQUFBO0FBTWY7QUFMSTtFQUFJLG1CQUFBO0VBQXFCLFNBQUE7RUFBVyxlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGtCQUFBO0FBYWpIO0FBWkk7RUFBSSxtQkFBQTtFQUFxQixTQUFBO0VBQVcsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxrQkFBQTtBQW9Cakg7QUFuQkk7RUFBRyxrQkFBQTtFQUFvQixjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBdUMsY0FBQTtBQTJCckg7QUExQlE7RUFBRyxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsMEJBQUE7RUFBNEIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0Msa0JBQUE7QUFrQ2hJIiwiZmlsZSI6IndlbGNvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWVsb2dveyB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgIGltZ3sgd2lkdGg6IDUwJTsgfVxyXG59XHJcbi53ZWxjb21lX3RleHR7IHBhZGRpbmc6IDAgMzBweDtcclxuICAgIGg0eyBwYWRkaW5nOiA0MHB4IDAgMCAwOyBtYXJnaW46IDA7IGZvbnQtc2l6ZTogMjBweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmOyB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cclxuICAgIGgyeyBwYWRkaW5nOiAwIDAgMjBweCAwOyBtYXJnaW46IDA7IGZvbnQtc2l6ZTogMjRweDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IHRleHQtYWxpZ246IGNlbnRlcjsgfVxyXG4gICAgcHsgdGV4dC1hbGlnbjogY2VudGVyOyBjb2xvcjogIzAwMDAwMDsgZm9udC1zaXplOiAxNHB4OyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgIG1hcmdpbjogMjBweCAwO1xyXG4gICAgICAgIGF7IGNvbG9yOiAjMDAwMDAwOyBmb250LXNpemU6IDE0cHg7IHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyBmb250LXdlaWdodDogNjAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgdGV4dC1hbGlnbjogY2VudGVyOyB9XHJcbiAgICB9XHJcbn1cclxuIl19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_welcome_welcome_module_ts.js.map