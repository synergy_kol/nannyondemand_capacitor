"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_registration1_registration1_module_ts"],{

/***/ 11337:
/*!********************************************************************!*\
  !*** ./src/app/User/registration1/registration1-routing.module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Registration1PageRoutingModule": () => (/* binding */ Registration1PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _registration1_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registration1.page */ 6617);




const routes = [
    {
        path: '',
        component: _registration1_page__WEBPACK_IMPORTED_MODULE_0__.Registration1Page
    }
];
let Registration1PageRoutingModule = class Registration1PageRoutingModule {
};
Registration1PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], Registration1PageRoutingModule);



/***/ }),

/***/ 23065:
/*!************************************************************!*\
  !*** ./src/app/User/registration1/registration1.module.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Registration1PageModule": () => (/* binding */ Registration1PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _registration1_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registration1-routing.module */ 11337);
/* harmony import */ var _registration1_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration1.page */ 6617);







let Registration1PageModule = class Registration1PageModule {
};
Registration1PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _registration1_routing_module__WEBPACK_IMPORTED_MODULE_0__.Registration1PageRoutingModule
        ],
        declarations: [_registration1_page__WEBPACK_IMPORTED_MODULE_1__.Registration1Page]
    })
], Registration1PageModule);



/***/ }),

/***/ 6617:
/*!**********************************************************!*\
  !*** ./src/app/User/registration1/registration1.page.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Registration1Page": () => (/* binding */ Registration1Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_registration1_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./registration1.page.html */ 33693);
/* harmony import */ var _registration1_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration1.page.scss */ 79759);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let Registration1Page = class Registration1Page {
    constructor(navCtrl, storage, loadingController, toastController, authService) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.authService = authService;
        this.fname = '';
        this.lname = '';
        this.dob = '';
        this.address = '';
        this.city = '';
        this.state = '';
        this.zip = '';
        this.phone = '';
        this.emgnphone = '';
        this.email = '';
        this.password = '';
        this.cnfpassword = '';
        this.isDisabled = false;
        this.isLoading = false;
        this.data = [];
        this.userdata = [];
        this.profileData = [];
        this.hideextra = true;
        this.step1Data = [];
    }
    ngOnInit() {
    }
    onKeyboard(event) {
        if (event != null) {
            event.setFocus();
        }
        else {
            this.onNext();
        }
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Loading...');
        this.storage.get('userDetails').then((val) => {
            if (val != null) {
                this.hideextra = false;
                this.userdata = val;
                this.getProfileDetails();
            }
            else {
                this.storage.get('setp1').then((val) => {
                    this.step1Data = val;
                    this.fname = this.step1Data.fname;
                    this.lname = this.step1Data.lname;
                    this.dob = this.step1Data.dob;
                    this.address = this.step1Data.address;
                    this.city = this.step1Data.city;
                    this.state = this.step1Data.state;
                    this.zip = this.step1Data.zip;
                    this.phone = this.step1Data.phone;
                    this.emgnphone = this.step1Data.emgnphone;
                    this.email = this.step1Data.email;
                });
                this.hideLoader();
            }
        });
        this.loginToken;
    }
    getProfileDetails() {
        var body = {
            user_id: this.userdata.user_id,
        };
        this.authService.postData("get-profile", body).then(result => {
            this.data = result;
            console.log("profile: ", this.data);
            this.profileData = this.data.result.data;
            this.fname = this.profileData.fname;
            this.lname = this.profileData.lname;
            this.dob = this.profileData.dob;
            this.address = this.profileData.address;
            this.city = this.profileData.city;
            this.state = this.profileData.state;
            this.zip = this.profileData.zip;
            this.phone = this.profileData.phone;
            this.emgnphone = this.profileData.emg_phone;
            this.email = this.profileData.email;
            this.hideLoader();
        }, error => {
            this.hideLoader();
        });
    }
    onNext() {
        const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const phonePattern = /[^0-9]/;
        const namePattern = /[^a-z A-Z]/;
        const passPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/;
        if (this.fname.trim() == '') {
            this.presentToast('Please enter your First Name');
        }
        else if (namePattern.test(this.fname)) {
            this.presentToast('First Name field should be Only Letters...');
        }
        else if (this.lname.trim() == '') {
            this.presentToast('Please enter your Last Name');
        }
        else if (namePattern.test(this.lname)) {
            this.presentToast('Last Name field should be Only Letters...');
        }
        else if (this.dob.trim() == '') {
            this.presentToast('Please choose Date of Birth');
        }
        else if (this.calculateAge(this.dob) < 16) {
            this.presentToast('Age should be greater than or equal to 16 years');
        }
        else if (this.address.trim() == '') {
            this.presentToast('Please enter Address');
        }
        else if (this.city.trim() == '') {
            this.presentToast('Please enter City');
        }
        else if (namePattern.test(this.city)) {
            this.presentToast('City field should be Only Letters...');
        }
        else if (this.state.trim() == '') {
            this.presentToast('Please enter State');
        }
        else if (namePattern.test(this.state)) {
            this.presentToast('State field should be Only Letters...');
        }
        else if (this.zip.trim() == '') {
            this.presentToast('Please enter Zip');
        }
        else if (this.phone.trim() == '') {
            this.presentToast('Please enter Phone Number');
        }
        else if (phonePattern.test(this.phone)) {
            this.presentToast('Phone field should be Only Numbers...');
        }
        else if (this.phone.length <= 9) {
            this.presentToast('Phone! Please enter minimum 10 Numbers');
        }
        else if (this.emgnphone.trim() == '') {
            this.presentToast('Please enter Emergency Phone Number');
        }
        else if (phonePattern.test(this.emgnphone)) {
            this.presentToast('Emergency Phone field should be Only Numbers...');
        }
        else if (this.emgnphone.length <= 9) {
            this.presentToast('Emergency Phone! Please enter minimum 10 Numbers');
        }
        else if (this.email.trim() == '') {
            this.presentToast('Please enter your Email ID');
        }
        else if (!emailPattern.test(this.email)) {
            this.presentToast('Wrong Email Format...');
        }
        else {
            if (this.userdata == "") {
                if (this.password.trim() == '') {
                    this.presentToast('Please enter your Password');
                }
                else if (this.password.length < 8) {
                    this.presentToast('Password must be contain atleast 8 letters');
                }
                else if (!passPattern.test(this.password)) {
                    this.presentToast('Password must be contain atleast one lowercase and one uppercase letter one digit and one special character.');
                }
                else if (this.cnfpassword.trim() == '') {
                    this.presentToast('Please enter confirm password');
                }
                else if (this.cnfpassword != this.password) {
                    this.presentToast('You Password and Confirm password must match');
                }
                else {
                    this.showLoader('Please wait...');
                    var body = {
                        email: this.email,
                    };
                    this.authService.postData("check-email", body).then(result => {
                        this.data = result;
                        if (this.data.status.error_code == 0) {
                            this.hideLoader();
                            var object = {
                                fname: this.fname,
                                lname: this.lname,
                                dob: this.dob,
                                address: this.address,
                                city: this.city,
                                state: this.state,
                                zip: this.zip,
                                phone: this.phone,
                                emgnphone: this.emgnphone,
                                email: this.email,
                                password: this.password
                            };
                            this.presentToast('Successfully Proceed..');
                            this.storage.set("userRegsData", object);
                            this.storage.set("setp1", object);
                            this.navCtrl.navigateForward("/registration2");
                        }
                        else {
                            this.presentToast("Sorry! Email already Registered.");
                            this.hideLoader();
                        }
                    }, error => {
                        this.hideLoader();
                    });
                }
            }
            else {
                this.showLoader('Please wait...');
                var editObject = {
                    fname: this.fname,
                    lname: this.lname,
                    dob: this.dob,
                    address: this.address,
                    city: this.city,
                    state: this.state,
                    zip: this.zip,
                    phone: this.phone,
                    emgnphone: this.emgnphone,
                    email: this.email,
                };
                this.presentToast('Successfully Proceed..');
                this.storage.set("userRegsData", editObject);
                setTimeout(() => {
                    this.hideLoader();
                    this.navCtrl.navigateForward("/registration2");
                }, 100);
            }
        }
    }
    calculateAge(dob) {
        if (dob != null) {
            var dob_entry = dob;
            var split_dob = dob_entry.split("T");
            var date_only = split_dob[0];
            var split_date_only = date_only.split("-");
            var month = split_date_only[1];
            var day = split_date_only[2];
            var year = split_date_only[0];
            var today = new Date();
            var age = today.getFullYear() - year;
            if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
                age--;
            }
            return age;
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
Registration1Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider }
];
Registration1Page = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-registration1',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_registration1_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_registration1_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], Registration1Page);



/***/ }),

/***/ 33693:
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/registration1/registration1.page.html ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title *ngIf=\"hideextra\">Parent Registration</ion-title>\n    <ion-title *ngIf=\"!hideextra\">Edit Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"step\">\n    <ul>\n      <li class=\"activestep\"><a></a><span>Step 1</span></li>\n      <li style=\"opacity: 0;\"><a></a></li>\n      <li><a></a><span>Step 2</span></li>\n    </ul>\n  </div>\n  <div class=\"register_panel\">\n    <p style=\"color: #f44336; font-size: 12px; margin: 0 0 15px 0; text-align: right;\">* below all fields are mandatory *\n    </p>\n    <ion-input class=\"cust_input\" placeholder=\"First name\" type=\"text\" [(ngModel)]=\"fname\"\n      (keyup.enter)=\"onKeyboard(Field1)\"></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"Last name\" type=\"text\" [(ngModel)]=\"lname\"\n      (keyup.enter)=\"onKeyboard(Field2)\" #Field1></ion-input>\n    <div class=\"dateofbirth\">\n      <ion-datetime class=\"cust_input\" placeholder=\"Date of Birth\" displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"dob\"\n        (keyup.enter)=\"onKeyboard(Field3)\" #Field2></ion-datetime>\n    </div>\n    <ion-input class=\"cust_input\" placeholder=\"Address\" type=\"text\" [(ngModel)]=\"address\"\n      (keyup.enter)=\"onKeyboard(Field4)\" #Field3></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"City\" type=\"text\" [(ngModel)]=\"city\" (keyup.enter)=\"onKeyboard(Field5)\"\n      #Field4></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"State/Province\" type=\"text\" [(ngModel)]=\"state\"\n      (keyup.enter)=\"onKeyboard(Field6)\" #Field5></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"ZIP/Postal\" type=\"text\" [(ngModel)]=\"zip\"\n      (keyup.enter)=\"onKeyboard(Field7)\" #Field6></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"Phone\" type=\"tel\" maxlength=\"15\" [(ngModel)]=\"phone\" (keyup.enter)=\"onKeyboard(Field8)\"\n      #Field7></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"Emergency Phone\" type=\"tel\" maxlength=\"15\" [(ngModel)]=\"emgnphone\"\n      (keyup.enter)=\"onKeyboard(Field9)\" #Field8></ion-input>\n    <ion-input *ngIf=\"hideextra\" class=\"cust_input\" placeholder=\"Email Address\" type=\"email\" [(ngModel)]=\"email\"\n      (keyup.enter)=\"onKeyboard(Field10)\" #Field9></ion-input>\n    <ion-input *ngIf=\"hideextra\" class=\"cust_input\" placeholder=\"Password\" type=\"Password\" [(ngModel)]=\"password\"\n      (keyup.enter)=\"onKeyboard(Field11)\" #Field10></ion-input>\n    <ion-input *ngIf=\"hideextra\" class=\"cust_input\" placeholder=\"Confirm Password\" type=\"Password\" [(ngModel)]=\"cnfpassword\"\n      (keyup.enter)=\"onKeyboard()\" #Field11></ion-input>\n  </div>\n  <ion-toolbar>\n    <ion-button color=\"warning\" slot=\"end\" shape=\"round\" (click)=\"onNext()\" class=\"sml_cus_btn\">Next &nbsp;<img\n        src=\"assets/images/white_r_arrow.png\" /></ion-button>\n  </ion-toolbar>\n</ion-content>");

/***/ }),

/***/ 79759:
/*!************************************************************!*\
  !*** ./src/app/User/registration1/registration1.page.scss ***!
  \************************************************************/
/***/ ((module) => {

module.exports = ".step {\n  background: #fff0e3;\n  padding: 8px 0;\n}\n.step ul {\n  padding: 0;\n  width: 300px;\n  margin: 0 auto;\n  margin-bottom: 5px;\n  list-style: none;\n  text-align: center;\n  position: relative;\n}\n.step ul li {\n  display: inline-block;\n  position: relative;\n  padding: 0 15%;\n  z-index: 999;\n}\n.step ul li a {\n  display: block;\n  width: 22px;\n  height: 22px;\n  background: #fff;\n  border: #f38320 solid 1px;\n  border-radius: 50%;\n  position: relative;\n}\n.step ul li span {\n  display: block;\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 7px 0 0 -5px;\n}\n.step ul li:first-child {\n  padding-left: 0;\n}\n.step ul li:last-child {\n  padding-right: 0;\n  text-align: right;\n}\n.step ul li.activestep a {\n  width: 30px;\n  height: 30px;\n  background: #f38320;\n  position: relative;\n  z-index: 999;\n  margin-top: 0;\n  margin-left: -3px;\n  top: 5px;\n}\n.step ul li.activestep a:after {\n  width: 13px;\n  height: 5px;\n  content: \"\";\n  position: absolute;\n  border-left: 3px #ffffff solid;\n  border-bottom: 3px #ffffff solid;\n  transform: rotate(-45deg);\n  left: 6px;\n  top: 8px;\n}\n.step ul:after {\n  background: #cccccc;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 18px;\n  width: 80%;\n  height: 2px;\n  content: \"\";\n  margin: 0 auto;\n  z-index: 0;\n}\n.register_panel {\n  padding: 20px;\n  padding-bottom: 0;\n}\n.sml_cus_btn {\n  box-shadow: #fdb77a 0 0 15px 5px;\n  margin: 20px;\n  border-radius: 30px;\n}\n.cust_input:hover {\n  border-color: #f38320;\n  box-shadow: #fff0e3 0 0 6px 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdHJhdGlvbjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFBcUIsY0FBQTtBQUV6QjtBQURJO0VBQ0ksVUFBQTtFQUFZLFlBQUE7RUFBYyxjQUFBO0VBQWdCLGtCQUFBO0VBQW9CLGdCQUFBO0VBQWtCLGtCQUFBO0VBQW9CLGtCQUFBO0FBUzVHO0FBUlE7RUFDSSxxQkFBQTtFQUF1QixrQkFBQTtFQUFvQixjQUFBO0VBQWdCLFlBQUE7QUFhdkU7QUFaWTtFQUFHLGNBQUE7RUFBZ0IsV0FBQTtFQUFhLFlBQUE7RUFBYyxnQkFBQTtFQUFrQix5QkFBQTtFQUEwQixrQkFBQTtFQUFtQixrQkFBQTtBQXFCekg7QUFwQlk7RUFBTSxjQUFBO0VBQWdCLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxvQkFBQTtBQTRCM0g7QUExQlE7RUFBZ0IsZUFBQTtBQTZCeEI7QUE1QlE7RUFBZSxnQkFBQTtFQUFrQixpQkFBQTtBQWdDekM7QUEvQlE7RUFBa0IsV0FBQTtFQUFhLFlBQUE7RUFBYyxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixZQUFBO0VBQWMsYUFBQTtFQUFlLGlCQUFBO0VBQW1CLFFBQUE7QUF5QzlJO0FBeENRO0VBQXVCLFdBQUE7RUFBYSxXQUFBO0VBQWEsV0FBQTtFQUFhLGtCQUFBO0VBQW9CLDhCQUFBO0VBQWdDLGdDQUFBO0VBQWtDLHlCQUFBO0VBQTJCLFNBQUE7RUFBVyxRQUFBO0FBbURsTTtBQWhESTtFQUNJLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW9CLE9BQUE7RUFBUyxRQUFBO0VBQVUsU0FBQTtFQUFXLFVBQUE7RUFBWSxXQUFBO0VBQWEsV0FBQTtFQUFhLGNBQUE7RUFBZ0IsVUFBQTtBQTJEckk7QUF4REE7RUFBaUIsYUFBQTtFQUFlLGlCQUFBO0FBNkRoQztBQTVEQTtFQUFjLGdDQUFBO0VBQWtDLFlBQUE7RUFBYyxtQkFBQTtBQWtFOUQ7QUFqRUE7RUFBbUIscUJBQUE7RUFBdUIsK0JBQUE7QUFzRTFDIiwiZmlsZSI6InJlZ2lzdHJhdGlvbjEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN0ZXB7IFxyXG4gICAgYmFja2dyb3VuZDogI2ZmZjBlMzsgcGFkZGluZzogOHB4IDA7XHJcbiAgICB1bHtcclxuICAgICAgICBwYWRkaW5nOiAwOyB3aWR0aDogMzAwcHg7IG1hcmdpbjogMCBhdXRvOyBtYXJnaW4tYm90dG9tOiA1cHg7IGxpc3Qtc3R5bGU6IG5vbmU7IHRleHQtYWxpZ246IGNlbnRlcjsgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGxpeyBcclxuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrOyBwb3NpdGlvbjogcmVsYXRpdmU7IHBhZGRpbmc6IDAgMTUlOyB6LWluZGV4OiA5OTk7XHJcbiAgICAgICAgICAgIGF7IGRpc3BsYXk6IGJsb2NrOyB3aWR0aDogMjJweDsgaGVpZ2h0OiAyMnB4OyBiYWNrZ3JvdW5kOiAjZmZmOyBib3JkZXI6I2YzODMyMCBzb2xpZCAxcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBwb3NpdGlvbjogcmVsYXRpdmU7IH0gIFxyXG4gICAgICAgICAgICBzcGFueyBkaXNwbGF5OiBibG9jazsgY29sb3I6ICM2NjY2NjY7IGZvbnQtc2l6ZTogMTNweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6ICdPcGVuIFNhbnMnLCBzYW5zLXNlcmlmOyBtYXJnaW46IDdweCAwIDAgLTVweDsgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBsaTpmaXJzdC1jaGlsZHsgcGFkZGluZy1sZWZ0OiAwOyB9XHJcbiAgICAgICAgbGk6bGFzdC1jaGlsZHsgcGFkZGluZy1yaWdodDogMDsgdGV4dC1hbGlnbjogcmlnaHQ7IH1cclxuICAgICAgICBsaS5hY3RpdmVzdGVwIGEgeyB3aWR0aDogMzBweDsgaGVpZ2h0OiAzMHB4OyBiYWNrZ3JvdW5kOiAjZjM4MzIwOyBwb3NpdGlvbjogcmVsYXRpdmU7IHotaW5kZXg6IDk5OTsgbWFyZ2luLXRvcDogMDsgbWFyZ2luLWxlZnQ6IC0zcHg7IHRvcDogNXB4OyB9XHJcbiAgICAgICAgbGkuYWN0aXZlc3RlcCBhOmFmdGVyeyB3aWR0aDogMTNweDsgaGVpZ2h0OiA1cHg7IGNvbnRlbnQ6IFwiXCI7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgYm9yZGVyLWxlZnQ6IDNweCAjZmZmZmZmIHNvbGlkOyBib3JkZXItYm90dG9tOiAzcHggI2ZmZmZmZiBzb2xpZDsgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTsgbGVmdDogNnB4OyB0b3A6IDhweDsgfVxyXG4gICAgXHJcbiAgICB9XHJcbiAgICB1bDphZnRlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2NjY2NjYzsgcG9zaXRpb246IGFic29sdXRlOyBsZWZ0OiAwOyByaWdodDogMDsgdG9wOiAxOHB4OyB3aWR0aDogODAlOyBoZWlnaHQ6IDJweDsgY29udGVudDogXCJcIjsgbWFyZ2luOiAwIGF1dG87IHotaW5kZXg6IDA7XHJcbiAgICB9XHJcbn1cclxuLnJlZ2lzdGVyX3BhbmVseyBwYWRkaW5nOiAyMHB4OyBwYWRkaW5nLWJvdHRvbTogMDsgfVxyXG4uc21sX2N1c19idG57IGJveC1zaGFkb3c6ICNmZGI3N2EgMCAwIDE1cHggNXB4OyBtYXJnaW46IDIwcHg7IGJvcmRlci1yYWRpdXM6IDMwcHg7IH1cclxuLmN1c3RfaW5wdXQ6aG92ZXJ7IGJvcmRlci1jb2xvcjogI2YzODMyMDsgYm94LXNoYWRvdzogI2ZmZjBlMyAwIDAgNnB4IDRweDt9XHJcbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_registration1_registration1_module_ts.js.map