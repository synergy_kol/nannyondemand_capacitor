"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_changepassword_changepassword_module_ts"],{

/***/ 26073:
/*!**********************************************************************!*\
  !*** ./src/app/User/changepassword/changepassword-routing.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangepasswordPageRoutingModule": () => (/* binding */ ChangepasswordPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _changepassword_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./changepassword.page */ 71430);




const routes = [
    {
        path: '',
        component: _changepassword_page__WEBPACK_IMPORTED_MODULE_0__.ChangepasswordPage
    }
];
let ChangepasswordPageRoutingModule = class ChangepasswordPageRoutingModule {
};
ChangepasswordPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ChangepasswordPageRoutingModule);



/***/ }),

/***/ 62465:
/*!**************************************************************!*\
  !*** ./src/app/User/changepassword/changepassword.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangepasswordPageModule": () => (/* binding */ ChangepasswordPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _changepassword_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./changepassword-routing.module */ 26073);
/* harmony import */ var _changepassword_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./changepassword.page */ 71430);







let ChangepasswordPageModule = class ChangepasswordPageModule {
};
ChangepasswordPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _changepassword_routing_module__WEBPACK_IMPORTED_MODULE_0__.ChangepasswordPageRoutingModule
        ],
        declarations: [_changepassword_page__WEBPACK_IMPORTED_MODULE_1__.ChangepasswordPage]
    })
], ChangepasswordPageModule);



/***/ }),

/***/ 71430:
/*!************************************************************!*\
  !*** ./src/app/User/changepassword/changepassword.page.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangepasswordPage": () => (/* binding */ ChangepasswordPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_changepassword_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./changepassword.page.html */ 23562);
/* harmony import */ var _changepassword_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./changepassword.page.scss */ 87105);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let ChangepasswordPage = class ChangepasswordPage {
    constructor(storage, authService, loadingController, toastController, navCtrl) {
        this.storage = storage;
        this.authService = authService;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.isLoading = false;
        this.isDisabled = false;
        this.userdata = [];
    }
    onKeyboard(event) {
        if (event != null) {
            event.setFocus();
        }
        else {
            this.onUpdate();
        }
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.loginToken();
    }
    ngOnInit() {
    }
    onUpdate() {
        const passPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/;
        if (this.password == null) {
            this.presentToast('Please enter your Old password');
        }
        else if (this.new_password == null) {
            this.presentToast('Please enter New password');
        }
        else if (this.new_password.length < 8) {
            this.presentToast('Please enter Minimum 8 Characters.');
        }
        else if (!passPattern.test(this.new_password)) {
            this.presentToast('Password must be contain atleast one lowercase and one uppercase letter one digit and one special character.');
        }
        else if (this.confirm_password == null) {
            this.presentToast('Please enter confirm New password');
        }
        else if (this.confirm_password != this.new_password) {
            this.presentToast('New Password and Confirm new password must match');
        }
        else {
            this.showLoader('Please wait...');
            this.storage.get('userDetails').then((val) => {
                this.userdata = val;
                console.log("profile: ", val);
                var body = {
                    u_id: this.userdata.user_id,
                    oldPassword: this.password,
                    newPassword: this.new_password
                };
                this.authService.postData("change-password", body).then(result => {
                    this.data = result;
                    console.log("changePassword: ", this.data);
                    if (this.data.status.error_code == 0) {
                        this.presentToast("Password has been Changed Successfully.");
                        this.navCtrl.pop();
                    }
                    else {
                        this.presentToast(this.data.status.message);
                    }
                    this.hideLoader();
                }, error => {
                    console.log(error);
                    this.hideLoader();
                });
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
ChangepasswordPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController }
];
ChangepasswordPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-changepassword',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_changepassword_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_changepassword_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ChangepasswordPage);



/***/ }),

/***/ 23562:
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/changepassword/changepassword.page.html ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Change Password</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div style=\"padding:20px;\">\n    <ion-input class=\"cust_input\" placeholder=\"Old Password\" type=\"password\" [(ngModel)]=\"password\" (keyup.enter)=\"onKeyboard(Field1)\"></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"New Password\" type=\"password\" [(ngModel)]=\"new_password\" #Field1\n        (keyup.enter)=\"onKeyboard(Field2)\"></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"Confirm Password\" type=\"password\" [(ngModel)]=\"confirm_password\" #Field2\n        (keyup.enter)=\"onKeyboard()\"></ion-input>\n      <ion-button color=\"warning\" expand=\"full\" shape=\"round\" (click)=\"onUpdate()()\" class=\"cus_btn\">Update</ion-button>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 87105:
/*!**************************************************************!*\
  !*** ./src/app/User/changepassword/changepassword.page.scss ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = ".cust_input:hover {\n  border-color: #f38320;\n  box-shadow: #fff0e3 0 0 6px 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNoYW5nZXBhc3N3b3JkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFtQixxQkFBQTtFQUF1QiwrQkFBQTtBQUcxQyIsImZpbGUiOiJjaGFuZ2VwYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY3VzdF9pbnB1dDpob3ZlcnsgYm9yZGVyLWNvbG9yOiAjZjM4MzIwOyBib3gtc2hhZG93OiAjZmZmMGUzIDAgMCA2cHggNHB4O30iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_changepassword_changepassword_module_ts.js.map