"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerhelp_providerhelp_module_ts"],{

/***/ 130:
/*!**********************************************************************!*\
  !*** ./src/app/Provider/providerhelp/providerhelp-routing.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderhelpPageRoutingModule": () => (/* binding */ ProviderhelpPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerhelp_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerhelp.page */ 7591);




const routes = [
    {
        path: '',
        component: _providerhelp_page__WEBPACK_IMPORTED_MODULE_0__.ProviderhelpPage
    }
];
let ProviderhelpPageRoutingModule = class ProviderhelpPageRoutingModule {
};
ProviderhelpPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProviderhelpPageRoutingModule);



/***/ }),

/***/ 62693:
/*!**************************************************************!*\
  !*** ./src/app/Provider/providerhelp/providerhelp.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderhelpPageModule": () => (/* binding */ ProviderhelpPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerhelp_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerhelp-routing.module */ 130);
/* harmony import */ var _providerhelp_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerhelp.page */ 7591);







let ProviderhelpPageModule = class ProviderhelpPageModule {
};
ProviderhelpPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerhelp_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProviderhelpPageRoutingModule
        ],
        declarations: [_providerhelp_page__WEBPACK_IMPORTED_MODULE_1__.ProviderhelpPage]
    })
], ProviderhelpPageModule);



/***/ }),

/***/ 7591:
/*!************************************************************!*\
  !*** ./src/app/Provider/providerhelp/providerhelp.page.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderhelpPage": () => (/* binding */ ProviderhelpPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerhelp_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerhelp.page.html */ 86046);
/* harmony import */ var _providerhelp_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerhelp.page.scss */ 60386);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let ProviderhelpPage = class ProviderhelpPage {
    constructor(authService, loadingController, storage, navCtrl) {
        this.authService = authService;
        this.loadingController = loadingController;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.isLoading = false;
        this.data = [];
        this.pageData = '';
        this.userdata = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.loginToken();
    }
    getDetails() {
        var body = {
            page_slug: "helps",
        };
        this.authService.postData("get-cms-content", body).then(result => {
            this.data = result;
            this.pageData = this.data.result.data.description;
            console.log("Content: ", this.data);
            this.hideLoader();
        }, error => {
            this.hideLoader();
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
ProviderhelpPage.ctorParameters = () => [
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController }
];
ProviderhelpPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-providerhelp',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerhelp_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerhelp_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProviderhelpPage);



/***/ }),

/***/ 86046:
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerhelp/providerhelp.page.html ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Help</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div style=\"padding: 20px; padding-top: 5px;\" [innerHTML]=\"pageData\"></div>\n</ion-content>");

/***/ }),

/***/ 60386:
/*!**************************************************************!*\
  !*** ./src/app/Provider/providerhelp/providerhelp.page.scss ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = "h4 {\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 14px;\n  color: #222222;\n  margin-bottom: 5px;\n}\n\np {\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  font-size: 13px;\n  color: #666666;\n  margin: 5px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVyaGVscC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBRyxnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxlQUFBO0VBQWlCLGNBQUE7RUFBZ0Isa0JBQUE7QUFNNUY7O0FBTEE7RUFBRSxnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxlQUFBO0VBQWlCLGNBQUE7RUFBZ0IsYUFBQTtBQWEzRiIsImZpbGUiOiJwcm92aWRlcmhlbHAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDR7Zm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMTRweDsgY29sb3I6ICMyMjIyMjI7IG1hcmdpbi1ib3R0b206IDVweDt9XHJcbnB7Zm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGZvbnQtc2l6ZTogMTNweDsgY29sb3I6ICM2NjY2NjY7IG1hcmdpbjogNXB4IDA7fSJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerhelp_providerhelp_module_ts.js.map