"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_successpayment_successpayment_module_ts"],{

/***/ 38795:
/*!**********************************************************************!*\
  !*** ./src/app/User/successpayment/successpayment-routing.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SuccesspaymentPageRoutingModule": () => (/* binding */ SuccesspaymentPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _successpayment_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./successpayment.page */ 30383);




const routes = [
    {
        path: '',
        component: _successpayment_page__WEBPACK_IMPORTED_MODULE_0__.SuccesspaymentPage
    }
];
let SuccesspaymentPageRoutingModule = class SuccesspaymentPageRoutingModule {
};
SuccesspaymentPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SuccesspaymentPageRoutingModule);



/***/ }),

/***/ 97030:
/*!**************************************************************!*\
  !*** ./src/app/User/successpayment/successpayment.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SuccesspaymentPageModule": () => (/* binding */ SuccesspaymentPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _successpayment_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./successpayment-routing.module */ 38795);
/* harmony import */ var _successpayment_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./successpayment.page */ 30383);







let SuccesspaymentPageModule = class SuccesspaymentPageModule {
};
SuccesspaymentPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _successpayment_routing_module__WEBPACK_IMPORTED_MODULE_0__.SuccesspaymentPageRoutingModule
        ],
        declarations: [_successpayment_page__WEBPACK_IMPORTED_MODULE_1__.SuccesspaymentPage]
    })
], SuccesspaymentPageModule);



/***/ }),

/***/ 30383:
/*!************************************************************!*\
  !*** ./src/app/User/successpayment/successpayment.page.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SuccesspaymentPage": () => (/* binding */ SuccesspaymentPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_successpayment_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./successpayment.page.html */ 48637);
/* harmony import */ var _successpayment_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./successpayment.page.scss */ 9157);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 78099);





let SuccesspaymentPage = class SuccesspaymentPage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ngOnInit() {
    }
    goBooking() {
        this.navCtrl.navigateRoot('/mybookings');
    }
};
SuccesspaymentPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.NavController }
];
SuccesspaymentPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-successpayment',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_successpayment_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_successpayment_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SuccesspaymentPage);



/***/ }),

/***/ 48637:
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/successpayment/successpayment.page.html ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"congratulation_screen\">\n    <div class=\"congratulation_panel\">\n      <img src=\"assets/images/congratulation_icon.png\" alt=\"congratulation_img\" />\n      <h3>Congratulations!</h3>\n      <p>Booking has been confirmed, Please your email for details.</p>\n      <ion-button color=\"warning\" expand=\"full\" shape=\"round\" (click)=\"goBooking()\" class=\"cus_btn\">Go To My Bookings\n      </ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 9157:
/*!**************************************************************!*\
  !*** ./src/app/User/successpayment/successpayment.page.scss ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = ".congratulation_screen {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  display: table;\n  padding: 40px;\n}\n.congratulation_screen .congratulation_panel {\n  display: table-cell;\n  text-align: center;\n  vertical-align: middle;\n}\n.congratulation_screen .congratulation_panel img {\n  width: 70%;\n}\n.congratulation_screen .congratulation_panel h3 {\n  padding: 40px 0 10px 0;\n  margin: 0;\n  color: #333333;\n  font-size: 26px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.congratulation_screen .congratulation_panel p {\n  margin: 0;\n  margin-bottom: 40px;\n  color: #666666;\n  font-size: 16px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  line-height: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN1Y2Nlc3NwYXltZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFBYSxZQUFBO0VBQWMsa0JBQUE7RUFBb0IsY0FBQTtFQUFnQixhQUFBO0FBS25FO0FBSkk7RUFBdUIsbUJBQUE7RUFBcUIsa0JBQUE7RUFBb0Isc0JBQUE7QUFTcEU7QUFSUTtFQUFLLFVBQUE7QUFXYjtBQVZRO0VBQUksc0JBQUE7RUFBd0IsU0FBQTtFQUFXLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtBQWtCbEc7QUFqQlE7RUFBRyxTQUFBO0VBQVcsbUJBQUE7RUFBc0IsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGlCQUFBO0FBMEJySSIsImZpbGUiOiJzdWNjZXNzcGF5bWVudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29uZ3JhdHVsYXRpb25fc2NyZWVue1xyXG4gICAgd2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgcG9zaXRpb246IGFic29sdXRlOyBkaXNwbGF5OiB0YWJsZTsgcGFkZGluZzogNDBweDtcclxuICAgIC5jb25ncmF0dWxhdGlvbl9wYW5lbHsgZGlzcGxheTogdGFibGUtY2VsbDsgdGV4dC1hbGlnbjogY2VudGVyOyB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgICAgIGltZ3sgd2lkdGg6IDcwJTsgfVxyXG4gICAgICAgIGgzeyBwYWRkaW5nOiA0MHB4IDAgMTBweCAwOyBtYXJnaW46IDA7IGNvbG9yOiAjMzMzMzMzOyBmb250LXNpemU6IDI2cHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICAgICAgcHsgbWFyZ2luOiAwOyBtYXJnaW4tYm90dG9tOiA0MHB4OyAgY29sb3I6ICM2NjY2NjY7IGZvbnQtc2l6ZTogMTZweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGxpbmUtaGVpZ2h0OiAyNHB4OyB9XHJcbiAgICB9XHJcbn1cclxuIl19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_successpayment_successpayment_module_ts.js.map