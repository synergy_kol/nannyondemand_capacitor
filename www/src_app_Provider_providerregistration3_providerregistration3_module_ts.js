"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerregistration3_providerregistration3_module_ts"],{

/***/ 14988:
/*!****************************************************************************************!*\
  !*** ./src/app/Provider/providerregistration3/providerregistration3-routing.module.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Providerregistration3PageRoutingModule": () => (/* binding */ Providerregistration3PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerregistration3_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerregistration3.page */ 50676);




const routes = [
    {
        path: '',
        component: _providerregistration3_page__WEBPACK_IMPORTED_MODULE_0__.Providerregistration3Page
    }
];
let Providerregistration3PageRoutingModule = class Providerregistration3PageRoutingModule {
};
Providerregistration3PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], Providerregistration3PageRoutingModule);



/***/ }),

/***/ 37702:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providerregistration3/providerregistration3.module.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Providerregistration3PageModule": () => (/* binding */ Providerregistration3PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerregistration3_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerregistration3-routing.module */ 14988);
/* harmony import */ var _providerregistration3_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerregistration3.page */ 50676);







let Providerregistration3PageModule = class Providerregistration3PageModule {
};
Providerregistration3PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerregistration3_routing_module__WEBPACK_IMPORTED_MODULE_0__.Providerregistration3PageRoutingModule
        ],
        declarations: [_providerregistration3_page__WEBPACK_IMPORTED_MODULE_1__.Providerregistration3Page]
    })
], Providerregistration3PageModule);



/***/ }),

/***/ 50676:
/*!******************************************************************************!*\
  !*** ./src/app/Provider/providerregistration3/providerregistration3.page.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Providerregistration3Page": () => (/* binding */ Providerregistration3Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerregistration3_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerregistration3.page.html */ 6104);
/* harmony import */ var _providerregistration3_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerregistration3.page.scss */ 20565);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let Providerregistration3Page = class Providerregistration3Page {
    constructor(navCtrl, loadingController, toastController, storage, authService) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.storage = storage;
        this.authService = authService;
        this.pop = false;
        this.experience = '';
        this.ratetype = '';
        this.rate = '';
        this.availableList = [];
        this.selectDay = '';
        this.selectTime = '';
        this.interest_in = '';
        this.data = [];
        this.userData = [];
        this.preData = '';
        this.isDisabled = false;
        this.isLoading = false;
        this.profileData = [];
        this.hideextra = true;
        this.userId = '';
        this.userdata = [];
        this.step3Data = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    getDetails() {
        this.storage.get('providerRegsData').then((result) => {
            this.hideLoader();
            this.preData = result;
            console.log('PreStep Data:', this.preData);
        });
    }
    ionViewWillEnter() {
        this.showLoader('Loading...');
        this.storage.get('userDetails').then((val) => {
            if (val != null) {
                this.userId = val.user_id;
                this.hideextra = false;
                this.userData = val;
                this.getProfileDetails();
            }
            else {
                this.storage.get('setp3').then((val) => {
                    this.step3Data = val;
                    this.interest_in = this.step3Data.interest_in;
                    this.experience = this.step3Data.experience;
                    this.ratetype = this.step3Data.rate_type;
                    this.rate = this.step3Data.rate;
                });
            }
        });
        this.getDetails();
        this.loginToken();
    }
    getProfileDetails() {
        var body = {
            user_id: this.userData.user_id,
        };
        this.authService.postData("get-profile", body).then(result => {
            this.data = result;
            console.log("profile: ", this.data);
            this.profileData = this.data.result.data;
            this.interest_in = this.profileData.interest_in;
            this.experience = this.profileData.experience;
            this.ratetype = this.profileData.rate_type;
            this.rate = this.profileData.rate;
            this.availableList = this.profileData.availabilities;
            this.hideLoader();
        }, error => {
            this.hideLoader();
        });
    }
    openPop() {
        this.pop = true;
    }
    closepop() {
        this.pop = false;
    }
    onSave(findday) {
        var somethingIsNotString = false;
        this.availableList.forEach(function (item) {
            if (item.day == findday) {
                somethingIsNotString = true;
            }
        });
        if (this.selectDay == "") {
            this.presentToast("Please Select Day");
        }
        else if (this.selectTime == "") {
            this.presentToast("Please Select Time");
        }
        else {
            this.showLoader('Please wait...');
            if (somethingIsNotString) {
                this.presentToast('Sorry! "' + findday + '" is Already in Availibility List.');
                this.hideLoader();
            }
            else {
                let data = {
                    day: this.selectDay,
                    time: this.selectTime
                };
                this.availableList.push(data);
                this.presentToast('Successfully Saved');
                setTimeout(() => {
                    this.pop = false;
                    this.selectDay = '';
                    this.selectTime = '';
                    this.hideLoader();
                    console.log(this.availableList);
                }, 3000);
            }
        }
    }
    rmSchedule(ind) {
        this.availableList.splice(ind, 1);
        console.log(this.availableList);
    }
    onBack() {
        this.navCtrl.pop();
    }
    onSubmit() {
        if (this.interest_in == '') {
            this.presentToast('Please Choose Interest In.');
        }
        else if (this.preData.servicetype != 3 && this.experience.trim() == "") {
            this.presentToast('Please enter your Year of Experience.');
        }
        else if (this.experience >= this.calculateAge(this.preData.dob)) {
            this.presentToast('Please make sure your Year of Experience not greater than or equal with you Age.');
        }
        else if (this.ratetype == '') {
            this.presentToast('Please Choose Rate Type.');
        }
        else if (this.rate.trim() == '') {
            this.presentToast('Please enter Rate');
        }
        else if (this.availableList == '') {
            this.presentToast('Please add minimum One Schedule.');
        }
        else {
            this.showLoader('Please wait...');
            var body = {
                service_type: this.preData.servicetype,
                f_name: this.preData.fname,
                l_name: this.preData.lname,
                dob: this.preData.dob,
                phone: this.preData.phone,
                emg_phone: this.preData.emgnphone,
                email: this.preData.email,
                password: this.preData.password,
                address: this.preData.address,
                state: this.preData.state,
                city: this.preData.city,
                zip: this.preData.zip,
                about_note: this.preData.aboutnote,
                identy_type: this.preData.identytype,
                will_driver: this.preData.willdriver,
                is_smoke: this.preData.issmoke,
                comfortable_pet: this.preData.comfortablepet,
                identity_documents: this.preData.identitydocuments,
                certificates: this.preData.certificates,
                intervention_reports: this.preData.interventionreports,
                home_images: this.preData.homeimages,
                police_doc: this.preData.policeimages,
                website: this.preData.website,
                interest_in: this.interest_in,
                experience: this.experience,
                rate_type: this.ratetype,
                rate: this.rate,
                availability: this.availableList,
            };
            this.authService.postData("provider-registration", body).then(result => {
                this.hideLoader();
                this.data = result;
                this.userData = this.data.result.data;
                console.log("Registration: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.storage.remove('providerRegsData');
                    this.storage.remove('setp1');
                    this.storage.remove('setp2');
                    this.storage.remove('setp3');
                    this.presentToast("Thanks! You has been succesfully Registered.");
                    this.storage.set("userDetails", this.userData);
                    this.navCtrl.navigateRoot('/providercongratulation');
                }
                else {
                    this.presentToast(this.data.status.message);
                    var object = {
                        interest_in: this.interest_in,
                        experience: this.experience,
                        rate_type: this.ratetype,
                        rate: this.rate,
                    };
                    this.storage.set("setp3", object);
                }
            }, error => {
                this.hideLoader();
            });
        }
    }
    calculateAge(dob) {
        this.age = dob;
        var dob_entry = this.preData.dob;
        var split_dob = dob_entry.split("T");
        var date_only = split_dob[0];
        var split_date_only = date_only.split("-");
        var month = split_date_only[1];
        var day = split_date_only[2];
        var year = split_date_only[0];
        var today = new Date();
        var age = today.getFullYear() - year;
        if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
            age--;
        }
        return age;
    }
    oneditSave() {
        if (this.experience.trim() == "") {
            this.presentToast('Please enter your Year of Experience.');
        }
        else if (this.experience >= this.calculateAge(this.preData.dob)) {
            this.presentToast('Please make sure your Year of Experience not greater than or equal with you Age.');
        }
        else if (this.ratetype == '') {
            this.presentToast('Please Choose Rate Type.');
        }
        else if (this.rate.trim() == '') {
            this.presentToast('Please enter Rate');
        }
        else if (this.availableList == '') {
            this.presentToast('Please add minimum One Schedule.');
        }
        else {
            this.showLoader('Please wait...');
            var body = {
                user_id: this.userId,
                service_type: this.preData.servicetype,
                f_name: this.preData.fname,
                l_name: this.preData.lname,
                dob: this.preData.dob,
                phone: this.preData.phone,
                emg_phone: this.preData.emgnphone,
                email: this.preData.email,
                address: this.preData.address,
                state: this.preData.state,
                city: this.preData.city,
                zip: this.preData.zip,
                about_note: this.preData.aboutnote,
                identy_type: this.preData.identytype,
                will_driver: this.preData.willdriver,
                is_smoke: this.preData.issmoke,
                comfortable_pet: this.preData.comfortablepet,
                identity_documents: this.preData.identitydocuments,
                certificates: this.preData.certificates,
                intervention_reports: this.preData.interventionreports,
                home_images: this.preData.homeimages,
                police_doc: this.preData.policeimages,
                interest_in: this.interest_in,
                experience: this.experience,
                rate_type: this.ratetype,
                rate: this.rate,
                availability: this.availableList,
            };
            this.authService.postData("update-provider", body).then(result => {
                this.hideLoader();
                this.data = result;
                this.userData = this.data.result.data;
                console.log("Edit Registration: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.storage.remove('providerRegsData');
                    this.presentToast("Thanks! You has been succesfully Updated.");
                    this.navCtrl.navigateRoot('/dashboard');
                }
                else {
                    this.presentToast(this.data.status.message);
                }
            }, error => {
                this.hideLoader();
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
Providerregistration3Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider }
];
Providerregistration3Page = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-providerregistration3',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerregistration3_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerregistration3_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], Providerregistration3Page);



/***/ }),

/***/ 6104:
/*!***********************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerregistration3/providerregistration3.page.html ***!
  \***********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title *ngIf=\"hideextra\">Become A Nanny</ion-title>\n    <ion-title *ngIf=\"!hideextra\">Edit Profile</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"step\">\n    <ul>\n      <li class=\"activestep\"><a></a><span>Step 1</span></li>\n      <li class=\"activestep\"><a></a><span>Step 2</span></li>\n      <li class=\"activestep\"><a></a><span>Step 3</span></li>\n    </ul>\n  </div>\n  <div class=\"register_panel\">\n    <p style=\"color: #f44336; font-size: 12px; margin: 0 0 15px 0; text-align: right;\">* Below all fields are mandatory *\n    </p>\n    <ion-grid class=\"hourlyplan\">\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"line-height: 50px; font-size: 14px;\">Interest In</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-select class=\"cust_input\" placeholder=\"Choose One\" [(ngModel)]=\"interest_in\" [interfaceOptions]=\"customActionSheetOptions\"\n            interface=\"action-sheet\" style=\"margin: 0;\">\n            <ion-select-option value=\"Live-In\">Live-In</ion-select-option>\n            <ion-select-option value=\"Live-Out\">Live-Out</ion-select-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"preData.servicetype != 3\">\n        <ion-col>\n          <ion-label style=\"line-height: 50px; font-size: 14px;\">Experience</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-input [(ngModel)]=\"experience\" placeholder=\"In Year\" type=\"tel\" class=\"cust_input\" style=\"margin: 0;\"></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-label style=\"line-height: 50px; font-size: 14px;\">Select Rate</ion-label>\n        </ion-col>\n        <ion-col>\n          <ion-select class=\"cust_input\" style=\"margin: 0; min-width: 130px; padding: 0 12px !important;\"\n            placeholder=\"Choose Type\" [(ngModel)]=\"ratetype\" [interfaceOptions]=\"customActionSheetOptions\"\n            interface=\"action-sheet\">\n            <ion-select-option value=\"Hourly\">Hourly</ion-select-option>\n          </ion-select>\n        </ion-col>\n        <ion-col>\n          <ion-input [(ngModel)]=\"rate\" class=\"cust_input amount\" style=\"margin: 0;\"><span>$</span></ion-input>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-item lines=\"none\" style=\"margin: 15px 0;\">\n      <h3 slot=\"start\" style=\"font-weight: bold; margin: 0; font-size: 20px;\">Available On</h3>\n      <ion-button color=\"dark\" slot=\"end\" shape=\"round\" size=\"small\" style=\"width: 130px; height: 35px;\"\n        (click)=\"openPop()\">Add Schedule</ion-button>\n    </ion-item>\n    <ion-row *ngFor=\"let item of availableList; let i = index\" style=\"padding: 0 15px;\">      \n      <ion-col size=\"1\" style=\"padding: 15px 0px;\">\n        <ion-checkbox style=\"margin: 0; margin-right: 10px;\" color=\"success\" checked=\"true\" (ionChange)=\"rmSchedule(i)\"></ion-checkbox>\n      </ion-col>\n      <ion-col size=\"6\" style=\"padding: 15px 10px;\">\n        <ion-label class=\"uplaodtext\">{{item.day}}</ion-label>\n      </ion-col>\n      <ion-col size=\"5\">\n        <div class=\"cust_input daychoose\">{{item.time}}</div>\n      </ion-col>\n    </ion-row>\n  </div>\n  \n</ion-content>\n\n<ion-footer>\n  <ion-toolbar class=\"ftr\">\n    <ion-button color=\"dark\" slot=\"start\" fill=\"clear\" shape=\"round\" (click)=\"onBack()\" class=\"sml_cus_btn\"\n      style=\"box-shadow: none;\">\n      <img src=\"assets/images/black_l_arrow.png\" />&nbsp; Back</ion-button>\n    <ion-button *ngIf=\"hideextra\" color=\"success\" slot=\"end\" shape=\"round\" (click)=\"onSubmit()\" class=\"sml_cus_btn\">Submit</ion-button>\n    <ion-button *ngIf=\"!hideextra\" color=\"success\" slot=\"end\" shape=\"round\" (click)=\"oneditSave()\" class=\"sml_cus_btn\">Save</ion-button>\n  </ion-toolbar>\n</ion-footer>\n\n<div class=\"custompop\" *ngIf=\"pop\">\n  <div class=\"custompop_in\">\n    <div class=\"popin\">\n      <h3>Choose Your Availibility</h3>\n      <ion-select class=\"cust_input\" [(ngModel)]=\"selectDay\" placeholder=\"Choose Day\"\n        [interfaceOptions]=\"customActionSheetOptions\" interface=\"action-sheet\">\n        <ion-select-option value=\"Monday\">Monday</ion-select-option>\n        <ion-select-option value=\"Tuesday\">Tuesday</ion-select-option>\n        <ion-select-option value=\"Wednesday\">Wednesday</ion-select-option>\n        <ion-select-option value=\"Thursday\">Thursday</ion-select-option>\n        <ion-select-option value=\"Friday\">Friday</ion-select-option>\n        <ion-select-option value=\"Saturday\">Saturday</ion-select-option>\n        <ion-select-option value=\"Sunday\">Sunday</ion-select-option>\n      </ion-select>\n      <ion-select class=\"cust_input\" [(ngModel)]=\"selectTime\" placeholder=\"Choose Timing\"\n        [interfaceOptions]=\"customActionSheetOptions\" interface=\"action-sheet\">\n        <ion-select-option value=\"Full Day\">Full Day</ion-select-option>\n        <ion-select-option value=\"Morning\">Morning</ion-select-option>\n        <ion-select-option value=\"Night\">Night</ion-select-option>\n      </ion-select>\n      <div style=\"display: flex; margin-top: 20px;\">\n        <ion-button color=\"medium\" expand=\"full\" shape=\"round\" fill=\"outline\" (click)=\"closepop()\" class=\"sml_cus_btn\"\n          style=\"width: 100%; box-shadow: none; margin: 0 3px;\">Cancel</ion-button>\n        <ion-button color=\"success\" expand=\"full\" shape=\"round\" (click)=\"onSave(selectDay)\" class=\"sml_cus_btn\"\n          style=\"width: 100%; box-shadow: none; margin: 0 3px;\">Save</ion-button>\n      </div>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ 20565:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providerregistration3/providerregistration3.page.scss ***!
  \********************************************************************************/
/***/ ((module) => {

module.exports = ".step {\n  background: #e7fef2;\n  padding: 8px 0;\n}\n.step ul {\n  padding: 0;\n  width: 300px;\n  margin: 0 auto;\n  margin-bottom: 5px;\n  list-style: none;\n  text-align: center;\n  position: relative;\n}\n.step ul li {\n  display: inline-block;\n  position: relative;\n  padding: 0 15%;\n  z-index: 999;\n}\n.step ul li a {\n  display: block;\n  width: 22px;\n  height: 22px;\n  background: #fff;\n  border: #41b578 solid 1px;\n  border-radius: 50%;\n  position: relative;\n}\n.step ul li span {\n  display: block;\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  margin: 7px 0 0 -5px;\n}\n.step ul li:first-child {\n  padding-left: 0;\n}\n.step ul li:last-child {\n  padding-right: 0;\n  text-align: right;\n}\n.step ul li.activestep a {\n  width: 30px;\n  height: 30px;\n  background: #41b578;\n  position: relative;\n  z-index: 999;\n  margin-top: 0;\n  margin-left: -3px;\n  top: 5px;\n}\n.step ul li.activestep a:after {\n  width: 13px;\n  height: 5px;\n  content: \"\";\n  position: absolute;\n  border-left: 3px #ffffff solid;\n  border-bottom: 3px #ffffff solid;\n  transform: rotate(-45deg);\n  left: 6px;\n  top: 8px;\n}\n.step ul:after {\n  background: #cccccc;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 18px;\n  width: 80%;\n  height: 2px;\n  content: \"\";\n  margin: 0 auto;\n  z-index: 0;\n}\n.register_panel {\n  padding: 20px 15px;\n  color: #666666;\n}\n.sml_cus_btn {\n  box-shadow: #9be2bd 0 0 15px 5px;\n  margin: 20px 30px;\n  border-radius: 30px;\n}\n.uplaodtext {\n  color: #666666;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n.uplaodbtn {\n  color: #41b578;\n  font-size: 11px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  background-color: #ffffff;\n  padding: 6px 12px;\n  border-radius: 30px;\n  text-transform: uppercase;\n  margin: 0;\n}\n.cust_input span {\n  position: absolute;\n  right: 10px;\n  border-left: 1px #9e9d9d solid;\n  padding-left: 4px;\n}\n.amount {\n  padding: 0 30px 0 12px !important;\n  text-align: center;\n}\n.daychoose {\n  margin: 0 0;\n  height: 40px;\n  text-align: center;\n  line-height: 38px;\n}\n.custompop {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.7);\n  top: 0;\n  left: 0;\n  z-index: 99;\n  width: 100%;\n  height: 100%;\n  display: table;\n}\n.custompop .custompop_in {\n  display: table-cell;\n  vertical-align: middle;\n  padding: 0 20px;\n}\n.custompop .popin {\n  background: #ffffff;\n  padding: 20px;\n  width: 100%;\n  border-radius: 20px;\n}\n.custompop .popin h3 {\n  font-size: 20px;\n  font-weight: bold;\n  text-align: center;\n  margin: 0 0 20px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVycmVnaXN0cmF0aW9uMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtFQUFxQixjQUFBO0FBRXpCO0FBREk7RUFDSSxVQUFBO0VBQVksWUFBQTtFQUFjLGNBQUE7RUFBZ0Isa0JBQUE7RUFBb0IsZ0JBQUE7RUFBa0Isa0JBQUE7RUFBb0Isa0JBQUE7QUFTNUc7QUFSUTtFQUNJLHFCQUFBO0VBQXVCLGtCQUFBO0VBQW9CLGNBQUE7RUFBZ0IsWUFBQTtBQWF2RTtBQVpZO0VBQUcsY0FBQTtFQUFnQixXQUFBO0VBQWEsWUFBQTtFQUFjLGdCQUFBO0VBQWtCLHlCQUFBO0VBQTBCLGtCQUFBO0VBQW1CLGtCQUFBO0FBcUJ6SDtBQXBCWTtFQUFNLGNBQUE7RUFBZ0IsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLG9CQUFBO0FBNEIzSDtBQTFCUTtFQUFnQixlQUFBO0FBNkJ4QjtBQTVCUTtFQUFlLGdCQUFBO0VBQWtCLGlCQUFBO0FBZ0N6QztBQS9CUTtFQUFrQixXQUFBO0VBQWEsWUFBQTtFQUFjLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW9CLFlBQUE7RUFBYyxhQUFBO0VBQWUsaUJBQUE7RUFBbUIsUUFBQTtBQXlDOUk7QUF4Q1E7RUFBdUIsV0FBQTtFQUFhLFdBQUE7RUFBYSxXQUFBO0VBQWEsa0JBQUE7RUFBb0IsOEJBQUE7RUFBZ0MsZ0NBQUE7RUFBa0MseUJBQUE7RUFBMkIsU0FBQTtFQUFXLFFBQUE7QUFtRGxNO0FBaERJO0VBQ0ksbUJBQUE7RUFBcUIsa0JBQUE7RUFBb0IsT0FBQTtFQUFTLFFBQUE7RUFBVSxTQUFBO0VBQVcsVUFBQTtFQUFZLFdBQUE7RUFBYSxXQUFBO0VBQWEsY0FBQTtFQUFnQixVQUFBO0FBMkRySTtBQXhEQTtFQUFpQixrQkFBQTtFQUFvQixjQUFBO0FBNkRyQztBQTVEQTtFQUFjLGdDQUFBO0VBQWtDLGlCQUFBO0VBQW1CLG1CQUFBO0FBa0VuRTtBQWpFQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtBQW9FSjtBQWxFQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsU0FBQTtBQXFFSjtBQW5FQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUFzRUo7QUFwRUE7RUFDSSxpQ0FBQTtFQUFtQyxrQkFBQTtBQXdFdkM7QUF0RUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUF5RUo7QUF0RUE7RUFDSSxrQkFBQTtFQUFvQiw4QkFBQTtFQUFnRCxNQUFBO0VBQVEsT0FBQTtFQUFTLFdBQUE7RUFBYSxXQUFBO0VBQWEsWUFBQTtFQUFjLGNBQUE7QUFnRmpJO0FBL0VJO0VBQWMsbUJBQUE7RUFBcUIsc0JBQUE7RUFBd0IsZUFBQTtBQW9GL0Q7QUFuRkk7RUFBTyxtQkFBQTtFQUFxQixhQUFBO0VBQWUsV0FBQTtFQUFhLG1CQUFBO0FBeUY1RDtBQXhGUTtFQUFHLGVBQUE7RUFBaUIsaUJBQUE7RUFBbUIsa0JBQUE7RUFBb0Isa0JBQUE7QUE4Rm5FIiwiZmlsZSI6InByb3ZpZGVycmVnaXN0cmF0aW9uMy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc3RlcHsgXHJcbiAgICBiYWNrZ3JvdW5kOiAjZTdmZWYyOyBwYWRkaW5nOiA4cHggMDtcclxuICAgIHVse1xyXG4gICAgICAgIHBhZGRpbmc6IDA7IHdpZHRoOiAzMDBweDsgbWFyZ2luOiAwIGF1dG87IG1hcmdpbi1ib3R0b206IDVweDsgbGlzdC1zdHlsZTogbm9uZTsgdGV4dC1hbGlnbjogY2VudGVyOyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbGl7IFxyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IHBvc2l0aW9uOiByZWxhdGl2ZTsgcGFkZGluZzogMCAxNSU7IHotaW5kZXg6IDk5OTtcclxuICAgICAgICAgICAgYXsgZGlzcGxheTogYmxvY2s7IHdpZHRoOiAyMnB4OyBoZWlnaHQ6IDIycHg7IGJhY2tncm91bmQ6ICNmZmY7IGJvcmRlcjojNDFiNTc4IHNvbGlkIDFweDsgYm9yZGVyLXJhZGl1czo1MCU7IHBvc2l0aW9uOiByZWxhdGl2ZTsgfSAgXHJcbiAgICAgICAgICAgIHNwYW57IGRpc3BsYXk6IGJsb2NrOyBjb2xvcjogIzY2NjY2NjsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7IG1hcmdpbjogN3B4IDAgMCAtNXB4OyB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxpOmZpcnN0LWNoaWxkeyBwYWRkaW5nLWxlZnQ6IDA7IH1cclxuICAgICAgICBsaTpsYXN0LWNoaWxkeyBwYWRkaW5nLXJpZ2h0OiAwOyB0ZXh0LWFsaWduOiByaWdodDsgfVxyXG4gICAgICAgIGxpLmFjdGl2ZXN0ZXAgYSB7IHdpZHRoOiAzMHB4OyBoZWlnaHQ6IDMwcHg7IGJhY2tncm91bmQ6ICM0MWI1Nzg7IHBvc2l0aW9uOiByZWxhdGl2ZTsgei1pbmRleDogOTk5OyBtYXJnaW4tdG9wOiAwOyBtYXJnaW4tbGVmdDogLTNweDsgdG9wOiA1cHg7IH1cclxuICAgICAgICBsaS5hY3RpdmVzdGVwIGE6YWZ0ZXJ7IHdpZHRoOiAxM3B4OyBoZWlnaHQ6IDVweDsgY29udGVudDogXCJcIjsgcG9zaXRpb246IGFic29sdXRlOyBib3JkZXItbGVmdDogM3B4ICNmZmZmZmYgc29saWQ7IGJvcmRlci1ib3R0b206IDNweCAjZmZmZmZmIHNvbGlkOyB0cmFuc2Zvcm06IHJvdGF0ZSgtNDVkZWcpOyBsZWZ0OiA2cHg7IHRvcDogOHB4OyB9XHJcbiAgICBcclxuICAgIH1cclxuICAgIHVsOmFmdGVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjY2NjY2NjOyBwb3NpdGlvbjogYWJzb2x1dGU7IGxlZnQ6IDA7IHJpZ2h0OiAwOyB0b3A6IDE4cHg7IHdpZHRoOiA4MCU7IGhlaWdodDogMnB4OyBjb250ZW50OiBcIlwiOyBtYXJnaW46IDAgYXV0bzsgei1pbmRleDogMDtcclxuICAgIH1cclxufVxyXG4ucmVnaXN0ZXJfcGFuZWx7IHBhZGRpbmc6IDIwcHggMTVweDsgY29sb3I6ICM2NjY2NjY7fVxyXG4uc21sX2N1c19idG57IGJveC1zaGFkb3c6ICM5YmUyYmQgMCAwIDE1cHggNXB4OyBtYXJnaW46IDIwcHggMzBweDsgYm9yZGVyLXJhZGl1czogMzBweDsgfVxyXG4udXBsYW9kdGV4dHtcclxuICAgIGNvbG9yOiAjNjY2NjY2O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGZvbnQtZmFtaWx5OiAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxufVxyXG4udXBsYW9kYnRue1xyXG4gICAgY29sb3I6ICM0MWI1Nzg7XHJcbiAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNnB4IDEycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG4uY3VzdF9pbnB1dCBzcGFue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICBib3JkZXItbGVmdDogMXB4ICM5ZTlkOWQgc29saWQ7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDRweDtcclxufVxyXG4uYW1vdW50e1xyXG4gICAgcGFkZGluZzogMCAzMHB4IDAgMTJweCAhaW1wb3J0YW50OyB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmRheWNob29zZXtcclxuICAgIG1hcmdpbjogMCAwO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGluZS1oZWlnaHQ6IDM4cHg7XHJcbn1cclxuXHJcbi5jdXN0b21wb3B7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7IGJhY2tncm91bmQ6IHJnYmEoJGNvbG9yOiAjMDAwMDAwLCAkYWxwaGE6IDAuNyk7IHRvcDogMDsgbGVmdDogMDsgei1pbmRleDogOTk7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDEwMCU7IGRpc3BsYXk6IHRhYmxlO1xyXG4gICAgLmN1c3RvbXBvcF9pbntkaXNwbGF5OiB0YWJsZS1jZWxsOyB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlOyBwYWRkaW5nOiAwIDIwcHg7fVxyXG4gICAgLnBvcGlue2JhY2tncm91bmQ6ICNmZmZmZmY7IHBhZGRpbmc6IDIwcHg7IHdpZHRoOiAxMDAlOyBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgIGgze2ZvbnQtc2l6ZTogMjBweDsgZm9udC13ZWlnaHQ6IGJvbGQ7IHRleHQtYWxpZ246IGNlbnRlcjsgbWFyZ2luOiAwIDAgMjBweCAwO31cclxuICAgIH1cclxufSJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerregistration3_providerregistration3_module_ts.js.map