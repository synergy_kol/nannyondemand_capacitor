"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_forgotpass_forgotpass_module_ts"],{

/***/ 92777:
/*!*********************************************************!*\
  !*** ./src/app/forgotpass/forgotpass-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ForgotpassPageRoutingModule": () => (/* binding */ ForgotpassPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _forgotpass_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./forgotpass.page */ 25312);




const routes = [
    {
        path: '',
        component: _forgotpass_page__WEBPACK_IMPORTED_MODULE_0__.ForgotpassPage
    }
];
let ForgotpassPageRoutingModule = class ForgotpassPageRoutingModule {
};
ForgotpassPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ForgotpassPageRoutingModule);



/***/ }),

/***/ 36100:
/*!*************************************************!*\
  !*** ./src/app/forgotpass/forgotpass.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ForgotpassPageModule": () => (/* binding */ ForgotpassPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _forgotpass_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./forgotpass-routing.module */ 92777);
/* harmony import */ var _forgotpass_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forgotpass.page */ 25312);







let ForgotpassPageModule = class ForgotpassPageModule {
};
ForgotpassPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _forgotpass_routing_module__WEBPACK_IMPORTED_MODULE_0__.ForgotpassPageRoutingModule
        ],
        declarations: [_forgotpass_page__WEBPACK_IMPORTED_MODULE_1__.ForgotpassPage]
    })
], ForgotpassPageModule);



/***/ }),

/***/ 25312:
/*!***********************************************!*\
  !*** ./src/app/forgotpass/forgotpass.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ForgotpassPage": () => (/* binding */ ForgotpassPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_forgotpass_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./forgotpass.page.html */ 91923);
/* harmony import */ var _forgotpass_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forgotpass.page.scss */ 37363);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);






let ForgotpassPage = class ForgotpassPage {
    constructor(navCtrl, authService, loadingController, toastController) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.email = '';
        this.isLoading = false;
    }
    ngOnInit() {
    }
    goLogin() {
        this.navCtrl.pop();
    }
    onForgotpass() {
        const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.email.trim() == '') {
            this.presentToast('Please enter yout registered Email');
        }
        else if (!emailPattern.test(this.email)) {
            this.presentToast('Wrong Email Format...');
        }
        else {
            this.showLoader('Please wait...');
            var body = { email: this.email };
            this.authService.postData("forgotPassword", body).then(result => {
                this.data = result;
                console.log("Forgot Pass: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.presentToast("Success! Please check your Email. Reset Password link sended to your Email.");
                    this.navCtrl.navigateRoot('/login');
                }
                else {
                    this.presentToast(this.data.status.message);
                }
                this.hideLoader();
            }, error => {
                console.log(error);
                this.hideLoader();
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.present();
        });
    }
};
ForgotpassPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.NavController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ToastController }
];
ForgotpassPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-forgotpass',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_forgotpass_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_forgotpass_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ForgotpassPage);



/***/ }),

/***/ 91923:
/*!****************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/forgotpass/forgotpass.page.html ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"login_screen\">\n    <div class=\"login_panel\">\n      <img src=\"assets/images/login_img.png\" alt=\"Log\" />\n      <h3>Forgot Password</h3>\n      <ion-input class=\"cust_input\" placeholder=\"Enter Your Email\" [(ngModel)]=\"email\"></ion-input>\n      <ion-button color=\"dark\" expand=\"full\" shape=\"round\" (click)=\"onForgotpass()\" class=\"cus_btn\">Submit</ion-button>\n    </div>\n  </div>\n</ion-content>\n<ion-toolbar class=\"ftr\" (click)=\"goLogin()\"><a>Back to Login</a></ion-toolbar>");

/***/ }),

/***/ 37363:
/*!*************************************************!*\
  !*** ./src/app/forgotpass/forgotpass.page.scss ***!
  \*************************************************/
/***/ ((module) => {

module.exports = ".login_screen {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  display: table;\n  padding: 35px;\n}\n.login_screen .login_panel {\n  display: table-cell;\n  text-align: center;\n  vertical-align: middle;\n}\n.login_screen .login_panel img {\n  width: 60%;\n  margin-bottom: 20px;\n}\n.login_screen .login_panel h3 {\n  padding: 20px 0;\n  margin: 0;\n  color: #222222;\n  font-size: 26px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.cust_input:hover {\n  border-color: #222428;\n  box-shadow: #eeeeee 0 0 6px 4px;\n}\n.ftr {\n  color: #444444;\n  font-size: 14px;\n  text-align: center;\n  font-family: \"Open Sans\", sans-serif;\n}\n.ftr a {\n  color: #444444;\n  text-decoration: underline;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvcmdvdHBhc3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtFQUFvQixjQUFBO0VBQWdCLGFBQUE7QUFLbkU7QUFKSTtFQUFjLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW9CLHNCQUFBO0FBUzNEO0FBUlE7RUFBSyxVQUFBO0VBQVksbUJBQUE7QUFZekI7QUFYUTtFQUFJLGVBQUE7RUFBaUIsU0FBQTtFQUFXLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtBQW1CM0Y7QUFoQkE7RUFBbUIscUJBQUE7RUFBdUIsK0JBQUE7QUFxQjFDO0FBcEJBO0VBQUssY0FBQTtFQUFnQixlQUFBO0VBQWlCLGtCQUFBO0VBQW9CLG9DQUFBO0FBMkIxRDtBQTFCSTtFQUFFLGNBQUE7RUFBZ0IsMEJBQUE7RUFBNEIsZ0JBQUE7QUErQmxEIiwiZmlsZSI6ImZvcmdvdHBhc3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvZ2luX3NjcmVlbntcclxuICAgIHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDEwMCU7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgZGlzcGxheTogdGFibGU7IHBhZGRpbmc6IDM1cHg7XHJcbiAgICAubG9naW5fcGFuZWx7IGRpc3BsYXk6IHRhYmxlLWNlbGw7IHRleHQtYWxpZ246IGNlbnRlcjsgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICBpbWd7IHdpZHRoOiA2MCU7IG1hcmdpbi1ib3R0b206IDIwcHg7IH1cclxuICAgICAgICBoM3sgcGFkZGluZzogMjBweCAwOyBtYXJnaW46IDA7IGNvbG9yOiAjMjIyMjIyOyBmb250LXNpemU6IDI2cHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICB9XHJcbn1cclxuLmN1c3RfaW5wdXQ6aG92ZXJ7IGJvcmRlci1jb2xvcjogIzIyMjQyODsgYm94LXNoYWRvdzogI2VlZWVlZSAwIDAgNnB4IDRweDt9XHJcbi5mdHJ7Y29sb3I6ICM0NDQ0NDQ7IGZvbnQtc2l6ZTogMTRweDsgdGV4dC1hbGlnbjogY2VudGVyOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcclxuICAgIGF7Y29sb3I6ICM0NDQ0NDQ7IHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lOyBmb250LXdlaWdodDogNjAwO31cclxufVxyXG4iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_forgotpass_forgotpass_module_ts.js.map