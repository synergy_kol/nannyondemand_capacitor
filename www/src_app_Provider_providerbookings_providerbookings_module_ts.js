"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerbookings_providerbookings_module_ts"],{

/***/ 13701:
/*!******************************************************************************!*\
  !*** ./src/app/Provider/providerbookings/providerbookings-routing.module.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderbookingsPageRoutingModule": () => (/* binding */ ProviderbookingsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerbookings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerbookings.page */ 77330);




const routes = [
    {
        path: '',
        component: _providerbookings_page__WEBPACK_IMPORTED_MODULE_0__.ProviderbookingsPage
    }
];
let ProviderbookingsPageRoutingModule = class ProviderbookingsPageRoutingModule {
};
ProviderbookingsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProviderbookingsPageRoutingModule);



/***/ }),

/***/ 55575:
/*!**********************************************************************!*\
  !*** ./src/app/Provider/providerbookings/providerbookings.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderbookingsPageModule": () => (/* binding */ ProviderbookingsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerbookings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerbookings-routing.module */ 13701);
/* harmony import */ var _providerbookings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerbookings.page */ 77330);







let ProviderbookingsPageModule = class ProviderbookingsPageModule {
};
ProviderbookingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerbookings_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProviderbookingsPageRoutingModule
        ],
        declarations: [_providerbookings_page__WEBPACK_IMPORTED_MODULE_1__.ProviderbookingsPage]
    })
], ProviderbookingsPageModule);



/***/ }),

/***/ 77330:
/*!********************************************************************!*\
  !*** ./src/app/Provider/providerbookings/providerbookings.page.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderbookingsPage": () => (/* binding */ ProviderbookingsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerbookings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerbookings.page.html */ 98286);
/* harmony import */ var _providerbookings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerbookings.page.scss */ 86797);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let ProviderbookingsPage = class ProviderbookingsPage {
    constructor(navCtrl, authService, loadingController, storage) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.loadingController = loadingController;
        this.storage = storage;
        this.isLoading = false;
        this.userdata = [];
        this.data = [];
        this.bookingData = [];
        this.nofound = false;
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-booking-details", body).then(result => {
                this.data = result;
                console.log("Booking: ", this.data);
                this.bookingData = this.data.result.data;
                if (this.bookingData.length == 0) {
                    this.nofound = true;
                }
                else {
                    this.nofound = false;
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    goDetails(id) {
        var object = {
            bookID: id
        };
        this.navCtrl.navigateForward(["/providerbookingdetails"], { queryParams: object });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
ProviderbookingsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage }
];
ProviderbookingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-providerbookings',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerbookings_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerbookings_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProviderbookingsPage);



/***/ }),

/***/ 98286:
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerbookings/providerbookings.page.html ***!
  \*************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>My Bookings</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <!-- <div class=\"booking_tab\">\n    <ul>\n      <li><a href=\"\">Past</a></li>\n      <li><a href=\"\">Current</a></li>\n      <li class=\"booking_active\"><a href=\"\">Upcoming</a></li>\n    </ul>\n  </div> -->\n  <div *ngIf=\"nofound\" style=\"padding:50px 20px; color: #bfbfbf; font-weight: bold; font-size: 22px; text-align: center;\">[ Sorry! Nothing Found. ]</div>\n  <div style=\"padding: 0 20px;\">\n    <div class=\"blokingbox\" *ngFor=\"let item of bookingData.reverse()\">\n      <div class=\"blokingbox_top\">\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"8\">\n              <div class=\"booking_box_img\">\n                <div class=\"booking_box_img_l\"><img [src]=\"item.parent_profile_image != '' ? item.parent_profile_image : 'assets/images/avatar.png'\" /></div>\n                <div class=\"booking_box_img_r\">\n                  <h3>{{item.parent_fname}} {{item.parent_lname}}</h3>\n                  <span>{{item.parent_address}}</span>\n                </div>\n              </div>\n            </ion-col>\n            <ion-col size=\"4\">\n              <ion-button (click)=\"goDetails(item.booking_detail_id)\" class=\"bookingdetails_btn\">View Details</ion-button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n      <ion-grid class=\"booking_bottom\">\n        <ion-row>\n          <ion-col>\n            <h5>Booking Date</h5>\n            <h3>{{item.date}}</h3>\n          </ion-col>\n          <ion-col>\n            <h5>No. of Child</h5>\n            <h3>{{item.parent_children_no}}</h3>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>    \n  </div>\n</ion-content>");

/***/ }),

/***/ 86797:
/*!**********************************************************************!*\
  !*** ./src/app/Provider/providerbookings/providerbookings.page.scss ***!
  \**********************************************************************/
/***/ ((module) => {

module.exports = ".booking_tab {\n  background: #e7fef2;\n  padding: 15px 0;\n}\n.booking_tab ul {\n  padding: 0;\n  margin: 0;\n  list-style: none;\n  text-align: center;\n}\n.booking_tab ul li {\n  display: inline-block;\n  width: 33.33%;\n}\n.booking_tab ul li a {\n  color: #888888;\n  font-size: 15px;\n  font-weight: 600;\n  font-family: \"Open Sans\", sans-serif;\n  display: block;\n  text-decoration: none;\n  position: relative;\n}\n.booking_tab ul li.booking_active a {\n  color: #41b578;\n}\n.booking_tab ul li.booking_active a:after {\n  width: 100%;\n  height: 3px;\n  background: #41b578;\n  position: absolute;\n  bottom: -14px;\n  left: 0;\n  content: \"\";\n}\n.blokingbox {\n  box-shadow: #dddddd 0 0 10px;\n  border-radius: 8px;\n  margin: 20px 0;\n}\n.blokingbox_top {\n  border-bottom: #dddddd solid 1px;\n  padding: 5px 0;\n}\n.booking_box_img {\n  display: flex;\n}\n.booking_box_img .booking_box_img_l {\n  width: 40px;\n  height: 40px;\n  border-radius: 50%;\n  margin-right: 10px;\n}\n.booking_box_img .booking_box_img_l img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n.booking_box_img .booking_box_img_r {\n  margin: 4px 0;\n}\n.booking_box_img .booking_box_img_r h3 {\n  margin: 0;\n  font-size: 15px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  color: #222222;\n}\n.booking_box_img .booking_box_img_r span {\n  color: #444444;\n  font-weight: 400;\n  font-size: 13px;\n  font-style: italic;\n  font-family: \"Open Sans\", sans-serif;\n}\n.bookingdetails_btn {\n  letter-spacing: 0;\n  margin: 7px 0;\n  padding: 0 5px;\n  border-radius: 8px;\n  color: #41b578 !important;\n  font-size: 12px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  --background: #fff;\n  text-transform: capitalize;\n  border: #41b578 solid 2px;\n  width: 100%;\n  height: 30px;\n  --box-shadow: none;\n}\n.booking_bottom {\n  padding: 5px;\n}\n.booking_bottom h5 {\n  padding: 0 0 5px 0px;\n  margin: 0;\n  color: #41b578;\n  font-size: 11px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.booking_bottom h3 {\n  padding: 0;\n  margin: 0;\n  color: #444444;\n  font-size: 13px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVyYm9va2luZ3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7RUFBcUIsZUFBQTtBQUV6QjtBQURJO0VBQ0ksVUFBQTtFQUFZLFNBQUE7RUFBVyxnQkFBQTtFQUFrQixrQkFBQTtBQU1qRDtBQUxRO0VBQ0kscUJBQUE7RUFBc0IsYUFBQTtBQVFsQztBQVBZO0VBQUcsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGNBQUE7RUFBZ0IscUJBQUE7RUFBdUIsa0JBQUE7QUFnQi9JO0FBZFE7RUFBcUIsY0FBQTtBQWlCN0I7QUFoQlE7RUFBMkIsV0FBQTtFQUFhLFdBQUE7RUFBYSxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixhQUFBO0VBQWUsT0FBQTtFQUFTLFdBQUE7QUF5QjlIO0FBdEJBO0VBQWEsNEJBQUE7RUFBOEIsa0JBQUE7RUFBb0IsY0FBQTtBQTRCL0Q7QUEzQkE7RUFBaUIsZ0NBQUE7RUFBaUMsY0FBQTtBQWdDbEQ7QUE5QkE7RUFDSSxhQUFBO0FBaUNKO0FBaENJO0VBQ0ksV0FBQTtFQUFZLFlBQUE7RUFBYSxrQkFBQTtFQUFtQixrQkFBQTtBQXFDcEQ7QUFwQ1E7RUFBSyxXQUFBO0VBQVksWUFBQTtFQUFhLGtCQUFBO0FBeUN0QztBQXZDSTtFQUFtQixhQUFBO0FBMEN2QjtBQXpDUTtFQUFHLFNBQUE7RUFBVyxlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGNBQUE7QUFnRC9GO0FBL0NRO0VBQU0sY0FBQTtFQUFnQixnQkFBQTtFQUFpQixlQUFBO0VBQWlCLGtCQUFBO0VBQW9CLG9DQUFBO0FBc0RwRjtBQW5EQTtFQUFxQixpQkFBQTtFQUFtQixhQUFBO0VBQWUsY0FBQTtFQUFnQixrQkFBQTtFQUFvQix5QkFBQTtFQUEyQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGtCQUFBO0VBQW9CLDBCQUFBO0VBQTRCLHlCQUFBO0VBQTJCLFdBQUE7RUFBYSxZQUFBO0VBQWMsa0JBQUE7QUFvRXJTO0FBbkVBO0VBQWlCLFlBQUE7QUF1RWpCO0FBdEVJO0VBQUksb0JBQUE7RUFBc0IsU0FBQTtFQUFXLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtBQThFNUY7QUE3RUk7RUFBSSxVQUFBO0VBQVksU0FBQTtFQUFXLGNBQUE7RUFBZ0IsZUFBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtBQXFGakYiLCJmaWxlIjoicHJvdmlkZXJib29raW5ncy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9va2luZ190YWJ7IFxyXG4gICAgYmFja2dyb3VuZDogI2U3ZmVmMjsgcGFkZGluZzogMTVweCAwO1xyXG4gICAgdWx7IFxyXG4gICAgICAgIHBhZGRpbmc6IDA7IG1hcmdpbjogMDsgbGlzdC1zdHlsZTogbm9uZTsgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgICAgICBsaXtcclxuICAgICAgICAgICAgZGlzcGxheTppbmxpbmUtYmxvY2s7IHdpZHRoOiAzMy4zMyU7XHJcbiAgICAgICAgICAgIGF7IGNvbG9yOiAjODg4ODg4OyBmb250LXNpemU6IDE1cHg7IGZvbnQtd2VpZ2h0OiA2MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyBkaXNwbGF5OiBibG9jazsgdGV4dC1kZWNvcmF0aW9uOiBub25lOyBwb3NpdGlvbjogcmVsYXRpdmU7IH1cclxuICAgICAgICB9XHJcbiAgICAgICAgbGkuYm9va2luZ19hY3RpdmUgYXsgY29sb3I6IzQxYjU3ODsgfVxyXG4gICAgICAgIGxpLmJvb2tpbmdfYWN0aXZlIGE6YWZ0ZXJ7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDNweDsgYmFja2dyb3VuZDogIzQxYjU3ODsgcG9zaXRpb246IGFic29sdXRlOyBib3R0b206IC0xNHB4OyBsZWZ0OiAwOyBjb250ZW50OiBcIlwiOyB9XHJcbiAgICB9XHJcbn1cclxuLmJsb2tpbmdib3h7IGJveC1zaGFkb3c6ICNkZGRkZGQgMCAwIDEwcHg7IGJvcmRlci1yYWRpdXM6IDhweDsgbWFyZ2luOiAyMHB4IDA7IH1cclxuLmJsb2tpbmdib3hfdG9weyBib3JkZXItYm90dG9tOiNkZGRkZGQgc29saWQgMXB4OyBwYWRkaW5nOiA1cHggMDsgfVxyXG5cclxuLmJvb2tpbmdfYm94X2ltZ3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuYm9va2luZ19ib3hfaW1nX2x7IFxyXG4gICAgICAgIHdpZHRoOjQwcHg7IGhlaWdodDo0MHB4OyBib3JkZXItcmFkaXVzOjUwJTsgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIGltZ3sgd2lkdGg6MTAwJTsgaGVpZ2h0OjEwMCU7IGJvcmRlci1yYWRpdXM6NTAlOyB9XHJcbiAgICB9XHJcbiAgICAuYm9va2luZ19ib3hfaW1nX3J7bWFyZ2luOiA0cHggMDtcclxuICAgICAgICBoM3ttYXJnaW46IDA7IGZvbnQtc2l6ZTogMTVweDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IGNvbG9yOiAjMjIyMjIyOyB9XHJcbiAgICAgICAgc3BhbnsgY29sb3I6ICM0NDQ0NDQ7IGZvbnQtd2VpZ2h0OjQwMDsgZm9udC1zaXplOiAxM3B4OyBmb250LXN0eWxlOiBpdGFsaWM7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICB9XHJcbn1cclxuLmJvb2tpbmdkZXRhaWxzX2J0bnsgbGV0dGVyLXNwYWNpbmc6IDA7IG1hcmdpbjogN3B4IDA7IHBhZGRpbmc6IDAgNXB4OyBib3JkZXItcmFkaXVzOiA4cHg7IGNvbG9yOiAjNDFiNTc4ICFpbXBvcnRhbnQ7IGZvbnQtc2l6ZTogMTJweDsgZm9udC13ZWlnaHQ6IDcwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IC0tYmFja2dyb3VuZDogI2ZmZjsgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7IGJvcmRlcjogIzQxYjU3OCBzb2xpZCAycHg7IHdpZHRoOiAxMDAlOyBoZWlnaHQ6IDMwcHg7IC0tYm94LXNoYWRvdzogbm9uZTsgfVxyXG4uYm9va2luZ19ib3R0b217IHBhZGRpbmc6IDVweDtcclxuICAgIGg1eyBwYWRkaW5nOiAwIDAgNXB4IDBweDsgbWFyZ2luOiAwOyBjb2xvcjogIzQxYjU3ODsgZm9udC1zaXplOiAxMXB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG4gICAgaDN7IHBhZGRpbmc6IDA7IG1hcmdpbjogMDsgY29sb3I6ICM0NDQ0NDQ7IGZvbnQtc2l6ZToxM3B4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG59XHJcbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerbookings_providerbookings_module_ts.js.map