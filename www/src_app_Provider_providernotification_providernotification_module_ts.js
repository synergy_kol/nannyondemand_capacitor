"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providernotification_providernotification_module_ts"],{

/***/ 20472:
/*!**************************************************************************************!*\
  !*** ./src/app/Provider/providernotification/providernotification-routing.module.ts ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProvidernotificationPageRoutingModule": () => (/* binding */ ProvidernotificationPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providernotification_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providernotification.page */ 44720);




const routes = [
    {
        path: '',
        component: _providernotification_page__WEBPACK_IMPORTED_MODULE_0__.ProvidernotificationPage
    }
];
let ProvidernotificationPageRoutingModule = class ProvidernotificationPageRoutingModule {
};
ProvidernotificationPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProvidernotificationPageRoutingModule);



/***/ }),

/***/ 1775:
/*!******************************************************************************!*\
  !*** ./src/app/Provider/providernotification/providernotification.module.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProvidernotificationPageModule": () => (/* binding */ ProvidernotificationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providernotification_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providernotification-routing.module */ 20472);
/* harmony import */ var _providernotification_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providernotification.page */ 44720);







let ProvidernotificationPageModule = class ProvidernotificationPageModule {
};
ProvidernotificationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providernotification_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProvidernotificationPageRoutingModule
        ],
        declarations: [_providernotification_page__WEBPACK_IMPORTED_MODULE_1__.ProvidernotificationPage]
    })
], ProvidernotificationPageModule);



/***/ }),

/***/ 44720:
/*!****************************************************************************!*\
  !*** ./src/app/Provider/providernotification/providernotification.page.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProvidernotificationPage": () => (/* binding */ ProvidernotificationPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providernotification_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providernotification.page.html */ 66636);
/* harmony import */ var _providernotification_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providernotification.page.scss */ 68620);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let ProvidernotificationPage = class ProvidernotificationPage {
    constructor(authService, loadingController, storage, navCtrl, alertController, toastController) {
        this.authService = authService;
        this.loadingController = loadingController;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.toastController = toastController;
        this.isLoading = false;
        this.userdata = [];
        this.data = [];
        this.notiData = [];
        this.allnotiData = [];
        this.nofound = false;
        this.requestdata = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-my-notifications", body).then(result => {
                this.data = result;
                console.log("Notification: ", this.data);
                this.notiData = this.data.result.data;
                this.allnotiData = this.notiData.all;
                if (this.allnotiData == '') {
                    this.nofound = true;
                }
                else {
                    this.nofound = false;
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    onAccept(scheduleId, notiId) {
        this.showLoader('Wait...');
        this.storage.get('userDetails').then((result) => {
            this.data = result;
            var acptbody = {
                user_id: this.data.user_id,
                accept_status: 1,
                nanny_request_schedule_id: scheduleId,
                notification_id: notiId
            };
            this.authService.postData("post-schedule-status", acptbody).then(val => {
                this.requestdata = val;
                console.log("Request Schedule:", this.requestdata);
                if (this.requestdata.status.error_code == 0) {
                    this.getDetails();
                    this.presentToast("Successfully Accepted Request.");
                }
                else {
                    this.presentToast(this.requestdata.status.message);
                    this.hideLoader();
                }
            }, error => {
                this.hideLoader();
            });
        });
    }
    onDeclined(scheduleId, notiId) {
        const alert = this.alertController.create({
            header: 'Warning!',
            message: 'Are you sure, you want to Declined this Request?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Wait...');
                        this.storage.get('userDetails').then((result) => {
                            this.data = result;
                            var dclnbody = {
                                user_id: this.data.user_id,
                                accept_status: 2,
                                nanny_request_schedule_id: scheduleId,
                                notification_id: notiId
                            };
                            this.authService.postData("post-schedule-status", dclnbody).then(val => {
                                this.requestdata = val;
                                console.log("Request Schedule:", this.requestdata);
                                if (this.requestdata.status.error_code == 0) {
                                    this.getDetails();
                                    this.presentToast("Successfully Declined this Request.");
                                }
                                else {
                                    this.presentToast(this.requestdata.status.message);
                                    this.hideLoader();
                                }
                            }, error => {
                                this.hideLoader();
                            });
                        });
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.present();
        });
    }
};
ProvidernotificationPage.ctorParameters = () => [
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.AlertController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController }
];
ProvidernotificationPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-providernotification',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providernotification_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providernotification_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProvidernotificationPage);



/***/ }),

/***/ 66636:
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providernotification/providernotification.page.html ***!
  \*********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Schedule Notifications</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"nofound\"\n    style=\"padding:50px 20px; color: #bfbfbf; font-weight: bold; font-size: 22px; text-align: center;\">[ Sorry! Nothing\n    Found. ]</div>\n  <div *ngIf=\"!nofound\" class=\"notification\">\n    <div class=\"notification_box\" *ngFor=\"let item of allnotiData\">\n      <div class=\"notification_icon\"><img src=\"assets/images/notification_icon.jpg\" alt=\"notification_icon\" /></div>\n      <p>{{item.notification_text}}</p>\n      <span>{{item.created_at}}</span>\n      <div *ngIf=\"item.nanny_request_schedule_id != null\">\n        <ion-button color=\"danger\" size=\"small\" shape=\"round\"\n          (click)=\"onDeclined(item.nanny_request_schedule_id, item.notification_id)\">Declined</ion-button>\n        <ion-button color=\"success\" size=\"small\" shape=\"round\"\n          (click)=\"onAccept(item.nanny_request_schedule_id, item.notification_id)\">Accept</ion-button>\n      </div>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 68620:
/*!******************************************************************************!*\
  !*** ./src/app/Provider/providernotification/providernotification.page.scss ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = ".notification {\n  padding: 0 20px;\n}\n\n.notification_box {\n  background: #ffffff;\n  box-shadow: #dddddd 0 0 10px;\n  border-radius: 8px;\n  margin: 20px 0;\n  padding: 10px 15px;\n  padding-left: 70px;\n  position: relative;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.notification_box p {\n  margin: 0;\n  color: #444444;\n  font-size: 14px;\n}\n\n.notification_box span {\n  margin: 10px 0 0 0;\n  color: #aaaaaa;\n  font-size: 12px;\n  display: block;\n  font-style: italic;\n}\n\n.notification_icon {\n  width: 70px;\n  text-align: center;\n  position: absolute;\n  left: 0;\n  top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb3ZpZGVybm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFlLGVBQUE7QUFFZjs7QUFBQTtFQUNJLG1CQUFBO0VBQXFCLDRCQUFBO0VBQThCLGtCQUFBO0VBQW9CLGNBQUE7RUFBZ0Isa0JBQUE7RUFBb0Isa0JBQUE7RUFBb0Isa0JBQUE7RUFBb0IsZ0JBQUE7RUFBa0Isb0NBQUE7QUFXeks7O0FBVkk7RUFBRyxTQUFBO0VBQVcsY0FBQTtFQUFnQixlQUFBO0FBZWxDOztBQWRJO0VBQU0sa0JBQUE7RUFBb0IsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGNBQUE7RUFBZ0Isa0JBQUE7QUFxQi9FOztBQW5CQTtFQUFvQixXQUFBO0VBQWEsa0JBQUE7RUFBb0Isa0JBQUE7RUFBb0IsT0FBQTtFQUFTLFNBQUE7QUEyQmxGIiwiZmlsZSI6InByb3ZpZGVybm90aWZpY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ub3RpZmljYXRpb257IHBhZGRpbmc6IDAgMjBweDsgfVxyXG5cclxuLm5vdGlmaWNhdGlvbl9ib3h7ICAgXHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmOyBib3gtc2hhZG93OiAjZGRkZGRkIDAgMCAxMHB4OyBib3JkZXItcmFkaXVzOiA4cHg7IG1hcmdpbjogMjBweCAwOyBwYWRkaW5nOiAxMHB4IDE1cHg7IHBhZGRpbmctbGVmdDogNzBweDsgcG9zaXRpb246IHJlbGF0aXZlOyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcclxuICAgIHB7IG1hcmdpbjogMDsgY29sb3I6ICM0NDQ0NDQ7IGZvbnQtc2l6ZTogMTRweDsgfVxyXG4gICAgc3BhbnsgbWFyZ2luOiAxMHB4IDAgMCAwOyBjb2xvcjogI2FhYWFhYTsgZm9udC1zaXplOiAxMnB4OyBkaXNwbGF5OiBibG9jazsgZm9udC1zdHlsZTogaXRhbGljOyB9XHJcbn1cclxuLm5vdGlmaWNhdGlvbl9pY29ueyB3aWR0aDogNzBweDsgdGV4dC1hbGlnbjogY2VudGVyOyBwb3NpdGlvbjogYWJzb2x1dGU7IGxlZnQ6IDA7IHRvcDogMTVweDsgfSJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providernotification_providernotification_module_ts.js.map