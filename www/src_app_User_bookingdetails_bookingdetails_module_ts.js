"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_bookingdetails_bookingdetails_module_ts"],{

/***/ 98779:
/*!**********************************************************************!*\
  !*** ./src/app/User/bookingdetails/bookingdetails-routing.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingdetailsPageRoutingModule": () => (/* binding */ BookingdetailsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _bookingdetails_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookingdetails.page */ 82034);




const routes = [
    {
        path: '',
        component: _bookingdetails_page__WEBPACK_IMPORTED_MODULE_0__.BookingdetailsPage
    }
];
let BookingdetailsPageRoutingModule = class BookingdetailsPageRoutingModule {
};
BookingdetailsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], BookingdetailsPageRoutingModule);



/***/ }),

/***/ 91762:
/*!**************************************************************!*\
  !*** ./src/app/User/bookingdetails/bookingdetails.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingdetailsPageModule": () => (/* binding */ BookingdetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _bookingdetails_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookingdetails-routing.module */ 98779);
/* harmony import */ var _bookingdetails_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookingdetails.page */ 82034);







let BookingdetailsPageModule = class BookingdetailsPageModule {
};
BookingdetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _bookingdetails_routing_module__WEBPACK_IMPORTED_MODULE_0__.BookingdetailsPageRoutingModule
        ],
        declarations: [_bookingdetails_page__WEBPACK_IMPORTED_MODULE_1__.BookingdetailsPage]
    })
], BookingdetailsPageModule);



/***/ }),

/***/ 82034:
/*!************************************************************!*\
  !*** ./src/app/User/bookingdetails/bookingdetails.page.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingdetailsPage": () => (/* binding */ BookingdetailsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_bookingdetails_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./bookingdetails.page.html */ 1355);
/* harmony import */ var _bookingdetails_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookingdetails.page.scss */ 78147);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);








let BookingdetailsPage = class BookingdetailsPage {
    constructor(navCtrl, authService, loadingController, storage, activatedRoute, toastController) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.loadingController = loadingController;
        this.storage = storage;
        this.activatedRoute = activatedRoute;
        this.toastController = toastController;
        this.isLoading = false;
        this.isDisabled = false;
        this.userdata = [];
        this.data = [];
        this.bookingDetails = [];
        this.bookingID = '';
        this.activatedRoute.queryParams.subscribe((res) => {
            this.bookingID = res.bookID;
        });
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.getDetails();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
                booking_id: this.bookingID
            };
            this.authService.postData("get-booking-details", body).then(result => {
                this.data = result;
                console.log("Booking Details: ", this.data);
                this.bookingDetails = this.data.result.data;
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    goNannyDetails(nanny_id) {
        var object = {
            nanny_id: nanny_id
        };
        this.navCtrl.navigateForward(["/nannydetails"], { queryParams: object });
    }
    onBookingComplete(detailsID) {
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
                booking_detail_id: detailsID
            };
            this.authService.postData("update-service-completed", body).then(result => {
                this.hideLoader();
                this.data = result;
                console.log("Complete Service: ", this.data);
                if (this.data.status.error_code == 0) {
                    this.presentToast("Service Completed Successfully.");
                    this.getDetails();
                }
                else {
                    this.presentToast(this.data.status.message);
                }
            }, error => {
                this.hideLoader();
            });
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
BookingdetailsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController }
];
BookingdetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-bookingdetails',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_bookingdetails_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_bookingdetails_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], BookingdetailsPage);



/***/ }),

/***/ 1355:
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/bookingdetails/bookingdetails.page.html ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Booking Details</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div style=\"padding: 0 20px;\">\n    <!-- <div class=\"blokingbox\">\n      <ion-grid class=\"booking_bottom\">\n        <ion-row>\n          <ion-col>\n            <h5 style=\"font-size: 18px; padding: 5px 0;\">#{{bookingDetails.booking_id}}</h5>\n          </ion-col>\n          <ion-col>\n            <h5 style=\"font-size: 18px; padding: 5px 0; text-align: right;\">${{bookingDetails.amount}}</h5>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div> -->\n    <div class=\"blokingbox\" *ngFor=\"let item of bookingDetails\">\n      <div class=\"blokingbox_top\">\n        <ion-grid>\n          <ion-row>\n            <ion-col>\n              <div class=\"booking_box_img\" (click)=\"goNannyDetails(item.nanny_id)\">\n                <div class=\"booking_box_img_l\"><img [src]=\"item.provider_profile_image != '' ? item.provider_profile_image : 'assets/images/avatar.png'\" /></div>\n                <div class=\"booking_box_img_r\">\n                  <h3>{{item.provider_fname}} {{item.provider_lname}}</h3>\n                </div>\n              </div>\n            </ion-col>\n            <ion-col>\n              <h3 style=\"margin: 10px 0; font-size: 19px; font-weight: bold; text-align: right;\">$ {{item.amount}}</h3>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n      <ion-grid class=\"booking_bottom\">\n        <ion-row>\n          <ion-col>\n            <h5>Booking Date</h5>\n            <h3>{{item.date}}</h3>\n          </ion-col>\n          <ion-col>\n            <h5>Time Slot</h5>\n            <h3>{{item.time_slot}}</h3>\n          </ion-col>\n          <ion-col>\n            <h5>Booking For</h5>\n            <h3>{{item.hour}} hr</h3>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <ion-grid class=\"person_info\">\n        <ion-row>\n          <ion-col>\n            <h5>No of children:<span> {{item.children_no}}</span></h5>\n            <h5>Booking Status: <b>Confirm</b></h5>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <div class=\"locationbooking\">\n        <img class=\"locationbooking_l\" src=\"assets/images/location_orng.png\" alt=\"green_location\" />\n        <h4>{{item.provider_address}}</h4>\n        <!-- <img class=\"locationbooking_r\" src=\"assets/images/direction_orng.png\" alt=\"green_direction\" /> -->\n      </div>\n      <div *ngIf=\"item.service_is_completed == 0\">\n        <ion-button color=\"warning\" expand=\"full\" (click)=\"onBookingComplete(item.booking_detail_id)\" class=\"cus_btn\" style=\"margin: 0;\">Complete Booking</ion-button>\n      </div>\n    </div>    \n  </div>\n</ion-content>");

/***/ }),

/***/ 78147:
/*!**************************************************************!*\
  !*** ./src/app/User/bookingdetails/bookingdetails.page.scss ***!
  \**************************************************************/
/***/ ((module) => {

module.exports = ".blokingbox {\n  box-shadow: #dddddd 0 0 10px;\n  border-radius: 8px;\n  margin: 20px 0;\n  overflow: hidden;\n}\n\n.blokingbox_top {\n  border-bottom: #dddddd solid 1px;\n  padding: 5px 0;\n}\n\n.booking_box_img {\n  display: flex;\n}\n\n.booking_box_img .booking_box_img_l {\n  width: 40px;\n  height: 40px;\n  border-radius: 50%;\n  margin-right: 10px;\n}\n\n.booking_box_img .booking_box_img_l img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n}\n\n.booking_box_img .booking_box_img_r {\n  margin: 4px 0;\n}\n\n.booking_box_img .booking_box_img_r h3 {\n  margin: 0;\n  font-size: 16px;\n  line-height: 32px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  color: #222222;\n}\n\n.booking_box_img .booking_box_img_r span {\n  color: #444444;\n  font-weight: 400;\n  font-size: 13px;\n  font-style: italic;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.booking_bottom {\n  padding: 5px;\n}\n\n.booking_bottom h5 {\n  padding: 0 0 5px 0px;\n  margin: 0;\n  color: #f38320;\n  font-size: 11px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.booking_bottom h3 {\n  padding: 0;\n  margin: 0;\n  color: #444444;\n  font-size: 13px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.locationbooking {\n  position: relative;\n  background: #f6f6f6;\n  padding: 15px 55px;\n  border-top: #dddddd solid 1px;\n}\n\n.locationbooking h4 {\n  padding: 0;\n  margin: 7px 0;\n  color: #888888;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.locationbooking_l {\n  position: absolute;\n  top: 15px;\n  left: 15px;\n  height: 30px;\n}\n\n.locationbooking_r {\n  position: absolute;\n  top: 19px;\n  right: 15px;\n  height: 25px;\n}\n\n.person_info {\n  padding: 5px;\n  border-top: #dddddd solid 1px;\n}\n\n.person_info h5 {\n  margin: 5px 0;\n  color: #888888;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n}\n\n.person_info span, .person_info b {\n  color: #f38320;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb2tpbmdkZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFhLDRCQUFBO0VBQThCLGtCQUFBO0VBQW9CLGNBQUE7RUFBZ0IsZ0JBQUE7QUFLL0U7O0FBSkE7RUFBaUIsZ0NBQUE7RUFBaUMsY0FBQTtBQVNsRDs7QUFQQTtFQUNJLGFBQUE7QUFVSjs7QUFUSTtFQUNJLFdBQUE7RUFBWSxZQUFBO0VBQWEsa0JBQUE7RUFBbUIsa0JBQUE7QUFjcEQ7O0FBYlE7RUFBSyxXQUFBO0VBQVksWUFBQTtFQUFhLGtCQUFBO0FBa0J0Qzs7QUFoQkk7RUFBbUIsYUFBQTtBQW1CdkI7O0FBbEJRO0VBQUcsU0FBQTtFQUFXLGVBQUE7RUFBaUIsaUJBQUE7RUFBbUIsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0MsY0FBQTtBQTBCbEg7O0FBekJRO0VBQU0sY0FBQTtFQUFnQixnQkFBQTtFQUFpQixlQUFBO0VBQWlCLGtCQUFBO0VBQW9CLG9DQUFBO0FBZ0NwRjs7QUE3QkE7RUFBaUIsWUFBQTtBQWlDakI7O0FBaENJO0VBQUksb0JBQUE7RUFBc0IsU0FBQTtFQUFXLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtBQXdDNUY7O0FBdkNJO0VBQUksVUFBQTtFQUFZLFNBQUE7RUFBVyxjQUFBO0VBQWdCLGVBQUE7RUFBZ0IsZ0JBQUE7RUFBa0Isb0NBQUE7QUErQ2pGOztBQTdDQTtFQUNJLGtCQUFBO0VBQW9CLG1CQUFBO0VBQXFCLGtCQUFBO0VBQW9CLDZCQUFBO0FBbURqRTs7QUFsREk7RUFBSSxVQUFBO0VBQVksYUFBQTtFQUFlLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixnQkFBQTtFQUFrQixvQ0FBQTtBQTBEdEY7O0FBeERBO0VBQW9CLGtCQUFBO0VBQW9CLFNBQUE7RUFBVSxVQUFBO0VBQVksWUFBQTtBQStEOUQ7O0FBOURBO0VBQW9CLGtCQUFBO0VBQW9CLFNBQUE7RUFBVSxXQUFBO0VBQWEsWUFBQTtBQXFFL0Q7O0FBcEVBO0VBQWMsWUFBQTtFQUFjLDZCQUFBO0FBeUU1Qjs7QUF4RUk7RUFDSSxhQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG9DQUFBO0FBMEVSOztBQXZFQTtFQUNJLGNBQUE7QUEwRUoiLCJmaWxlIjoiYm9va2luZ2RldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsb2tpbmdib3h7IGJveC1zaGFkb3c6ICNkZGRkZGQgMCAwIDEwcHg7IGJvcmRlci1yYWRpdXM6IDhweDsgbWFyZ2luOiAyMHB4IDA7IG92ZXJmbG93OiBoaWRkZW47IH1cclxuLmJsb2tpbmdib3hfdG9weyBib3JkZXItYm90dG9tOiNkZGRkZGQgc29saWQgMXB4OyBwYWRkaW5nOiA1cHggMDsgfVxyXG5cclxuLmJvb2tpbmdfYm94X2ltZ3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuYm9va2luZ19ib3hfaW1nX2x7IFxyXG4gICAgICAgIHdpZHRoOjQwcHg7IGhlaWdodDo0MHB4OyBib3JkZXItcmFkaXVzOjUwJTsgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgICAgIGltZ3sgd2lkdGg6MTAwJTsgaGVpZ2h0OjEwMCU7IGJvcmRlci1yYWRpdXM6NTAlOyB9XHJcbiAgICB9XHJcbiAgICAuYm9va2luZ19ib3hfaW1nX3J7bWFyZ2luOiA0cHggMDtcclxuICAgICAgICBoM3ttYXJnaW46IDA7IGZvbnQtc2l6ZTogMTZweDsgbGluZS1oZWlnaHQ6IDMycHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyBjb2xvcjogIzIyMjIyMjsgfVxyXG4gICAgICAgIHNwYW57IGNvbG9yOiAjNDQ0NDQ0OyBmb250LXdlaWdodDo0MDA7IGZvbnQtc2l6ZTogMTNweDsgZm9udC1zdHlsZTogaXRhbGljOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG4gICAgfVxyXG59XHJcbi5ib29raW5nX2JvdHRvbXsgcGFkZGluZzogNXB4O1xyXG4gICAgaDV7IHBhZGRpbmc6IDAgMCA1cHggMHB4OyBtYXJnaW46IDA7IGNvbG9yOiAjZjM4MzIwOyBmb250LXNpemU6IDExcHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbiAgICBoM3sgcGFkZGluZzogMDsgbWFyZ2luOiAwOyBjb2xvcjogIzQ0NDQ0NDsgZm9udC1zaXplOjEzcHg7IGZvbnQtd2VpZ2h0OiA3MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbn1cclxuLmxvY2F0aW9uYm9va2luZ3tcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTsgYmFja2dyb3VuZDogI2Y2ZjZmNjsgcGFkZGluZzogMTVweCA1NXB4OyBib3JkZXItdG9wOiNkZGRkZGQgc29saWQgMXB4O1xyXG4gICAgaDR7IHBhZGRpbmc6IDA7IG1hcmdpbjogN3B4IDA7IGNvbG9yOiAjODg4ODg4OyBmb250LXNpemU6IDE0cHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB9XHJcbn1cclxuLmxvY2F0aW9uYm9va2luZ19seyBwb3NpdGlvbjogYWJzb2x1dGU7IHRvcDoxNXB4OyBsZWZ0OiAxNXB4OyBoZWlnaHQ6IDMwcHg7IH1cclxuLmxvY2F0aW9uYm9va2luZ19yeyBwb3NpdGlvbjogYWJzb2x1dGU7IHRvcDoxOXB4OyByaWdodDogMTVweDsgaGVpZ2h0OiAyNXB4OyB9XHJcbi5wZXJzb25faW5mb3sgcGFkZGluZzogNXB4OyBib3JkZXItdG9wOiNkZGRkZGQgc29saWQgMXB4O1xyXG4gICAgaDV7XHJcbiAgICAgICAgbWFyZ2luOiA1cHggMDtcclxuICAgICAgICBjb2xvcjogIzg4ODg4ODtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjtcclxuICAgIH1cclxufVxyXG4ucGVyc29uX2luZm8gc3BhbiwgLnBlcnNvbl9pbmZvIGJ7XHJcbiAgICBjb2xvcjojZjM4MzIwO1xyXG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_bookingdetails_bookingdetails_module_ts.js.map