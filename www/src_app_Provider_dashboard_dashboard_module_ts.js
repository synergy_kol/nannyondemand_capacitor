"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_dashboard_dashboard_module_ts"],{

/***/ 46966:
/*!****************************************************************!*\
  !*** ./src/app/Provider/dashboard/dashboard-routing.module.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPageRoutingModule": () => (/* binding */ DashboardPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard.page */ 47425);




const routes = [
    {
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_0__.DashboardPage
    }
];
let DashboardPageRoutingModule = class DashboardPageRoutingModule {
};
DashboardPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], DashboardPageRoutingModule);



/***/ }),

/***/ 19344:
/*!********************************************************!*\
  !*** ./src/app/Provider/dashboard/dashboard.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPageModule": () => (/* binding */ DashboardPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dashboard-routing.module */ 46966);
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page */ 47425);







let DashboardPageModule = class DashboardPageModule {
};
DashboardPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_0__.DashboardPageRoutingModule
        ],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_1__.DashboardPage]
    })
], DashboardPageModule);



/***/ }),

/***/ 47425:
/*!******************************************************!*\
  !*** ./src/app/Provider/dashboard/dashboard.page.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DashboardPage": () => (/* binding */ DashboardPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_dashboard_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./dashboard.page.html */ 94047);
/* harmony import */ var _dashboard_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dashboard.page.scss */ 74049);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let DashboardPage = class DashboardPage {
    constructor(menuCtrl, navCtrl, alertController, storage, loadingController, authService) {
        this.menuCtrl = menuCtrl;
        this.navCtrl = navCtrl;
        this.alertController = alertController;
        this.storage = storage;
        this.loadingController = loadingController;
        this.authService = authService;
        this.isLoading = false;
        this.userdata = [];
        this.data = [];
        this.profileData = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.swipeGesture(false);
        this.showLoader('Please wait...');
        this.getDetails();
        this.loadPage();
        this.loginToken();
    }
    getDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-profile", body).then(result => {
                this.data = result;
                console.log("profile: ", this.data);
                this.profileData = this.data.result.data;
                // if(this.profileData.payment_method == ''){
                //   this.navCtrl.navigateRoot('/payment-connect');
                // }
                this.hideLoader();
                this.age = this.calculateAge(this.age);
            }, error => {
                this.hideLoader();
            });
        });
    }
    loadPage() {
        setTimeout(() => {
            this.getDetails();
            this.loadPage();
        }, 1000);
    }
    calculateAge(dob) {
        this.age = dob;
        var dob_entry = this.profileData.dob;
        var today = new Date();
        var birthDate = new Date(dob_entry);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    goProfile() {
        this.navCtrl.navigateForward('/providerprofile');
    }
    goBooking() {
        this.navCtrl.navigateForward('/providerbookings');
    }
    goEarning() {
        this.navCtrl.navigateForward('/earnings');
    }
    goNoti() {
        this.navCtrl.navigateForward('/providernotification');
    }
    goHelp() {
        this.navCtrl.navigateForward('/providerhelp');
    }
    goTC() {
        this.navCtrl.navigateForward('/providertermasconditions');
    }
    goChangepass() {
        this.navCtrl.navigateForward('/providerchangepassword');
    }
    goChangepayment() {
        this.navCtrl.navigateRoot('/payment-connect');
    }
    onLogout() {
        const alert = this.alertController.create({
            header: 'Confirm Logout!',
            message: 'Are you sure, you want to Logout?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Logging out...');
                        setTimeout(() => {
                            this.storage.remove('userDetails');
                            this.hideLoader();
                            this.navCtrl.navigateRoot('/welcome');
                        }, 1000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
DashboardPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.MenuController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.AlertController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider }
];
DashboardPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-dashboard',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_dashboard_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_dashboard_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], DashboardPage);



/***/ }),

/***/ 94047:
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/dashboard/dashboard.page.html ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\" style=\"text-align: center;\">\n    <ion-title>Welcome to Dashboard</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"dashboard_top\">\n    <div class=\"dashboard_img\">\n      <ion-img [src]=\"profileData.profile_image != '' ? profileData.profile_image : 'assets/images/avatar.png'\"></ion-img>\n    </div>\n    <h3>{{profileData.fname}} {{profileData.lname}}, {{age}}</h3>\n    <span>{{profileData.email}}</span>\n    <span>{{profileData.phone}}</span>\n  </div>\n\n  <ion-list class=\"dashboardlisting\">\n    <ion-item (click)=\"goProfile()\">\n      <img src=\"assets/images/dashboard_menu1.jpg\" alt=\"dashboard_menu1\" />\n      <ion-label>My Profile</ion-label>\n    </ion-item>\n    <ion-item (click)=\"goNoti()\">\n      <img src=\"assets/images/dashboard_menu4.jpg\" alt=\"dashboard_menu4\" />\n      <ion-label>Schedule Notifications</ion-label>\n      <ion-badge *ngIf=\"profileData.unread_notifications != 0\" slot=\"end\" color=\"danger\" style=\"border-radius: 20px; padding: 3px 10px;\">{{profileData.unread_notifications}}</ion-badge>\n    </ion-item>\n    <ion-item (click)=\"goChangepass()\">\n      <img src=\"assets/images/dashboard_menu5.jpg\" alt=\"dashboard_menu7\" />\n      <ion-label>Change Password</ion-label>\n    </ion-item>\n    <ion-item (click)=\"goChangepayment()\">\n      <img src=\"assets/images/dashboard_menu3.jpg\" alt=\"dashboard_menu7\" />\n      <ion-label>Change Payment Type</ion-label>\n    </ion-item>\n    <ion-item (click)=\"goBooking()\">\n      <img src=\"assets/images/dashboard_menu2.jpg\" alt=\"dashboard_menu2\" />\n      <ion-label>My Bookings</ion-label>\n    </ion-item>\n    <ion-item (click)=\"goEarning()\">\n      <img src=\"assets/images/dashboard_menu3.jpg\" alt=\"dashboard_menu3\" />\n      <ion-label>My Earnings</ion-label>\n    </ion-item>    \n    <ion-item (click)=\"goHelp()\">\n      <img src=\"assets/images/dashboard_menu5.jpg\" alt=\"dashboard_menu5\" />\n      <ion-label>Help</ion-label>\n    </ion-item>\n    <ion-item (click)=\"goTC()\">\n      <img src=\"assets/images/dashboard_menu6.jpg\" alt=\"dashboard_menu6\" />\n      <ion-label>Terms & Conditions</ion-label>\n    </ion-item>\n    <ion-item lines=\"none\" (click)=\"onLogout()\">\n      <img src=\"assets/images/dashboard_menu7.jpg\" alt=\"dashboard_menu7\" />\n      <ion-label>Logout</ion-label>\n    </ion-item>\n  </ion-list>\n</ion-content>");

/***/ }),

/***/ 74049:
/*!********************************************************!*\
  !*** ./src/app/Provider/dashboard/dashboard.page.scss ***!
  \********************************************************/
/***/ ((module) => {

module.exports = ".dashboard_top {\n  background: #41b578;\n  padding: 10px 0 15px 0;\n}\n.dashboard_top h3 {\n  padding: 0;\n  margin: 0 0 2px 0;\n  color: #ffffff;\n  font-size: 18px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.dashboard_top span {\n  padding: 2px 0;\n  margin: 0;\n  color: #ffffff;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n  display: block;\n}\n.dashboard_img {\n  width: 120px;\n  height: 120px;\n  border-radius: 50%;\n  margin: 12px auto;\n  border: #fff solid 5px;\n  overflow: hidden;\n}\n.dashboard_img ion-img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  overflow: hidden;\n  object-fit: cover;\n}\n.dashboardlisting {\n  margin-left: -20px;\n}\n.dashboardlisting ion-item ion-label {\n  padding: 5px 20px !important;\n  color: #444444 !important;\n  font-size: 14px !important;\n  font-family: \"Open Sans\", sans-serif !important;\n  font-weight: 600;\n}\n.dashboardlisting ion-item img {\n  padding-left: 20px;\n  width: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhc2hib2FyZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBZ0IsbUJBQUE7RUFBcUIsc0JBQUE7QUFHckM7QUFGSTtFQUFJLFVBQUE7RUFBWSxpQkFBQTtFQUFtQixjQUFBO0VBQWdCLGVBQUE7RUFBZ0IsZ0JBQUE7RUFBa0Isb0NBQUE7RUFBc0Msa0JBQUE7QUFXL0g7QUFWSTtFQUFNLGNBQUE7RUFBZ0IsU0FBQTtFQUFXLGNBQUE7RUFBZ0IsZUFBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxrQkFBQTtFQUFvQixjQUFBO0FBb0JqSjtBQWxCQTtFQUFnQixZQUFBO0VBQWMsYUFBQTtFQUFlLGtCQUFBO0VBQW1CLGlCQUFBO0VBQW1CLHNCQUFBO0VBQXdCLGdCQUFBO0FBMkIzRztBQTFCSTtFQUFTLFdBQUE7RUFBYSxZQUFBO0VBQWMsa0JBQUE7RUFBbUIsZ0JBQUE7RUFBa0IsaUJBQUE7QUFpQzdFO0FBL0JBO0VBQW9CLGtCQUFBO0FBbUNwQjtBQWxDQTtFQUFzQyw0QkFBQTtFQUE4Qix5QkFBQTtFQUEyQiwwQkFBQTtFQUEyQiwrQ0FBQTtFQUFnRCxnQkFBQTtBQTBDMUs7QUF6Q0E7RUFBZ0Msa0JBQUE7RUFBb0IsV0FBQTtBQThDcEQiLCJmaWxlIjoiZGFzaGJvYXJkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kYXNoYm9hcmRfdG9weyBiYWNrZ3JvdW5kOiAjNDFiNTc4OyBwYWRkaW5nOiAxMHB4IDAgMTVweCAwOyBcclxuICAgIGgzeyBwYWRkaW5nOiAwOyBtYXJnaW46IDAgMCAycHggMDsgY29sb3I6ICNmZmZmZmY7IGZvbnQtc2l6ZToxOHB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgdGV4dC1hbGlnbjogY2VudGVyOyB9XHJcbiAgICBzcGFueyBwYWRkaW5nOiAycHggMDsgbWFyZ2luOiAwOyBjb2xvcjogI2ZmZmZmZjsgZm9udC1zaXplOjE0cHg7IGZvbnQtd2VpZ2h0OiA0MDA7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyB0ZXh0LWFsaWduOiBjZW50ZXI7IGRpc3BsYXk6IGJsb2NrOyB9XHJcbn1cclxuLmRhc2hib2FyZF9pbWd7IHdpZHRoOiAxMjBweDsgaGVpZ2h0OiAxMjBweDsgYm9yZGVyLXJhZGl1czo1MCU7IG1hcmdpbjogMTJweCBhdXRvOyBib3JkZXI6ICNmZmYgc29saWQgNXB4OyBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgaW9uLWltZ3sgd2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgYm9yZGVyLXJhZGl1czo1MCU7IG92ZXJmbG93OiBoaWRkZW47IG9iamVjdC1maXQ6IGNvdmVyO31cclxufVxyXG4uZGFzaGJvYXJkbGlzdGluZ3sgIG1hcmdpbi1sZWZ0OiAtMjBweDsgfVxyXG4uZGFzaGJvYXJkbGlzdGluZyBpb24taXRlbSBpb24tbGFiZWx7IHBhZGRpbmc6IDVweCAyMHB4ICFpbXBvcnRhbnQ7IGNvbG9yOiAjNDQ0NDQ0ICFpbXBvcnRhbnQ7IGZvbnQtc2l6ZToxNHB4ICFpbXBvcnRhbnQ7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmIWltcG9ydGFudDsgZm9udC13ZWlnaHQ6IDYwMDsgfVxyXG4uZGFzaGJvYXJkbGlzdGluZyBpb24taXRlbSBpbWd7IHBhZGRpbmctbGVmdDogMjBweDsgd2lkdGg6IDQwcHg7IH0iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_dashboard_dashboard_module_ts.js.map