"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_schedule_schedule_module_ts"],{

/***/ 84248:
/*!**********************************************************!*\
  !*** ./src/app/User/schedule/schedule-routing.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchedulePageRoutingModule": () => (/* binding */ SchedulePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _schedule_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./schedule.page */ 73249);




const routes = [
    {
        path: '',
        component: _schedule_page__WEBPACK_IMPORTED_MODULE_0__.SchedulePage
    }
];
let SchedulePageRoutingModule = class SchedulePageRoutingModule {
};
SchedulePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SchedulePageRoutingModule);



/***/ }),

/***/ 37999:
/*!**************************************************!*\
  !*** ./src/app/User/schedule/schedule.module.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchedulePageModule": () => (/* binding */ SchedulePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _schedule_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./schedule-routing.module */ 84248);
/* harmony import */ var _schedule_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./schedule.page */ 73249);







let SchedulePageModule = class SchedulePageModule {
};
SchedulePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _schedule_routing_module__WEBPACK_IMPORTED_MODULE_0__.SchedulePageRoutingModule
        ],
        declarations: [_schedule_page__WEBPACK_IMPORTED_MODULE_1__.SchedulePage]
    })
], SchedulePageModule);



/***/ }),

/***/ 73249:
/*!************************************************!*\
  !*** ./src/app/User/schedule/schedule.page.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SchedulePage": () => (/* binding */ SchedulePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_schedule_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./schedule.page.html */ 57895);
/* harmony import */ var _schedule_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./schedule.page.scss */ 23975);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let SchedulePage = class SchedulePage {
    constructor(navCtrl, loadingController, toastController, storage, authService) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.storage = storage;
        this.authService = authService;
        this.pop = false;
        this.data = [];
        this.userdata = [];
        this.isDisabled = false;
        this.isLoading = false;
        this.minutes = 29;
        this.seconds = 60;
        this.scheduleData = [];
        this.today = new Date().toISOString();
        this.date = "";
        this.timeslot = "";
        this.workinghour = "";
        this.scheduleSelectedData = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    onChecked(ev, data, indx) {
        if (ev.target.checked) {
            this.scheduleSelectedData.push(data);
        }
        else {
            this.scheduleSelectedData.splice(indx, 1);
        }
        console.log(this.scheduleSelectedData);
    }
    ionViewWillEnter() {
        this.showLoader('Loading...');
        this.loginToken();
        this.findSchedule();
    }
    findSchedule() {
        this.storage.get('userDetails').then((result) => {
            this.data = result;
            var scghrdulebody = {
                user_id: this.data.user_id
            };
            this.authService.postData("get-booked-schedule", scghrdulebody).then(result => {
                this.data = result;
                console.log("Schedule:", this.data);
                if (this.data.status.error_code == 0) {
                    this.scheduleData = this.data.result.data;
                    this.payon = this.data.result.is_accepted;
                }
                else {
                    this.presentToast(this.data.status.message);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    onBook() {
        this.showLoader('Loading...');
        this.storage.set("payDetails", this.scheduleSelectedData);
        setTimeout(() => {
            this.scheduleSelectedData = [];
            this.navCtrl.navigateForward("/payment");
            this.hideLoader();
        }, 3000);
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
SchedulePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider }
];
SchedulePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-schedule',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_schedule_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_schedule_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SchedulePage);



/***/ }),

/***/ 57895:
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/schedule/schedule.page.html ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"nannydetails\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Schedule List</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf=\"scheduleData == ''\" style=\"padding:50px 20px; color: #bfbfbf; font-weight: bold; font-size: 22px; text-align: center;\">[ Sorry! Nothing Found. ]</div>\n  <ion-item lines=\"none\" class=\"pl_input\"\n    style=\"padding: 0 !important; position: relative; --background: #eeeeee; margin: 1px 0;\"\n    *ngFor=\"let data of scheduleData.reverse(); let i = index\">\n    <ion-checkbox slot=\"start\" (ionChange)=\"onChecked($event, data, i)\" color=\"warning\" [disabled]=\"data.request_status != 1\"></ion-checkbox>\n    <ion-label>{{data.date}}\n      <div style=\"margin-top: 5px;\">\n        <ion-badge color=\"dark\" style=\"border-radius: 20px; padding: 3px 10px; margin-right: 5px;\">\n          {{data.time_slot}} / {{data.hour}} hr\n        </ion-badge>\n      </div>\n    </ion-label>\n    <ion-label slot=\"end\" style=\"text-align: right;\" *ngIf=\"data.request_status == 1\">\n      <ion-badge color=\"warning\" style=\"border-radius: 20px; padding: 5px 10px; margin-right: 5px;\">Accepted</ion-badge>\n    </ion-label>\n    <ion-label slot=\"end\" style=\"text-align: right;\" *ngIf=\"data.request_status == 0\">\n      <ion-badge color=\"dark\" style=\"border-radius: 20px; padding: 5px 10px; margin-right: 5px;\">Pending</ion-badge>\n    </ion-label>\n    <ion-label slot=\"end\" style=\"text-align: right;\" *ngIf=\"data.request_status == 2\">\n      <ion-badge color=\"danger\" style=\"border-radius: 20px; padding: 5px 10px; margin-right: 5px;\">Declined</ion-badge>\n    </ion-label>\n  </ion-item>\n</ion-content>\n\n<ion-footer *ngIf=\"scheduleSelectedData != ''\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-button color=\"light\" fill=\"clear\" expand=\"full\" (click)=\"onBook()\"\n      style=\"font-weight: bold; font-family: 'Open Sans', sans-serif; font-weight: 600; text-transform: uppercase; font-size: 14px;\">\n      Pay Now</ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 23975:
/*!**************************************************!*\
  !*** ./src/app/User/schedule/schedule.page.scss ***!
  \**************************************************/
/***/ ((module) => {

module.exports = ".custompop {\n  position: absolute;\n  background: rgba(0, 0, 0, 0.7);\n  top: 0;\n  left: 0;\n  z-index: 99;\n  width: 100%;\n  height: 100%;\n  display: table;\n}\n.custompop .custompop_in {\n  display: table-cell;\n  vertical-align: middle;\n  padding: 0 20px;\n}\n.custompop .popin {\n  background: #ffffff;\n  padding: 20px;\n  width: 100%;\n  border-radius: 20px;\n}\n.custompop .popin h3 {\n  font-size: 20px;\n  font-weight: bold;\n  text-align: center;\n  margin: 0 0 20px 0;\n}\n.cust_input span {\n  position: absolute;\n  right: 10px;\n  border-left: 1px #9e9d9d solid;\n  padding-left: 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjaGVkdWxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQW9CLDhCQUFBO0VBQWdELE1BQUE7RUFBUSxPQUFBO0VBQVMsV0FBQTtFQUFhLFdBQUE7RUFBYSxZQUFBO0VBQWMsY0FBQTtBQVFqSTtBQVBJO0VBQWMsbUJBQUE7RUFBcUIsc0JBQUE7RUFBd0IsZUFBQTtBQVkvRDtBQVhJO0VBQU8sbUJBQUE7RUFBcUIsYUFBQTtFQUFlLFdBQUE7RUFBYSxtQkFBQTtBQWlCNUQ7QUFoQlE7RUFBRyxlQUFBO0VBQWlCLGlCQUFBO0VBQW1CLGtCQUFBO0VBQW9CLGtCQUFBO0FBc0JuRTtBQW5CQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsaUJBQUE7QUFzQkoiLCJmaWxlIjoic2NoZWR1bGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmN1c3RvbXBvcHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgYmFja2dyb3VuZDogcmdiYSgkY29sb3I6ICMwMDAwMDAsICRhbHBoYTogMC43KTsgdG9wOiAwOyBsZWZ0OiAwOyB6LWluZGV4OiA5OTsgd2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgZGlzcGxheTogdGFibGU7XHJcbiAgICAuY3VzdG9tcG9wX2lue2Rpc3BsYXk6IHRhYmxlLWNlbGw7IHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7IHBhZGRpbmc6IDAgMjBweDt9XHJcbiAgICAucG9waW57YmFja2dyb3VuZDogI2ZmZmZmZjsgcGFkZGluZzogMjBweDsgd2lkdGg6IDEwMCU7IGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgICAgaDN7Zm9udC1zaXplOiAyMHB4OyBmb250LXdlaWdodDogYm9sZDsgdGV4dC1hbGlnbjogY2VudGVyOyBtYXJnaW46IDAgMCAyMHB4IDA7fVxyXG4gICAgfVxyXG59XHJcbi5jdXN0X2lucHV0IHNwYW57XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMTBweDtcclxuICAgIGJvcmRlci1sZWZ0OiAxcHggIzllOWQ5ZCBzb2xpZDtcclxuICAgIHBhZGRpbmctbGVmdDogNHB4O1xyXG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_schedule_schedule_module_ts.js.map