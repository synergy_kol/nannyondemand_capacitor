"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_User_home_home_module_ts"],{

/***/ 70034:
/*!**************************************************!*\
  !*** ./src/app/User/home/home-routing.module.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageRoutingModule": () => (/* binding */ HomePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 63605);




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], HomePageRoutingModule);



/***/ }),

/***/ 47730:
/*!******************************************!*\
  !*** ./src/app/User/home/home.module.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageModule": () => (/* binding */ HomePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home-routing.module */ 70034);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page */ 63605);







//import { StarRatingModule } from 'ionic5-star-rating';
let HomePageModule = class HomePageModule {
};
HomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _home_routing_module__WEBPACK_IMPORTED_MODULE_0__.HomePageRoutingModule,
            //StarRatingModule
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_1__.HomePage]
    })
], HomePageModule);



/***/ }),

/***/ 63605:
/*!****************************************!*\
  !*** ./src/app/User/home/home.page.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePage": () => (/* binding */ HomePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./home.page.html */ 97793);
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page.scss */ 44730);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_app_MyEvents__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/MyEvents */ 40576);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);








let HomePage = class HomePage {
    constructor(storage, navCtrl, menuCtrl, authService, toastController, loadingController, events) {
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.authService = authService;
        this.toastController = toastController;
        this.loadingController = loadingController;
        this.events = events;
        this.serviceList = {
            initialSlide: 0,
            slidesPerView: 3,
            spaceBetween: 5,
        };
        this.nannylistopt = {
            initialSlide: 0,
            slidesPerView: 1.6,
            spaceBetween: 5,
        };
        this.isLoading = false;
        this.isDisabled = false;
        this.serviceData = [];
        this.nannyData = [];
        this.popularnannyData = [];
        this.userdata = [];
        this.profileData = [];
    }
    ngOnInit() {
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.swipeGesture(true);
        this.showLoader('Please wait...');
        this.loginToken();
        var serBody = {
            source: 'mob'
        };
        this.authService.postData("service-type-list", serBody).then(result => {
            this.data = result;
            this.serviceData = this.data.result.data;
            console.log("serviceData: ", this.serviceData);
            this.hideLoader();
        }, error => {
            this.hideLoader();
        });
        this.nannyList();
        this.popularnannyList();
        this.profileDetails();
        this.loadPage();
    }
    onView() {
        this.navCtrl.navigateForward("/schedule");
    }
    profileDetails() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var body = {
                user_id: this.userdata.user_id,
            };
            this.authService.postData("get-profile", body).then(result => {
                this.data = result;
                console.log("profile: ", this.data);
                this.profileData = this.data.result.data;
                this.events.publishSomeData({ profile: this.profileData });
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    loadPage() {
        setTimeout(() => {
            this.profileDetails();
            this.loadPage();
        }, 1000);
    }
    nannyList() {
        this.storage.get('userDetails').then((val) => {
            var body = {
                user_id: val.user_id
            };
            this.authService.postData("get-nany-list", body).then(result => {
                this.data = result;
                this.nannyData = this.data.result.data;
                console.log("Nanny List: ", this.nannyData);
            }, error => {
                this.hideLoader();
            });
        });
    }
    popularnannyList() {
        this.storage.get('userDetails').then((val) => {
            var body = {
                user_id: val.user_id,
                sort_order: 'DESC'
            };
            this.authService.postData("get-nany-list", body).then(result => {
                this.data = result;
                this.popularnannyData = this.data.result.data;
                console.log("Popular Nanny List: ", this.nannyData);
            }, error => {
                this.hideLoader();
            });
        });
    }
    onFilter() {
        this.navCtrl.navigateForward('filter');
    }
    goNannyList(type, id) {
        var object = {
            ser_type: type,
            ser_id: id
        };
        this.navCtrl.navigateForward(["/nannylist"], { queryParams: object });
    }
    splitAge(age) {
        var ageArr = age.split(' ');
        return ageArr[0];
    }
    goNannyDetails(nanny_id) {
        var object = {
            nanny_id: nanny_id
        };
        this.navCtrl.navigateForward(["/nannydetails"], { queryParams: object });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.MenuController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_4__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: src_app_MyEvents__WEBPACK_IMPORTED_MODULE_3__.MyEvents }
];
HomePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-home',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], HomePage);



/***/ }),

/***/ 97793:
/*!*********************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/User/home/home.page.html ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"user-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button><img src=\"assets/images/menu_icon.png\" style=\"height: 20px;\" /></ion-menu-button>\n    </ion-buttons>\n    <div class=\"home_search\" (click)=\"onFilter()\">\n      <img class=\"search_icon\" src=\"assets/images/address_icon.png\" />\n      <ion-input class=\"search_input\" placeholder=\"Search\" disabled></ion-input>\n      <img class=\"filter_icon\" src=\"assets/images/filter_icon.png\" alt=\"filter_icon\" />\n    </div>   \n    <ion-buttons slot=\"end\">\n      <ion-tab-button tab=\"schedule\" style=\"color: #ffffff;\" (click)=\"onView()\">\n        <ion-badge *ngIf=\"profileData.unread_notifications != 0\" style=\"background: red; text-align: center; top: 2px; left: 10px; z-index: 9; font-size: 9px; width: 15px; line-height: 15px; height: 15px; padding: 1px;\">{{profileData.unread_notifications}}</ion-badge>\n        <ion-icon name=\"calendar\" style=\"font-size: 24px; padding: 5px; padding-left: 0;\"></ion-icon>        \n      </ion-tab-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <img src=\"assets/images/home_bg.png\" style=\"width: 100%;\" />\n  <div style=\"padding: 0 20px;\">\n    <div class=\"parts\">\n      <h3>Search by service</h3>\n      <ion-slides [options]=\"serviceList\">\n        <ion-slide *ngFor=\"let data of serviceData\" (click)=\"goNannyList(data.service_name, data.service_type_id)\">\n          <a class=\"sericon\"><img [src]=\"data.service_image\" alt=\"icon\" /><span>{{data.service_name}}</span></a>\n        </ion-slide>\n      </ion-slides>\n    </div>\n    <div class=\"parts\">\n      <h3>Popular Nanny</h3>\n      <ion-slides [options]=\"nannylistopt\">\n        <ion-slide *ngFor=\"let data of popularnannyData | slice:0:4\" (click)=\"goNannyDetails(data.user_id)\">\n          <div class=\"sliderbox\">           \n            <div class=\"box_image\"><img [src]=\"data.profile_image != '' ? data.profile_image : 'assets/images/avatar.png'\" alt=\"slider\" /></div>\n            <div class=\"box_details\">\n              <div style=\"display: flex; align-items: center;\">\n                <ionic5-star-rating #rating activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\"\n                  defaultColor=\"#c4c4c4\" readonly=\"true\" rating=\"{{data.avg_rate}}\" fontSize=\"15px\">\n                </ionic5-star-rating>\n                <!-- <p *ngIf=\"data.avg_rate > 0\" style=\"margin: 0; margin-left: 10px; font-size: 14px;\">({{data.avg_rate}})</p> -->\n              </div>\n              <h3>{{data.fname}} {{data.lname}}, {{splitAge(data.age)}}</h3>\n              <span>{{data.city}}</span>\n              <a><img src=\"assets/images/licence_icon.png\" alt=\"licence_icon\" /></a>\n              <a *ngIf=\"data.certificate == 'Yes'\"><img src=\"assets/images/badge_icon.png\" alt=\"badge_icon\" /></a>\n              <a *ngIf=\"data.comfortable_pet == 'Yes'\"><img src=\"assets/images/paw_icon.png\" alt=\"paw_icon\" /></a>\n            </div>\n          </div>\n        </ion-slide>\n      </ion-slides>\n    </div>\n    <div class=\"parts\">\n      <h3>Recently Added</h3>\n      <ion-slides [options]=\"nannylistopt\">\n        <ion-slide *ngFor=\"let data of nannyData | slice:0:4\" (click)=\"goNannyDetails(data.user_id)\">\n          <div class=\"sliderbox\">           \n            <div class=\"box_image\"><img [src]=\"data.profile_image != '' ? data.profile_image : 'assets/images/avatar.png'\" alt=\"slider\" /></div>\n            <div class=\"box_details\">\n              <div style=\"display: flex; align-items: center;\">\n                <ionic5-star-rating #rating activeIcon=\"star\" defaultIcon=\"star\" activeColor=\"#fec400\"\n                  defaultColor=\"#c4c4c4\" readonly=\"true\" rating=\"{{data.avg_rate}}\" fontSize=\"15px\">\n                </ionic5-star-rating>\n                <!-- <p *ngIf=\"data.avg_rate > 0\" style=\"margin: 0; margin-left: 10px; font-size: 14px;\">({{data.avg_rate}})</p> -->\n              </div>\n              <h3>{{data.fname}} {{data.lname}}, {{splitAge(data.age)}}</h3>\n              <span>{{data.city}}</span>\n              <a><img src=\"assets/images/licence_icon.png\" alt=\"licence_icon\" /></a>\n              <a *ngIf=\"data.certificate == 'Yes'\"><img src=\"assets/images/badge_icon.png\" alt=\"badge_icon\" /></a>\n              <a *ngIf=\"data.comfortable_pet == 'Yes'\"><img src=\"assets/images/paw_icon.png\" alt=\"paw_icon\" /></a>\n            </div>\n          </div>\n        </ion-slide>\n      </ion-slides>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 44730:
/*!******************************************!*\
  !*** ./src/app/User/home/home.page.scss ***!
  \******************************************/
/***/ ((module) => {

module.exports = ".home_search {\n  border: 1px #ffffff solid;\n  width: 95%;\n  margin: 0 auto;\n  border-radius: 50px;\n  background: #fff;\n  position: relative;\n  padding: 0 45px 0 30px;\n}\n.home_search .search_icon {\n  position: absolute;\n  left: 10px;\n  top: 7px;\n  height: 15px;\n  opacity: 0.6;\n}\n.home_search .filter_icon {\n  position: absolute;\n  right: 10px;\n  top: 7px;\n  height: 17px;\n}\n.home_search .search_input {\n  height: 30px;\n  font-size: 12px;\n  margin: 0;\n  padding: 0 !important;\n  text-align: left;\n  background: #ffffff;\n  --background: #ffffff;\n  color: #666666;\n  font-size: 13px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  border: 1px #ffffff solid;\n}\n.parts {\n  margin: 20px 0;\n}\n.parts h3 {\n  padding: 0 0 15px 0;\n  margin: 0;\n  color: #222222;\n  font-size: 22px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.parts .sericon {\n  display: block;\n  text-decoration: none;\n}\n.parts .sericon img {\n  width: 80%;\n}\n.parts .sericon span {\n  display: block;\n  color: #444444;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  padding: 5px 0 0 0;\n}\n.sliderbox {\n  background: #ffffff;\n  box-shadow: #dddddd 0 1px 5px;\n  width: 250px;\n  border-radius: 10px;\n  margin: 5px;\n  overflow: hidden;\n}\n.sliderbox .box_image {\n  width: 100%;\n  height: 130px;\n  background: #eeeeee;\n  overflow: hidden;\n}\n.sliderbox .box_image img {\n  height: 100%;\n  object-fit: cover;\n  object-position: top;\n}\n.sliderbox .box_details {\n  padding: 10px;\n  text-align: left;\n}\n.sliderbox .box_details h3 {\n  padding: 0 0 2px 0;\n  margin: 0;\n  color: #333333;\n  font-size: 16px;\n  font-weight: 800;\n  font-family: \"Open Sans\", sans-serif;\n}\n.sliderbox .box_details span {\n  color: #555555;\n  font-size: 12px;\n  font-style: italic;\n  padding: 0 0 12px 0;\n  display: block;\n}\n.sliderbox .box_details a {\n  margin: 3px 12px 0 0;\n}\n.sliderbox .box_details a img {\n  height: 20px;\n}\n.sliderbox h3 {\n  width: 200px;\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0kseUJBQUE7RUFBMkIsVUFBQTtFQUFZLGNBQUE7RUFBZ0IsbUJBQUE7RUFBcUIsZ0JBQUE7RUFBa0Isa0JBQUE7RUFBb0Isc0JBQUE7QUFNdEg7QUFMSTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQU9SO0FBTEk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQU9SO0FBTEk7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFBcUIscUJBQUE7RUFBdUIsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLHlCQUFBO0FBYTdJO0FBVkE7RUFDSSxjQUFBO0FBYUo7QUFaSTtFQUNJLG1CQUFBO0VBQ0EsU0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQ0FBQTtBQWNSO0FBWkk7RUFBVSxjQUFBO0VBQWdCLHFCQUFBO0FBZ0I5QjtBQWZRO0VBQUssVUFBQTtBQWtCYjtBQWpCUTtFQUFNLGNBQUE7RUFBZ0IsY0FBQTtFQUFnQixlQUFBO0VBQWdCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGtCQUFBO0FBeUJ0SDtBQXRCQTtFQUNJLG1CQUFBO0VBQXFCLDZCQUFBO0VBQStCLFlBQUE7RUFBYyxtQkFBQTtFQUFxQixXQUFBO0VBQWEsZ0JBQUE7QUE4QnhHO0FBN0JJO0VBQ0ksV0FBQTtFQUFhLGFBQUE7RUFBZSxtQkFBQTtFQUFxQixnQkFBQTtBQWtDekQ7QUFqQ1E7RUFBSyxZQUFBO0VBQWMsaUJBQUE7RUFBbUIsb0JBQUE7QUFzQzlDO0FBcENJO0VBQ0ksYUFBQTtFQUFlLGdCQUFBO0FBdUN2QjtBQXRDUTtFQUFHLGtCQUFBO0VBQW9CLFNBQUE7RUFBVyxjQUFBO0VBQWdCLGVBQUE7RUFBZ0IsZ0JBQUE7RUFBa0Isb0NBQUE7QUE4QzVGO0FBN0NRO0VBQU0sY0FBQTtFQUFnQixlQUFBO0VBQWdCLGtCQUFBO0VBQW9CLG1CQUFBO0VBQXFCLGNBQUE7QUFvRHZGO0FBbkRRO0VBQUcsb0JBQUE7QUFzRFg7QUFyRFk7RUFBSyxZQUFBO0FBd0RqQjtBQXJEQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUF1REoiLCJmaWxlIjoiaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmhvbWVfc2VhcmNoeyBcclxuICAgIGJvcmRlcjogMXB4ICNmZmZmZmYgc29saWQ7IHdpZHRoOiA5NSU7IG1hcmdpbjogMCBhdXRvOyBib3JkZXItcmFkaXVzOiA1MHB4OyBiYWNrZ3JvdW5kOiAjZmZmOyBwb3NpdGlvbjogcmVsYXRpdmU7IHBhZGRpbmc6IDAgNDVweCAwIDMwcHg7XHJcbiAgICAuc2VhcmNoX2ljb257XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDEwcHg7XHJcbiAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgICAgIG9wYWNpdHk6IDAuNjtcclxuICAgIH1cclxuICAgIC5maWx0ZXJfaWNvbntcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgcmlnaHQ6IDEwcHg7XHJcbiAgICAgICAgdG9wOiA3cHg7XHJcbiAgICAgICAgaGVpZ2h0OiAxN3B4O1xyXG4gICAgfVxyXG4gICAgLnNlYXJjaF9pbnB1dCB7XHJcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZmZmZjsgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmOyBjb2xvcjogIzY2NjY2NjsgZm9udC1zaXplOiAxM3B4OyBmb250LXdlaWdodDogNDAwOyBmb250LWZhbWlseTogJ09wZW4gU2FucycsIHNhbnMtc2VyaWY7IGJvcmRlcjogMXB4ICNmZmZmZmYgc29saWQ7XHJcbiAgICB9XHJcbn1cclxuLnBhcnRze1xyXG4gICAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgICBoM3tcclxuICAgICAgICBwYWRkaW5nOiAwIDAgMTVweCAwO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBjb2xvcjogIzIyMjIyMjtcclxuICAgICAgICBmb250LXNpemU6MjJweDtcclxuICAgICAgICBmb250LXdlaWdodDogNzAwO1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgfVxyXG4gICAgLnNlcmljb257IGRpc3BsYXk6IGJsb2NrOyB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgICAgICAgaW1neyB3aWR0aDogODAlOyB9XHJcbiAgICAgICAgc3BhbnsgZGlzcGxheTogYmxvY2s7IGNvbG9yOiAjNDQ0NDQ0OyBmb250LXNpemU6MTRweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IHBhZGRpbmc6IDVweCAwIDAgMDsgfVxyXG4gICAgfVxyXG59XHJcbi5zbGlkZXJib3h7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmOyBib3gtc2hhZG93OiAjZGRkZGRkIDAgMXB4IDVweDsgd2lkdGg6IDI1MHB4OyBib3JkZXItcmFkaXVzOiAxMHB4OyBtYXJnaW46IDVweDsgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIC5ib3hfaW1hZ2V7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7IGhlaWdodDogMTMwcHg7IGJhY2tncm91bmQ6ICNlZWVlZWU7IG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgaW1neyBoZWlnaHQ6IDEwMCU7IG9iamVjdC1maXQ6IGNvdmVyOyBvYmplY3QtcG9zaXRpb246IHRvcDt9XHJcbiAgICB9XHJcbiAgICAuYm94X2RldGFpbHN7XHJcbiAgICAgICAgcGFkZGluZzogMTBweDsgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICBoM3twYWRkaW5nOiAwIDAgMnB4IDA7IG1hcmdpbjogMDsgY29sb3I6ICMzMzMzMzM7IGZvbnQtc2l6ZToxNnB4OyBmb250LXdlaWdodDogODAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG4gICAgICAgIHNwYW57IGNvbG9yOiAjNTU1NTU1OyBmb250LXNpemU6MTJweDsgZm9udC1zdHlsZTogaXRhbGljOyBwYWRkaW5nOiAwIDAgMTJweCAwOyBkaXNwbGF5OiBibG9jazsgfVxyXG4gICAgICAgIGF7IG1hcmdpbjogM3B4IDEycHggMCAwOyBcclxuICAgICAgICAgICAgaW1neyBoZWlnaHQ6IDIwcHg7IH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbmgzIHtcclxuICAgIHdpZHRoOiAyMDBweDtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7IFxyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgXHJcbiAgICB9XHJcblxyXG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_User_home_home_module_ts.js.map