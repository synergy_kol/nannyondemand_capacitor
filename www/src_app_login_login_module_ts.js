"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_login_login_module_ts"],{

/***/ 62359:
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 60955);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 69549:
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 62359);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 60955);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 60955:
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./login.page.html */ 99403);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 6051);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var _awesome_cordova_plugins_onesignal_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/onesignal/ngx */ 7490);








let LoginPage = class LoginPage {
    constructor(navCtrl, loadingController, toastController, authService, storage, oneSignal) {
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.authService = authService;
        this.storage = storage;
        this.oneSignal = oneSignal;
        this.email = '';
        this.password = '';
        this.isDisabled = false;
        this.isLoading = false;
    }
    ngOnInit() {
        this.storage.create();
    }
    onKeyboard(event) {
        if (event != null) {
            event.setFocus();
        }
        else {
            this.onLogin();
        }
    }
    ionViewWillEnter() {
        this.oneSignal.startInit('a1b8a842-b105-4ccd-8365-ca353da71c75', '217316564970');
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
            //console.log('Recived:', res);
        });
        this.oneSignal.handleNotificationOpened().subscribe(() => {
            //console.log('Opened:', res);
        });
        this.oneSignal.endInit();
        this.oneSignal.getIds().then(identity => {
            console.log(identity.userId + " It's Devices ID");
            this.playerID = identity.userId;
        });
    }
    onLogin() {
        const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (this.email.trim() == '') {
            this.presentToast('Please enter your registered Email ID');
        }
        else if (!emailPattern.test(this.email)) {
            this.presentToast('Wrong Email Format...');
        }
        else if (this.password.trim() == '') {
            this.presentToast('Please enter Password');
        }
        else {
            this.showLoader('Please wait...');
            var body = {
                email: this.email,
                password: this.password,
                device_token: this.playerID
            };
            this.authService.postData("login", body).then(result => {
                this.data = result;
                this.userData = this.data.result.data;
                console.log("Login: ", this.userData);
                this.hideLoader();
                if (this.data.status.error_code == 0) {
                    this.storage.set("userDetails", this.userData);
                    if (this.userData.role_id == 1) {
                        this.navCtrl.navigateRoot('/home');
                    }
                    else {
                        this.navCtrl.navigateRoot('/dashboard');
                    }
                }
                else {
                    this.presentToast(this.data.status.message);
                }
            }, error => {
                this.hideLoader();
            });
        }
    }
    goRegister() {
        this.navCtrl.navigateRoot('/welcome');
    }
    goForgot() {
        this.navCtrl.navigateForward('/forgotpass');
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthServiceProvider },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage },
    { type: _awesome_cordova_plugins_onesignal_ngx__WEBPACK_IMPORTED_MODULE_4__.OneSignal }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-login',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_login_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], LoginPage);



/***/ }),

/***/ 99403:
/*!******************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/login/login.page.html ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"login_screen\">\n    <div class=\"login_panel\">\n      <img src=\"assets/images/login_img.png\" alt=\"Log\" />\n      <h3>Login</h3>\n      <ion-input class=\"cust_input\" placeholder=\"User Email\" type=\"email\" [(ngModel)]=\"email\"\n        (keyup.enter)=\"onKeyboard(Field1)\"></ion-input>\n      <ion-input class=\"cust_input\" placeholder=\"Password\" type=\"password\" [(ngModel)]=\"password\" #Field1\n        (keyup.enter)=\"onKeyboard()\"></ion-input>\n      <span (click)=\"goForgot()\">Forgot Password?</span>\n      <ion-button color=\"dark\" expand=\"full\" shape=\"round\" (click)=\"onLogin()\" class=\"cus_btn\">Login</ion-button>\n    </div>\n  </div>\n</ion-content>\n<ion-toolbar class=\"ftr\" (click)=\"goRegister()\">Dont have an account? <a>Register Now</a></ion-toolbar>");

/***/ }),

/***/ 6051:
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/***/ ((module) => {

module.exports = ".login_screen {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  display: table;\n  padding: 35px;\n}\n.login_screen .login_panel {\n  display: table-cell;\n  text-align: center;\n  vertical-align: middle;\n}\n.login_screen .login_panel img {\n  width: 60%;\n  margin-bottom: 20px;\n}\n.login_screen .login_panel h3 {\n  padding: 20px 0;\n  margin: 0;\n  color: #222222;\n  font-size: 26px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n}\n.login_screen .login_panel span {\n  text-align: right;\n  color: #444444;\n  font-size: 12px;\n  display: block;\n  padding: 0 10px;\n  font-family: \"Open Sans\", sans-serif;\n  font-weight: 600;\n}\n.cust_input:hover {\n  border-color: #222428;\n  box-shadow: #eeeeee 0 0 6px 4px;\n}\n.ftr {\n  color: #444444;\n  font-size: 14px;\n  text-align: center;\n  font-family: \"Open Sans\", sans-serif;\n}\n.ftr a {\n  color: #444444;\n  text-decoration: underline;\n  font-weight: 600;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFBYSxZQUFBO0VBQWMsa0JBQUE7RUFBb0IsY0FBQTtFQUFnQixhQUFBO0FBS25FO0FBSkk7RUFBYyxtQkFBQTtFQUFxQixrQkFBQTtFQUFvQixzQkFBQTtBQVMzRDtBQVJRO0VBQUssVUFBQTtFQUFZLG1CQUFBO0FBWXpCO0FBWFE7RUFBSSxlQUFBO0VBQWlCLFNBQUE7RUFBVyxjQUFBO0VBQWdCLGVBQUE7RUFBaUIsZ0JBQUE7RUFBa0Isb0NBQUE7QUFtQjNGO0FBbEJRO0VBQUssaUJBQUE7RUFBbUIsY0FBQTtFQUFnQixlQUFBO0VBQWlCLGNBQUE7RUFBZ0IsZUFBQTtFQUFpQixvQ0FBQTtFQUFzQyxnQkFBQTtBQTJCeEk7QUF4QkE7RUFBbUIscUJBQUE7RUFBdUIsK0JBQUE7QUE2QjFDO0FBNUJBO0VBQUssY0FBQTtFQUFnQixlQUFBO0VBQWlCLGtCQUFBO0VBQW9CLG9DQUFBO0FBbUMxRDtBQWxDSTtFQUFFLGNBQUE7RUFBZ0IsMEJBQUE7RUFBNEIsZ0JBQUE7QUF1Q2xEIiwiZmlsZSI6ImxvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbl9zY3JlZW57XHJcbiAgICB3aWR0aDogMTAwJTsgaGVpZ2h0OiAxMDAlOyBwb3NpdGlvbjogYWJzb2x1dGU7IGRpc3BsYXk6IHRhYmxlOyBwYWRkaW5nOiAzNXB4O1xyXG4gICAgLmxvZ2luX3BhbmVseyBkaXNwbGF5OiB0YWJsZS1jZWxsOyB0ZXh0LWFsaWduOiBjZW50ZXI7IHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICAgICAgaW1neyB3aWR0aDogNjAlOyBtYXJnaW4tYm90dG9tOiAyMHB4OyB9XHJcbiAgICAgICAgaDN7IHBhZGRpbmc6IDIwcHggMDsgbWFyZ2luOiAwOyBjb2xvcjogIzIyMjIyMjsgZm9udC1zaXplOiAyNnB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgfVxyXG4gICAgICAgIHNwYW57dGV4dC1hbGlnbjogcmlnaHQ7IGNvbG9yOiAjNDQ0NDQ0OyBmb250LXNpemU6IDEycHg7IGRpc3BsYXk6IGJsb2NrOyBwYWRkaW5nOiAwIDEwcHg7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmOyBmb250LXdlaWdodDogNjAwO31cclxuICAgIH1cclxufVxyXG4uY3VzdF9pbnB1dDpob3ZlcnsgYm9yZGVyLWNvbG9yOiAjMjIyNDI4OyBib3gtc2hhZG93OiAjZWVlZWVlIDAgMCA2cHggNHB4O31cclxuLmZ0cntjb2xvcjogIzQ0NDQ0NDsgZm9udC1zaXplOiAxNHB4OyB0ZXh0LWFsaWduOiBjZW50ZXI7IGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgYXtjb2xvcjogIzQ0NDQ0NDsgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7IGZvbnQtd2VpZ2h0OiA2MDA7fVxyXG59XHJcbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_login_login_module_ts.js.map