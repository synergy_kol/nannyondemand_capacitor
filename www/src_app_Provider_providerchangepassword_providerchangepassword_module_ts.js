"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_Provider_providerchangepassword_providerchangepassword_module_ts"],{

/***/ 13830:
/*!******************************************************************************************!*\
  !*** ./src/app/Provider/providerchangepassword/providerchangepassword-routing.module.ts ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderchangepasswordPageRoutingModule": () => (/* binding */ ProviderchangepasswordPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _providerchangepassword_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerchangepassword.page */ 2119);




const routes = [
    {
        path: '',
        component: _providerchangepassword_page__WEBPACK_IMPORTED_MODULE_0__.ProviderchangepasswordPage
    }
];
let ProviderchangepasswordPageRoutingModule = class ProviderchangepasswordPageRoutingModule {
};
ProviderchangepasswordPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProviderchangepasswordPageRoutingModule);



/***/ }),

/***/ 51646:
/*!**********************************************************************************!*\
  !*** ./src/app/Provider/providerchangepassword/providerchangepassword.module.ts ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderchangepasswordPageModule": () => (/* binding */ ProviderchangepasswordPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _providerchangepassword_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./providerchangepassword-routing.module */ 13830);
/* harmony import */ var _providerchangepassword_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerchangepassword.page */ 2119);







let ProviderchangepasswordPageModule = class ProviderchangepasswordPageModule {
};
ProviderchangepasswordPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _providerchangepassword_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProviderchangepasswordPageRoutingModule
        ],
        declarations: [_providerchangepassword_page__WEBPACK_IMPORTED_MODULE_1__.ProviderchangepasswordPage]
    })
], ProviderchangepasswordPageModule);



/***/ }),

/***/ 2119:
/*!********************************************************************************!*\
  !*** ./src/app/Provider/providerchangepassword/providerchangepassword.page.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProviderchangepasswordPage": () => (/* binding */ ProviderchangepasswordPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerchangepassword_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./providerchangepassword.page.html */ 74081);
/* harmony import */ var _providerchangepassword_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./providerchangepassword.page.scss */ 1714);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);







let ProviderchangepasswordPage = class ProviderchangepasswordPage {
    constructor(storage, authService, loadingController, toastController, navCtrl) {
        this.storage = storage;
        this.authService = authService;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.isLoading = false;
        this.isDisabled = false;
        this.userdata = [];
    }
    onKeyboard(event) {
        if (event != null) {
            event.setFocus();
        }
        else {
            this.onUpdate();
        }
    }
    loginToken() {
        this.storage.get('userDetails').then((val) => {
            this.userdata = val;
            var tokenbody = {
                user_id: this.userdata.user_id,
                auth_token: this.userdata.auth_token
            };
            this.authService.postData("check-token", tokenbody).then(result => {
                this.data = result;
                if (this.data.status.error_code == 0) {
                    console.log("Token: ", this.data);
                }
                else {
                    this.showLoader('Logging out...');
                    setTimeout(() => {
                        this.storage.remove('userDetails');
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/welcome');
                    }, 3000);
                }
                this.hideLoader();
            }, error => {
                this.hideLoader();
            });
        });
    }
    ionViewWillEnter() {
        this.showLoader('Please wait...');
        this.loginToken();
    }
    ngOnInit() {
    }
    onUpdate() {
        const passPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}/;
        if (this.password == null) {
            this.presentToast('Please enter your Old password');
        }
        else if (this.new_password == null) {
            this.presentToast('Please enter New password');
        }
        else if (this.new_password.length < 8) {
            this.presentToast('Please enter Minimum 8 Character.');
        }
        else if (!passPattern.test(this.new_password)) {
            this.presentToast('Password must be contain atleast one lowercase and one uppercase letter one digit and one special character.');
        }
        else if (this.confirm_password == null) {
            this.presentToast('Please enter confirm New password');
        }
        else if (this.confirm_password != this.new_password) {
            this.presentToast('New Password and Confirm new password must match');
        }
        else {
            this.showLoader('Please wait...');
            this.storage.get('userDetails').then((val) => {
                this.userdata = val;
                console.log("profile: ", val);
                var body = {
                    u_id: this.userdata.user_id,
                    oldPassword: this.password,
                    newPassword: this.new_password
                };
                this.authService.postData("change-password", body).then(result => {
                    this.data = result;
                    console.log("changePassword: ", this.data);
                    if (this.data.status.error_code == 0) {
                        this.presentToast("Password has been Changed Successfully.");
                        this.navCtrl.pop();
                    }
                    else {
                        this.presentToast(this.data.status.message);
                    }
                    this.hideLoader();
                }, error => {
                    console.log(error);
                    this.hideLoader();
                });
            });
        }
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.isDisabled = true;
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000
            });
            toast.onDidDismiss().then(() => {
                this.isDisabled = false;
            });
            toast.present();
        });
    }
};
ProviderchangepasswordPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__.Storage },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.NavController }
];
ProviderchangepasswordPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-providerchangepassword',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_providerchangepassword_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_providerchangepassword_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProviderchangepasswordPage);



/***/ }),

/***/ 74081:
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/Provider/providerchangepassword/providerchangepassword.page.html ***!
  \*************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar class=\"provider-toolbar\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"welcome\" style=\"color: #ffffff;\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Change Password</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div style=\"padding:20px;\">\n    <ion-input class=\"cust_input\" placeholder=\"Old Password\" type=\"password\" [(ngModel)]=\"password\" (keyup.enter)=\"onKeyboard(Field1)\"></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"New Password\" type=\"password\" [(ngModel)]=\"new_password\" #Field1\n        (keyup.enter)=\"onKeyboard(Field2)\"></ion-input>\n    <ion-input class=\"cust_input\" placeholder=\"Confirm Password\" type=\"password\" [(ngModel)]=\"confirm_password\" #Field2\n        (keyup.enter)=\"onKeyboard()\"></ion-input>\n      <ion-button color=\"success\" expand=\"full\" shape=\"round\" (click)=\"onUpdate()()\" class=\"cus_btn\">Update</ion-button>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 1714:
/*!**********************************************************************************!*\
  !*** ./src/app/Provider/providerchangepassword/providerchangepassword.page.scss ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm92aWRlcmNoYW5nZXBhc3N3b3JkLnBhZ2Uuc2NzcyJ9 */";

/***/ })

}]);
//# sourceMappingURL=src_app_Provider_providerchangepassword_providerchangepassword_module_ts.js.map