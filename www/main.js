(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["main"],{

/***/ 40576:
/*!*****************************!*\
  !*** ./src/app/MyEvents.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MyEvents": () => (/* binding */ MyEvents)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 64008);



let MyEvents = class MyEvents {
    constructor() {
        this.fooSubject = new rxjs__WEBPACK_IMPORTED_MODULE_0__.Subject();
    }
    publishSomeData(data) {
        this.fooSubject.next(data);
    }
    getObservable() {
        return this.fooSubject;
    }
};
MyEvents = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], MyEvents);



/***/ }),

/***/ 83696:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);
/* harmony import */ var _awesome_cordova_plugins_network_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @awesome-cordova-plugins/network/ngx */ 67210);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @awesome-cordova-plugins/screen-orientation/ngx */ 54553);
/* harmony import */ var _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/camera/ngx */ 30692);
/* harmony import */ var _awesome_cordova_plugins_calendar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/calendar/ngx */ 21807);
/* harmony import */ var _awesome_cordova_plugins_stripe_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/stripe/ngx */ 117);
/* harmony import */ var _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @awesome-cordova-plugins/in-app-browser/ngx */ 80838);
/* harmony import */ var _awesome_cordova_plugins_device_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @awesome-cordova-plugins/device/ngx */ 71614);








// import { FilePath } from '@ionic-native/file-path/ngx';
// import { File } from '@ionic-native/file/ngx';

//import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';



const routes = [
    {
        path: '',
        redirectTo: 'welcome',
        pathMatch: 'full'
    },
    {
        path: 'welcome',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_welcome_welcome_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./welcome/welcome.module */ 35783)).then(m => m.WelcomePageModule)
    },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 69549)).then(m => m.LoginPageModule)
    },
    {
        path: 'forgotpass',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_forgotpass_forgotpass_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./forgotpass/forgotpass.module */ 36100)).then(m => m.ForgotpassPageModule)
    },
    {
        path: 'registration1',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_registration1_registration1_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/registration1/registration1.module */ 23065)).then(m => m.Registration1PageModule)
    },
    {
        path: 'registration2',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_registration2_registration2_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/registration2/registration2.module */ 89991)).then(m => m.Registration2PageModule)
    },
    {
        path: 'congratulation',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_congratulation_congratulation_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/congratulation/congratulation.module */ 612)).then(m => m.CongratulationPageModule)
    },
    {
        path: 'home',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_home_home_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/home/home.module */ 47730)).then(m => m.HomePageModule)
    },
    {
        path: 'nannylist',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_nannylist_nannylist_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/nannylist/nannylist.module */ 52594)).then(m => m.NannylistPageModule)
    },
    {
        path: 'nannydetails',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_nannydetails_nannydetails_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/nannydetails/nannydetails.module */ 43909)).then(m => m.NannydetailsPageModule)
    },
    {
        path: 'payment',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_payment_payment_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/payment/payment.module */ 99711)).then(m => m.PaymentPageModule)
    },
    {
        path: 'successpayment',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_successpayment_successpayment_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/successpayment/successpayment.module */ 97030)).then(m => m.SuccesspaymentPageModule)
    },
    {
        path: 'myprofile',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_myprofile_myprofile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/myprofile/myprofile.module */ 24078)).then(m => m.MyprofilePageModule)
    },
    {
        path: 'mybookings',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_mybookings_mybookings_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/mybookings/mybookings.module */ 80860)).then(m => m.MybookingsPageModule)
    },
    {
        path: 'bookingdetails',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_bookingdetails_bookingdetails_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/bookingdetails/bookingdetails.module */ 91762)).then(m => m.BookingdetailsPageModule)
    },
    {
        path: 'help',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_help_help_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/help/help.module */ 59158)).then(m => m.HelpPageModule)
    },
    {
        path: 'termsconditions',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_termsconditions_termsconditions_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/termsconditions/termsconditions.module */ 55771)).then(m => m.TermsconditionsPageModule)
    },
    {
        path: 'changepassword',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_changepassword_changepassword_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/changepassword/changepassword.module */ 62465)).then(m => m.ChangepasswordPageModule)
    },
    {
        path: 'wrte-review',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_wrte-review_wrte-review_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/wrte-review/wrte-review.module */ 63024)).then(m => m.WrteReviewPageModule)
    },
    {
        path: 'calender-view',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_calender-view_calender-view_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/calender-view/calender-view.module */ 73212)).then(m => m.CalenderViewPageModule)
    },
    {
        path: 'providerregistration1',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerregistration1_providerregistration1_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerregistration1/providerregistration1.module */ 17391)).then(m => m.Providerregistration1PageModule)
    },
    {
        path: 'providerregistration2',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerregistration2_providerregistration2_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerregistration2/providerregistration2.module */ 20264)).then(m => m.Providerregistration2PageModule)
    },
    {
        path: 'providerregistration3',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerregistration3_providerregistration3_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerregistration3/providerregistration3.module */ 37702)).then(m => m.Providerregistration3PageModule)
    },
    {
        path: 'providercongratulation',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providercongratulation_providercongratulation_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providercongratulation/providercongratulation.module */ 6186)).then(m => m.ProvidercongratulationPageModule)
    },
    {
        path: 'dashboard',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_dashboard_dashboard_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/dashboard/dashboard.module */ 19344)).then(m => m.DashboardPageModule)
    },
    {
        path: 'providerprofile',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerprofile_providerprofile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerprofile/providerprofile.module */ 29343)).then(m => m.ProviderprofilePageModule)
    },
    {
        path: 'providerbookings',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerbookings_providerbookings_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerbookings/providerbookings.module */ 55575)).then(m => m.ProviderbookingsPageModule)
    },
    {
        path: 'providerbookingdetails',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerbookingdetails_providerbookingdetails_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerbookingdetails/providerbookingdetails.module */ 43845)).then(m => m.ProviderbookingdetailsPageModule)
    },
    {
        path: 'earnings',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_earnings_earnings_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/earnings/earnings.module */ 4395)).then(m => m.EarningsPageModule)
    },
    {
        path: 'providernotification',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providernotification_providernotification_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providernotification/providernotification.module */ 1775)).then(m => m.ProvidernotificationPageModule)
    },
    {
        path: 'providerhelp',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerhelp_providerhelp_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerhelp/providerhelp.module */ 62693)).then(m => m.ProviderhelpPageModule)
    },
    {
        path: 'providertermasconditions',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providertermasconditions_providertermasconditions_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providertermasconditions/providertermasconditions.module */ 50633)).then(m => m.ProvidertermasconditionsPageModule)
    },
    {
        path: 'providerchangepassword',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_providerchangepassword_providerchangepassword_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/providerchangepassword/providerchangepassword.module */ 51646)).then(m => m.ProviderchangepasswordPageModule)
    },
    {
        path: 'payment-connect',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_Provider_payment-connect_payment-connect_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./Provider/payment-connect/payment-connect.module */ 57251)).then(m => m.PaymentConnectPageModule)
    },
    {
        path: 'filter',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_filter_filter_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/filter/filter.module */ 6929)).then(m => m.FilterPageModule)
    },
    {
        path: 'schedule',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_User_schedule_schedule_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./User/schedule/schedule.module */ 37999)).then(m => m.SchedulePageModule)
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.NgModule)({
        imports: [
            _angular_common_http__WEBPACK_IMPORTED_MODULE_10__.HttpClientModule,
            //IonicStorageModule.forRoot(),
            _angular_router__WEBPACK_IMPORTED_MODULE_11__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_11__.PreloadAllModules })
        ],
        providers: [
            //FileTransfer,
            src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_0__.AuthServiceProvider,
            _awesome_cordova_plugins_network_ngx__WEBPACK_IMPORTED_MODULE_1__.Network,
            _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_2__.ScreenOrientation,
            _awesome_cordova_plugins_camera_ngx__WEBPACK_IMPORTED_MODULE_3__.Camera,
            //FilePath,
            //File,
            _awesome_cordova_plugins_calendar_ngx__WEBPACK_IMPORTED_MODULE_4__.Calendar,
            //PayPal,
            _awesome_cordova_plugins_stripe_ngx__WEBPACK_IMPORTED_MODULE_5__.Stripe,
            _awesome_cordova_plugins_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__.InAppBrowser,
            _awesome_cordova_plugins_device_ngx__WEBPACK_IMPORTED_MODULE_7__.Device
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_11__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 2050:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./app.component.html */ 75158);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 30836);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth-providers/auth-service/auth-service */ 81901);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @awesome-cordova-plugins/screen-orientation/ngx */ 54553);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_auth_providers_network_service_network_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/auth-providers/network-service/network-service */ 1168);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 9820);
/* harmony import */ var _MyEvents__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./MyEvents */ 40576);





//import { SplashScreen } from '@ionic-native/splash-screen/ngx';
//import { StatusBar } from '@ionic-native/status-bar/ngx';







let AppComponent = class AppComponent {
    constructor(platform, 
    //private splashScreen: SplashScreen,
    //private statusBar: StatusBar,
    menuCtrl, navCtrl, loadingController, alertController, authService, storage, screenOrientation, router, networkservice, toastController, events) {
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this.navCtrl = navCtrl;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.authService = authService;
        this.storage = storage;
        this.screenOrientation = screenOrientation;
        this.router = router;
        this.networkservice = networkservice;
        this.toastController = toastController;
        this.events = events;
        this.isLoading = false;
        this.counter = 0;
        this.data = [];
        this.fname = '';
        this.lname = '';
        this.email = '';
        this.phone = '';
        this.profileimage = '';
        this.events.getObservable().subscribe((val) => {
            console.log('Profile Data:', val);
            this.fname = val.profile.fname;
            this.lname = val.profile.lname;
            this.email = val.profile.email;
            this.phone = val.profile.phone;
            this.profileimage = val.profile.profile_image;
        });
        this.initializeApp();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
            this.menuCtrl.swipeGesture(false);
            //this.statusBar.styleDefault();
            //this.statusBar.backgroundColorByHexString('#ffffff');
            //this.splashScreen.hide();
            this.storage.create();
            this.networkSubscriber();
            this.storage.get('userDetails').then((val) => {
                console.log('User Details', val);
                if (val != null) {
                    this.data = val;
                    if (val.role_id == 1) {
                        this.navCtrl.navigateRoot('/home');
                    }
                    else {
                        this.navCtrl.navigateRoot('/dashboard');
                    }
                }
                else {
                    this.navCtrl.navigateRoot('/welcome');
                }
            });
            this.platform.backButton.subscribe(() => {
                console.log("URL: ", this.router.url);
                if (this.router.url === '/welcome' || this.router.url === '/home') {
                    if (this.counter == 0) {
                        this.counter++;
                        this.presentToast('Press again to exit');
                        setTimeout(() => { this.counter = 0; }, 3000);
                    }
                    else {
                        navigator['app'].exitApp();
                    }
                }
            });
        });
    }
    networkSubscriber() {
        this.networkservice.getNetworkStatus().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.debounceTime)(300))
            .subscribe((connected) => {
            this.isConnected = connected;
            if (this.isConnected == true) {
                console.log('Connected', this.isConnected);
            }
            else {
                this.presentToast("SORRY!, You don't have any Network Connection.");
            }
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 3000
            });
            toast.present();
        });
    }
    onLogout() {
        const alert = this.alertController.create({
            header: 'Confirm Logout!',
            message: 'Are you sure, you want to Logout?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        console.log('Confirm Cancel: blah');
                    }
                }, {
                    text: 'Yes',
                    handler: () => {
                        this.showLoader('Logging out...');
                        setTimeout(() => {
                            this.storage.remove('userDetails');
                            this.hideLoader();
                            this.navCtrl.navigateRoot('/welcome');
                        }, 1000);
                    }
                }
            ]
        }).then(a => {
            a.present();
        });
    }
    showLoader(text) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loadingController.create({
                message: text
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loadingController.dismiss().then(() => console.log('dismissed'));
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.MenuController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.NavController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.AlertController },
    { type: src_auth_providers_auth_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthServiceProvider },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__.Storage },
    { type: _awesome_cordova_plugins_screen_orientation_ngx__WEBPACK_IMPORTED_MODULE_4__.ScreenOrientation },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router },
    { type: src_auth_providers_network_service_network_service__WEBPACK_IMPORTED_MODULE_5__.NetworkServiceProvider },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.ToastController },
    { type: _MyEvents__WEBPACK_IMPORTED_MODULE_6__.MyEvents }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-root',
        template: _Users_pritam_Documents_Souvik_Ghosh_NannyCapacitor_node_modules_ngtools_webpack_src_loaders_direct_resource_js_app_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], AppComponent);



/***/ }),

/***/ 34750:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/platform-browser */ 86219);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component */ 2050);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 83696);
/* harmony import */ var src_auth_providers_network_service_network_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/auth-providers/network-service/network-service */ 1168);
/* harmony import */ var _awesome_cordova_plugins_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/onesignal/ngx */ 7490);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ 17897);
/* harmony import */ var _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/file/ngx */ 57154);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 59832);








//import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';




let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_9__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonicModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule
        ],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_11__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonicRouteStrategy },
            src_auth_providers_network_service_network_service__WEBPACK_IMPORTED_MODULE_2__.NetworkServiceProvider,
            _awesome_cordova_plugins_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__.OneSignal,
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__.Storage,
            _awesome_cordova_plugins_file_ngx__WEBPACK_IMPORTED_MODULE_5__.File,
            _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__.FileTransfer,
            _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_6__.FileTransferObject
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 81901:
/*!*********************************************************!*\
  !*** ./src/auth-providers/auth-service/auth-service.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthServiceProvider": () => (/* binding */ AuthServiceProvider)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/file-transfer/ngx */ 59832);





let apiUrl = 'https://nannyondemandfm.com/dev/api/';
let AuthServiceProvider = class AuthServiceProvider {
    constructor(http, transfer, loadingCtrl, toastCtrl) {
        this.http = http;
        this.transfer = transfer;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.isDisabled = false;
    }
    postData(apiName, data) {
        return new Promise((resolve, reject) => {
            const httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpHeaders({
                    'Content-Type': 'application/x-www-form-urlencoded'
                })
            };
            this.http.post(apiUrl + apiName, data, httpOptions)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });
    }
    getData(apiName) {
        return new Promise((resolve, reject) => {
            this.http.get(apiUrl + apiName)
                .subscribe(res => {
                resolve(res);
            }, (err) => {
                reject(err);
            });
        });
    }
    fileUpload(targetPath, apiFunc, options) {
        let favoritesURL = apiUrl + apiFunc;
        const fileTransfer = this.transfer.create();
        return fileTransfer.upload(targetPath, favoritesURL, options)
            .then(res => res.response)
            .catch();
    }
    showLoader(text) {
        this.loading = this.loadingCtrl.create({
            message: text
        });
        //this.loading.present();
    }
    hideLoader() {
        if (this.loading != null) {
            //this.loading.dismiss().catch(() => { });
        }
    }
};
AuthServiceProvider.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient },
    { type: _awesome_cordova_plugins_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_0__.FileTransfer },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ToastController }
];
AuthServiceProvider = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)()
], AuthServiceProvider);



/***/ }),

/***/ 1168:
/*!***************************************************************!*\
  !*** ./src/auth-providers/network-service/network-service.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NetworkServiceProvider": () => (/* binding */ NetworkServiceProvider)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _awesome_cordova_plugins_network_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/network/ngx */ 67210);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 51590);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 44850);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 18252);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 878);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 83301);






let NetworkServiceProvider = class NetworkServiceProvider {
    constructor(network, platform) {
        this.network = network;
        this.platform = platform;
        this.online$ = rxjs__WEBPACK_IMPORTED_MODULE_1__.Observable.create(observer => {
            observer.next(true);
        }).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.mapTo)(true));
        if (this.platform.is('cordova')) {
            // on Device
            this.online$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.merge)(this.network.onConnect().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.mapTo)(true)), this.network.onDisconnect().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.mapTo)(false)));
        }
        else {
            // on Browser
            this.online$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.merge)((0,rxjs__WEBPACK_IMPORTED_MODULE_4__.of)(navigator.onLine), (0,rxjs__WEBPACK_IMPORTED_MODULE_5__.fromEvent)(window, 'online').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.mapTo)(true)), (0,rxjs__WEBPACK_IMPORTED_MODULE_5__.fromEvent)(window, 'offline').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.mapTo)(false)));
        }
    }
    getNetworkType() {
        return this.network.type;
    }
    getNetworkStatus() {
        return this.online$;
    }
};
NetworkServiceProvider.ctorParameters = () => [
    { type: _awesome_cordova_plugins_network_ngx__WEBPACK_IMPORTED_MODULE_0__.Network },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform }
];
NetworkServiceProvider = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Injectable)()
], NetworkServiceProvider);



/***/ }),

/***/ 18260:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 90271:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 42577);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 34750);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 18260);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-accordion_2.entry.js": [
		83477,
		"common",
		"node_modules_ionic_core_dist_esm_ion-accordion_2_entry_js"
	],
	"./ion-action-sheet.entry.js": [
		67196,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		38081,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		75017,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		69721,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		99216,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		96612,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-breadcrumb_2.entry.js": [
		42694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-breadcrumb_2_entry_js"
	],
	"./ion-button_2.entry.js": [
		22938,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		51379,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		97552,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		37218,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		97479,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		64134,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		71439,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		76397,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		33296,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		12413,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		39411,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		99133,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		79003,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		96065,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		86991,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		82947,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-picker-column-internal.entry.js": [
		25919,
		"common",
		"node_modules_ionic_core_dist_esm_ion-picker-column-internal_entry_js"
	],
	"./ion-picker-internal.entry.js": [
		93109,
		"node_modules_ionic_core_dist_esm_ion-picker-internal_entry_js"
	],
	"./ion-popover.entry.js": [
		99459,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		20301,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		43799,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		12140,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		86197,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		41975,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		58387,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		98659,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		26404,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		85253,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		92619,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		82871,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		17668,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		55342,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		86185,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		97337,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		4833,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		9468,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		25705,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		87463,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 75158:
/*!***************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/app.component.html ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-app>\n  <ion-menu contentId=\"main-content\" type=\"overlay\">\n    <ion-content>\n      <div class=\"dashboard_top\">\n        <div class=\"dashboard_img\">\n          <img [src]=\"profileimage != '' ? profileimage : 'assets/images/avatar.png'\" alt=\"profile_img\" />\n        </div>\n        <h3>{{fname}} {{lname}}</h3>\n        <span>{{email}}</span>\n        <span>{{phone}}</span>\n      </div>\n      <ion-menu-toggle auto-hide=\"false\">\n        <ion-list class=\"dashboardlisting\">\n          <ion-item routerLink=\"/myprofile\">\n            <ion-icon name=\"person-circle-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>My Profile</ion-label>\n          </ion-item>\n          <ion-item routerLink=\"/schedule\">\n            <ion-icon name=\"calendar-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>Schedule List</ion-label>\n          </ion-item>\n          <ion-item routerLink=\"/changepassword\">\n            <ion-icon name=\"key-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>Change Password</ion-label>\n          </ion-item>\n          <ion-item routerLink=\"/mybookings\">\n            <ion-icon name=\"wallet-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>My Bookings</ion-label>\n          </ion-item>\n          <ion-item routerLink=\"/nannylist\">\n            <ion-icon name=\"receipt-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>Nanny List</ion-label>\n          </ion-item>\n          <ion-item routerLink=\"/help\">\n            <ion-icon name=\"information-circle-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>Help</ion-label>\n          </ion-item>\n          <ion-item routerLink=\"/termsconditions\">\n            <ion-icon name=\"newspaper-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>Terms & Conditions</ion-label>\n          </ion-item>\n          <ion-item lines=\"none\" (click)=\"onLogout()\">\n            <ion-icon name=\"log-out-outline\" style=\"padding-left: 18px; color: #3c3c3c;\"></ion-icon>\n            <ion-label>Logout</ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-menu-toggle>\n    </ion-content>\n  </ion-menu>\n  <ion-router-outlet id=\"main-content\"></ion-router-outlet>\n</ion-app>");

/***/ }),

/***/ 30836:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = ".dashboard_top {\n  background: #f38320;\n  padding: 10px 0 15px 0;\n}\n.dashboard_top h3 {\n  padding: 0;\n  margin: 0 0 2px 0;\n  color: #ffffff;\n  font-size: 18px;\n  font-weight: 700;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n}\n.dashboard_top span {\n  padding: 2px 0;\n  margin: 0;\n  color: #ffffff;\n  font-size: 14px;\n  font-weight: 400;\n  font-family: \"Open Sans\", sans-serif;\n  text-align: center;\n  display: block;\n}\n.dashboard_img {\n  width: 120px;\n  height: 120px;\n  border-radius: 50%;\n  margin: 12px auto;\n  border: #fff solid 5px;\n}\n.dashboard_img img {\n  width: 100%;\n  height: 100%;\n  border-radius: 50%;\n  object-fit: cover;\n}\n.dashboardlisting {\n  margin-left: -20px;\n}\n.dashboardlisting ion-item ion-label {\n  padding: 5px 20px !important;\n  color: #444444 !important;\n  font-size: 14px !important;\n  font-family: \"Open Sans\", sans-serif !important;\n  font-weight: 600;\n}\n.dashboardlisting ion-item img {\n  padding-left: 20px;\n  width: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFnQixtQkFBQTtFQUFxQixzQkFBQTtBQUdyQztBQUZFO0VBQUksVUFBQTtFQUFZLGlCQUFBO0VBQW1CLGNBQUE7RUFBZ0IsZUFBQTtFQUFnQixnQkFBQTtFQUFrQixvQ0FBQTtFQUFzQyxrQkFBQTtBQVc3SDtBQVZFO0VBQU0sY0FBQTtFQUFnQixTQUFBO0VBQVcsY0FBQTtFQUFnQixlQUFBO0VBQWdCLGdCQUFBO0VBQWtCLG9DQUFBO0VBQXNDLGtCQUFBO0VBQW9CLGNBQUE7QUFvQi9JO0FBbEJBO0VBQWdCLFlBQUE7RUFBYyxhQUFBO0VBQWUsa0JBQUE7RUFBbUIsaUJBQUE7RUFBbUIsc0JBQUE7QUEwQm5GO0FBekJFO0VBQUssV0FBQTtFQUFhLFlBQUE7RUFBYyxrQkFBQTtFQUFtQixpQkFBQTtBQStCckQ7QUE3QkE7RUFBb0Isa0JBQUE7QUFpQ3BCO0FBaENBO0VBQXNDLDRCQUFBO0VBQThCLHlCQUFBO0VBQTJCLDBCQUFBO0VBQTJCLCtDQUFBO0VBQWdELGdCQUFBO0FBd0MxSztBQXZDQTtFQUFnQyxrQkFBQTtFQUFvQixXQUFBO0FBNENwRCIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGFzaGJvYXJkX3RvcHsgYmFja2dyb3VuZDogI2YzODMyMDsgcGFkZGluZzogMTBweCAwIDE1cHggMDsgXG4gIGgzeyBwYWRkaW5nOiAwOyBtYXJnaW46IDAgMCAycHggMDsgY29sb3I6ICNmZmZmZmY7IGZvbnQtc2l6ZToxOHB4OyBmb250LXdlaWdodDogNzAwOyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZjsgdGV4dC1hbGlnbjogY2VudGVyOyB9XG4gIHNwYW57IHBhZGRpbmc6IDJweCAwOyBtYXJnaW46IDA7IGNvbG9yOiAjZmZmZmZmOyBmb250LXNpemU6MTRweDsgZm9udC13ZWlnaHQ6IDQwMDsgZm9udC1mYW1pbHk6IFwiT3BlbiBTYW5zXCIsIHNhbnMtc2VyaWY7IHRleHQtYWxpZ246IGNlbnRlcjsgZGlzcGxheTogYmxvY2s7IH1cbn1cbi5kYXNoYm9hcmRfaW1neyB3aWR0aDogMTIwcHg7IGhlaWdodDogMTIwcHg7IGJvcmRlci1yYWRpdXM6NTAlOyBtYXJnaW46IDEycHggYXV0bzsgYm9yZGVyOiAjZmZmIHNvbGlkIDVweDsgXG4gIGltZ3sgd2lkdGg6IDEwMCU7IGhlaWdodDogMTAwJTsgYm9yZGVyLXJhZGl1czo1MCU7IG9iamVjdC1maXQ6IGNvdmVyOyB9XG59XG4uZGFzaGJvYXJkbGlzdGluZ3sgIG1hcmdpbi1sZWZ0OiAtMjBweDsgfVxuLmRhc2hib2FyZGxpc3RpbmcgaW9uLWl0ZW0gaW9uLWxhYmVseyBwYWRkaW5nOiA1cHggMjBweCAhaW1wb3J0YW50OyBjb2xvcjogIzQ0NDQ0NCAhaW1wb3J0YW50OyBmb250LXNpemU6MTRweCAhaW1wb3J0YW50OyBmb250LWZhbWlseTogXCJPcGVuIFNhbnNcIiwgc2Fucy1zZXJpZiFpbXBvcnRhbnQ7IGZvbnQtd2VpZ2h0OiA2MDA7IH1cbi5kYXNoYm9hcmRsaXN0aW5nIGlvbi1pdGVtIGltZ3sgcGFkZGluZy1sZWZ0OiAyMHB4OyB3aWR0aDogNDBweDsgfSJdfQ== */";

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(90271)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map