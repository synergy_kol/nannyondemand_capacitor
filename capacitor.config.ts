import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.met.nannyOnDemand',
  appName: 'Nanny On Demand',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
