import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';

import { OneSignal } from '@awesome-cordova-plugins/onesignal/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: any = '';
  password: any = '';
  isDisabled: boolean = false;
  isLoading = false;
  data: any;
  userData: any;
  playerID: any;

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private authService: AuthServiceProvider,
    private storage: Storage,
    private oneSignal: OneSignal
  ) { }

  ngOnInit() {
    this.storage.create();
  }

  onKeyboard(event) {
    if (event != null) {
      event.setFocus();
    } else {
      this.onLogin();
    }
  }

  ionViewWillEnter() {
    this.oneSignal.startInit('a1b8a842-b105-4ccd-8365-ca353da71c75', '217316564970');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
    this.oneSignal.handleNotificationReceived().subscribe(() => {
      //console.log('Recived:', res);
    });
    this.oneSignal.handleNotificationOpened().subscribe(() => {
      //console.log('Opened:', res);
    });
    this.oneSignal.endInit();

    this.oneSignal.getIds().then(identity => {
      console.log(identity.userId + " It's Devices ID");
      this.playerID = identity.userId;
    });
  }

  onLogin() {
    const emailPattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.email.trim() == '') {
      this.presentToast('Please enter your registered Email ID');
    } else if (!emailPattern.test(this.email)) {
      this.presentToast('Wrong Email Format...');
    } else if (this.password.trim() == '') {
      this.presentToast('Please enter Password');
    } else {
      this.showLoader('Please wait...');
      var body = {
        email: this.email,
        password: this.password,
        device_token: this.playerID
      };
      this.authService.postData("login", body).then(result => {
        this.data = result;
        this.userData = this.data.result.data;
        console.log("Login: ", this.userData);
        this.hideLoader();
        if (this.data.status.error_code == 0) {
          this.storage.set("userDetails", this.userData);
          if (this.userData.role_id == 1) {
            this.navCtrl.navigateRoot('/home');
          } else {
            this.navCtrl.navigateRoot('/dashboard');
          }
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }
  goRegister() {
    this.navCtrl.navigateRoot('/welcome');
  }
  goForgot() {
    this.navCtrl.navigateForward('/forgotpass');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
