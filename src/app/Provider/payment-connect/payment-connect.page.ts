import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { InAppBrowser, InAppBrowserOptions } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-payment-connect',
  templateUrl: './payment-connect.page.html',
  styleUrls: ['./payment-connect.page.scss'],
})
export class PaymentConnectPage implements OnInit {

  userdata: any = [];

  paymentType: any = '';
  isLoading = false;
  data: any;
  isDisabled: boolean = false;
  paymentMethod: any = '';
  isShow: boolean = false;

  constructor(
    public storage: Storage,
    public navCtrl: NavController,
    private iab: InAppBrowser,
    public authService: AuthServiceProvider,
    public toastController: ToastController,
    public loadingController: LoadingController,
    private alertController: AlertController
  ) { }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-profile", body).then(result => {
        this.data = result;
        console.log("profile: ", this.data);
        this.paymentType = this.data.result.data.payment_method;
        this.paymentMethod = this.data.result.data.payment_method;
        this.hideLoader();
        this.onPayType();
      },
        error => {
          this.hideLoader();
        });
    });
    this.loginToken();
  }

  onPayType() {
    if (this.paymentType == this.paymentMethod) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  ngOnInit() {
  }

  onConnect() {
    if (this.paymentType == '') {
      this.presentToast('Please Choose Payment Type.');
    } else {
      if (this.paymentType == 'Stripe') {
        this.showLoader('Please wait...');
        this.storage.get('userDetails').then((val) => {
          const options: InAppBrowserOptions = {
            zoom: 'no',
            location: 'no',
            /* toolbar: 'no', */
            closebuttoncaption: 'Close',
          };
          const browser = this.iab.create('https://nannyondemandfm.com/dev/api/connectStripe/' + val.user_id, 'system', options);
          browser.on('loadstart').subscribe(event => {
            var newurl = String(event.url).split('&');
            console.log("new url: ", newurl[0]);
            if (newurl[0] == "https://nannyondemandfm.com/dev/api/stripResponse?scope=read_write") {
              setTimeout(function () {
                browser.close();
              }, 500);
              this.hideLoader();
              this.payTypeSave();
              const alert = this.alertController.create({
                header: 'Successfully Conected!',
                message: 'Stripe account has been conncted succesfully.',
                buttons: [
                  {
                    text: 'Continue',
                    handler: () => {
                      this.showLoader('Please Wait...');
                      setTimeout(() => {
                        this.hideLoader();
                        this.navCtrl.navigateRoot('/dashboard');
                      }, 3000);
                    }
                  }
                ]
              }).then(a => {
                a.present();
              });
            } else if (newurl[0] == "https://nannyondemandfm.com/dev/api/stripResponse?error=access_denied") {
              setTimeout(function () {
                browser.close();
              }, 500);
              this.hideLoader();
              this.navCtrl.navigateRoot('/dashboard');
            }
          });
        });
      }
      // else {
      //   this.showLoader('Please wait...');
      //   this.storage.get('userDetails').then((val) => {
      //     const options: InAppBrowserOptions = {
      //       zoom: 'no',
      //       location: 'no',
      //       /* toolbar: 'no', */
      //       closebuttoncaption: 'Close',
      //     };
      //     //https://devfitser.com/NannyonDemand/dev/api/stripResponse?scope=read_write&code=ac_IZmhoEcWmyh6znfZzJuV1XKAOXJsa8Px
      //     const browser = this.iab.create('https://devfitser.com/NannyonDemand/dev/api/connectPaypal/' + val.user_id, 'system', options);
      //     browser.on('loadstart').subscribe(event => {
      //       console.log("url: ", event.url);
      //       var newurl = String(event.url).split('=');
      //       console.log("new url: ", newurl[0]);
      //       if (newurl[0] == "https://devfitser.com/NannyonDemand/dev/api/responsePaypal?code") {
      //         setTimeout(function () {
      //           browser.close();
      //         }, 500);
      //         this.hideLoader();
      //         this.payTypeSave();
      //         const alert = this.alertController.create({
      //           header: 'Successfully Conected!',
      //           message: 'Paypal account has been conncted succesfully.',
      //           buttons: [
      //             {
      //               text: 'Continue',
      //               handler: () => {
      //                 this.showLoader('Please Wait...');
      //                 setTimeout(() => {
      //                   this.hideLoader();
      //                   this.navCtrl.navigateRoot('/dashboard');
      //                 }, 3000);
      //               }
      //             }
      //           ]
      //         }).then(a => {
      //           a.present();
      //         });
      //       } else if (newurl[0] == "https://devfitser.com/NannyonDemand/dev/api/responsePaypal?error_uri") {
      //         setTimeout(function () {
      //           browser.close();
      //         }, 500);
      //         this.hideLoader();
      //         this.navCtrl.navigateRoot('/dashboard');
      //       }
      //     });
      //   });
      // }
    }
  }

  payTypeSave() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        payment_method: this.paymentType
      };
      this.authService.postData("update-payment-method", body).then(result => {
        this.data = result;
        console.log("Payment Type: ", this.data);
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  close() {
    this.navCtrl.navigateRoot('/dashboard');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
