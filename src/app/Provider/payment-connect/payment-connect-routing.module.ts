import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentConnectPage } from './payment-connect.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentConnectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentConnectPageRoutingModule {}
