import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProviderhelpPage } from './providerhelp.page';

const routes: Routes = [
  {
    path: '',
    component: ProviderhelpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProviderhelpPageRoutingModule {}
