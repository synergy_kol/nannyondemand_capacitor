import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProviderprofilePage } from './providerprofile.page';

const routes: Routes = [
  {
    path: '',
    component: ProviderprofilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProviderprofilePageRoutingModule {}
