import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-providercongratulation',
  templateUrl: './providercongratulation.page.html',
  styleUrls: ['./providercongratulation.page.scss'],
})
export class ProvidercongratulationPage implements OnInit {

  constructor(
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
  }

  onContinue() {
    this.navCtrl.navigateForward('/login');
  }

}
