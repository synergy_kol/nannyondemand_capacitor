import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProvidertermasconditionsPageRoutingModule } from './providertermasconditions-routing.module';

import { ProvidertermasconditionsPage } from './providertermasconditions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProvidertermasconditionsPageRoutingModule
  ],
  declarations: [ProvidertermasconditionsPage]
})
export class ProvidertermasconditionsPageModule {}
