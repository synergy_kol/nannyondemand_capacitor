import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProviderbookingdetailsPage } from './providerbookingdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ProviderbookingdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProviderbookingdetailsPageRoutingModule {}
