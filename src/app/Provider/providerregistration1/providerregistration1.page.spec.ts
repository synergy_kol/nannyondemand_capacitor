import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Providerregistration1Page } from './providerregistration1.page';

describe('Providerregistration1Page', () => {
  let component: Providerregistration1Page;
  let fixture: ComponentFixture<Providerregistration1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Providerregistration1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Providerregistration1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
