import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-providerbookings',
  templateUrl: './providerbookings.page.html',
  styleUrls: ['./providerbookings.page.scss'],
})
export class ProviderbookingsPage implements OnInit {

  isLoading = false;
  userdata: any = [];
  data: any = [];
  bookingData: any = [];
  nofound = false;

  constructor(
    private navCtrl: NavController,
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
  ) {}

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.loginToken();
  }

  getDetails(){
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-booking-details", body).then(result => {
        this.data = result;
        console.log("Booking: ", this.data);
        this.bookingData = this.data.result.data;
        if(this.bookingData.length == 0){
          this.nofound = true;
        } else {
          this.nofound = false;
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  goDetails(id) {
    var object = {
      bookID: id
    }
    this.navCtrl.navigateForward(["/providerbookingdetails"], { queryParams: object });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
