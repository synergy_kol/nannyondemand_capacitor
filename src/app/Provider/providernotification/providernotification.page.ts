import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-providernotification',
  templateUrl: './providernotification.page.html',
  styleUrls: ['./providernotification.page.scss'],
})
export class ProvidernotificationPage implements OnInit {

  isLoading = false;
  userdata: any = [];
  data: any = [];
  notiData: any = [];
  allnotiData: any = [];
  nofound = false;
  requestdata: any = [];
  
  constructor(
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
    private navCtrl: NavController,
    private alertController: AlertController,
    private toastController: ToastController
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.loginToken();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-my-notifications", body).then(result => {
        this.data = result;
        console.log("Notification: ", this.data);
        this.notiData = this.data.result.data;
        this.allnotiData = this.notiData.all;
        if (this.allnotiData == '') {
          this.nofound = true;
        } else {
          this.nofound = false;          
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onAccept(scheduleId, notiId) {
    this.showLoader('Wait...');
    this.storage.get('userDetails').then((result) => {
      this.data = result;
      var acptbody = {
        user_id: this.data.user_id,
        accept_status: 1,
        nanny_request_schedule_id: scheduleId,
        notification_id: notiId
      };
      this.authService.postData("post-schedule-status", acptbody).then(val => {
        this.requestdata = val;
        console.log("Request Schedule:", this.requestdata);
        if (this.requestdata.status.error_code == 0) {
          this.getDetails();
          this.presentToast("Successfully Accepted Request.");
        } else {
          this.presentToast(this.requestdata.status.message);
          this.hideLoader();
        }        
      },
        error => {
          this.hideLoader();
        });
    })
  }

  onDeclined(scheduleId, notiId) {
    const alert = this.alertController.create({
      header: 'Warning!',
      message: 'Are you sure, you want to Declined this Request?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.showLoader('Wait...');
            this.storage.get('userDetails').then((result) => {
              this.data = result;
              var dclnbody = {
                user_id: this.data.user_id,
                accept_status: 2,
                nanny_request_schedule_id: scheduleId,
                notification_id: notiId
              };
              this.authService.postData("post-schedule-status", dclnbody).then(val => {
                this.requestdata = val;
                console.log("Request Schedule:", this.requestdata);
                if (this.requestdata.status.error_code == 0) {
                  this.getDetails();
                  this.presentToast("Successfully Declined this Request.");                
                } else {
                  this.presentToast(this.requestdata.status.message);
                  this.hideLoader();
                }                
              },
                error => {
                  this.hideLoader();
                });
            })
          }
        }
      ]
    }).then(a => {
      a.present();
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}
