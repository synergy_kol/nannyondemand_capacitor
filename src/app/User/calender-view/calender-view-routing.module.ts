import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalenderViewPage } from './calender-view.page';

const routes: Routes = [
  {
    path: '',
    component: CalenderViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalenderViewPageRoutingModule {}
