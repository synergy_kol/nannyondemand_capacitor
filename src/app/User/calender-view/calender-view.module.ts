import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CalenderViewPageRoutingModule } from './calender-view-routing.module';
import { CalenderViewPage } from './calender-view.page';
import { NgCalendarModule  } from 'ionic2-calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCalendarModule,
    CalenderViewPageRoutingModule
  ],
  declarations: [CalenderViewPage]
})
export class CalenderViewPageModule {}
