import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccesspaymentPage } from './successpayment.page';

const routes: Routes = [
  {
    path: '',
    component: SuccesspaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccesspaymentPageRoutingModule {}
