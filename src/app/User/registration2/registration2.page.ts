import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-registration2',
  templateUrl: './registration2.page.html',
  styleUrls: ['./registration2.page.scss'],
})
export class Registration2Page implements OnInit {

  customAlertOptions: any = {
    header: 'Choose Cares'
  };

  pop = false;
  childageyear = 0;
  childagemonth = 0;
  childData: any = [];
  about: any = '';
  ispetinhouse: any = "No";
  checkpet = false;
  checkgovsub = false;
  isgovsubsidy: any = "No";
  medical: any = '';
  careData: any = [];

  careList: any = [
    { title: "Cooking" },
    { title: "Pickup and Drop off from school" },
    { title: "Housekeeping" },
    { title: "Home Work Help" },
    { title: "Dog Walk" }
  ]

  otherbox = false;
  othercare: any = '';

  data: any = [];
  userData: any = [];

  preData: any = '';
  isDisabled: boolean = false;
  isLoading = false;

  profileData: any = [];
  hideextra = true;
  getcareData: any = [];

  userId: any = '';
  userdata: any = [];

  step2Data: any = [];

  constructor(
    private navCtrl: NavController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private storage: Storage,
    private authService: AuthServiceProvider
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  getDetails() {
    this.storage.get('userRegsData').then((result) => {
      this.hideLoader();
      this.preData = result;
      console.log('PreStep Data:', this.preData);
    })
  }

  ionViewWillEnter() {
    this.showLoader('Loading...');
    this.storage.get('userDetails').then((val) => {
      if (val != null) {
        this.userId = val.user_id;
        this.hideextra = false;
        this.userData = val;
        this.getProfileDetails();
      } else {
        this.storage.get('setp2').then((val) => {
          this.step2Data = val;
          this.isgovsubsidy = this.step2Data.govt_subsidy;
          this.about = this.step2Data.about_note;
          this.medical = this.step2Data.medical_condition;
          this.ispetinhouse = this.step2Data.pets_in_house;
          this.childData = this.step2Data.children_info;
          this.getcareData = this.step2Data.additional_care;
          this.othercare = this.step2Data.other_care;
        });
      }
    });
    this.getDetails();
    this.loginToken();
  }

  getProfileDetails() {
    var body = {
      user_id: this.userData.user_id,
    };
    this.authService.postData("get-profile", body).then(result => {
      this.data = result;
      console.log("profile: ", this.data);
      this.profileData = this.data.result.data;
      this.isgovsubsidy = this.profileData.govt_subsidy;
      this.about = this.profileData.about_note;
      this.medical = this.profileData.medical_condition;
      this.ispetinhouse = this.profileData.pets_in_house;
      this.childData = this.profileData.children_info;
      this.getcareData = this.profileData.additional_care;
      this.othercare = this.profileData.other_care;
      console.log("test data:", this.getcareData);
      if (this.profileData.govt_subsidy == "Yes") {
        this.checkgovsub = true;
      } else {
        this.checkgovsub = false;
      }
      if (this.profileData.pets_in_house == "Yes") {
        this.checkpet = true;
      } else {
        this.checkpet = false;
      }
      this.hideLoader();
    },
      error => {
        this.hideLoader();
      });
  }


  addMore() {
    this.pop = true;
  }
  closepop() {
    this.pop = false;
  }
  onSave() {
    this.showLoader('Please wait...');
    let data = {
      age: this.childageyear,
      month: this.childagemonth
    }
    this.childData.push(data);
    this.presentToast('Successfully Child Saved');
    setTimeout(() => {
      this.pop = false;
      this.hideLoader();
      this.childageyear = 0;
      this.childagemonth = 0;
    }, 3000)
  }
  qntyearDown() {
    if (this.childageyear <= 0) {
      this.presentToast("Sorry! You can't.");
    } else {
      this.childageyear--
    }
  }
  qntyearUp() {
    this.childageyear++
  }
  qntmonthDown() {
    if (this.childagemonth <= 0) {
      this.presentToast("Sorry! You can't.");
    } else {
      this.childagemonth--
    }
  }
  qntmonthUp() {
    this.childagemonth++
  }

  petChange(evt) {
    this.checkpet == !this.checkpet;
    if (evt.detail.checked == true) {
      this.ispetinhouse = "Yes";
    } else {
      this.ispetinhouse = "No";
    }
  }

  subsidyChange(evtgov) {
    this.checkgovsub == !this.checkgovsub;
    if (evtgov.detail.checked == true) {
      this.isgovsubsidy = "Yes";
    } else {
      this.isgovsubsidy = "No";
    }
  }

  selectCare() {
    /* if (event.target.checked) {
      this.careData.push(checkbox);
    } else {
      let index = this.removeCheckedFromArray(checkbox);
      this.careData.splice(index, 1);
    }
    if (checkbox == "Others") {
      this.otherbox = !this.otherbox;
    } */
    console.log(this.getcareData);
  }
  removeCheckedFromArray(checkbox: String) {
    return this.careData.findIndex((category) => {
      return category === checkbox;
    })
  }

  onBack() {
    this.navCtrl.pop();
  }
  onSubmit() {
    if (this.about.trim() == '') {
      this.presentToast('Please enter somthing about you');
    } else if (this.childData.length == 0) {
      this.presentToast('Please Need to add at least One Child Minimum.');
    } else if (this.medical.trim() == '') {
      this.presentToast('Please enter Medical Condition');
    } else {
      this.showLoader('Please wait...');
      var body = {
        f_name: this.preData.fname,
        l_name: this.preData.lname,
        dob: this.preData.dob,
        phone: this.preData.phone,
        emg_phone: this.preData.emgnphone,
        email: this.preData.email,
        password: this.preData.password,
        address: this.preData.address,
        state: this.preData.state,
        city: this.preData.city,
        zip: this.preData.zip,
        govt_subsidy: this.isgovsubsidy,
        about_note: this.about,
        children_no: this.childData.length,
        medical_condition: this.medical,
        pets_in_house: this.ispetinhouse,
        children_info: this.childData,
        additional_care: this.getcareData,
        other_care: this.othercare
      }
      this.authService.postData("parent-registration", body).then(result => {
        this.hideLoader();
        this.data = result;
        this.userData = this.data.result.data;
        console.log("Registration: ", this.data);
        if (this.data.status.error_code == 0) {
          this.storage.remove('userRegsData');
          this.storage.remove('setp1');
          this.storage.remove('setp2');
          this.presentToast("Thanks! You has been succesfully Registered.");
          this.storage.set("userDetails", this.userData);
          this.navCtrl.navigateRoot('/congratulation');
        } else {
          this.presentToast(this.data.status.message);
          var object = {
            govt_subsidy: this.isgovsubsidy,
            about_note: this.about,
            children_no: this.childData.length,
            medical_condition: this.medical,
            pets_in_house: this.ispetinhouse,
            children_info: this.childData,
            additional_care: this.getcareData,
            other_care: this.othercare
          }
          this.storage.set("setp2", object);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  oneditSave() {
    if (this.about.trim() == '') {
      this.presentToast('Please enter somthing about you');
    } else if (this.childData.length == 0) {
      this.presentToast('Please Need to add at least One Child Minimum.');
    } else if (this.medical.trim() == '') {
      this.presentToast('Please enter Medical Condition');
    } else {
      this.showLoader('Please wait...');
      var body = {
        user_id: this.userId,
        f_name: this.preData.fname,
        l_name: this.preData.lname,
        dob: this.preData.dob,
        phone: this.preData.phone,
        emg_phone: this.preData.emgnphone,
        email: this.preData.email,
        address: this.preData.address,
        state: this.preData.state,
        city: this.preData.city,
        zip: this.preData.zip,
        govt_subsidy: this.isgovsubsidy,
        about_note: this.about,
        children_no: this.childData.length,
        medical_condition: this.medical,
        pets_in_house: this.ispetinhouse,
        children_info: this.childData,
        additional_care: this.getcareData,
        other_care: this.othercare
      }
      this.authService.postData("update-parent", body).then(result => {
        this.hideLoader();
        this.data = result;
        this.userData = this.data.result.data;
        console.log("Edit Registration: ", this.data);
        if (this.data.status.error_code == 0) {
          this.storage.remove('userRegsData');
          this.presentToast("Thanks! You has been succesfully Updated.");
          this.navCtrl.navigateRoot('/myprofile');
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.hideLoader();
        });
    }
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
