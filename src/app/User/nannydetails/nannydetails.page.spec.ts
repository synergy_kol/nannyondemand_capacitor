import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NannydetailsPage } from './nannydetails.page';

describe('NannydetailsPage', () => {
  let component: NannydetailsPage;
  let fixture: ComponentFixture<NannydetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NannydetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NannydetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
