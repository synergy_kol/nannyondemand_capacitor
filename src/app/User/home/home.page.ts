import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { MyEvents } from 'src/app/MyEvents';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  serviceList = {
    initialSlide: 0,
    slidesPerView: 3,
    spaceBetween: 5,
  }
  nannylistopt = {
    initialSlide: 0,
    slidesPerView: 1.6,
    spaceBetween: 5,
  };

  isLoading = false;
  data: any;
  isDisabled: boolean = false;
  serviceData: any = [];
  nannyData: any = [];
  popularnannyData: any = [];

  userdata: any = [];
  profileData: any = [];

  constructor(
    public storage: Storage,
    public navCtrl: NavController,
    private menuCtrl: MenuController,
    public authService: AuthServiceProvider,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public events: MyEvents
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ionViewWillEnter() {
    this.menuCtrl.swipeGesture(true);
    this.showLoader('Please wait...');
    this.loginToken();
    var serBody = {
      source: 'mob'
    };
    this.authService.postData("service-type-list", serBody).then(result => {
      this.data = result;
      this.serviceData = this.data.result.data;
      console.log("serviceData: ", this.serviceData);
      this.hideLoader();
    }, error => {
      this.hideLoader();
    });

    this.nannyList();
    this.popularnannyList();
    this.profileDetails();
    this.loadPage();
  }

  onView() {
    this.navCtrl.navigateForward("/schedule");
  }

  profileDetails() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
      };
      this.authService.postData("get-profile", body).then(result => {
        this.data = result;
        console.log("profile: ", this.data);
        this.profileData = this.data.result.data;
        this.events.publishSomeData({ profile: this.profileData });
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  loadPage() {
    setTimeout(() => {
      this.profileDetails();
      this.loadPage();
    }, 1000)
  }

  nannyList() {
    this.storage.get('userDetails').then((val) => {
      var body = {
        user_id: val.user_id
      };
      this.authService.postData("get-nany-list", body).then(result => {
        this.data = result;
        this.nannyData = this.data.result.data;
        console.log("Nanny List: ", this.nannyData);
      },
        error => {
          this.hideLoader();
        });
    });
  }

  popularnannyList() {
    this.storage.get('userDetails').then((val) => {
      var body = {
        user_id: val.user_id,
        sort_order: 'DESC'
      };
      this.authService.postData("get-nany-list", body).then(result => {
        this.data = result;
        this.popularnannyData = this.data.result.data;
        console.log("Popular Nanny List: ", this.nannyData);
      },
        error => {
          this.hideLoader();
        });
    });
  }

  onFilter() {
    this.navCtrl.navigateForward('filter');
  }

  goNannyList(type, id) {
    var object = {
      ser_type: type,
      ser_id: id
    }
    this.navCtrl.navigateForward(["/nannylist"], { queryParams: object });
  }

  splitAge(age) {
    var ageArr = age.split(' ');
    return ageArr[0];
  }

  goNannyDetails(nanny_id) {
    var object = {
      nanny_id: nanny_id
    }
    this.navCtrl.navigateForward(["/nannydetails"], { queryParams: object });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
