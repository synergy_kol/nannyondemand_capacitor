import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {

  selectedChip: any = 0;
  userdata: any = [];
  data: any = [];
  isLoading = false;

  rate: any = [];
  rateList: any = [
    { title: "0-100", check: false },
    { title: "100-200", check: false },
    { title: "200-300", check: false },
    { title: "300-400", check: false },
    { title: "400-500", check: false },
    { title: "500-1000", check: false },
  ];
  rateType: any = [];
  rateTypeList: any = [
    { title: "Weekly", check: false },
    { title: "Hourly", check: false },
  ];
  ratings: any = [];
  ratingsList: any = [
    { title: "1", check: false },
    { title: "2", check: false },
    { title: "3", check: false },
    { title: "4", check: false },
    { title: "5", check: false },
  ];
  interestIn: any = [];
  interestInList: any = [
    { title: "Live-In", check: false },
    { title: "Live-Out", check: false },
  ];

  constructor(
    private navCtrl: NavController,
    private storage: Storage,
    private authService: AuthServiceProvider,
    public loadingController: LoadingController,
  ) { }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);
        } else {
          this.showLoader('Logging out...');
          setTimeout(() => {
            this.storage.remove('userDetails');
            this.hideLoader();
            this.navCtrl.navigateRoot('/welcome');
          }, 3000);
        }
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.loginToken();
  }


  selectRate(event, checkbox: String, indx) {
    if (event.target.checked) {
      //var obj = {value: checkbox};
      this.rate.push(checkbox);
      this.UpdateRate(indx);
    } else {
      let index = this.removeRateFromArray(checkbox, indx);
      this.rate.splice(index, 1);
    }    
  }
  UpdateRate(indx) {
    this.rateList[indx].check = true;
  }
  removeRateFromArray(checkbox: String, indx) {
    this.rateList[indx].check = false;
    console.log(this.rateList);
    return this.rate.findIndex((category) => {
      return category === checkbox;
    })    
  }

  selectRateType(event, checkbox: String, indx) {
    if (event.target.checked) {
      this.rateType.push(checkbox);
      this.UpdateRateType(indx);
    } else {
      let index = this.removeRateTypeFromArray(checkbox, indx);
      this.rateType.splice(index, 1);
    }
    console.log("Rate Type:", this.rateType);
  }
  UpdateRateType(indx) {
    this.rateTypeList[indx].check = true;
  }
  removeRateTypeFromArray(checkbox: String, indx) {
    this.rateTypeList[indx].check = false;
    return this.rateType.findIndex((category) => {
      return category === checkbox;
    })
  }

  selectRating(event, checkbox: String, indx) {
    if (event.target.checked) {
      this.ratings.push(checkbox);
      this.UpdateRating(indx);
    } else {
      let index = this.removeRatingsFromArray(checkbox, indx);
      this.ratings.splice(index, 1);
    }
    console.log("Rate Type:", this.ratings);
  }
  UpdateRating(indx) {
    this.ratingsList[indx].check = true;
  }
  removeRatingsFromArray(checkbox: String, indx) {
    this.ratingsList[indx].check = false;
    return this.ratings.findIndex((category) => {
      return category === checkbox;
    })
  }

  selectInterest(event, checkbox: String, indx) {
    if (event.target.checked) {
      this.interestIn.push(checkbox);
      this.UpdateInterest(indx);
    } else {
      let index = this.removeInterestFromArray(checkbox, indx);
      this.interestIn.splice(index, 1);
    }
    console.log("Rate Type:", this.interestIn);
  }
  UpdateInterest(indx) {
    this.interestInList[indx].check = true;
  }
  removeInterestFromArray(checkbox: String, indx) {
    this.interestInList[indx].check = false;
    return this.interestIn.findIndex((category) => {
      return category === checkbox;
    })
  }

  onFilterPayment(id) {
    console.log(id);
    this.selectedChip = id;
  }

  onCancel() {
    this.navCtrl.pop();
  }

  onApply() {
    var object = {
      rate: this.rate,
      ratetype: this.rateType,
      ratings: this.ratings,
      interest: this.interestIn
    }
    this.navCtrl.navigateBack(["/nannylist"], { queryParams: object });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
