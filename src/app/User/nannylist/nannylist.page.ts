import { Component, OnInit } from '@angular/core';
import { ActionSheetController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-nannylist',
  templateUrl: './nannylist.page.html',
  styleUrls: ['./nannylist.page.scss'],
})
export class NannylistPage implements OnInit {

  isLoading = false;
  data: any;
  isDisabled: boolean = false;
  nannyData: any = [];
  serviceType: any = '';
  serviceId: any = '';
  userdata: any;

  rate: any = [];
  rateType: any = [];
  ratings: any = [];
  interest: any = [];
  others: any = [];

  constructor(
    public storage: Storage,
    public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public toastController: ToastController,
    public loadingController: LoadingController,
    private actionSheet: ActionSheetController,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.serviceType = res.ser_type;
      this.serviceId = res.ser_id;
      this.rate = res.rate;
      this.rateType = res.ratetype;
      this.ratings = res.ratings;
      this.interest = res.interest;
      console.log("new rate:", this.rate);
    })
   }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getAllData('');
    this.loginToken();
  }

  getAllData(sort) {
    this.storage.get('userDetails').then((val) => {
      var body = {
        user_id: val.user_id,
        sort_by_rate: sort,
        service_type_id: this.serviceId,
        by_rate: this.rate,
        by_ratetype: this.rateType,
        by_ratings: this.ratings,
        by_interest: this.interest
      };
      this.authService.postData("get-nany-list", body).then(result => {
        this.data = result;
        this.nannyData = this.data.result.data;
        console.log("nannyData: ", this.nannyData);
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  splitAge(age) {
    var ageArr = age.split(' ');
    return ageArr[0];
  }

  goNannyDetails(nanny_id) {
    var object = {
      nanny_id: nanny_id
    }
    this.navCtrl.navigateForward(["/nannydetails"], { queryParams: object });
  }

  onSort() {
    let actionSheet = this.actionSheet.create({
      header: 'Sort',
      buttons: [{
        text: 'Rate High to Low',
        handler: () => {
          this.showLoader('Please wait...');
          this.getAllData('DESC');
        }
      }, {
        text: 'Rate Low to High',
        handler: () => {
          this.showLoader('Please wait...');
          this.getAllData('ASC');
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    }).then(a => {
      a.present();
    });
  }

  onFilter() {
    this.navCtrl.navigateForward('filter');
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
