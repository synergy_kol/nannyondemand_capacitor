import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NannylistPage } from './nannylist.page';

describe('NannylistPage', () => {
  let component: NannylistPage;
  let fixture: ComponentFixture<NannylistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NannylistPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NannylistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
