import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NannylistPage } from './nannylist.page';

const routes: Routes = [
  {
    path: '',
    component: NannylistPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NannylistPageRoutingModule {}
