import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthServiceProvider } from 'src/auth-providers/auth-service/auth-service';

@Component({
  selector: 'app-bookingdetails',
  templateUrl: './bookingdetails.page.html',
  styleUrls: ['./bookingdetails.page.scss'],
})
export class BookingdetailsPage implements OnInit {

  isLoading = false;
  isDisabled: boolean = false;
  userdata: any = [];
  data: any = [];
  bookingDetails: any = [];

  bookingID: any = '';

  constructor(
    private navCtrl: NavController,
    private authService: AuthServiceProvider,
    private loadingController: LoadingController,
    private storage: Storage,
    private activatedRoute: ActivatedRoute,
    private toastController: ToastController
  ) {
    this.activatedRoute.queryParams.subscribe((res) => {
      this.bookingID = res.bookID;
    });
  }

  ngOnInit() {
  }

  loginToken() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var tokenbody = {
        user_id: this.userdata.user_id,
        auth_token: this.userdata.auth_token
      };
      this.authService.postData("check-token", tokenbody).then(result => {
        this.data = result;        
        if (this.data.status.error_code == 0) {
          console.log("Token: ", this.data);          
        } else {
          this.showLoader('Logging out...');
            setTimeout(() => {
              this.storage.remove('userDetails');
              this.hideLoader();
              this.navCtrl.navigateRoot('/welcome');
            }, 3000);
        }
        this.hideLoader();
      },
      error => {
          this.hideLoader();
      });
    });    
  }

  ionViewWillEnter() {
    this.showLoader('Please wait...');
    this.getDetails();
    this.loginToken();
  }

  getDetails() {
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        booking_id: this.bookingID
      };
      this.authService.postData("get-booking-details", body).then(result => {
        this.data = result;
        console.log("Booking Details: ", this.data);
        this.bookingDetails = this.data.result.data;
        this.hideLoader();
      },
        error => {
          this.hideLoader();
        });
    });
  }

  goNannyDetails(nanny_id) {
    var object = {
      nanny_id: nanny_id
    }
    this.navCtrl.navigateForward(["/nannydetails"], { queryParams: object });
  }

  onBookingComplete(detailsID) {
    this.showLoader('Please wait...');
    this.storage.get('userDetails').then((val) => {
      this.userdata = val;
      var body = {
        user_id: this.userdata.user_id,
        booking_detail_id: detailsID
      };
      this.authService.postData("update-service-completed", body).then(result => {
        this.hideLoader();
        this.data = result;
        console.log("Complete Service: ", this.data);
        if (this.data.status.error_code == 0) {
          this.presentToast("Service Completed Successfully.");
          this.getDetails();
        } else {
          this.presentToast(this.data.status.message);
        }
      },
        error => {
          this.hideLoader();
        });
    });
  }

  async showLoader(text) {
    this.isLoading = true;
    return await this.loadingController.create({
      message: text
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async hideLoader() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  async presentToast(msg) {
    this.isDisabled = true;
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.onDidDismiss().then(() => {
      this.isDisabled = false;
    });
    toast.present();
  }

}
